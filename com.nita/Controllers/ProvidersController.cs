﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.nita.IdentityModels;
using com.nita.Settings;
using com.nita.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Nita.ViewModels.ProviderViewModels;

namespace Nita.Controllers
{
    [Authorize]
    public class ProvidersController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IOptions<AppSettings> appSettings;
        private readonly IServiceProvider provider;
        private readonly IHostingEnvironment hostingEnvironment;

        public UserManager<ApplicationUser> UserManager => _userManager;

        public ProvidersController(
           IServiceProvider prov,
           IHostingEnvironment env,
           IOptions<AppSettings> appSettings, UserManager<ApplicationUser> userManager)
        {
            this.provider = prov;
            this.appSettings = appSettings;
            this.hostingEnvironment = env;
            this._userManager = userManager;
        }

        // GET: Providers
        public ActionResult Index(string countyId = "")
        {
            int passed;
            if (countyId!="" && countyId!="all") {
                 passed = Convert.ToInt32(countyId);
            }
            else {
                passed = 0;
            }
            var db = provider.GetRequiredService<ApplicationDbContext>();
            

            var counties = db.Counties.ToList();
            ViewBag.Counties = counties;
            if (passed == 0)
            {
                var trainingProviders = db.Trainers.Where(t=>t.Type == TrainerType.Institution).ToList();
                ViewBag.Trainers = trainingProviders;
                return View();
            }
            else if (passed != 0)
            {
                List<TrainerProfile> listProviders = new List<TrainerProfile>();
                
                var trainingProviders = db
                .Addresses
                .OrderBy(e => e.Id)
                .Where(a => a.CountyId == passed)
                .Join(db.Trainers.Where(t => t.Type == TrainerType.Institution),
                    addresses => addresses.Id,
                    trainers => trainers.AddressId,
                    (trainer, addresses) =>
                        new { addresses })
                .ToList();
                foreach (var trainer in trainingProviders)
                {
                    listProviders.Add(trainer.addresses);
                }
                ViewBag.Trainers = listProviders;
                return View();
            }
            else if (passed != 0)
            {
                List<TrainerProfile> listProviders = new List<TrainerProfile>();
                var trainingProviders = db
                .Addresses
                .OrderBy(e => e.Id)
                .Where(a => a.CountyId == passed)
                .Join(db.Trainers,
                    addresses => addresses.Id,
                    trainers => trainers.AddressId,
                    (trainer, addresses) =>
                        new { addresses })
                .ToList();
                foreach (var trainer in trainingProviders) {
                    listProviders.Add(trainer.addresses);
                }
                ViewBag.Trainers = listProviders;
                return View();
            }
            else {
                var trainingProviders = db.Trainers.Where(trainer => trainer.Type == TrainerType.Institution).ToList();
                ViewBag.Trainers = trainingProviders;
                return View();
            }
        }

        public ActionResult IndexMc(string countyId = "") {
            int passed;
            if (countyId != "" && countyId != "all")
            {
                passed = Convert.ToInt32(countyId);
            }
            else
            {
                passed = 0;
            }
            var db = provider.GetRequiredService<ApplicationDbContext>();

            var counties = db.Counties.ToList();
            ViewBag.Counties = counties;
            if (passed == 0)
            {
                var trainingProviders = db.Trainers.Where(t=>t.Type == TrainerType.MasterCraftman).ToList();
                ViewBag.Trainers = trainingProviders;
                return View();
            }
            else if (passed != 0)
            {
                List<TrainerProfile> listProviders = new List<TrainerProfile>();
               
                var trainingProviders = db
                .Addresses
                .OrderBy(e => e.Id)
                .Where(a => a.CountyId == passed)
                .Join(db.Trainers.Where(t => t.Type == TrainerType.MasterCraftman),
                    addresses => addresses.Id,
                    trainers => trainers.AddressId,
                    (trainer, addresses) =>
                        new { addresses })
                .ToList();
                foreach (var trainer in trainingProviders)
                {
                    listProviders.Add(trainer.addresses);
                }
                ViewBag.Trainers = listProviders;
                return View();
            }
            else if (passed != 0)
            {
                List<TrainerProfile> listProviders = new List<TrainerProfile>();
                var trainingProviders = db
                .Addresses
                .OrderBy(e => e.Id)
                .Where(a => a.CountyId == passed)
                .Join(db.Trainers,
                    addresses => addresses.Id,
                    trainers => trainers.AddressId,
                    (trainer, addresses) =>
                        new { addresses })
                .ToList();
                foreach (var trainer in trainingProviders)
                {
                    listProviders.Add(trainer.addresses);
                }
                ViewBag.Trainers = listProviders;
                return View();
            }
            else
            {
                var trainingProviders = db.Trainers.Where(trainer => trainer.Type == TrainerType.MasterCraftman).ToList();
                ViewBag.Trainers = trainingProviders;
                return View();
            }
        }

        // GET: Providers/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Providers/Create
        public ActionResult Create()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            var counties = db.Counties.ToList();
            var banks = db.PBanks.ToList();
            ViewBag.Banks = banks;
            ViewBag.Counties = counties;
            return View();
        }

        // GET: Providers/Create
        public ActionResult CreateMc()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            var counties = db.Counties.ToList();
            var banks = db.PBanks.ToList();
            ViewBag.Banks = banks;
            ViewBag.Counties = counties;
            return View();
        }

        public JsonResult GetBranches(int id)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            var branches = db.BankBranch.ToList();
            return Json(new { list = branches });
        }

        // POST: Providers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(ProviderModels model)
        {
            int TpId = 0;
            try
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                var fullName = model.FullName.ToString();
                var contactPersonName = model.ContactPersonName.ToString();
                var contactPersonPosition = model.ContactPersonPosition.ToString();
                var roadStreet = model.StreetRoad.ToString();
                var buildingName = model.BuildingName.ToString();
                var contactEmail = model.ContactEmail.ToString();
                var landMark = model.LandMark.ToString();
                var countyId = model.CountyId.ToString();
                var trainerType = model.TrainerType.ToString();
                var telephoneNumber = model.Telephone.ToString();
                var postalAddress = model.PostalAddress.ToString();
                AccountType type = AccountType.Institution;
                TrainerType tType = TrainerType.Institution;
                if (trainerType == "Institution") {
                    type = AccountType.Institution;
                    tType = TrainerType.Institution;
                }
                else {
                    type = AccountType.MasterCraftsMan;
                    tType = TrainerType.MasterCraftman;
                }
                
                var user = new ApplicationUser()
                {
                    UserName = contactEmail,
                    Email = contactEmail,
                    PhoneNumber = telephoneNumber,
                    AccountType = type
                };

                //Create USer Account
                var result = await UserManager.CreateAsync(user, "123456789nita");
                if (result.Succeeded) {
                    //address
                    var address = new UserAddress();
                    address.BuildingName = buildingName;
                    address.CountyId = Convert.ToInt32(countyId);
                    address.NearbyLandmark = landMark;
                    address.Street = roadStreet;
                    address.Nationality = "Kenyan";
                    address.PAddress = postalAddress;
                    address.WTel = telephoneNumber;
                    //Add
                    db.Addresses.Add(address);
                    db.SaveChanges();

                    var provider = new TrainerProfile();
                    provider.ApplicationUserId = user.Id;
                    provider.FullName = fullName;
                    provider.AddressId = address.Id;
                    provider.Position = contactPersonPosition;
                    provider.Status = TrainerStatus.Active;
                    provider.Type = tType;
                    provider.PostalAddress = postalAddress;
                    provider.ContactPersonName = contactPersonName;
                    provider.ContactEmail = contactEmail;
                    provider.DateCreated = DateTime.Now;
                    provider.Telephone = telephoneNumber;

                    db.Trainers.Add(provider);
                    db.SaveChanges();

                    TpId= provider.Id;
                }
                // TODO: Add insert logic here

                return RedirectToAction("CreateEmployer", new {id = TpId });
            }
            catch
            {
                return View();
            }
        }

        public ActionResult CreateEmployer(int id) {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            ViewBag.Id = id;
            var employers = db.TrainerEmployer.Where(e=>e.TrainerId == id).ToList();
            var counties = db.Counties.ToList();
            ViewBag.Counties = counties;
            ViewBag.Employers = employers;
            return View();
        }

        public ActionResult SaveEmployer(Employer employer) {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            var TEmployer = new TrainerEmployer
            {
                Name = employer.BusinessName,
                Industry = employer.Industry,
                CountyId = employer.CountyId,
                Notes = employer.Notes,
                TrainerId = employer.Id
            };
            db.TrainerEmployer.Add(TEmployer);
            db.SaveChanges();
            return RedirectToAction("CreateEmployer", new { id = employer.Id });
        }

        public async Task<ActionResult> SaveFiAsync(McModel mcModel) {
            int Id = 0;
            try
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                if (mcModel != null)
                {
                    var bankAcc = new BankAccount
                    {
                        BranchId = mcModel.BranchId,
                        Name = mcModel.AccountName,
                        Number = mcModel.AccountNo,
                        DateAdded = DateTime.Now
                    };
                    db.PBankAccounts.Add(bankAcc);
                    db.SaveChanges();

                    var user = new ApplicationUser()
                    {
                        UserName = mcModel.Email,
                        Email = mcModel.Email,
                        PhoneNumber = mcModel.Telephone,
                        AccountType = AccountType.Institution
                    };

                    //Create USer Account
                    var result = await UserManager.CreateAsync(user, "123456789nita");
                    if (result.Succeeded)
                    {
                        //address
                        var address = new UserAddress();
                        address.BuildingName = "";
                        address.CountyId = mcModel.CountyId;
                        address.NearbyLandmark = mcModel.PhysicalLocation;
                        address.Street = "";
                        address.Nationality = "Kenyan";
                        address.PAddress = mcModel.PostalAddressBiz;
                        address.WTel = mcModel.Telephone;
                        //Add
                        db.Addresses.Add(address);
                        db.SaveChanges();

                        var provider = new TrainerProfile();
                        provider.ApplicationUserId = user.Id;
                        provider.FullName = mcModel.EnterpriseName;
                        provider.AddressId = address.Id;
                        provider.Position = mcModel.Position;
                        provider.Status = TrainerStatus.Active;
                        provider.Type = TrainerType.Institution;
                        provider.PostalAddress = mcModel.PostalAddressMc;
                        provider.ContactPersonName = mcModel.McFullName.FirstName+" "+mcModel.McFullName.MiddleName+" "+mcModel.McFullName.SurName;
                        provider.ContactEmail = mcModel.Email;
                        provider.DateCreated = DateTime.Now;
                        provider.Telephone = mcModel.Telephone;
                        provider.BankAccId = bankAcc.Id;

                        db.Trainers.Add(provider);
                        db.SaveChanges();
                        Id = provider.Id;
                    }

                    return RedirectToAction("CreateEmployer", new {id = Id });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> SaveMcAsync(McModel mcModel) {
            try
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                if (mcModel != null)
                {
                    var bankAcc = new BankAccount
                    {
                        BranchId = mcModel.BranchId,
                        Name = mcModel.AccountName,
                        Number = mcModel.AccountNo,
                        DateAdded = DateTime.Now
                    };
                    db.PBankAccounts.Add(bankAcc);
                    db.SaveChanges();

                    var user = new ApplicationUser()
                    {
                        UserName = mcModel.Email,
                        Email = mcModel.Email,
                        PhoneNumber = mcModel.Telephone,
                        AccountType = AccountType.MasterCraftsMan
                    };

                    //Create USer Account
                    var result = await UserManager.CreateAsync(user, "123456789nita");
                    if (result.Succeeded)
                    {
                        //address
                        var address = new UserAddress();
                        address.BuildingName = "";
                        address.CountyId = mcModel.CountyId;
                        address.NearbyLandmark = mcModel.PhysicalLocation;
                        address.Street = "";
                        address.Nationality = "Kenyan";
                        address.PAddress = mcModel.PostalAddressBiz;
                        address.WTel = mcModel.Telephone;
                        //Add
                        db.Addresses.Add(address);
                        db.SaveChanges();

                        var provider = new TrainerProfile();
                        provider.ApplicationUserId = user.Id;
                        provider.FullName = mcModel.EnterpriseName;
                        provider.AddressId = address.Id;
                        provider.Position = mcModel.Position;
                        provider.Status = TrainerStatus.Active;
                        provider.Type = TrainerType.MasterCraftman;
                        provider.PostalAddress = mcModel.PostalAddressMc;
                        provider.ContactPersonName = mcModel.McFullName.FirstName + " " + mcModel.McFullName.MiddleName + " "+mcModel.McFullName.SurName;
                        provider.ContactEmail = mcModel.Email;
                        provider.DateCreated = DateTime.Now;
                        provider.Telephone = mcModel.Telephone;
                        provider.BankAccId = bankAcc.Id;
                        provider.IdNo = mcModel.IdNo;

                        db.Trainers.Add(provider);
                        db.SaveChanges();
                    }

                    return RedirectToAction(nameof(IndexMc));
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction(nameof(IndexMc));
            }
            return RedirectToAction(nameof(IndexMc));
        }

        // GET: Providers/Edit/5
        public ActionResult Edit(int id)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            var tProvider = db.Trainers.Find(id);
            McModel providerModel = null;
            if (tProvider != null) {
                string[] words = tProvider.ContactPersonName.Split(' ');
                FullName fullName =new FullName();
                if (words.Length == 3)
                {
                    fullName.FirstName = words[0];
                    fullName.MiddleName = words[1];
                    fullName.SurName = words[2];
                }

                if (words.Length == 2)
                {
                    fullName.FirstName = words[0];
                    fullName.MiddleName = words[1];
                    fullName.SurName = "";
                }

                if (words.Length == 1)
                {
                    fullName.FirstName = words[0];
                    fullName.MiddleName = "";
                    fullName.SurName = "";
                }

                if (words.Length ==0)
                {
                    fullName.FirstName = "";
                    fullName.MiddleName = "";
                    fullName.SurName = "";
                }
                
                var Account = db.PBankAccounts.Find(tProvider.BankAccId);
                var userAddress = db.Addresses.Find(tProvider.Id);
                providerModel = new McModel()
                {
                    IdNo = tProvider.IdNo,
                    McFullName = fullName,
                    Telephone = tProvider.Telephone,
                    Email = tProvider.ContactEmail,
                    Position = tProvider.Position,
                    EnterpriseName = tProvider.EnterpriseName,
                    PostalAddressBiz = tProvider.PostalAddress,
                    PostalAddressMc = tProvider.PostalAddress,
                    PhysicalLocation = userAddress.NearbyLandmark,
                    CountyId = userAddress.CountyId,
                    AccountNo = Account.Number,
                    AccountName = Account.Name,
                    BranchId =Account.BranchId,
                    BankId = db.PBanks.Find(db.BankBranch.Find(Account.BranchId).BankId).Id
                };
                //ViewBag.ProviderData = providerModel;
            }
            var counties = db.Counties.ToList();
            var banks = db.PBanks.ToList();
            ViewBag.Banks = banks;
            ViewBag.Counties = counties;

            return View(providerModel);
        }

        public ActionResult EditMc(int id)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            var tProvider = db.Trainers.Find(id);
            McModel providerModel = null;
            if (tProvider != null)
            {
                string[] words = tProvider.ContactPersonName.Split(' ');
                FullName fullName = new FullName();
                if (words.Length == 3)
                {
                    fullName.FirstName = words[0];
                    fullName.MiddleName = words[1];
                    fullName.SurName = words[2];
                }

                if (words.Length == 2)
                {
                    fullName.FirstName = words[0];
                    fullName.MiddleName = words[1];
                    fullName.SurName = "";
                }

                if (words.Length == 1)
                {
                    fullName.FirstName = words[0];
                    fullName.MiddleName = "";
                    fullName.SurName = "";
                }

                if (words.Length == 0)
                {
                    fullName.FirstName = "";
                    fullName.MiddleName = "";
                    fullName.SurName = "";
                }

                var Account = db.PBankAccounts.Find(tProvider.BankAccId);
                var userAddress = db.Addresses.Find(tProvider.Id);
                providerModel = new McModel()
                {
                    IdNo = tProvider.IdNo,
                    McFullName = fullName,
                    Telephone = tProvider.Telephone,
                    Email = tProvider.ContactEmail,
                    Position = tProvider.Position,
                    EnterpriseName = tProvider.EnterpriseName,
                    PostalAddressBiz = tProvider.PostalAddress,
                    PostalAddressMc = tProvider.PostalAddress,
                    PhysicalLocation = userAddress.NearbyLandmark,
                    CountyId = userAddress.CountyId,
                    AccountNo = Account.Number,
                    AccountName = Account.Name,
                    BranchId = Account.BranchId,
                    BankId = db.PBanks.Find(db.BankBranch.Find(Account.BranchId).BankId).Id
                };
                //ViewBag.ProviderData = providerModel;
            }
            var counties = db.Counties.ToList();
            var banks = db.PBanks.ToList();
            ViewBag.Banks = banks;
            ViewBag.Counties = counties;

            return View(providerModel);
        }
        // POST: Providers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProviderModels model)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            try
            {
                var tpId = model.Id;
                var applicationUserId = model.ApplicationUserId;
                var addressId = model.AddressId;
                var fullName = model.FullName.ToString();
                var contactPersonName = model.ContactPersonName.ToString();
                var contactPersonPosition = model.ContactPersonPosition.ToString();
                var roadStreet = model.StreetRoad.ToString();
                var buildingName = model.BuildingName.ToString();
                var contactEmail = model.ContactEmail.ToString();
                var landMark = model.LandMark.ToString();
                var countyId = model.CountyId.ToString();
                var trainerType = model.TrainerType.ToString();
                var telephoneNumber = model.Telephone.ToString();
                var postalAddress = model.PostalAddress.ToString();
                AccountType type = AccountType.Institution;
                TrainerType tType = TrainerType.Institution;
                if (trainerType == "Institution")
                {
                    type = AccountType.Institution;
                    tType = TrainerType.Institution;
                }
                else
                {
                    type = AccountType.MasterCraftsMan;
                    tType = TrainerType.MasterCraftman;
                }
                var user = db.Users.Find(applicationUserId);
                user.Email = contactEmail;
                user.PhoneNumber = telephoneNumber;
                user.AccountType = type;
                db.SaveChanges();

                //address
                var address = db.Addresses.Find(addressId);
                address.BuildingName = buildingName;
                address.CountyId = Convert.ToInt32(countyId);
                address.NearbyLandmark = landMark;
                address.Street = roadStreet;
                address.Nationality = "Kenyan";
                address.PAddress = postalAddress;
                address.WTel = telephoneNumber;
                //Add
                db.SaveChanges();

                var provider = db.Trainers.Find(tpId);
                provider.ApplicationUserId = user.Id;
                provider.FullName = fullName;
                provider.AddressId = address.Id;
                provider.Position = contactPersonPosition;
                provider.Status = TrainerStatus.Active;
                provider.Type = tType;
                provider.PostalAddress = postalAddress;
                provider.ContactPersonName = contactPersonName;
                provider.ContactEmail = contactEmail;
                provider.DateCreated = DateTime.Now;
                provider.Telephone = telephoneNumber;
                
                db.SaveChanges();



                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // GET: Providers/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Providers/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult LockTrainer(int id) {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (id != 0) {
                var tProvider = db.Trainers.Find(id);
                tProvider.Status = TrainerStatus.Inactive;
                db.SaveChanges();
            }
            return RedirectToAction(nameof(Index));
        }

        public ActionResult UnLockTrainer(int id)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (id != 0)
            {
                var tProvider = db.Trainers.Find(id);
                tProvider.Status = TrainerStatus.Active;
                db.SaveChanges();
            }
            return RedirectToAction(nameof(Index));
        }

        public ActionResult TerminateTrainer(int id)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (id != 0)
            {
                var tProvider = db.Trainers.Find(id);
                tProvider.Status = TrainerStatus.Terminated;
                db.SaveChanges();
            }
            return RedirectToAction(nameof(Index));
        }
    }
}