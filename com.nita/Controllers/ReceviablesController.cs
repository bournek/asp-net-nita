﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using com.nita.IdentityModels;
using com.nita.ViewModels;
using com.nita.Services;
using com.nita.Constants;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.WindowsAzure.Storage.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nita.ViewModels;
using Nita.IdentityModels;

namespace Nita.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class ReceviablesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ReceviablesController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult AddProduct([FromBody] ReceivableProductViewModel viewProductModel)
        {
            if (_context.Products.Count(p => p.Code == viewProductModel.Code) != 0)
            {
                return Json(new
                {
                    status = 0,
                    message = "Product code already exists"
                });
            }

            var product = new Product
            {
                FProductCategoryId = viewProductModel.FProductCategoryId,
                SellPrice = viewProductModel.SellPrice,
                CostPrice = viewProductModel.CostPrice,
                Code = viewProductModel.Code,
                ProductName = viewProductModel.ProductName,
                FTaxesId = viewProductModel.FTaxesId,
                MarkUp = viewProductModel.MarkUp,
                DiscountStatus=viewProductModel.DiscountStatus
            };

            _context.Products.Add(product);
            _context.SaveChanges();


            return Json(new
            {
                status = 1,
                message = "Product save successfully"
            });
        }

        public JsonResult AddCustomer([FromBody] ReceivableCustomerViewModel viewCustomerModel)
        {
            var customer = new Customer
            {
                CustomerAddress = viewCustomerModel.CustomerAddress,
                CustomerBalance = viewCustomerModel.CustomerBalance,
                CustomerCode = viewCustomerModel.CustomerCode,
                CustomerEmail = viewCustomerModel.CustomerEmail,
                CustomerMobile = viewCustomerModel.CustomerMobile,
                CustomerName = viewCustomerModel.CustomerName,
                ActiveStatus = viewCustomerModel.ActiveStatus,
                FCurrencyId=viewCustomerModel.FCurrencyId,
                FCustomerTypeId = viewCustomerModel.FCustomerTypeId,
                RegNo = viewCustomerModel.RegNo
            };

            _context.Customers.Add(customer);
            _context.SaveChanges();


            return Json(new
            {
                status = 1,
                message = "Customer save successfully"
            });
        }


        public JsonResult GetProducts(string searchString = "")
        {
            var products = _context
                .Products
                .Where(p => p.ProductName.Contains(searchString) && p.Status == 1)
                .Include(k => k.FProductCategory)
                .Include(k => k.FTaxes)
                .ToList();

            return Json(new
            {
                status = products.Count > 0 ? 1 : 0,
                message = "products fetched successfully successfully",
                data = products
            });
        }

        public JsonResult GetCustomers(string searchString = "")
        {
            var customers = _context
                .Customers
                .Where(p => p.CustomerName.Contains(searchString)|| p.CustomerCode==searchString && p.Status == 1)
                .Include(k => k.FCustomerType)
                .ToList();

            return Json(new
            {
                status = customers.Count > 0 ? 1 : 0,
                message = customers.Count + "customers fetched successfully successfully",
                data = customers
            });
        }

        public JsonResult GetFProductCategories(string searchString = "")
        {
            var productCategories = _context
                .FProductCategories
                .Where(p => p.Name.Contains(searchString))
                .ToList();

            return Json(new
            {
                status = productCategories.Count > 0 ? 1 : 0,
                message = "Product Categories fetched successfully ",
                data = productCategories
            });
        }

        public JsonResult GetAllCustomerCategories(string searchString = "")
        {
            var customerTypes = _context
                .FCustomerTypes
                .Where(p => p.Name.Contains(searchString))
                .ToList();

            return Json(new
            {
                status = customerTypes.Count > 0 ? 1 : 0,
                message = "Customer types fetched successfully ",
                data = customerTypes
            });
        }

        public JsonResult GetAllCustomerInvoices(string searchString = "")
        {
            var customerInvoices = _context
                .CustomerInvoices
                .Where(p => p.InvoiceNo.Contains(searchString))
                .Take(50)
                .Include(p => p.CustomerInvoiceProducts)
                .Include(p => p.Customer)
                .ToList();

            var invoiceList = new List<dynamic>();
            foreach (var cI in customerInvoices)
            {
                var invoiceProducts = cI.CustomerInvoiceProducts;
                var sum = 0.00;
                var discount = 0.00;
                foreach (var products in invoiceProducts)
                {
                    sum += products.Quantity * products.ProductSellPrice;
                    discount += products.Discount;
                }

                invoiceList.Add(new
                {
                    cI,
                    TotalAmount = (sum - discount)
                });
                sum = 0.00;
            }

            return Json(new
            {
                status = invoiceList.Count > 0 ? 1 : 0,
                message = "Customer Invoices fetched successfully ",
                data = invoiceList
            });
        }

        public JsonResult GetInvoiceProducts(int id)
        {
            var invoiceProducts = _context.CustomerInvoiceProducts
                .Include(p => p.Product)
                .Include(f => f.FinanceTax)
                .Where(i => i.CustomerInvoiceId == id && i.Status == 0)
                .ToList();


            return Json(new
            {
                status = invoiceProducts.Count > 0 ? 1 : 0,
                message = "products fetched successfully successfully",
                data = invoiceProducts
            });
        }

        public JsonResult RemoveProductFromInvoice(int id)
        {
            try
            {
                var product = _context.CustomerInvoiceProducts.First(b => b.Id == id);
                product.Status = 1;

//                var customerInvoiceProduct = new CustomerInvoiceProduct { Id = 1 };
//                _context.Entry(customerInvoiceProduct).State = EntityState.Deleted;
//                _context.SaveChanges();

//                var employer = new CustomerInvoiceProduct { Id = 1 };
//                _context.CustomerInvoiceProducts.Attach(employer);
//                _context.CustomerInvoiceProducts.Remove(employer);
                _context.SaveChanges();


                ResponseStatus = 1;
                this.Message = "Product Removed successfully";
            }
            catch (DbUpdateException ex)
            {
                Message = ex.Message;
                ResponseStatus = 0;
            }

            return Json(new
            {
                status = ResponseStatus,
                message = Message
            });
        }

        public JsonResult DeleteProduct(int id)
        {
            try
            {
                var product = _context.Products.First(b => b.Id == id);
                product.Status = 0;
                _context.SaveChanges();
                ResponseStatus = 1;
                this.Message = "Product Deleted successfully";
            }
            catch (DbUpdateException ex)
            {
                Message = ex.Message;
                ResponseStatus = 0;
            }

            return Json(new
            {
                status = ResponseStatus,
                message = Message
            });
        }

        public int ResponseStatus { get; set; }

        public string Message { get; set; }


        public JsonResult GetCategoriesWithProducts(string searchString = "")
        {
            var productsCategories = _context
                .FProductCategories
                .Where(p => p.Name.Contains(searchString))
                .ToList();
            var productsList = new List<dynamic>();
            foreach (var productCats in productsCategories)
            {
                var products = _context.Products.Where(p => p.FProductCategoryId == productCats.Id);
                productsList.Add(new {productCats, products});
            }

            return Json(new
            {
                status = productsList.Count > 0 ? 1 : 0,
                message = "products fetched successfully successfully",
                data = productsList
            });
        }

        public JsonResult GetTaxes()
        {
            var taxes = _context
                .FTaxes
                //.Where(p=>p.Status==0)
                .ToList();
            return Json(new
            {
                status = taxes.Count > 0 ? 1 : 0,
                message = "taxes fetched successfully successfully",
                data = taxes
            });
        }

        public JsonResult GetCurrencies()
        {
            var currencies = _context
                .FCurrencies
                //.Where(p=>p.Status==0)
                .ToList();
            return Json(new
            {
                status = currencies.Count > 0 ? 1 : 0,
                message = "currencies fetched successfully successfully",
                data = currencies
            });
        }

        public string ReturnInvoiceNumber()
        {
            int? invoiceNumber = _context.CustomerInvoices.Max(u => (int?) u.Id);
            var inNumber = "NITA";
            if (invoiceNumber == null)
            {
                inNumber += "001";
            }

            else
            {
                invoiceNumber += 1;
                if (invoiceNumber.ToString().Length < 2)

                    inNumber += "00" + invoiceNumber;

                if (invoiceNumber.ToString().Length < 3 && invoiceNumber.ToString().Length > 1)

                    inNumber += "0" + invoiceNumber;
                else
                    inNumber += invoiceNumber;
            }

            inNumber += "/" + DateTime.Now.Year;
            return inNumber;
        }

        public JsonResult ReturnJsonInvoiceNumber()
        {
            int? invoiceNumber = _context.CustomerInvoices.Max(u => (int?) u.Id);
            var inNumber = "NITA";
            if (invoiceNumber == null)
            {
                inNumber += "001";
            }

            else
            {
                invoiceNumber += 1;
                if (invoiceNumber.ToString().Length < 2)

                    inNumber += "00" + invoiceNumber;

                if (invoiceNumber.ToString().Length < 3 && invoiceNumber.ToString().Length > 1)

                    inNumber += "0" + invoiceNumber;
                else
                    inNumber += invoiceNumber;
            }

            inNumber += "/" + DateTime.Now.Year;

            return Json(new
            {
                status = 1,
                message = "Invoice Number fetched successfully successfully",
                data = inNumber
            });
        }


        public async Task<JsonResult> AddCustomerInvoice([FromBody] ViewProductInvoiceModel viewProductInvoiceModel)
        {
            var products = viewProductInvoiceModel.InvoiceProducts;

            var invoiceNumber = ReturnInvoiceNumber();

            var customerInvoice = new CustomerInvoice
            {
                CustomerId = viewProductInvoiceModel.CustomerId,
                DateDue = viewProductInvoiceModel.DueDate,
                FCurrencyId = viewProductInvoiceModel.FCurrencyId,
                Recurrency = viewProductInvoiceModel.Recurrency,
                Terms = viewProductInvoiceModel.Terms,
                InvoiceNo = invoiceNumber
            };

            await _context.CustomerInvoices.AddAsync(customerInvoice);

            await _context.SaveChangesAsync();

            var pList = new List<dynamic>();
            foreach (var product in products)
            {
                var invoice = new CustomerInvoiceProduct
                {
                    CustomerId = viewProductInvoiceModel.CustomerId,
                    Discount = product.Discount,
                    ProductSellPrice = product.SellPrice,
                    FTaxesId = product.FTaxesId,
                    FinanceTaxId = product.FTaxesId,
                    Quantity = product.Quantity,
                    CustomerInvoiceId = customerInvoice.Id,
                    ProductId = product.ProductId
                };

                _context.CustomerInvoiceProducts.Add(invoice);
                _context.SaveChanges();
            }

            return Json(new
            {
                status = customerInvoice.Id > 0 ? 1 : 0,
                message = customerInvoice.Id > 0
                    ? "Invoice save successfully - INV NO. " + invoiceNumber
                    : "Could not save invoice",
                data = invoiceNumber
            });
        }

        public JsonResult GetInvoiceTerms()
        {
            var invoiceT = new
            {
                InvoiceTerm.Due_On_Receipt,
                InvoiceTerm.Due_At_The_End_Of_Month,
                InvoiceTerm.Due_In_30_Days,
                InvoiceTerm.Due_In_60_Days,
                InvoiceTerm.Due_On_Day_Of_Next_Month
            };
            return Json(new
            {
                status = 1,
                message = "taxes fetched successfully successfully",
                data = invoiceT
            });
        }

        public JsonResult GetGlAccounts(string searchString = "")
        {
            var gLAccounts = _context.FLedgers

                .ToList();

            var listGlAccounts = new List<dynamic>();
            foreach (var gLAccount in gLAccounts)
            {

                var pModes = _context.FPaymentAccounts.Where(p => p.LedgerId == gLAccount.Id);
                listGlAccounts
                    .Add(new {gLAccount, pModes});
            }

            return Json(new
            {
                status = listGlAccounts.Count > 0 ? 1 : 0,
                message = "GL Accounts fetched successfully successfully",
                data = listGlAccounts
            });
        

        }

        public JsonResult GetGlAccountsPayAccount(int legderId)
        {
            var gLAccountsPayAccounts = _context.FPaymentAccounts
                .ToList();


            return Json(new
            {
                status = gLAccountsPayAccounts.Count > 0 ? 1 : 0,
                message = "GL Accounts Pay Account fetched successfully successfully",
                data = gLAccountsPayAccounts
            });
        }





        public JsonResult GetInvoicePayments(string searchString="")
        {
            var invoicePaymemnt = _context.InvoicePayments
                .ToList();
                



            return Json(new
            {
                status = invoicePaymemnt.Count > 0 ? 1 : 0,
                message = "Invoice Payments  fetched successfully successfully",
                data = invoicePaymemnt
            });
        }


        public JsonResult GetCustomerInvoices(int customerId)
        {

            var customerInvoices = _context
                .CustomerInvoices
                .Where(p => p.CustomerId==customerId)

                .ToList();

            var invoiceList = new List<dynamic>();
            foreach (var cI in customerInvoices)
            {
                var invoiceProducts = _context
                    .CustomerInvoiceProducts
                    .Where(i=>i.CustomerInvoiceId==cI.Id&&i.Status==0)
                    .Include(p=>p.Product)
                    .ToList();
                var sum = 0.00;
                var discount = 0.00;

                foreach (var products in invoiceProducts)
                {
                    sum += products.Quantity * products.ProductSellPrice;
                    discount += products.Discount;
                }

                invoiceList.Add(new
                {
                    cI,
                    invoiceProducts,
                    TotalAmount = (sum - discount)
                });
//                sum = 0.00;
            }

            return Json(new
            {
                status = invoiceList.Count > 0 ? 1 : 0,
                message = "Customer Invoices fetched successfully ",
                data = invoiceList
            });
           
        }


    }
}