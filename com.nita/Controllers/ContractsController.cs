﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.nita.IdentityModels;
using com.nita.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Nita.ViewModels.ContractViewModels;

namespace Nita.Controllers
{
    [Authorize]
    public class ContractsController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly IServiceProvider _provider;
        // GET: Contracts

        public ContractsController(
      IServiceProvider prov,
      IHostingEnvironment env,
      IOptions<AppSettings> appSettings, UserManager<ApplicationUser> userManager)
        {
            this._provider = prov;
            this._appSettings = appSettings;
            this._hostingEnvironment = env;
            this._userManager = userManager;
        }
        public ActionResult Index()
        {
            ContractModel contractModel = new ContractModel();
            //List<TradeAreaItem> areaItemsList = new List<TradeAreaItem>();
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            var contracts = db.TrainerContract.ToList();
            
            ViewBag.Contracts = contracts;
            var providers = db.Trainers.ToList();
            ViewBag.Trainers = providers;
            var cycles = db.Cycles.ToList();
            ViewBag.Cycles = cycles;
            //var tradeAreas = db.TradeAreas.ToList();
            //foreach (var tradeArea in tradeAreas) {
            //    TradeAreaItem item = new TradeAreaItem();
            //    item.TradeAreaName = tradeArea.Name;
            //    item.TradeAreaId = tradeArea.Id;
            //    item.Selected = false;
            //    areaItemsList.Add(item);
            //}
            //contractModel.TradeAreasList = areaItemsList;
            return View(contractModel);
        }

        public ActionResult SaveData(ContractModel contractModel) {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            int id = 0;
            if (contractModel != null) {
                TrainerContract trainerContract = new TrainerContract
                {
                    ContractName = contractModel.ContractName,
                    ContractStatus = contractModel.ContractStatus,
                    ContractNumber = contractModel.ContractNo,
                    CycleId = contractModel.CycleId,
                    Terminated = contractModel.TerminationStatus,
                    TrainerId = contractModel.TrainerId,
                    DateCreated = DateTime.Now
                };
                db.TrainerContract.Add(trainerContract);
                db.SaveChanges();
                id = trainerContract.Id;
            }

            return RedirectToAction("CreateNext",new {Id = id });
        }

        // GET: Contracts/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        //[HttpPost]
        //public ActionResult SaveTradeAreas(ListTr tradeAreaSave) {

        //    return RedirectToAction(nameof(Index));
        //}
        public ActionResult LeadTrainer(int id) {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            var tradeAreaContracts = db.TradeAreaContract.Where(t => t.ContractId == id).ToList();
            List<TradeArea> tradeAreaList = new List<TradeArea>();
            foreach (var tArea in tradeAreaContracts)
            {
                var tradeAreaId = tArea.TradeAreaId;
                var tradeArea = db.TradeAreas.Find(tradeAreaId);
                tradeAreaList.Add(tradeArea);
            }

            ViewBag.Id = id;
            ViewBag.TradeAreas = tradeAreaList;
            return View();
        }

        // GET: Contracts/Create
        public ActionResult Create()
        {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            var providers = db.Trainers.ToList();
            ViewBag.Trainers = providers;
            var cycles = db.Cycles.ToList();
            ViewBag.Cycles = cycles;

            return View();
        }

        public ActionResult CreateNext(int Id) {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            List<TradeContract> tradeAreaList = new List<TradeContract>();
            var tradeAreaContracts = db.TradeAreaContract.Where(t => t.ContractId == Id).ToList();
            var tradeAreas = db.TradeAreas.ToList();
            foreach (var tArea in tradeAreaContracts)
            {
                var tradeAreaId = tArea.TradeAreaId;
                var tradeArea = db.TradeAreas.Find(tradeAreaId);
                tradeAreaList.Add(new TradeContract {
                    Id = tradeArea.Id,
                    Name = tradeArea.Name,
                    Capacity = tArea.Capacity,
                    TradeContractId = tArea.Id
                    
                });
            }
            foreach (var area in tradeAreaList) {
                tradeAreas.RemoveAll(x => x.Id == area.Id);
            }
            ViewBag.TradeAreaList = tradeAreaList;
            ViewBag.Id = Id;
            
            ViewBag.TradeAreas = tradeAreas;
            return View();
        }

        public ActionResult removeTradeArea(int id, int contractId) {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            var tArea = db.TradeAreaContract.Where(t=>t.Id == id).FirstOrDefault();
            db.TradeAreaContract.Remove(tArea);
            db.SaveChanges();
            return RedirectToAction("CreateNext", new { Id = contractId });
        }

        public ActionResult ContractTradeAreas(int id) {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            
            var contractTerminal = db.TrainerContract.Find(id).Terminated;
            ViewBag.TerminatedCotract = contractTerminal;
            List<TradeArea> tradeAreaList = new List<TradeArea>();
            var tradeAreas = db.TradeAreas.ToList();
            var tradeAreaContracts = db.TradeAreaContract.Where(t => t.ContractId == id).ToList();
            foreach (var tArea in tradeAreaContracts) {
                var tradeAreaId = tArea.TradeAreaId;
                var tradeArea = db.TradeAreas.Find(tradeAreaId);
                tradeAreaList.Add(tradeArea);
            }
            ViewBag.TradeAreaList = tradeAreaList;
            ViewBag.Id = id;
            ViewBag.TradeAreas = tradeAreas;
            return View();
        }

        public ActionResult SaveTradeArea(int id, int contractId) {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            var tradeAreaContract = new TradeAreaContract()
            {
                ContractId = contractId,
                TradeAreaId = id
            };
            db.TradeAreaContract.Add(tradeAreaContract);
            db.SaveChanges();
            return RedirectToAction("ContractTradeAreas",new { id = contractId });
        }

        public ActionResult AddTradeArea(TradeAreaSave tradeArea)
        {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            var tradeAreaContract = new TradeAreaContract()
            {
                ContractId = tradeArea.ContractId,
                TradeAreaId = tradeArea.TradeAreaId,
                Capacity = tradeArea.Capacity
            };
            db.TradeAreaContract.Add(tradeAreaContract);
            db.SaveChanges();
            return RedirectToAction("CreateNext", new {Id = tradeArea.ContractId });
        }

        // POST: Contracts/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult TerminateContract(int id) {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            var contract = db.TrainerContract.Find(id);
            var trainerId = contract.TrainerId;
            contract.Terminated = true;
            db.SaveChanges();

            var trainer = db.Trainers.Find(trainerId);
            trainer.Status = TrainerStatus.Terminated;
            return RedirectToAction(nameof(Index));
        }

        // GET: Contracts/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Contracts/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Contracts/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Contracts/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}