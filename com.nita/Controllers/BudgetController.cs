﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.nita.IdentityModels;
using com.nita.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Nita.ViewModels.BudgetViewModels;

namespace Nita.Controllers
{
    [Authorize]
    public class BudgetController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly IServiceProvider _provider;
        // GET: Contracts

        public BudgetController(
            IServiceProvider prov,
            IHostingEnvironment env,
            IOptions<AppSettings> appSettings, UserManager<ApplicationUser> userManager)
        {
            this._provider = prov;
            this._appSettings = appSettings;
            this._hostingEnvironment = env;
            this._userManager = userManager;
        }
        public IActionResult Index(int? cycleId,int? deptId)
        {
            var cId = Convert.ToInt32(cycleId);
            var dId = Convert.ToInt32(deptId);
            
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            List<VoteHead> voteList = new List<VoteHead>();
            var periods = db.BudgetPeriod.ToList();
            var items=new List<FinanceBudget>();
            if (cId!=0 && dId!=0)
            {
                var bPeriods = db.BudgetPeriod.Where(p => p.FYId == cId).ToList();
                foreach (var bPeriod in bPeriods)
                {
                    var periodItems = db.FBudgets.Where(f => f.PeriodId == bPeriod.Id && f.DeptId == dId).ToList();
                    items.AddRange(periodItems);
                }

                var bCycles = db.Cycles.Where(c => c.FYId == cId).ToList();
                foreach (var bCycle in bCycles)
                {
                    var cycleItems = db.FBudgets.Where(f => f.CycleId == bCycle.Id && f.DeptId == dId).ToList();
                    items.AddRange(cycleItems);
                }
                //items = db.FBudgets.Where(f => f.YearId == cId && f.DeptId == dId).ToList();
                ViewBag.Message = db.FiscalYears.Single(c => c.Id == cId).Name + " Fiscal Year | " + db.HrDepartments.Single(d => d.Id == dId).Name+" Department Budget Items";
                ViewBag.dId = dId;
                ViewBag.yId = cId;
            }else if (cId!=0 && dId==0)
            {
                var bPeriods = db.BudgetPeriod.Distinct().Where(p => p.FYId == cId).ToList();
                foreach (var bPeriod in bPeriods)
                {
                    var periodItems = db.FBudgets.Where(f => f.PeriodId == bPeriod.Id).ToList();
                    items.AddRange(periodItems);
                }

                var bCycles = db.Cycles.Where(c => c.FYId == cId).ToList();
                foreach (var bCycle in bCycles)
                {
                    var cycleItems = db.FBudgets.Where(f => f.CycleId == bCycle.Id).ToList();
                    items.AddRange(cycleItems);
                }
                //items = db.FBudgets.Where(x => x.YearId == cId).ToList();
                ViewBag.Message = db.FiscalYears.Single(c => c.Id == cId).Name + " Fiscal Year | " +  "All Department(s) Budget Items";
                ViewBag.dId = 0;
                ViewBag.yId = cId;
            }
            else if (cId == 0 && dId != 0)
            {
                items = db.FBudgets.Where(x => x.DeptId == dId).ToList();
                ViewBag.Message =  "All Fiscal Year(s) | " + db.HrDepartments.Single(c => c.Id == dId).Name + " Department(s) Budget Items";
                ViewBag.dId = dId;
                ViewBag.yId = 0;
            }
            else
            {
                items = db.FBudgets.ToList();
                ViewBag.Message = "All Financial Years | All Departments Budget Items";
                ViewBag.dId = 0;
                ViewBag.yId = 0;
            }

            var distinctItems = items.GroupBy(x => x.Id).Select(y => y.First());

            foreach (var item in distinctItems)
            {
                var vote = new VoteHead()
                {
                   Number = item.Id,
                   AccName = db.FinanceAccounts.Single(a=>a.Id == item.AccountId).Name.ToString(),
                   Amount = item.Amount,
                    Enforced = item.Enforced
                };
                voteList.Add(vote);
            }
            var accounts = db.FinanceAccounts.ToList();
            var departments = db.HrDepartments.ToList();
            var fYears = db.FiscalYears.ToList();
            var cycles = db.Cycles.ToList();
            ViewBag.Periods = periods;
            ViewBag.VoteHeads = voteList;
            ViewBag.Accounts = accounts;
            ViewBag.Departments = departments;
            ViewBag.FYears = fYears;
            ViewBag.Cycles = cycles;
            return View();
        }

        public ActionResult MainBudget(int? yId)
        {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            List<MainBudgetItem> mainBudgetItems = new List<MainBudgetItem>();
            var accounts = db.FinanceAccounts.ToList();
            var total = 0.0;
            if (yId == 0 || yId == null)
            {
                
                foreach (var account in accounts)
                {
                    var accountItems = db.FBudgets.Where(a => a.AccountId == account.Id).ToList();
                    var sum = 0.0;
                    foreach (var accItem in accountItems)
                    {
                        sum += accItem.Amount;
                    }


                    mainBudgetItems.Add(new MainBudgetItem()
                    {
                        Id = account.Id,
                        Name = account.Name,
                        Amount = sum
                    });
                    total += sum;
                }

                ViewBag.Total = total;
                ViewBag.Message = "All Fiscal Years Budget";
            }
            else
            {
                foreach (var account in accounts)
                {
                    var accountItems = db.FBudgets.Where(a => a.AccountId == account.Id).ToList();
                    var sum = 0.0;
                    foreach (var accItem in accountItems)
                    {
                            if (accItem.CycleId != 0)
                            {
                                var response = db.Cycles.Where(c => c.Id == accItem.CycleId).FirstOrDefault();
                                if (response != null)
                                {
                                    var cyid = response.FYId;
                                    if (cyid == yId)
                                    {
                                        sum += accItem.Amount;
                                    }
                            }
                                else
                                {
                                sum += 0;
                            }
                            }
                            else
                            {
                                var response = db.BudgetPeriod.Where(c => c.Id == accItem.PeriodId).First();
                                if (response != null)
                                {
                                    var pyid = response.FYId;
                                    if (pyid == yId)
                                    {
                                        sum += accItem.Amount;
                                    }
                            }
                                else
                                {
                                    sum += 0;
                            }
                            }
                            
                        
                    }

                    mainBudgetItems.Add(new MainBudgetItem()
                    {
                        Id = account.Id,
                        Name = account.Name,
                        Amount = sum
                    });
                    total += sum;
                }
                ViewBag.Total = total;
                ViewBag.fyId = yId;
                ViewBag.Message = db.FiscalYears.Single(f=>f.Id == yId).Name+" Fiscal Year Budget";
            }

            var fYears = db.FiscalYears.ToList();
            ViewBag.FYears = fYears;
            ViewBag.MainBudgetItems = mainBudgetItems;
            return View();
        }

        public ActionResult BudgetPeriod(int? errorCode)
        {
            if (errorCode != 0 && errorCode != null)
            {
                var error = Convert.ToInt32(errorCode);
                if (error == 1)
                {
                    ViewBag.Error = 1;
                    ViewBag.DateMessage = "Start Date must fall within the fiscal year dates";
                }
                if (error == 2)
                {
                    ViewBag.Error = 2;
                    ViewBag.DateMessage = "End Date must fall within the fiscal year";
                }

                if (error == 3)
                {
                    ViewBag.Error = 3;
                    ViewBag.DateMessage = "Period Cannot be deleted because it has some related records";
                }
            }
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            var fYears = db.FiscalYears.ToList();
            var periods = db.BudgetPeriod.ToList();
            ViewBag.FYears = fYears;
            ViewBag.Periods = periods;
            return View();
        }

        public ActionResult Create(BudgetItem budgetItem)
        {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            int id = budgetItem.Id;
            int? cycleId = null;
            int? periodId = null;
            if (budgetItem.CycleId != 0)
            {
                cycleId = budgetItem.CycleId;
            }

            if (budgetItem.PeriodId !=0)
            {
                periodId = budgetItem.PeriodId;
            }

            if (id == 0)
            {
                
                db.FBudgets.Add(new FinanceBudget()
                {
                    AccountId = budgetItem.AccountId,
                    CycleId = cycleId,
                    DeptId = budgetItem.DeptId,
                    Amount = budgetItem.Amount,
                    Name = "",
                    Enforced = budgetItem.Enforce,
                    PeriodId = budgetItem.PeriodId
                });
                db.SaveChanges();
            }
            else
            {
                var fBudgetItem = db.FBudgets.Find(id);
                fBudgetItem.CycleId = budgetItem.CycleId;
                fBudgetItem.AccountId = budgetItem.AccountId;
                fBudgetItem.Amount = budgetItem.Amount;
                fBudgetItem.DeptId = budgetItem.DeptId;
                fBudgetItem.Enforced = budgetItem.Enforce;
                fBudgetItem.PeriodId = periodId;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> SavePeriod(BudgetPeriodItem budget)
        {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            var period = new BudgetPeriod();
            
            var id = budget.PeriodId;
            var day = budget.Day;
            var month = budget.Month;
            var year = budget.Year;
            var date = month + "/" + day + "/" + year;
            var day1 = budget.Day1;
            var month1 = budget.Month1;
            var year1 = budget.Year1;
            var date1 = month1 + "/" + day1 + "/" + year1;
            var startDate = Convert.ToDateTime(date);
            var endDate = Convert.ToDateTime(date1);

            var fiscalYear = db.FiscalYears.Find(budget.YearId);
            if (fiscalYear.BeginDate > startDate || fiscalYear.EndDate < startDate)
            {
                return RedirectToAction("BudgetPeriod", new { errorCode = 1 });
            }

            if (fiscalYear.BeginDate > endDate || fiscalYear.EndDate < endDate)
            {

                return RedirectToAction("BudgetPeriod", new { errorCode = 2 });
            }
            //var duration = cycleModel.Duration;
            if (id == 0)
            {
                period.PeriodName = budget.PeriodName;
                period.PeriodFrom = startDate;
                period.PeriodTo = endDate;
                period.FYId = budget.YearId;
                await db.BudgetPeriod.AddAsync(period);

                db.SaveChanges();
            }
            else
            {
                var periode = db.BudgetPeriod.Find(id);
                periode.PeriodName = budget.PeriodName;
                periode.PeriodFrom = startDate;
                periode.PeriodTo = endDate;
                periode.FYId = budget.YearId;
                db.SaveChanges();
            }
            return RedirectToAction("BudgetPeriod");
        }

        public JsonResult Edit(int id)
        {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            var budgetItem = db.FBudgets.Single(i=>i.Id == id);
            var Id = budgetItem.Id;
            var cycleId = budgetItem.CycleId;
            var periodId = budgetItem.PeriodId;
            var amount = budgetItem.Amount;
            var deptId = budgetItem.DeptId;
            var enforced = budgetItem.Enforced;
            var accountId = budgetItem.AccountId;
            return Json(new {Id = Id, CycleId= cycleId, PeriodId = periodId, Amount = amount,DeptId=deptId,Enforced=enforced,AccId =accountId });
        }

        public JsonResult EditPeriod(int id)
        {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            var period = db.BudgetPeriod.Find(id);
            var name = period.PeriodName;
            var Id = period.Id;
            var day = period.PeriodFrom.Day.ToString();
            var month = period.PeriodFrom.Month.ToString();
            var year = period.PeriodFrom.Year.ToString();
            var day1 = period.PeriodFrom.Day.ToString();
            var month1 = period.PeriodFrom.Month.ToString();
            var year1 = period.PeriodFrom.Year.ToString();
            var yearId = period.FYId;
            return Json(new
            {
                Id = Id,name=name,day=day,month=month,year=year,
                day1 = day1,month1 = month1,year1 = year1,yearId = yearId
            });
        }

        public ActionResult DeletePeriod(int id)
        {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            if (db.FBudgets.Where(p=>p.PeriodId == id).Any())
            {
                return RedirectToAction("BudgetPeriod", new { errorCode = 3 });
            }

            db.Remove(db.BudgetPeriod.Find(id));
            db.SaveChanges();
            return RedirectToAction("BudgetPeriod");
        }

        public ActionResult Delete(int id)
        {
            var db = _provider.GetRequiredService<ApplicationDbContext>();
            db.Remove(db.FBudgets.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}