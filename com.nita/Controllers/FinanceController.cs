﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using com.nita.Constants;
using com.nita.IdentityModels;
using com.nita.Services;
using com.nita.Settings;
using com.nita.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Nita.Controllers
{
    [Authorize(DefaultRoles.NitaSysAdmin)]
    public partial class FinanceController : Controller
    {
        private readonly IOptions<AppSettings> appSettings;
        private readonly IServiceProvider provider;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly ILogger logger;
        private readonly IFinanceService _service;

        public FinanceController(IServiceProvider prov,
            IHostingEnvironment env, IFinanceService srv,
            IOptions<AppSettings> appSettings)
        {
            this.provider = prov;
            this.appSettings = appSettings;
            this.hostingEnvironment = env;
            this._service = srv;
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("finance", Name = FinanceControllerRoute.GetIndex)]
        public IActionResult Index()
        {
            var apAccs = appSettings.Value.Accounts.APCreditorAccs;
            //
            if (apAccs == null || apAccs.Count == 0)
            {
                return RedirectToRoute(FinanceControllerRoute.GetUpdateSettings, new { msg = "Specify default behaviours for GL Accounts..." });
            }

            return this.View(FinanceControllerAction.Index);
        }

        #region Payments Register
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("fpregistry", Name = FinanceControllerRoute.GetPaymentsRegistry)]
        public IActionResult PaymentsRegistry()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.PaymentsRegistry);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("unpostedpys", Name = FinanceControllerRoute.GetUnpostedPayments)]
        public IActionResult UnpostedPayments()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.UnpostedPayments);
        }
        #endregion

        #region Invoices
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allinvoices", Name = FinanceControllerRoute.GetAllInvoices)]
        public IActionResult AllInvoices()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllInvoices);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addinvoice", Name = FinanceControllerRoute.GetAddInvoice)]
        public IActionResult AddInvoice()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //Check supplier
            if (!db.FSuppliers.Any())
            {
                return RedirectToRoute(FinanceControllerRoute.GetAddSupplier, new { msg = "You need to add a supplier before adding an invoice." });
            }
            //TODO :: Check user role

            //Check child account
            if (!db.FinanceAccounts.Include(j => j.Parent).Any(j => j.Parent != null))
            {
                return RedirectToRoute(FinanceUtilsControllerRoute.AddVendorType, new { msg = "An account is required to add an invoice." });
            }
            //
            var model = new AddInvoiceViewModel()
            {
                //DocNo = Utils.GetInvoiceDocNo(db)
            };
            //
            return this.View(FinanceControllerAction.CreateInvoice, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addinvoice", Name = FinanceControllerRoute.GetAddInvoice)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddInvoice(AddInvoiceViewModel model)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();


            //
            if (ModelState.IsValid)
            {
                //
                if (db.FInvoices.Any(k => k.OrderNo.ToLower().Equals(model.OrderNo.ToLower())))
                {
                    ModelState.AddModelError(string.Empty, "An invoice with same order number exist.");
                }

                //
                var invoice = new FInvoice()
                {
                    OrderNo = model.OrderNo,
                    DueDate = model.DueDate,
                    InvoiceDate = model.InvoiceDate
                };
                //
                try
                {
                    //
                    var personnel = db.HrStaff.Include(l => l.ApplicationUser).Single(l => l.ApplicationUser.UserName == User.Identity.Name);
                    invoice.PersonnelId = personnel.Id;
                    //
                    invoice.DocNo = Utils.GetInvoiceDocNo(db);
                    //
                    switch (model.Term)
                    {
                        case (int)InvoiceTerm.Due_At_The_End_Of_Month:
                            //
                            invoice.Term = InvoiceTerm.Due_At_The_End_Of_Month;
                            invoice.DueDate = new DateTime(model.InvoiceDate.Year, model.InvoiceDate.Month + 1, 1);
                            break;
                        case (int)InvoiceTerm.Due_In_30_Days:
                            //
                            invoice.Term = InvoiceTerm.Due_In_30_Days;
                            invoice.DueDate = model.InvoiceDate.AddDays(30);
                            break;
                        case (int)InvoiceTerm.Due_In_45_Days:
                            //
                            invoice.Term = InvoiceTerm.Due_In_45_Days;
                            invoice.DueDate = model.InvoiceDate.AddDays(45);
                            break;
                        case (int)InvoiceTerm.Due_In_60_Days:
                            //
                            invoice.Term = InvoiceTerm.Due_In_60_Days;
                            invoice.DueDate = model.InvoiceDate.AddDays(60);
                            break;
                        case (int)InvoiceTerm.Due_On_Day_Of_Next_Month:
                            //
                            invoice.Term = InvoiceTerm.Due_On_Day_Of_Next_Month;
                            invoice.DueDate = new DateTime(model.InvoiceDate.Year, model.InvoiceDate.Month + 1, 1);
                            break;
                        case (int)InvoiceTerm.Custom:
                            //
                            if (model.BillDate.Value.CompareTo(DateTime.Now) < 0)
                            {
                                ModelState.AddModelError("", "Invalid billing date specified...");
                                //
                                return this.View(FinanceControllerAction.CreateInvoice, model);
                            }
                            //
                            invoice.Term = InvoiceTerm.Custom;
                            //
                            invoice.DueDate = model.BillDate.Value;
                            break;
                        default:
                            //
                            invoice.Term = InvoiceTerm.Due_On_Receipt;
                            invoice.DueDate = model.InvoiceDate;
                            break;
                    }
                    //GL Account
                    //
                    try
                    {
                        var payables = JsonConvert.DeserializeObject<List<JsonAccountPayable>>(model.Payables);
                        //
                        var list = await _service.GetAccountsPayable(payables, PayablesType.AccPayables, ModelState);
                        //
                        if (ModelState.ErrorCount > 0)
                        {
                            return this.View(FinanceControllerAction.CreateInvoice, model);
                        }
                        else
                        {
                            list.ForEach(o => invoice.Payments.Add(o));
                        }

                    }
                    catch
                    {
                        ModelState.AddModelError("", "Invoice Account Payables could not be parsed. Please try again");
                        return this.View(FinanceControllerAction.CreateInvoice, model);
                    }
                    //Ledger
                    var lgr = db.FLedgers.Include(l => l.Projects).Single(h => h.Id == model.Ledger);
                    invoice.LedgerId = lgr.Id;
                    //Supplier
                    var sup = db.FSuppliers.Single(k => k.Id == model.Supplier);
                    invoice.SupplierId = sup.Id;
                    //Project
                    if (model.Project > 0)
                    {
                        var proj = lgr.Projects.Single(l => l.Id == model.Project);
                        invoice.ProjectId = proj.Id;
                    }
                    //
                    db.FInvoices.Add(invoice);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllInvoices, new { msg = "Invoice created succesifully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Invoice could NOT be created. Please try again..");
                }
            }
            //model.DocNo = Utils.GetInvoiceDocNo(db);
            //
            return this.View(FinanceControllerAction.CreateInvoice, model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        [HttpPost("dropinvoice", Name = FinanceControllerRoute.GetDropInvoice)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DropInvoice(int inv)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (inv > 0)
            {
                var invoice = db.FInvoices.Include(l => l.Payments).Single(l => l.Id == inv);

                //
                invoice.Status = EntityStatus.Deleted;

                //
                foreach (var payment in invoice.Payments)
                {
                    payment.Status = EntityStatus.Deleted;
                }

                //
                await db.SaveChangesAsync();

                return new JsonResult(new { s = 1, msg = "Supplier Deleted Successifully.." });
            }
            //
            return new JsonResult(new { s = 0, err = "Supplier Not found." });
        }
        #endregion

        #region Invoice Waiver
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allsupiw", Name = FinanceControllerRoute.GetAllInvoiceWaivers)]
        public IActionResult AllSupplierInvoiceWaivers()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllInvoiceWaivers);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addsupiw", Name = FinanceControllerRoute.GetAddInvoiceWaiver)]
        public async Task<IActionResult> AddSupplierInvoiceWaiver()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            var model = new AddSupplierInvoiceWaiverViewModel();
            model.Ref = (await Utils.GenerateInvoiceWaiverRef(db)).ToString("0000");
            //
            return this.View(FinanceControllerAction.AddInvoiceWaiver, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addsupiw", Name = FinanceControllerRoute.GetAddInvoiceWaiver)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddSupplierInvoiceWaiver(AddSupplierInvoiceWaiverViewModel model, params int[] taxes)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (ModelState.IsValid)
            {
                //
                try
                {
                    var invoice = db.FInvoices.Include(l => l.Payments).ThenInclude(l => l.GLAccount).Include(l => l.Payments).ThenInclude(l => l.GLTransactions).Include(l => l.Supplier).ThenInclude(l => l.VendorType).Single(l => l.Id == model.InvoiceId);
                    //
                    model.Invoice = invoice;
                    //
                    var waiver = new SupplierInvoiceWaiver()
                    {
                        RefNo = await Utils.GenerateInvoiceWaiverRef(db),
                        DateCreated = DateTime.Now,
                        GrossAmount = model.Amount,
                        Status = EntityStatus.Active,
                        Description = model.Details
                    };
                    //
                    waiver.InvoiceId = invoice.Id;
                    var personnel = db.HrStaff.Include(l => l.ApplicationUser).Single(l => l.ApplicationUser.UserName == User.Identity.Name);
                    waiver.PersonnelId = personnel.Id;
                    //
                    var acc = db.FinanceAccounts.Single(l => l.Id == model.AccountId);
                    //waiver.AccountId = acc.Id;
                    var dept = db.HrDepartments.Single(l => l.Id == model.DeptId);
                    //waiver.DeptId = dept.Id;
                    var payment = new FPayment()
                    {
                        AccountId = acc.Id,
                        CurrencyId = db.FCurrencies.Single(p => p.IsBase).Id,
                        GrossAmount = model.Amount,
                        DeptId = dept.Id,
                        PayableTo = invoice.Supplier.Name,
                        PayeeName = invoice.Supplier.Name,
                    };
                    //
                    FinanceAccount apAcc = invoice.Payments.FirstOrDefault(l => l.GLTransactions.Any(d => d.Kind != GLTransactionKind.Tax)).GLAccount;
                    if (apAcc != null)
                    {
                        //Credit
                        var trans = new GLTransaction()
                        {
                            Amount = model.Amount,
                            DateEntered = DateTime.Now,
                            AccountId = apAcc.Id,
                            Type = GLTransactionType.Credit,
                        };
                        payment.GLTransactions.Add(trans);
                        //Debit
                        var trans1 = new GLTransaction()
                        {
                            Amount = model.Amount,
                            DateEntered = DateTime.Now,
                            AccountId = acc.Id,
                            Type = GLTransactionType.Debit,
                        };
                        payment.GLTransactions.Add(trans1);
                    }
                    waiver.Payment = payment;
                    //
                    db.FSIWaivers.Add(waiver);
                    //
                    await db.SaveChangesAsync();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllInvoiceWaivers, new { msg = "Supplier Invoice Waiver created succesifully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Supplier Invoice Waiver could NOT be created. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.AddInvoiceWaiver, model);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("updatesupiw", Name = FinanceControllerRoute.UpdateInvoiceWaiver)]
        public async Task<IActionResult> UpdateSupplierInvoiceWaiver(int wid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (wid > 0)
            {
                try
                {

                    var model = new AddSupplierInvoiceWaiverViewModel();

                    model.Waiver = db.FSIWaivers.Single(p => p.Id == wid);

                    return this.View(FinanceControllerAction.AddInvoiceWaiver, model);
                }
                catch { }
            }

            //
            return NotFound();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("updatesupiw", Name = FinanceControllerRoute.UpdateInvoiceWaiver)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateSupplierInvoiceWaiver(AddSupplierInvoiceWaiverViewModel model, params int[] taxes)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (ModelState.IsValid)
            {
                if (model.WaiverId == 0)
                    return NotFound();

                //
                try
                {
                    var waiver = db.FSIWaivers.Include(l => l.Invoice).ThenInclude(l => l.Payments).Include(l => l.Invoice).ThenInclude(l => l.Payments).ThenInclude(l => l.GLTransactions).Single(l => l.Id == model.WaiverId);
                    //
                    model.Waiver = waiver;
                    //
                    if (waiver.GrossAmount != model.Amount)
                        waiver.GrossAmount = model.Amount;
                    //
                    if (waiver.GrossAmount != model.Amount)
                        waiver.Description = model.Details;

                    //
                    await db.SaveChangesAsync();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllInvoiceWaivers, new { msg = "Supplier Invoice Waiver created succesifully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Supplier Invoice Waiver could NOT be created. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.AddInvoiceWaiver, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        [HttpPost("dropsupiw", Name = FinanceControllerRoute.DropInvoiceWaiver)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DropInvoiceWaiver(int wid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (wid > 0)
            {
                var waiver = db.FSIWaivers.Include(l => l.Payment).Single(l => l.Id == wid);

                waiver.Status = EntityStatus.Deleted;

                //
                waiver.Payment.Status = EntityStatus.Deleted;

                //
                await db.SaveChangesAsync();

                return new JsonResult(new { s = 1, msg = "Supplier Invoice waiver Deleted Successifully.." });
            }
            //
            return new JsonResult(new { s = 0, err = "Supplier Not found." });
        }
        #endregion

        #region Supplier/Vendors
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allsup", Name = FinanceControllerRoute.GetAllSuppliers)]
        public IActionResult AllSuppliers()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllSuppliers);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addsup", Name = FinanceControllerRoute.GetAddSupplier)]
        public IActionResult AddSupplier()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (!db.FVendorTypes.Any())
            {
                return RedirectToRoute(FinanceUtilsControllerRoute.AddVendorType, new { msg = "You need to add a vendor type before adding a supplier." });
            }
            //
            return this.View(FinanceControllerAction.AddSupplier);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addsup", Name = FinanceControllerRoute.GetAddSupplier)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddSupplier(AddSupplierViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                //model.RefNo = Utils.GetSupplierReference(db);

                try
                {
                    //
                    var sup = new FSupplier()
                    {
                        Name = model.Name,
                        Address = model.Address,
                        BankAccount = model.BankAccount,
                        CellNo = model.CellNo,
                        Discount = model.Discount,
                        Email = model.Email,
                        ExemptFromTax = model.ExemptFromTax,
                        Ext = model.Ext,
                        FaxNo = model.FaxNo,
                        HoldPayments = model.HoldPayments,
                        IsActive = model.IsActive,
                        KRAPIN = model.KRAPIN,
                        TelNo = model.TelNo,
                        VATNo = model.VATNo
                    };
                    //

                    //
                    sup.RefNo = await Utils.GenerateSupplierRef(db);
                    //
                    var vtyp = db.FVendorTypes.Single(p => p.Id == model.VendorType);
                    sup.VendorTypeId = vtyp.Id;

                    //
                    var latlng = JsonConvert.DeserializeObject<JsonLatLng>(model.LatLng);
                    var loc = new Location()
                    {
                        Name = model.Loc,
                        Coodinates = new LatLng()
                        {
                            Longitude = latlng.lng,
                            Latitude = latlng.lat
                        }
                    };
                    //
                    var county = db.Counties.Single(l => l.Id == model.County);
                    loc.CountyId = county.Id;
                    db.Locations.Add(loc);
                    db.SaveChanges();
                    //
                    sup.LocationId = loc.Id;
                    //
                    db.FSuppliers.Add(sup);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllSuppliers, new { msg = "Supplier '" + model.Name + "' added successfully" });
                } catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, "Create Supplier failed.");
                }
            }
            //
            return this.View(FinanceControllerAction.AddSupplier, model);
        }

        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("updatesup", Name = FinanceControllerRoute.UpdateSupplier)]
        public IActionResult UpdateSupplier(int sid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (sid > 0)
            {
                try
                {
                    var model = new AddSupplierViewModel()
                    {
                        Supplier = db.FSuppliers.Single(l => l.Id == sid)
                    };
                    //
                    return this.View(FinanceControllerAction.UpdateSupplier, model);
                }
                catch
                {

                }
            }
            //
            return NotFound();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("updatesup", Name = FinanceControllerRoute.UpdateSupplier)]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateSupplier(AddSupplierViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.SupplierId == 0)
                {
                    return NotFound();
                }
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();

                var sup = db.FSuppliers.Single(p => p.Id == model.SupplierId);
                model.Supplier = sup;
                try
                {
                    //
                    if (sup.Name != model.Name)
                        sup.Name = model.Name;
                    //
                    if (sup.Address != model.Address)
                        sup.Address = model.Address;
                    //
                    if (sup.BankAccount != model.BankAccount)
                        sup.BankAccount = model.BankAccount;
                    //
                    if (sup.CellNo != model.CellNo)
                        sup.CellNo = model.CellNo;
                    //
                    if (sup.Discount != model.Discount)
                        sup.Discount = model.Discount;
                    //
                    if (sup.Email != model.Email)
                        sup.Email = model.Email;
                    //
                    if (sup.ExemptFromTax != model.ExemptFromTax)
                        sup.ExemptFromTax = model.ExemptFromTax;
                    //
                    if (sup.Ext != model.Ext)
                        sup.Ext = model.Ext;
                    //
                    if (sup.FaxNo != model.FaxNo)
                        sup.FaxNo = model.FaxNo;
                    //
                    if (sup.HoldPayments != model.HoldPayments)
                        sup.HoldPayments = model.HoldPayments;
                    //
                    if (sup.IsActive != model.IsActive)
                        sup.IsActive = model.IsActive;
                    //
                    if (sup.KRAPIN != model.KRAPIN)
                        sup.KRAPIN = model.KRAPIN;
                    //
                    if (sup.TelNo != model.TelNo)
                        sup.TelNo = model.TelNo;
                    //
                    if (sup.VATNo != model.VATNo)
                        sup.VATNo = model.VATNo;
                    //
                    if (sup.VendorTypeId != model.VendorType)
                    {
                        //
                        var vtyp = db.FVendorTypes.Single(p => p.Id == model.VendorType);
                        sup.VendorTypeId = vtyp.Id;
                    }
                    //
                    db.FSuppliers.Add(sup);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllSuppliers, new { msg = "Supplier '" + model.Name + "'wa updated successfully" });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, "Update Supplier failed.");
                }
            }
            //
            return this.View(FinanceControllerAction.AddSupplier, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        [HttpPost("dropsup", Name = FinanceControllerRoute.DropSupplier)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DropSupplier(int sid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (sid > 0)
            {
                var Supplier = db.FSuppliers.Include(l => l.Invoices).ThenInclude(l => l.Payments).Single(l => l.Id == sid);

                Supplier.Status = EntityStatus.Deleted;
                //
                foreach (var inv in Supplier.Invoices)
                {
                    foreach (var pay in inv.Payments)
                    {
                        pay.Status = EntityStatus.Deleted;
                    }
                    inv.Status = EntityStatus.Deleted;
                }
                //
                await db.SaveChangesAsync();

                return new JsonResult(new { s = 1, msg = "Supplier Deleted Successifully.." });
            }
            //
            return new JsonResult(new { s = 0, err = "Supplier Not found." });
        }
        #endregion

        #region Regular Payment Vouchers
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allrvouchers", Name = FinanceControllerRoute.GetAllRegularPVouchers)]
        public IActionResult AllRegularPVouchers()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllRegularPVouchers);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addrvoucher", Name = FinanceControllerRoute.GetAddRegularPVoucher)]
        public IActionResult AddRegularPaymentVoucher(int iid, string invs)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (!db.FInvoices.Any())
            {
                return RedirectToRoute(FinanceControllerRoute.GetAddInvoice, new { msg = "Create an invoice before creating a payment voucher" });
            }
            var model = new AddPaymentVoucherViewModel()
            {
                VoucherNo = Utils.GenerateVoucherNo(db).ToString(),
            };
            //
            try
            {
                //
                if (iid > 0)
                {
                    model.SelectedInvoices.Add(db.FInvoices.SingleOrDefault(l => l.Id == iid));
                    model.InvoiceIds = JsonConvert.SerializeObject(new int[] { iid });
                } else if (invs != null)
                {
                    var list = JsonConvert.DeserializeObject<int[]>(invs);
                    //
                    foreach (var e in list)
                    {
                        model.SelectedInvoices.Add(db.FInvoices.SingleOrDefault(l => l.Id == e));
                    }
                    //
                    model.InvoiceIds = JsonConvert.SerializeObject(list);
                }
            }
            catch
            {
                return new NotFoundResult();
            }
            //
            return this.View(FinanceControllerAction.CreateRegularPVoucher, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addrvoucher", Name = FinanceControllerRoute.GetAddRegularPVoucher)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddRegularPaymentVoucher(AddPaymentVoucherViewModel model, params int[] taxes)
        {
            if (ModelState.IsValid)
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                model.VoucherNo = Utils.GenerateVoucherNo(db).ToString();
                if (!string.IsNullOrEmpty(model.InvoiceIds))
                {
                    var list = JsonConvert.DeserializeObject<int[]>(model.InvoiceIds);
                    foreach (var e in list)
                    {
                        model.SelectedInvoices.Add(db.FInvoices.SingleOrDefault(l => l.Id == e));
                    }
                }
                //
                var pv = new FPaymentVoucher()
                {
                    CheckNo = model.CheckNo,
                    VoucherDate = DateTime.Now,
                    Description = model.Description,
                    Type = PVType.Regular,
                    Status = PVStatus.Pending
                };
                //Ledger
                var ledger = db.FLedgers.Single(p => p.Id == model.Ledger);
                pv.LedgerId = ledger.Id;
                //Project
                if (model.Project > 0)
                {
                    //var rpoj = db.FProjects.Single(p => p.Id == model.Sponsor);
                    ////
                    //pv.SponsorId = sponsor.Id;
                }
                //
                var personnel = db.Users.Include(j => j.StaffProfile).Single(l => l.UserName == User.Identity.Name).StaffProfile;
                //Pesonel
                pv.PersonnelId = personnel.Id;
                //
                pv.VoucherNo = Utils.GenerateVoucherNo(db).ToString();

                try
                {
                    //
                    if (model.Invoices != null && model.Invoices.Length > 0)
                    {
                        var sup = db.FSuppliers.Single(p => p.Id == model.Supplier);
                        pv.SupplierId = sup.Id;

                        //Continue

                        foreach (var i in model.Invoices)
                        {
                            var invoice = db.FInvoices/*.Include(l=>l.Accounts)*/.Include(l => l.Payments).Single(l => l.Id == i);

                            //
                            //Tax
                            foreach (var payment in invoice.Payments)
                            {
                                //Add Payment
                                pv.Payments.Add(payment);
                            }

                        }
                    }
                    else
                    {
                        //Unregistered details
                        var valid = true;
                        //Gl Acc
                        var acc = db.FinanceAccounts.SingleOrDefault(l => l.Id == model.GLAccount);
                        if (acc == null)
                        {
                            ModelState.AddModelError(string.Empty, "Please specify some invoice or a GL Account for the payment voucher.");
                            valid = false;
                        }
                        //Gl Acc
                        var ap = db.FinanceAccounts.SingleOrDefault(l => l.Id == model.APAccount);
                        if (acc == null)
                        {
                            ModelState.AddModelError(string.Empty, "Please specify some invoice or a A/P (Creditors) Account for the payment voucher.");
                            valid = false;
                        }
                        var dept = db.HrDepartments.Include(k => k.Budgets).ThenInclude(l => l.Cycle).Include(k => k.Budgets).ThenInclude(l => l.Period).SingleOrDefault(l => l.Id == model.Dept);
                        if (dept == null)
                        {
                            ModelState.AddModelError(string.Empty, "Please specify a department for this payment voucher.");
                        }
                        //Budget validations
                        if (acc.HasBudget)
                        {
                            FinanceBudget budget = await Utils.GetCurrentDeptBudget(dept, acc);

                            //
                            if (budget == null)
                            {
                                ModelState.AddModelError("", "GL Account '" + acc.Name + "' requires a budgeting, but a budget allocation was found in department '" + dept.Name + "'.");
                                valid = false;
                            }

                            //Used funds
                            if (budget.Amount - await Utils.GetSpentBudgetAmount(budget) <= 0)
                            {
                                ModelState.AddModelError("", "GL Account '" + acc.Name + "' budget has been exhausted in department '" + dept.Name + "'. Please request for more funds and try again..");
                                valid = false;
                            }

                        }
                        //
                        if (!valid)
                        {
                            return this.View(FinanceControllerAction.CreateRegularPVoucher, model);
                        }

                        //Continue
                        var payment = new FPayment()
                        {
                            AccountId = acc.Id,
                            DeptId = dept.Id,
                            GrossAmount = model.GrossAmount,
                            //
                            PayableTo = model.PayableTo,
                            PayeeName = model.PayeeName
                        };
                        //
                        var tamt = 0.0;

                        //Continue
                        pv.MgmtAmount = model.MgmtAmount;
                        //
                        if (model.Sponsor > 0)
                        {
                            var sponsor = db.FSponsors.Single(p => p.Id == model.Sponsor);
                            //
                            //pv.SponsorId = sponsor.Id;
                        }
                        //Tax
                        foreach (var t in taxes)
                        {
                            var tax = db.FTaxes.Include(l => l.GLAccount).Single(k => k.Id == t);
                            payment.Taxes.Add(tax);
                            //
                            tamt = (model.GrossAmount - model.Discount) * tax.Rate / 100;
                            //
                            var tn = new GLTransaction()
                            {
                                AccountId = tax.GLAccount.Id,
                                Amount = tamt,
                                Kind = GLTransactionKind.Tax,
                                Type = GLTransactionType.Debit
                            };
                            //
                            payment.GLTransactions.Add(tn);
                        }
                        //
                        var trans = new GLTransaction()
                        {
                            AccountId = ap.Id,
                            Type = GLTransactionType.Debit,
                            Kind = GLTransactionKind.Regular,
                            Amount = model.GrossAmount - (model.Discount + tamt)
                        };
                        //
                        payment.GLTransactions.Add(trans);
                        //
                        var cur = db.FCurrencies.Single(l => l.IsBase);
                        //
                        payment.CurrencyId = cur.Id;
                        //
                        pv.Payments.Add(payment);
                        //
                        pv.MgmtAmount = model.MgmtAmount;
                        //
                    }
                    //
                    db.FPaymentVouchers.Add(pv);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllRegularPVouchers, new { msg = "Payment Voucher created successfully" });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(String.Empty, "Add payment voucher failed.");
                }
            }
            //
            return this.View(FinanceControllerAction.CreateRegularPVoucher, model);
        }


        #endregion

        #region Tax Payment Vouchers
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("alltvouchers", Name = FinanceControllerRoute.GetAllTaxPVouchers)]
        public IActionResult AllTaxPVouchers()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllTaxPVouchers);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addtvoucher", Name = FinanceControllerRoute.GetAddTaxPVoucher)]
        public IActionResult AddTaxPaymentVoucher(int iid, string invs)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (!db.FInvoices.Any())
            {
                return RedirectToRoute(FinanceControllerRoute.GetAddInvoice, new { msg = "You need to create an invoice before creating a payment voucher" });
            }
            var model = new AddPaymentVoucherViewModel()
            {
                VoucherNo = Utils.GenerateVoucherNo(db).ToString()
            };
            //
            try
            {
                //
                if (iid > 0)
                {
                    model.SelectedInvoices.Add(db.FInvoices.SingleOrDefault(l => l.Id == iid));
                    model.InvoiceIds = JsonConvert.SerializeObject(new int[] { iid });
                }
                else if (invs != null)
                {
                    var list = JsonConvert.DeserializeObject<int[]>(invs);
                    //
                    foreach (var e in list)
                    {
                        model.SelectedInvoices.Add(db.FInvoices.SingleOrDefault(l => l.Id == e));
                    }
                    //
                    model.InvoiceIds = JsonConvert.SerializeObject(list);
                }
            }
            catch
            {
                return new NotFoundResult();
            }
            //
            return this.View(FinanceControllerAction.CreateTaxPVoucher, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addtvoucher", Name = FinanceControllerRoute.GetAddTaxPVoucher)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddTaxPaymentVoucher(AddPaymentVoucherViewModel model)
        {
            if (ModelState.IsValid)
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                model.VoucherNo = Utils.GenerateVoucherNo(db).ToString();
                //
                if (!string.IsNullOrEmpty(model.InvoiceIds))
                {
                    var list = JsonConvert.DeserializeObject<int[]>(model.InvoiceIds);
                    foreach (var e in list)
                    {
                        model.SelectedInvoices.Add(db.FInvoices.SingleOrDefault(l => l.Id == e));
                    }
                }
                //
                var pv = new FPaymentVoucher()
                {
                    CheckNo = model.CheckNo,
                    VoucherDate = DateTime.Now,
                    Description = model.Description,
                    Type = PVType.Tax,
                    Status = PVStatus.Pending
                };
                //Ledger
                var ledger = db.FLedgers.Single(p => p.Id == model.Ledger);
                pv.LedgerId = ledger.Id;
                //Project
                if (model.Project > 0)
                {
                    //var rpoj = db.FProjects.Single(p => p.Id == model.Sponsor);
                    ////
                    //pv.SponsorId = sponsor.Id;
                }

                //
                var sup = db.FSuppliers.Single(p => p.Id == model.Supplier);
                pv.SupplierId = sup.Id;
                //
                var personnel = db.Users.Include(j => j.StaffProfile).Single(l => l.UserName == User.Identity.Name).StaffProfile;
                //Pesonel
                pv.PersonnelId = personnel.Id;
                //
                pv.VoucherNo = Utils.GenerateVoucherNo(db).ToString();

                //The Tax
                var tax = db.FTaxes.Include(k => k.GLAccount).Single(p => p.Id == model.Tax);
                //
                try
                {
                    //Selected Payments
                    if (model.Invoices != null && model.Invoices.Length > 0)
                    {
                        //Continue
                        foreach (var i in model.Invoices)
                        {
                            //var payment = db.FWallet.Include(l=>l.Taxes).Include(l=>l.GLTransactions).SingleOrDefault(l => l.Id == i && l.Taxes.Any(s=>s.Id == tax.Id));
                            var payment = db.FWallet.Include(l => l.Taxes).Include(l => l.Voucher).SingleOrDefault(l => l.Id == i && l.Taxes.Any(s => s.Id == tax.Id));
                            ////Tax
                            //if (payment!=null && payment.Taxes.Any(l=>l.Id == tax.Id))
                            //{
                            //    if(payment.GLTransactions.Any(o=>o.Type == GLTransactionType.Debit && o.Kind == GLTransactionKind.Tax && o.AccountId == tax.AccountId))
                            //    {
                            //        //Already exists
                            //        ModelState.AddModelError("", "Please specified payment was already included in another voucher..");
                            //        return this.View(FinanceControllerAction.CreateTaxPVoucher, model);
                            //    }
                            //    //Add
                            //    pv.Payments.Add(payment);
                            //}
                            if (payment == null)
                            {
                                ModelState.AddModelError("", "Please specified payment does not have the selected tax type..");
                                return this.View(FinanceControllerAction.CreateTaxPVoucher, model);
                            }
                            else if (payment.Voucher != null && payment.Voucher.Status != PVStatus.Cancelled || payment.GLTransactions.Any(l => l.Kind == GLTransactionKind.Tax && l.Type == GLTransactionType.Debit))
                            {
                                //Already exists
                                ModelState.AddModelError("", "Please specified payment was already included in another voucher..");
                                return this.View(FinanceControllerAction.CreateTaxPVoucher, model);
                            }
                            //

                            //Add
                            pv.Payments.Add(payment);
                        }
                    }
                    else
                    {

                        //    //Unregistered details
                        //    var valid = true;
                        //    //Gl Acc
                        //    var acc = db.FinanceAccounts.SingleOrDefault(l => l.Id == model.GLAccount);
                        //    if (acc == null)
                        //    {
                        //        ModelState.AddModelError(string.Empty, "Please specify some invoice or a GL Account for the payment voucher.");
                        //    }
                        //    //Gl Acc
                        //    var ap = db.FinanceAccounts.SingleOrDefault(l => l.Id == model.APAccount);
                        //    if (acc == null)
                        //    {
                        //        ModelState.AddModelError(string.Empty, "Please specify some invoice or a A/P (Creditors) Account for the payment voucher.");
                        //    }
                        //    var dept = db.HrDepartments.Include(k => k.Budgets).ThenInclude(l => l.Cycle).Include(k => k.Budgets).ThenInclude(l => l.Period).SingleOrDefault(l => l.Id == model.Dept);
                        //    if (dept == null)
                        //    {
                        //        ModelState.AddModelError(string.Empty, "Please specify a department for this payment voucher.");
                        //    }
                        //    //Budget validations
                        //    if (acc.HasBudget)
                        //    {
                        //        FinanceBudget budget = await Utils.GetCurrentDeptBudget(dept, acc);

                        //        //
                        //        if (budget == null)
                        //        {
                        //            ModelState.AddModelError("", "GL Account '" + acc.Name + "' requires a budgeting, but a budget allocation was found in department '"+dept.Name+"'.");
                        //            valid = false;
                        //        }

                        //        //Used funds
                        //        if (budget.Amount - await Utils.GetSpentBudgetAmount(budget) <= 0)
                        //        {
                        //            ModelState.AddModelError("", "GL Account '" + acc.Name + "' budget has been exhausted in department '"+dept.Name+"'. Please request for more funds and try again..");
                        //            valid = false;
                        //        }

                        //    }
                        //    //
                        //    if (!valid)
                        //    {
                        //        return this.View(FinanceControllerAction.CreateRegularPVoucher, model);
                        //    }

                        //    //Continue
                        //    var payment = new FPayment()
                        //    {
                        //        AccountId = acc.Id,
                        //        DeptId = dept.Id,
                        //        GrossAmount = model.GrossAmount,
                        //        //
                        //        PayableTo = model.PayableTo,
                        //        PayeeName = model.PayeeName
                        //    };
                        //    //
                        //    var tamt = 0.0;

                        //    //Continue
                        //    pv.MgmtAmount = model.MgmtAmount;
                        //    //
                        //    if (model.Sponsor > 0)
                        //    {
                        //        var sponsor = db.FSponsors.Single(p => p.Id == model.Sponsor);
                        //        //
                        //        //pv.SponsorId = sponsor.Id;
                        //    }
                        //    //Tax
                        //    //foreach (var t in taxes)
                        //    //{
                        //    //    var tax = db.FTaxes.Include(l => l.GLAccount).Single(k => k.Id == t);
                        //    //    payment.Taxes.Add(tax);
                        //    //    //
                        //    //    tamt = (model.GrossAmount - model.Discount) * tax.Rate / 100;
                        //    //    //
                        //    //    var tn = new GLTransaction()
                        //    //    {
                        //    //        AccountId = tax.GLAccount.Id,
                        //    //        Amount = tamt,
                        //    //        Kind = GLTransactionKind.Tax,
                        //    //        Type = GLTransactionType.Debit
                        //    //    };
                        //    //    //
                        //    //    payment.GLTransactions.Add(tn);
                        //    //}
                        //    //
                        //    var trans = new GLTransaction()
                        //    {
                        //        AccountId = ap.Id,
                        //        Type = GLTransactionType.Debit,
                        //        Kind = GLTransactionKind.Regular,
                        //        Amount = model.GrossAmount - (model.Discount + tamt)
                        //    };
                        //    //
                        //    payment.GLTransactions.Add(trans);
                        //    //
                        //    var cur = db.FCurrencies.Single(l => l.IsBase);
                        //    //
                        //    payment.CurrencyId = cur.Id;
                        //    //
                        //    pv.Payments.Add(payment);
                        //    //
                        //    pv.MgmtAmount = model.MgmtAmount;
                        //    //
                    }
                    //
                    if (pv.Payments.Count == 0)
                    {
                        ModelState.AddModelError("", "Please specify at least one invoice and try again..");
                        return this.View(FinanceControllerAction.CreateTaxPVoucher, model);
                    }
                    //
                    pv.TaxId = tax.Id;
                    //
                    db.FPaymentVouchers.Add(pv);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllTaxPVouchers, new { msg = "Tax Payment Voucher created successfully" });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(String.Empty, "Add tax payment voucher failed.");
                }
            }
            //
            return this.View(FinanceControllerAction.CreateTaxPVoucher, model);
        }

        #endregion

        #region GL Post Regular Payment Voucher

        #region Payment Vouchers
        /// <summary>
        /// @BOURNE K
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpostedpvoucher", Name = FinanceControllerRoute.GetAllPostedRegularPVs)]
        public IActionResult AllPostedRegularPVs(int typ = 0)
        {
            //
            switch (typ)
            {
                case (int)PVType.ExpenseClaim:
                    //
                    ViewBag.Type = PVType.ExpenseClaim;
                    break;
                case (int)PVType.Imprest:
                    //
                    ViewBag.Type = PVType.Imprest;
                    break;
                default:
                    //
                    ViewBag.Type = PVType.Regular;
                    break;

            }
            //
            return this.View(FinanceControllerAction.AllPostedRegularPVs);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("postpvoucher", Name = FinanceControllerRoute.GetPostPaymentVoucher)]
        public IActionResult PostPaymentVoucher(int pid, string returnurl)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var voucher = db.FPaymentVouchers.Include(l => l.Personnel)
                .Include(k => k.Ledger).ThenInclude(l => l.PaymentAccounts).Include(k => k.Ledger).ThenInclude(l => l.PVoucherPrefixes)
                .Include(k => k.Supplier).Include(l => l.Payments).ThenInclude(l => l.CurrencyUsed).Include(l => l.Payments).ThenInclude(l => l.Invoice)
                .Include(l => l.Payments).ThenInclude(l => l.ExpenseClaim).ThenInclude(l => l.Staff)
                .Include(l => l.Payments).ThenInclude(l => l.ImprestWarrant).ThenInclude(l => l.Staff)
                .Include(k => k.Payments).ThenInclude(k => k.Taxes).SingleOrDefault(l => l.Id == pid);
            //
            if (voucher == null)
            {
                if (returnurl != null)
                    return Redirect(returnurl);
                else
                    return RedirectToRoute(FinanceControllerRoute.GetAllRegularPVouchers, new { msg = "Please specify the voucher you wish to post." });
            }
            else if (voucher.PaymentAccount != null)
            {
                return RedirectToRoute(FinanceControllerRoute.GetAllRegularPVouchers, new { msg = "Payment voucher specified was already posted." });
            }
            //continue
            var model = new PostPaymentVoucherViewModel()
            {
                VoucherId = voucher.Id,
                Voucher = voucher
            };
            //
            var total = 0.0; var ttax = 0.0;
            foreach (var p in voucher.Payments)
            {
                if (p.Taxes.Count > 0)
                {
                    foreach (var t in p.Taxes)
                    {
                        var n = p.GrossAmount - p.Discount; //less discount
                        //
                        var tax = Utils.ApplyTax(n, t);
                        ttax += tax;
                        total += (n - tax);//Less tax
                    }
                }
                else
                {
                    total += p.GrossAmount;
                }
            }
            //
            model.NetAmount = total;
            model.NetTax = ttax;
            //
            return this.View(FinanceControllerAction.PostRegularPV, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("postpvoucher", Name = FinanceControllerRoute.GetPostPaymentVoucher)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PostPaymentVoucher(PostPaymentVoucherViewModel model)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (ModelState.IsValid)
            {
                var voucher = db.FPaymentVouchers.Include(l => l.Payments).ThenInclude(l => l.GLAccount).Include(l => l.Payments).ThenInclude(l => l.Taxes)
                    .Include(l => l.Payments).ThenInclude(l => l.GLTransactions).ThenInclude(l => l.Account).Include(l => l.Supplier).Include(l => l.Ledger).ThenInclude(l => l.PVoucherPrefixes).Include(l => l.Personnel).Single(p => p.Id == model.VoucherId);
                //
                model.Voucher = voucher;
                //
                try
                {
                    var pmode = db.FLedgers.Include(l => l.PaymentAccounts).Single(l => l.Id == voucher.LedgerId).PaymentAccounts.SingleOrDefault(l => l.Id == model.BankMode);
                    //
                    if (pmode == null)
                    {
                        ModelState.AddModelError("", "Invalid payment mode selected. Try again..");
                        return this.View(FinanceControllerAction.PostRegularPV, model);
                    }

                    await _service.PostPaymentVoucher(voucher, pmode, model.CheckNo, User, ModelState);
                    //
                    if (ModelState.ErrorCount > 0)
                    {
                        return this.View(FinanceControllerAction.PostRegularPV, model);
                    }
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllPostedRegularPVs, new { msg = "Payment voucher was posted successfully.", typ = (int)voucher.Type });
                }
                catch
                {
                    ModelState.AddModelError("", "Payment voucher could not be posted. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.PostRegularPV, model);
        }
        #endregion

        #region TAX PVs
        /// <summary>
        /// @BOURNE K
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpostedtvoucher", Name = FinanceControllerRoute.GetAllPostedTaxPVs)]
        public IActionResult AllPostedTaxVoucher()
        {
            ////
            //ViewData["Mode"] = 1;
            ViewBag.Type = PVType.Tax;
            //
            return this.View(FinanceControllerAction.AllPostedTaxPVs);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("posttvoucher", Name = FinanceControllerRoute.GetPostTaxPV)]
        public IActionResult PostTaxVoucher(int pid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            var voucher = db.FPaymentVouchers.Include(l => l.TaxPaid).Include(l => l.Personnel).Include(k => k.Ledger).ThenInclude(l => l.PaymentAccounts).Include(k => k.Ledger).ThenInclude(l => l.PVoucherPrefixes).Include(k => k.Supplier).Include(l => l.Payments).ThenInclude(k => k.Taxes).SingleOrDefault(l => l.Id == pid);
            if (voucher == null)
            {
                return RedirectToRoute(FinanceControllerRoute.GetAllTaxPVouchers, new { msg = "Please identify a voucher and click on 'Post Voucher' button" });
            }
            else if (voucher.PaymentAccount != null)
            {
                return RedirectToRoute(FinanceControllerRoute.GetAllRegularPVouchers, new { msg = "Tax Payment voucher specified was already posted." });
            }
            //continue
            var model = new PostPaymentVoucherViewModel()
            {
                VoucherId = voucher.Id,
                Voucher = voucher
            };
            //
            var total = 0.0; var tax = 0.0;
            foreach (var p in voucher.Payments)
            {
                if (p.Taxes.Count > 0)
                {
                    foreach (var t in p.Taxes)
                    {
                        var d = Utils.ApplyTax(p.GrossAmount, t);
                        tax += (p.GrossAmount - d);
                        total += d;
                    }
                }
                else
                {
                    total += p.GrossAmount;
                }
            }
            //
            model.NetAmount = total;
            model.NetTax = tax;
            //
            return this.View(FinanceControllerAction.PostTaxPV, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("posttvoucher", Name = FinanceControllerRoute.GetPostTaxPV)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PostTaxVoucher(PostPaymentVoucherViewModel model)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (ModelState.IsValid)
            {
                var voucher = db.FPaymentVouchers.Include(l => l.Payments).ThenInclude(l => l.GLAccount).Include(l => l.Payments).ThenInclude(l => l.Taxes)
                    .Include(l => l.Payments).ThenInclude(l => l.GLTransactions).ThenInclude(l => l.Account).Include(l => l.Supplier).Include(l => l.Ledger).ThenInclude(l => l.PVoucherPrefixes).Include(l => l.Personnel).Single(p => p.Id == model.VoucherId);
                //
                model.Voucher = voucher;
                //
                try
                {
                    var pmode = db.FLedgers.Include(l => l.PaymentAccounts).Single(l => l.Id == voucher.LedgerId).PaymentAccounts.SingleOrDefault(l => l.Id == model.BankMode);
                    //
                    if (pmode == null)
                    {
                        ModelState.AddModelError("", "Invalid payment mode selected. Try again..");
                        return this.View(FinanceControllerAction.PostRegularPV, model);
                    }
                    //
                    voucher.ModeId = pmode.Id;
                    //
                    if (voucher.CheckNo != model.CheckNo)
                        voucher.CheckNo = model.CheckNo;
                    //
                    var valid = true;
                    //
                    var total = 0.0;
                    for (var j = 0; j < voucher.Payments.Count; j++)
                    {
                        var payment = voucher.Payments.ElementAt(j);
                        if (payment.GLTransactions.Count != 0)
                        {

                            //Get First Entry transaction
                            var tn = payment.GLTransactions.Single(l => l.Kind == GLTransactionKind.Tax && l.Type == GLTransactionType.Debit);

                            //Check if a 2nd entry has been made
                            if (!payment.GLTransactions.Any(k => k.Kind == GLTransactionKind.Tax && k.Type == GLTransactionType.Credit && k.AccountId == tn.AccountId))
                            {
                                //No Double Entry found

                                //
                                var pamt = await Utils.ComputeAmountPayable(payment);

                                //
                                total += pamt;
                                //var t1 = p.GLTransactions.Where(l=>l.AccountAId == p.AccountId &&);
                                //
                                var trans = new GLTransaction()
                                {
                                    AccountId = pmode.AccountId,//Double Entry
                                    Kind = GLTransactionKind.Tax,
                                    Type = GLTransactionType.Credit //We are reducing
                                };
                                //
                                trans.Amount = pamt;
                                trans.PaymentId = payment.Id;
                                //
                                payment.GLTransactions.Add(trans);
                            }
                            else
                            {
                                logger.LogWarning(2, "An attempt to do a 'Double Posting' for tax payment id = " + payment.Id);
                                ModelState.AddModelError("", "Tax Voucher Payment was processed with errors. Some payments seem to have been posted already..");
                                valid = false;
                            }
                        }
                        else
                        {
                            //No debit transaction
                            ModelState.AddModelError("", "No Debit GL entry found for some payments in the voucher.");
                            valid = false;
                        }

                    }
                    //
                    if (!valid)
                    {
                        return this.View(FinanceControllerAction.PostTaxPV, model);
                    }
                    //Save GL Transactions
                    //db.SaveChanges();
                    //
                    voucher.Status = PVStatus.Posted;
                    //
                    voucher.PayDate = DateTime.Now;
                    //
                    var pp = db.HrStaff.Include(l => l.ApplicationUser).Single(l => l.ApplicationUser.UserName == User.Identity.Name);
                    //
                    voucher.PostingPersonnelId = pp.Id;
                    //
                    await db.SaveChangesAsync();

                    //update payment account used
                    foreach (var p in voucher.Payments)
                    {
                        var payment = db.FWallet.Include(l => l.PaymentAccounts).Single(l => l.Id == p.Id);
                        if (!payment.PaymentAccounts.Any(i => i.Id == pmode.Id))
                        {
                            //
                            payment.PaymentAccounts.Add(pmode);
                            //
                            await db.SaveChangesAsync();
                        }
                    }
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllPostedTaxPVs, new { msg = "Tax Payment voucher was posted successfully." });
                }
                catch
                {
                    ModelState.AddModelError("", "Payment voucher could not be posted. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.PostTaxPV, model);
        }
        #endregion

        #endregion

        #region Post All Payments
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpPost("postallpys", Name = FinanceControllerRoute.GetPostAllPayments)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PostAllPayments(PostAllPaymentsViewModel model)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            if (ModelState.IsValid)
            {
                if ((model.ecs == null || model.ecs.Length == 0 && model.iws == null || model.iws.Length == 0 && model.pcs == null || model.pcs.Length == 0 && model.pvs == null || model.pvs.Length == 0) && !model.PostAll)
                {
                    //
                    ModelState.AddModelError("", "Please specify the payment(s) you wish to post and try again..");
                    return this.View(FinanceControllerAction.UnpostedPayments, model);
                }
                //
                var valid = true;
                var count = 0;
                if (!model.PostAll)
                {
                    //we have a expense claim
                    if (model.ecs.Length > 0)
                    {
                        var list = new List<ExpenseClaim>();
                        //
                        foreach (var id in model.ecs)
                        {
                            var ec = db.FExpenseClaims.SingleOrDefault(j => j.Id == id);
                            if (ec != null)
                                list.Add(ec);
                        }
                        //
                        if (list.Count > 0)
                        {
                            var ents = new List<JsonPaymentEntry>();
                            list.ForEach(h => count += Task.Run(() => _service.DisburseExpenseClaims(h, ents, User, ModelState)).Result);
                        }
                    }
                    //we have a imprest warrant
                    if (model.iws.Length > 0)
                    {
                        //
                        var list = new List<ImprestWarrant>();
                        //
                        foreach (var id in model.iws)
                        {
                            var iw = db.FImprestWarrants.SingleOrDefault(j => j.Id == id);
                            if (iw != null)
                                list.Add(iw);
                        }
                        //
                        if (list.Count > 0)
                        {
                            count += await _service.DisburseImprestWarrants(list, "Auto-Posted on " + DateTime.Now.ToString("dd-MMM-yyyy"), User, ModelState);
                        }
                    }
                    //We have a petty-cash
                    if (model.pcs.Length > 0)
                    {
                        //
                        var list = new List<PettyCashReq>();
                        //
                        foreach (var id in model.pcs)
                        {
                            var pc = db.FPettyCashReqs.SingleOrDefault(j => j.Id == id);
                            if (pc != null)
                                list.Add(pc);
                        }
                        //
                        if (list.Count > 0)
                        {
                            //Ledger
                            var ledger = db.FLedgers.Include(l => l.PaymentAccounts).FirstOrDefault(l => l.PaymentAccounts.Any(j => j.Mode == PayMode.Cheque || j.EFTEnabled));
                            if (ledger == null) {
                                ModelState.AddModelError("", "Please create a payment-mode with a 'CHEQUE' paymode option and try again...");
                                //
                                valid = false;
                            }
                            //mode
                            var mode = ledger.PaymentAccounts.First(l => l.Mode == PayMode.Cheque);
                            //project
                            FProject project = null;
                            //
                            var personnel = db.HrStaff.Include(l => l.ApplicationUser).Include(k => k.Address).SingleOrDefault(l => l.ApplicationUser.UserName == User.Identity.Name);
                            //cashier
                            var cashier = db.FCashierOffices.First(l => l.LocationId == personnel.LocationId);
                            //
                            count += await _service.DisbursePettyCahsReqs(list, ledger, project, cashier, mode, "CASH", "Auto-potsed on " + DateTime.Now.ToString("dd-MMM-yyyy"), User, ModelState);
                        }
                    }
                    //We have a voucher
                    if (model.pvs.Length > 0)
                    {
                        //
                        var list = new List<FPaymentVoucher>();
                        //
                        foreach (var id in model.pvs)
                        {
                            var pv = db.FPaymentVouchers.SingleOrDefault(j => j.Id == id);
                            if (pv != null)
                                list.Add(pv);
                        }
                        //
                        if (list.Count > 0)
                        {
                            var pmode = list.FirstOrDefault().Ledger.PaymentAccounts.First(l => l.Mode == PayMode.Cheque && l.Status == EntityStatus.Active);
                            if (pmode != null)
                                count += await _service.PostPaymentVouchers(list, pmode, null, User, ModelState);
                            else
                                ModelState.AddModelError("", "No \"Cheque\" payment mode found for Ledger \"" + list.FirstOrDefault().Ledger.Name + "\"");
                        }
                    }

                }
                else
                {
                    //Post All

                    //Expense Claims
                    var ecs = db.FExpenseClaims.Include(l => l.Disbursement).Include(l => l.Payments).ThenInclude(l => l.Voucher).Where(l => l.Disbursement == null || !l.Payments.Any(p => p.Voucher != null)).ToList();
                    if (ecs.Count > 0) {
                        var ents = new List<JsonPaymentEntry>();
                        ecs.ForEach(h => count += Task.Run(() => _service.DisburseExpenseClaims(h, ents, User, ModelState)).Result);
                    }
                    //Imprest Warrants
                    var iws = db.FImprestWarrants.Include(l => l.Disbursement).Include(l => l.Payments).ThenInclude(l => l.Voucher).Where(l => l.Disbursement == null || !l.Payments.Any(p => p.Voucher != null)).ToList();
                    if (iws.Count > 0)
                    {
                        count += await _service.DisburseImprestWarrants(iws, "Auto-Posted on " + DateTime.Now.ToString("dd-MMM-yyyy"), User, ModelState);
                    }
                    //petty-cash
                    var pcs = db.FPettyCashReqs.Include(j => j.Disbursement).Include(l => l.Payments).Where(l => l.Disbursement == null).ToList();
                    if (pcs.Count > 0)
                    {
                        //count += await _service.DisbursePettyCahsReqs(pcs, ModelState);
                        //Ledger
                        var ledger = db.FLedgers.Include(l => l.PaymentAccounts).FirstOrDefault(l => l.PaymentAccounts.Any(j => j.Mode == PayMode.Cheque || j.EFTEnabled));
                        if (ledger == null)
                        {
                            ModelState.AddModelError("", "Please create a payment-mode with a 'CHEQUE' paymode option and try again...");
                            //
                            valid = false;
                        }
                        //mode
                        var mode = ledger.PaymentAccounts.First(l => l.Mode == PayMode.Cheque);
                        //project
                        FProject project = null;
                        //
                        var personnel = db.HrStaff.Include(l => l.ApplicationUser).Include(k => k.Address).SingleOrDefault(l => l.ApplicationUser.UserName == User.Identity.Name);
                        //cashier
                        var cashier = db.FCashierOffices.First(l => l.LocationId == personnel.LocationId);
                        //
                        count += await _service.DisbursePettyCahsReqs(pcs, ledger, project, cashier, mode, "CASH", "Auto-potsed on " + DateTime.Now.ToString("dd-MMM-yyyy"), User, ModelState);
                    }
                    //vouchers
                    var pvs = db.FPaymentVouchers.Where(p => p.Status == PVStatus.Pending && p.PayDate == null).ToList();
                    if (pvs.Count > 0)
                    {
                        var pmode = pvs.FirstOrDefault().Ledger.PaymentAccounts.First(l => l.Mode == PayMode.Cheque && l.Status == EntityStatus.Active);
                        if (pmode != null)
                            count += await _service.PostPaymentVouchers(pvs, pmode, null, User, ModelState);
                        else
                            ModelState.AddModelError("", "No \"Cheque\" payment mode found for Ledger \"" + pvs.FirstOrDefault().Ledger.Name + "\"");

                    }
                }
                //
                if (ModelState.ErrorCount > 0)
                {
                    return this.View(FinanceControllerAction.UnpostedPayments, model);
                }
                //
                if (count == 0)
                {
                    ModelState.AddModelError("", "No unposted payments where found..");
                }
                else
                {
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetPaymentsRegistry, new { msg = count + " payments have been posted." });
                }
            }
            //
            return this.View(FinanceControllerAction.UnpostedPayments, model);
        }
        #endregion

        #region Imprest Warrant
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allmprwar", Name = FinanceControllerRoute.GetAllImprestWarrants)]
        public IActionResult AllImprestWarrant()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllImprestWarrants);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addmprwar", Name = FinanceControllerRoute.GetAddImprestWarrant)]
        public async Task<IActionResult> AddImprestWarrant()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            var model = new AddImprestWarrantViewModel()
            {
                Ref = (await Utils.GenerateWarrantRef(db)).ToString("0000")
            };
            //
            return this.View(FinanceControllerAction.AddImprestWarrant, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addmprwar", Name = FinanceControllerRoute.GetAddImprestWarrant)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddImprestWarrant(AddImprestWarrantViewModel model, params int[] taxes)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (ModelState.IsValid)
            {
                //
                try
                {
                    //
                    var warrant = new ImprestWarrant()
                    {
                        CreateDate = DateTime.Now,
                        Description = model.Description,
                        EstimatedDaysAway = model.EstimatedDaysAway,
                        NatureOfDuty = model.NatureOfDuty,
                        OrderNo = model.OrderNo,
                        ProposedItinerary = model.ProposedItinerary,
                        SurrenderDate = model.SurrenderDate
                    };
                    warrant.Ref = await Utils.GenerateWarrantRef(db);

                    //
                    var lgr = db.FLedgers.Single(l => l.Id == model.Ledger);
                    warrant.LedgerId = lgr.Id;
                    //
                    var staff = db.HrStaff.Single(l => l.Id == model.Staff);
                    warrant.StaffId = staff.Id;
                    //
                    var personnel = db.Users.Include(k => k.StaffProfile).Single(l => l.UserName == User.Identity.Name);
                    warrant.PersonnelId = personnel.Id;
                    //
                    if (model.Project > 0) {
                        var proj = db.FProjects.Single(k => k.Id == model.Project);
                        warrant.ProjectId = proj.Id;
                    }
                    //
                    try
                    {
                        var payables = JsonConvert.DeserializeObject<List<JsonAccountPayable>>(model.Payables);
                        //
                        //foreach(var p in payables)
                        //{
                        //    var acc = db.FinanceAccounts.Single(l => l.Id == p.AccId);
                        //    //
                        //    var payment = new FPayment()
                        //    {
                        //        AccountId = acc.Id,
                        //        GrossAmount = p.GrossAmount
                        //    };
                        //    //
                        //    if(p.Taxes!= null && p.Taxes.Length > 0)
                        //    {
                        //        foreach (var t in p.Taxes)
                        //        {
                        //            var tax = db.FTaxes.SingleOrDefault(l => l.Id == t);
                        //            if (tax != null)
                        //            {
                        //                payment.Taxes.Add(tax);
                        //            }
                        //        }
                        //    }
                        //    //
                        //    var dept = db.HrDepartments.Single(l => l.Id == p.Dept);
                        //    payment.DeptId = dept.Id;
                        //    //
                        //    var cur = db.FCurrencies.Single(l=>l.IsBase);
                        //    //
                        //    payment.CurrencyId = cur.Id;
                        //    //
                        //    warrant.Payments.Add(payment);
                        //}
                        //var payables = JsonConvert.DeserializeObject<List<JsonAccountPayable>>(model.Payables);
                        //
                        var list = await _service.GetAccountsPayable(payables, PayablesType.Unspecified, ModelState);
                        //
                        if (ModelState.ErrorCount > 0)
                        {
                            return this.View(FinanceControllerAction.AddImprestWarrant, model);
                        }
                        else
                        {
                            list.ForEach(o => warrant.Payments.Add(o));
                        }
                        //

                    }
                    catch {
                        ModelState.AddModelError("", "Imprest warrant Account Payables could not be parsed. Please try again");
                        return this.View(FinanceControllerAction.AddImprestWarrant, model);
                    }
                    //

                    db.FImprestWarrants.Add(warrant);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllImprestWarrants, new { msg = "Imprest Warrant created succesifully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Imprest Warrant could NOT be created. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.AddImprestWarrant, model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        [HttpPost("dropmprwar", Name = FinanceControllerRoute.DropImprestWarrant)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DropImprestWarrant(int wid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (wid > 0)
            {
                var warrant = db.FImprestWarrants.Include(l => l.Payments).Single(l => l.Id == wid);
                //
                warrant.Status = EntityStatus.Deleted;

                //Payments
                foreach (var pay in warrant.Payments)
                {
                    pay.Status = EntityStatus.Deleted;
                }

                //
                await db.SaveChangesAsync();

                return new JsonResult(new { s = 1, msg = "Imprest Warrant Deleted Successifully.." });
            }
            //
            return new JsonResult(new { s = 0, err = "Imprest Warrant  Not found." });
        }

        #endregion

        #region Imprest Disbursement
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allmprdis", Name = FinanceControllerRoute.GetAllImprestDisbursements)]
        public IActionResult AllImprestDisbursement()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllImprestDisbursements);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addmprdis", Name = FinanceControllerRoute.GetAddImprestDisbursement)]
        public async Task<IActionResult> AddImprestDisbursement(int? iid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            var model = new AddImprestDisbursementViewModel();
            model.Imprest = db.FImprestWarrants.Include(k => k.Staff).Include(k => k.Payments).Include(l => l.Ledger).Include(l => l.Project).SingleOrDefault(l => l.Id == iid);

            model.Ref = await Utils.GenerateDisbursementRef(db);
            //
            return this.View(FinanceControllerAction.AddImprestDisbursement, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addmprdis", Name = FinanceControllerRoute.GetAddImprestDisbursement)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddImprestDisbursement(AddImprestDisbursementViewModel model, params int[] taxes)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (ModelState.IsValid)
            {
                //
                var warrant = db.FImprestWarrants.Single(l => l.Id == model.WarrantId);
                //
                try
                {
                    await _service.DisburseImprestWarrants(new List<ImprestWarrant> { warrant }, model.Description, User, ModelState);
                    //
                    if (ModelState.ErrorCount == 0)
                        return RedirectToRoute(FinanceControllerRoute.GetAllImprestDisbursements, new { msg = "Imprest Disbursement created succesifully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Imprest Disbursement could NOT be created. Please try again..");
                }
            }
            //
            model.Ref = await Utils.GenerateDisbursementRef(db);
            //
            return this.View(FinanceControllerAction.AddImprestDisbursement, model);
        }
        #endregion

        #region Imprest Surrender
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allmprsur", Name = FinanceControllerRoute.GetAllImprestSurrenders)]
        public IActionResult AllImprestSurrender()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllImprestSurrenders);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addmprsur", Name = FinanceControllerRoute.GetAddImprestSurrender)]
        public IActionResult AddImprestSurrender(int did)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var dis = db.FImprestDisbursements.Include(l => l.Surrender).Include(l => l.Warrant).ThenInclude(l => l.Ledger).ThenInclude(l => l.PaymentAccounts)
                .Include(l => l.Warrant).ThenInclude(l => l.Ledger)
                .Include(l => l.Warrant).ThenInclude(l => l.Staff)
                .Include(l => l.Warrant).ThenInclude(l => l.Project)
                .Include(l => l.Warrant).ThenInclude(l => l.Payments).SingleOrDefault(l => l.Id == did);
            //
            var model = new AddImprestSurrenderViewModel();
            //
            if (dis != null && dis.Surrender != null)
                ModelState.AddModelError("", "Imprest Warrant IW\"" + dis.Ref.ToString("0000") + "\" has already been disbursed.");
            else
                model.Disbursement = dis;
            //
            return this.View(FinanceControllerAction.AddImprestSurrender, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addmprsur", Name = FinanceControllerRoute.GetAddImprestSurrender)]
        [ValidateAntiForgeryToken]
        public IActionResult AddImprestSurrender(AddImprestSurrenderViewModel model, params int[] taxes)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (ModelState.IsValid)
            {
                //
                try
                {
                    var dis = db.FImprestDisbursements.Include(l => l.Surrender).Include(j => j.Warrant).ThenInclude(l => l.Staff)
                        .Include(l => l.Warrant).ThenInclude(l => l.Payments).Include(l => l.Warrant).ThenInclude(l => l.Ledger).ThenInclude(l => l.PaymentAccounts)
                        .Single(l => l.Id == model.IWDisbursementId);
                    //
                    if (dis.Surrender != null)
                    {
                        ModelState.AddModelError("", "This Imprest Warrant has already been disbursed.");
                    }
                    //
                    model.Disbursement = dis;
                    //
                    var sur = new ImprestSurrender()
                    {
                        ModeAccNumber = model.ModeAccNumber
                    };
                    //
                    sur.DisbursementId = dis.Id;
                    //
                    var staff = db.HrStaff.Single(l => l.Id == model.Staff);
                    sur.StaffId = staff.Id;
                    //
                    var personnel = db.HrStaff.Include(k => k.ApplicationUser).Single(p => p.ApplicationUser.UserName == User.Identity.Name);
                    sur.PersonnelId = personnel.Id;
                    //
                    var totalSpent = 0.0;
                    var payables = JsonConvert.DeserializeObject<List<JsonAccountPayable>>(model.Payables);
                    //
                    payables.ForEach(o => { totalSpent += o.SpentAmount; });
                    //
                    var totalDis = 0.0;
                    foreach (var p in dis.Warrant.Payments)
                    {
                        var payment = db.FWallet.Include(j => j.Taxes).Include(k => k.Department).Single(k => k.Id == p.Id);
                        //
                        totalDis += payment.GrossAmount;

                        foreach (var t in payment.Taxes)
                        {
                            //Apply Taxes
                        }

                    }
                    //
                    sur.AmountSpent = totalSpent;
                    //
                    if (totalDis - totalSpent < 0)
                    {
                        ModelState.AddModelError("", "Total amount spent cannot exceed total amount disbursed.");
                        //
                        return this.View(FinanceControllerAction.AddImprestSurrender, model);
                    }
                    //
                    if (totalDis - totalSpent > 0)
                    {
                        if (model.ModeId == null || model.ModeId == 0)
                        {
                            model.Disbursement = dis;
                            ModelState.AddModelError("", "Please specify the payment mode for Unspent Amount settlement.");
                            //
                            return this.View(FinanceControllerAction.AddImprestSurrender, model);
                        }
                        if (string.IsNullOrEmpty(model.ModeAccNumber))
                        {
                            model.Disbursement = dis;
                            ModelState.AddModelError("", "Please specify the payment mode 'Account Number' for Unspent Amount settlement.");
                            //
                            return this.View(FinanceControllerAction.AddImprestSurrender, model);
                        }

                        //
                        var mode = db.FPaymentAccounts.Single(l => l.Id == model.ModeId);
                        sur.ModeId = mode.Id;
                        //
                        //sur.LedgerId = dis.Warrant.LedgerId;
                    }

                    //
                    db.FImprestSurrenders.Add(sur);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllImprestSurrenders, new { msg = "Imprest Surrender created succesifully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Imprest Surrender could NOT be created. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.AddImprestSurrender, model);
        }
        #endregion

        #region Imprest Warrant Payment Vouchers
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allimpvouchers", Name = FinanceControllerRoute.GetAllImprestPVouchers)]
        public IActionResult AllImprestPVouchers()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllImprestPVouchers);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addimpvoucher", Name = FinanceControllerRoute.GetAddImprestPVoucher)]
        public IActionResult AddImprestPVoucher(int wid, string iws)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (!db.FImprestWarrants.Any())
            {
                return RedirectToRoute(FinanceControllerRoute.GetAddImprestWarrant, new { msg = "Create an imprest warrant before creating a imprest payment voucher" });
            }
            var model = new AddPaymentVoucherViewModel()
            {
                VoucherNo = Utils.GenerateVoucherNo(db).ToString()
            };
            //
            //
            try
            {
                //
                if (wid > 0)
                {
                    model.ImprestWarrants.Add(db.FImprestWarrants.SingleOrDefault(l => l.Id == wid));
                    model.ImprestIds = JsonConvert.SerializeObject(new int[] { wid });
                }
                else if (iws != null)
                {
                    var list = JsonConvert.DeserializeObject<int[]>(iws);
                    //
                    foreach (var e in list)
                    {
                        model.ImprestWarrants.Add(db.FImprestWarrants.SingleOrDefault(l => l.Id == e));
                    }
                    //
                    model.InvoiceIds = iws;
                }
            }
            catch
            {
                return new NotFoundResult();
            }
            //
            return this.View(FinanceControllerAction.CreateImprestPVoucher, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addimpvoucher", Name = FinanceControllerRoute.GetAddImprestPVoucher)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddImprestPVoucher(AddPaymentVoucherViewModel model, params int[] iws)
        {
            if (ModelState.IsValid)
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                ////
                model.VoucherNo = Utils.GenerateVoucherNo(db).ToString();
                //model.Claim = db.FExpenseClaims.SingleOrDefault(l => l.Id == model.ClaimId);
                if (!string.IsNullOrEmpty(model.ImprestIds))
                {
                    var list = JsonConvert.DeserializeObject<int[]>(model.ImprestIds);
                    foreach (var e in list)
                    {
                        model.ImprestWarrants.Add(db.FImprestWarrants.SingleOrDefault(l => l.Id == e));
                    }
                }
                //
                var apAccs = appSettings.Value.Accounts.APCreditorAccs;
                if (apAccs == null || apAccs.Count == 0)
                {
                    return RedirectToRoute(FinanceControllerRoute.GetUpdateSettings, new { msg = "Specify a default 'Accounts Payable(Creditors)' GL Account and try again." });
                }
                //
                var pv = new FPaymentVoucher()
                {
                    CheckNo = model.CheckNo,
                    VoucherDate = DateTime.Now,
                    Description = model.Description,
                    Type = PVType.Imprest,
                    Status = PVStatus.Pending
                };
                //Ledger
                var ledger = db.FLedgers.Include(l => l.PaymentAccounts).ThenInclude(k => k.GLAccount).Single(p => p.Id == model.Ledger);
                pv.LedgerId = ledger.Id;
                //Project
                if (model.Project > 0)
                {
                    //var rpoj = db.FProjects.Single(p => p.Id == model.Sponsor);
                    ////
                    //pv.SponsorId = sponsor.Id;
                }
                //
                var personnel = db.Users.Include(j => j.StaffProfile).Single(l => l.UserName == User.Identity.Name).StaffProfile;
                //Personnel
                pv.PersonnelId = personnel.Id;
                //
                pv.VoucherNo = Utils.GenerateVoucherNo(db).ToString();
                //
                var apAcc = db.FinanceAccounts.Single(k => k.Id == apAccs.FirstOrDefault(j => j.IsDefault).AccId);
                if (apAcc == null)
                    apAcc = db.FinanceAccounts.Single(k => k.Id == apAccs.First().AccId);
                //
                try
                {

                    foreach (var j in iws)
                    {
                        var warrant = db.FImprestWarrants.Include(l => l.Payments).ThenInclude(l => l.CurrencyUsed)
                            .Include(l => l.Payments).ThenInclude(l => l.GLTransactions).Single(k => k.Id == j);
                        //
                        foreach (var p in warrant.Payments)
                        {
                            pv.Payments.Add(p);

                            //GT Transactions
                            //Credit
                            var trans = new GLTransaction()
                            {
                                Amount = p.GrossAmount,
                                DateEntered = DateTime.Now,
                                AccountId = apAcc.Id,//Get Money from..
                                Type = GLTransactionType.Credit,
                            };
                            p.GLTransactions.Add(trans);
                            //Debit
                            var trans1 = new GLTransaction()
                            {
                                Amount = p.GrossAmount,
                                DateEntered = DateTime.Now,
                                AccountId = p.AccountId,//Pay money to..
                                Type = GLTransactionType.Debit,
                            };
                            p.GLTransactions.Add(trans1);

                            //
                            pv.Payments.Add(p);
                        }
                    }
                    //
                    if (pv.Payments.Count == 0)
                    {
                        ModelState.AddModelError(String.Empty, "Add Staff Expense Claims to voucher failed.");
                        //
                        return this.View(FinanceControllerAction.CreateImprestPVoucher, model);
                    }
                    //
                    db.FPaymentVouchers.Add(pv);
                    await db.SaveChangesAsync();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllImprestPVouchers, new { msg = "Imprest Payment Voucher created successfully" });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(String.Empty, "Add imprest payment voucher failed.");
                }
            }
            //
            return this.View(FinanceControllerAction.CreateImprestPVoucher, model);
        }

        #endregion

        #region Expense Claim
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allexpcl", Name = FinanceControllerRoute.GetAllExpenseClaims)]
        public IActionResult AllExpenseClaim()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllExpenseClaims);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addexpcl", Name = FinanceControllerRoute.GetAddExpenseClaim)]
        public async Task<IActionResult> AddExpenseClaim(int wid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            var model = new AddExpenseClaimViewModel() {
                Ref = (await Utils.GenerateExpenseClaimRef(db)).ToString("0000")
            };
            //
            if (wid > 0)
            {
                model.Warrant = db.FImprestWarrants.SingleOrDefault(k => k.Id == wid);
            }
            //
            return this.View(FinanceControllerAction.AddExpenseClaim, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addexpcl", Name = FinanceControllerRoute.GetAddExpenseClaim)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddExpenseClaim(AddExpenseClaimViewModel model, params int[] taxes)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (ModelState.IsValid)
            {
                //
                if (model.Payables == null) {
                    ModelState.AddModelError("", "Please specifiy atleast one GL Account and try again..");
                    return View(FinanceControllerAction.AddExpenseClaim, model);
                }
                //
                try
                {
                    //
                    var staff = db.HrStaff.Single(p => p.Id == model.Staff);

                    var claim = new ExpenseClaim()
                    {
                        StaffId = staff.Id,
                        OrderNo = model.OrderNo,
                        DutyCalled = model.DutyCalled,
                        Ref = await Utils.GenerateExpenseClaimRef(db)
                    };
                    //
                    if (model.Project > 0)
                    {
                        var proj = db.FProjects.Single(l => l.Id == model.Project);
                        claim.ProjectId = proj.Id;
                    }
                    //
                    var personnel = db.HrStaff.Include(p => p.ApplicationUser).Single(p => p.ApplicationUser.UserName == User.Identity.Name);
                    claim.PersonnelId = personnel.Id;
                    //
                    //
                    var lgr = db.FLedgers.Single(k => k.Id == model.Ledger);
                    claim.LedgerId = lgr.Id;
                    //
                    if (model.Imprest > 0)
                    {
                        var impr = db.FImprestWarrants.SingleOrDefault(l => l.Id == model.Imprest);
                        if (impr != null && impr.StaffId == staff.Id)
                            claim.WarrantId = impr.Id;
                    }
                    //
                    if (model.Mileage != null && model.Mileage.Distance > 0 && model.Mileage.RegNo != null)
                    {
                        claim.Mileage = model.Mileage;
                    }
                    //
                    var camt = 0.0;
                    if (model.ClaimEntries != null) {
                        //
                        var entries = JsonConvert.DeserializeObject<List<JsonClaimItem>>(model.ClaimEntries);
                        //
                        var list = await _service.GetClaimEntries(entries, ModelState);

                        if (ModelState.ErrorCount > 0)
                        {
                            return this.View(FinanceControllerAction.AddExpenseClaim, model);
                        }
                        else
                        {
                            list.ForEach(k => { claim.ClaimEntries.Add(k); camt += (k.Quantity * k.UnitCost); });
                        }
                    }
                    //
                    try
                    {
                        var payables = JsonConvert.DeserializeObject<List<JsonAccountPayable>>(model.Payables);
                        //
                        var list = await _service.GetAccountsPayable(payables, PayablesType.Unspecified, ModelState);
                        //
                        if (ModelState.ErrorCount > 0)
                        {
                            return this.View(FinanceControllerAction.AddExpenseClaim, model);
                        }
                        else
                        {
                            var amt = 0.0;
                            list.ForEach(o => { claim.Payments.Add(o); amt += (o.GrossAmount - o.Discount); });
                            //
                            if (camt != amt)
                            {
                                ModelState.AddModelError("", "Claim Entries amount do not match GL Accounts Payable value...");
                                return this.View(FinanceControllerAction.AddExpenseClaim, model);
                            }
                            claim.Amount = amt;
                        }
                        //

                    }
                    catch
                    {
                        ModelState.AddModelError("", "Invoice Account Payables could not be parsed. Please try again");
                        return this.View(FinanceControllerAction.AddExpenseClaim, model);
                    }
                    //
                    db.FExpenseClaims.Add(claim);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllExpenseClaims, new { msg = "Expense Claim created succesifully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Expense Claim could NOT be created. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.AddExpenseClaim, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eid"></param>
        /// <returns></returns>
        [HttpPost("dropexpcl", Name = FinanceControllerRoute.DropExpenseClaim)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DropExpenseClaim(int eid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (eid > 0)
            {
                var warrant = db.FExpenseClaims.Include(l => l.Payments).Single(l => l.Id == eid);
                //
                warrant.Status = EntityStatus.Deleted;

                //Payments
                foreach (var pay in warrant.Payments)
                {
                    pay.Status = EntityStatus.Deleted;
                }

                //
                await db.SaveChangesAsync();

                return new JsonResult(new { s = 1, msg = "Expense Claim Deleted Successifully.." });
            }
            //
            return new JsonResult(new { s = 0, err = "Expense Claim Not found." });
        }
        #endregion

        #region Expense Disbursement
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allexpdis", Name = FinanceControllerRoute.GetAllExpenseDisbursements)]
        public IActionResult AllExpenseDisbursement()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllExpenseDisbursements);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addexpdis", Name = FinanceControllerRoute.GetAddExpenseDisbursement)]
        public IActionResult AddExpenseDisbursement(int cid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            var model = new AddExpenseDisbursementViewModel();
            if (cid > 0)
            {
                model.Claim = db.FExpenseClaims.Include(k => k.Ledger).Include(k => k.Imprest).Include(k => k.Ledger).ThenInclude(l => l.PaymentAccounts).Include(k => k.Mileage)
                    .Include(l => l.Payments).ThenInclude(l => l.CurrencyUsed).Include(l => l.Payments).ThenInclude(l => l.GLAccount)
                    .Include(l => l.Project).Include(l => l.Staff).Include(l => l.Personnel).SingleOrDefault(l => l.Id == cid);
            }
            //
            return this.View(FinanceControllerAction.AddExpenseDisbursement, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addexpdis", Name = FinanceControllerRoute.GetAddExpenseDisbursement)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddExpenseDisbursement(AddExpenseDisbursementViewModel model)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (ModelState.IsValid)
            {
                //
                if (string.IsNullOrEmpty(model.PaymentEntries))
                {
                    ModelState.AddModelError(string.Empty, "No Payment Entries found...");
                    //
                    return this.View(FinanceControllerAction.AddExpenseDisbursement, model);
                }
                //
                try
                {
                    //
                    var claim = db.FExpenseClaims.Include(k => k.Ledger).ThenInclude(l => l.PaymentAccounts).Include(k => k.Imprest).Include(k => k.Ledger).ThenInclude(l => l.PaymentAccounts).Include(k => k.Mileage)
                    .Include(l => l.Payments).ThenInclude(l => l.CurrencyUsed).Include(l => l.Payments).ThenInclude(l => l.GLAccount)
                    .Include(l => l.Project).Include(l => l.Staff).Include(l => l.Personnel).SingleOrDefault(l => l.Id == model.ClaimId);
                    //
                    model.Claim = claim;
                    //
                    //
                    var entries = JsonConvert.DeserializeObject<List<JsonPaymentEntry>>(model.PaymentEntries);
                    if (entries.Count == 0)
                    {
                        ModelState.AddModelError(string.Empty, "No Payment Entries found.");
                        //
                        return this.View(FinanceControllerAction.AddExpenseDisbursement, model);
                    }
                    //

                    await _service.DisburseExpenseClaims(claim, entries, User, ModelState);
                    //
                    if (ModelState.ErrorCount > 0)
                        return this.View(FinanceControllerAction.AddExpenseDisbursement, model);
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllExpenseDisbursements, new { msg = "Expense Disbursement processed succesifully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Expense Disbursement could NOT be processed. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.AddExpenseDisbursement, model);
        }
        #endregion

        #region Expense Claim Payment Vouchers
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allecpvouchers", Name = FinanceControllerRoute.GetAllExpenseClaimPVouchers)]
        public IActionResult AllExpenseClaimPVouchers()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllExpenseClaimPVouchers);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addecpvoucher", Name = FinanceControllerRoute.GetAddExpenseClaimPVoucher)]
        public IActionResult AddExpenseClaimPVoucher(int cid, string ecs)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (!db.FInvoices.Any())
            {
                return RedirectToRoute(FinanceControllerRoute.GetAddExpenseClaim, new { msg = "Create an expense claim before creating a payment voucher" });
            }
            //
            var apAccs = appSettings.Value.Accounts.APCreditorAccs;
            if (apAccs == null || apAccs.Count == 0)
            {
                return RedirectToRoute(FinanceControllerRoute.GetUpdateSettings, new { msg = "Specify a default 'Accounts Payable(Creditors)' GL Account..." });
            }
            //
            var model = new AddPaymentVoucherViewModel()
            {
                VoucherNo = Utils.GenerateVoucherNo(db).ToString(),
            };
            //
            try
            {
                //
                if (cid > 0)
                {
                    model.ImprestWarrants.Add(db.FImprestWarrants.SingleOrDefault(l => l.Id == cid));
                    model.ImprestIds = JsonConvert.SerializeObject(new int[] { cid });
                }
                else if (ecs != null)
                {
                    var list = JsonConvert.DeserializeObject<int[]>(ecs);
                    //
                    foreach (var e in list)
                    {
                        model.ImprestWarrants.Add(db.FImprestWarrants.SingleOrDefault(l => l.Id == e));
                    }
                    //
                    model.ClaimIds = ecs;
                }
            }
            catch
            {
                return new NotFoundResult();
            }
            //
            return this.View(FinanceControllerAction.CreateExpenseClaimPVoucher, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addecpvoucher", Name = FinanceControllerRoute.GetAddExpenseClaimPVoucher)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddExpenseClaimPVoucher(AddPaymentVoucherViewModel model, params int[] ecs)
        {
            if (ModelState.IsValid)
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                model.VoucherNo = Utils.GenerateVoucherNo(db).ToString();
                //model.Claim = db.FExpenseClaims.SingleOrDefault(l => l.Id == model.ClaimId);
                if (!string.IsNullOrEmpty(model.ClaimIds))
                {
                    var list = JsonConvert.DeserializeObject<int[]>(model.ClaimIds);
                    foreach (var e in list)
                    {
                        model.ExpenseClaims.Add(db.FExpenseClaims.SingleOrDefault(l => l.Id == e));
                    }
                }
                //
                var apAccs = appSettings.Value.Accounts.APCreditorAccs;
                if (apAccs == null || apAccs.Count == 0)
                {
                    return RedirectToRoute(FinanceControllerRoute.GetUpdateSettings, new { msg = "Specify a default 'Accounts Payable(Creditors)' GL Account and try again." });
                }
                //
                var pv = new FPaymentVoucher()
                {
                    CheckNo = model.CheckNo,
                    VoucherDate = DateTime.Now,
                    Description = model.Description,
                    Type = PVType.ExpenseClaim,
                    Status = PVStatus.Pending
                };
                //Ledger
                var ledger = db.FLedgers.Include(l => l.PaymentAccounts).ThenInclude(k => k.GLAccount).Single(p => p.Id == model.Ledger);
                pv.LedgerId = ledger.Id;
                //Project
                if (model.Project > 0)
                {
                    //var rpoj = db.FProjects.Single(p => p.Id == model.Sponsor);
                    ////
                    //pv.SponsorId = sponsor.Id;
                }
                //
                var personnel = db.Users.Include(j => j.StaffProfile).Single(l => l.UserName == User.Identity.Name).StaffProfile;
                //Personnel
                pv.PersonnelId = personnel.Id;
                //
                pv.VoucherNo = Utils.GenerateVoucherNo(db).ToString();
                //
                var apAcc = db.FinanceAccounts.Single(k => k.Id == apAccs.FirstOrDefault(j => j.IsDefault).AccId);
                if (apAcc == null)
                    apAcc = db.FinanceAccounts.Single(k => k.Id == apAccs.First().AccId);
                //
                try
                {

                    foreach (var j in ecs)
                    {
                        var claim = db.FExpenseClaims.Include(l => l.Payments).ThenInclude(l => l.CurrencyUsed)
                            .Include(l => l.Payments).ThenInclude(l => l.GLTransactions).Single(k => k.Id == j);
                        //
                        foreach (var p in claim.Payments)
                        {
                            pv.Payments.Add(p);

                            //GT Transactions
                            //Credit
                            var trans = new GLTransaction()
                            {
                                Amount = p.GrossAmount,
                                DateEntered = DateTime.Now,
                                AccountId = apAcc.Id,//Get Money from..
                                Type = GLTransactionType.Credit,
                            };
                            p.GLTransactions.Add(trans);
                            //Debit
                            var trans1 = new GLTransaction()
                            {
                                Amount = p.GrossAmount,
                                DateEntered = DateTime.Now,
                                AccountId = p.AccountId,//Pay money to..
                                Type = GLTransactionType.Debit,
                            };
                            p.GLTransactions.Add(trans1);

                            //
                            pv.Payments.Add(p);
                        }
                    }
                    //
                    if (pv.Payments.Count == 0)
                    {
                        ModelState.AddModelError(String.Empty, "Add Staff Expense Claims to voucher failed.");
                        //
                        return this.View(FinanceControllerAction.CreateExpenseClaimPVoucher, model);
                    }
                    //
                    db.FPaymentVouchers.Add(pv);
                    await db.SaveChangesAsync();

                    //We good
                    return RedirectToRoute(FinanceControllerRoute.GetAllExpenseClaimPVouchers, new { msg = "Payment Voucher created successfully" });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(String.Empty, "Add payment voucher failed.");
                }
            }
            //
            return this.View(FinanceControllerAction.CreateExpenseClaimPVoucher, model);
        }

        #endregion

        #region Petty-Cash Cashier Office
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpccoffice", Name = FinanceControllerRoute.GetAllPCCashierOffices)]
        public IActionResult AllPCCashierOffices()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllPCCashierOffices);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addpccoffice", Name = FinanceControllerRoute.GetAddPCCashierOffice)]
        public async Task<IActionResult> AddPCCashierOffice()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            var model = new AddCashierOfficeViewModel()
            {
                RefNo = (await Utils.GenerateCashierOfficeRefNo(db)).ToString("0000")
            };
            //
            return this.View(FinanceControllerAction.AddPCCashierOffice, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addpccoffice", Name = FinanceControllerRoute.GetAddPCCashierOffice)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPCCashierOffice(AddCashierOfficeViewModel model, params int[] taxes)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (ModelState.IsValid)
            {
                model.RefNo = (await Utils.GenerateCashierOfficeRefNo(db)).ToString("0000");
                //
                try
                {
                    var office = new FCashier()
                    {
                        Name = model.Name,
                        RefNo = await Utils.GenerateCashierOfficeRefNo(db)
                    };
                    //
                    var dept = db.HrDepartments.Single(p => p.Id == model.DeptId);
                    office.DeptId = dept.Id;
                    //
                    var location = db.Locations.Single(l => l.Id == model.LocationId);
                    office.LocationId = location.Id;
                    //
                    var lgr = db.FLedgers.Include(l => l.PaymentAccounts).Single(p => p.Id == model.LedgerId);

                    //
                    var acc = lgr.PaymentAccounts.Single(l => l.Id == model.Mode);

                    //
                    office.PaymentAccounts.Add(acc);

                    db.FCashierOffices.Add(office);
                    await db.SaveChangesAsync();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllPCCashierOffices, new { msg = "Petty Cash Cashier office created succesifully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Petty Cash Cashier Office could NOT be created. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.AddPCCashierOffice, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oid"></param>
        /// <returns></returns>
        [HttpPost("droppccoffice", Name = FinanceControllerRoute.DropPCCashierOffice)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DropPCCashierOffice(int oid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (oid > 0)
            {
                var office = db.FCashierOffices.Include(l => l.Attendants).Single(l => l.Id == oid);
                //
                office.Status = EntityStatus.Deleted;

                //Payments
                foreach (var att in office.Attendants)
                {
                    att.Status = EntityStatus.Deleted;
                }

                //
                await db.SaveChangesAsync();

                return new JsonResult(new { s = 1, msg = "Cashier Office Deleted Successifully.." });
            }
            //
            return new JsonResult(new { s = 0, err = "Cashier office Not found." });
        }

        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addpccoatt", Name = FinanceControllerRoute.GetAddPCCashierOfficeAttendant)]
        public async Task<IActionResult> AddPCCashierOfficeAttendant(int oid, int sid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            var model = new AddCashierOfficeAttendantViewModel()
            {
                //Ref = (await Utils.GeneratePettyCashRefNo(db)).ToString("0000")
            };
            //
            if (oid > 0)
            {
                model.Office = db.FCashierOffices.Single(k => k.Id == oid);
            }
            if (sid > 0)
            {
                model.Staff = db.HrStaff.Single(k => k.Id == sid);
            }
            //
            return this.View(FinanceControllerAction.AddPCCashierOfficeAttendant, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addpccoatt", Name = FinanceControllerRoute.GetAddPCCashierOfficeAttendant)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPCCashierOfficeAttendant(AddCashierOfficeAttendantViewModel model, params int[] taxes)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (ModelState.IsValid)
            {
                //
                try
                {
                    var profile = new FCAttendantProfile() {
                        Status = model.IsActive ? EntityStatus.Active : EntityStatus.Suspended
                    };
                    //
                    var staff = db.HrStaff.Single(l => l.Id == model.StaffId);
                    profile.StaffId = staff.Id;
                    //
                    var personnel = db.HrStaff.Include(l => l.ApplicationUser).Single(l => l.ApplicationUser.UserName == User.Identity.Name);
                    profile.PersonnelId = staff.Id;
                    //
                    var office = db.FCashierOffices.Single(p => p.Id == model.OfficeId);
                    profile.OfficeId = office.Id;
                    //
                    db.FCashiers.Add(profile);
                    await db.SaveChangesAsync();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllPCCashierOffices, new { msg = "Petty Cash Cashier office attendant created succesifully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Petty Cash Cashier Office attendant could NOT be created. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.AddPCCashierOfficeAttendant, model);
        }
        #endregion

        #region Pettycash Item
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpcitems", Name = FinanceControllerRoute.GetAllPettycashItems)]
        public IActionResult AllPettycashItems()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllPettycashItems);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addpcitem", Name = FinanceControllerRoute.GetAddPettycashItem)]
        public async Task<IActionResult> AddPettycashItem()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            var model = new AddPettyCashItemViewModel()
            {
                Ref = (await Utils.GeneratePettyCashRefNo(db)).ToString("0000")
            };
            //
            return this.View(FinanceControllerAction.AddPettycashItem, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addpcitem", Name = FinanceControllerRoute.GetAddPettycashItem)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPettycashItem(AddPettyCashItemViewModel model, params int[] taxes)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (ModelState.IsValid)
            {
                //
                try
                {
                    //
                    var pc = new PettyCashReq()
                    {
                        Description = model.Description,
                        Ref = await Utils.GeneratePettyCashRefNo(db),
                    };
                    //Ledger
                    var lgr = db.FLedgers.Single(l => l.Id == model.LedgerId);
                    pc.LedgerId = lgr.Id;
                    //Project
                    if (model.ProjectId > 0)
                    {
                        //
                        var proj = db.FProjects.Single(l => l.Id == model.ProjectId);
                        pc.ProjectId = proj.Id;
                    }
                    //
                    if ((model.StaffId > 0 && model.TraineeId > 0 && model.TrainerId > 0) || (model.StaffId > 0 && model.TraineeId > 0) || (model.TraineeId > 0 && model.TrainerId > 0) || (model.StaffId > 0 && model.TrainerId > 0))
                    {
                        ModelState.AddModelError("", "Please specify a single entitled user account, not multiple..");
                        return this.View(FinanceControllerAction.AddPettycashItem, model);
                    }
                    //
                    if (model.StaffId > 0)
                    {
                        var staff = db.HrStaff.Include(l => l.ApplicationUser).Single(p => p.Id == model.StaffId);
                        pc.StaffId = staff.Id;
                    }
                    else if (model.TraineeId > 0)
                    {
                        var trainee = db.Trainees.Include(l => l.ApplicationUser).Single(p => p.Id == model.TraineeId);
                        pc.TraineeId = trainee.Id;
                    }
                    else if (model.TrainerId > 0)
                    {
                        var trainer = db.Trainers.Include(l => l.ApplicationUser).Single(p => p.Id == model.TrainerId);
                        pc.TrainerId = trainer.Id;
                    }
                    else {
                        ModelState.AddModelError("", "Please specify an entitled user account, (Staff, Beneficiary, or Master Craftman)");
                        return this.View(FinanceControllerAction.AddPettycashItem, model);
                    }
                    //
                    if (model.Payables != null)
                    {
                        try
                        {
                            var payables = JsonConvert.DeserializeObject<List<JsonAccountPayable>>(model.Payables);
                            //
                            var list = await _service.GetAccountsPayable(payables, PayablesType.Unspecified, ModelState);
                            //
                            if (ModelState.ErrorCount > 0)
                            {
                                return this.View(FinanceControllerAction.CreateInvoice, model);
                            }
                            else
                            {
                                list.ForEach(o => pc.Payments.Add(o));
                            }

                        }
                        catch
                        {
                            ModelState.AddModelError("", "GL Account Payable(s) could not be parsed. Please try again");
                            return this.View(FinanceControllerAction.AddPettycashItem, model);
                        }
                    }
                    //
                    if (pc.Payments.Count == 0)
                    {
                        ModelState.AddModelError("", "No GL Account payable(s) have been specified..");
                        return this.View(FinanceControllerAction.AddPettycashItem, model);
                    }
                    //
                    var personnel = db.Users.Include(l => l.StaffProfile).Single(p => p.UserName == User.Identity.Name);
                    pc.PersonnelId = personnel.StaffProfile.Id;
                    //
                    db.FPettyCashReqs.Add(pc);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllPettycashItems, new { msg = "Petty Cash Item created succesifully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Petty Cash Item could NOT be created. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.AddPettycashItem, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost("droppcitem", Name = FinanceControllerRoute.DropPettyCashReq)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DropPettyCashReq(int pid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (pid > 0)
            {
                try
                {
                    var req = db.FPettyCashReqs.Include(l => l.Payments).Single(l => l.Id == pid);
                    //
                    req.Status = EntityStatus.Deleted;

                    //Payments
                    foreach (var pay in req.Payments)
                    {
                        pay.Status = EntityStatus.Deleted;
                    }

                    //
                    await db.SaveChangesAsync();

                    return new JsonResult(new { s = 1, msg = "Petty-Cash request Deleted Successifully.." });
                }
                catch { }
            }
            return new JsonResult(new { s = 0, msg = "Operation failed.." });
        }
        #endregion

        #region Pettycash Disbursement
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpcdis", Name = FinanceControllerRoute.GetAllPettycashDisbursements)]
        public IActionResult AllPettycashDisbursements()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllPettycashDisbursements);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addpcdis", Name = FinanceControllerRoute.GetAddPettycashDisbursement)]
        public IActionResult AddPettycashDisbursement(int pid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            var model = new AddPettycashDisbursementViewModel() { };
            //
            if (pid > 0)
            {
                model.Request = db.FPettyCashReqs.Include(k => k.Ledger).Include(l => l.Disbursement)
                    .Include(l => l.Staff).Include(l => l.Trainee).Include(l => l.Payments).Include(l => l.Trainer).SingleOrDefault(k => k.Id == pid);
                //
                model.RequestId = pid;
            }
            //
            return this.View(FinanceControllerAction.AddPettycashDisbursement, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addpcdis", Name = FinanceControllerRoute.GetAddPettycashDisbursement)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPettycashDisbursement(AddPettycashDisbursementViewModel model)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();

            //
            model.Request = db.FPettyCashReqs.Include(k => k.Ledger).Include(l => l.Disbursement)
                    .Include(l => l.Staff).Include(l => l.Trainee).Include(l => l.Trainer).Include(l => l.Payments).SingleOrDefault(k => k.Id == model.RequestId);
            //
            if (ModelState.IsValid)
            {
                //
                try
                {
                    //
                    var dis = new PettycashDisbursement()
                    {
                        DateDisbursed = DateTime.Now,
                        Desc = "Pettycash disbursement",
                        Status = EntityStatus.Active
                    };
                    //
                    var personnel = db.HrStaff.Include(k => k.ApplicationUser).Single(l => l.ApplicationUser.UserName == User.Identity.Name);
                    //
                    dis.PersonnelId = personnel.Id;
                    //
                    dis.RequestId = model.Request.Id;
                    //

                    //
                    dis.DateCreated = DateTime.Now;
                    //
                    db.FPCDisbursement.Add(dis);
                    await db.SaveChangesAsync();
                    //
                    return RedirectToRoute(FinanceControllerRoute.GetAllPettycashDisbursements, new { msg = "Pettycash Disbursement created succesifully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Petty cash Disbursement could NOT be created. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.AddPettycashDisbursement, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="did"></param>
        /// <returns></returns>
        [HttpPost("droppcdis", Name = FinanceControllerRoute.DropPettyCashDisbursement)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DropPettyCashDisbursement(int did)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (did > 0)
            {
                try
                {
                    var dis = db.FPCDisbursement.Include(l => l.Request).ThenInclude(l => l.Payments).Single(l => l.Id == did);
                    //
                    dis.Status = EntityStatus.Deleted;

                    //Payments
                    //foreach (var pay in dis.Request.Payments)
                    //{
                    //    pay.Status = EntityStatus.Deleted;
                    //}

                    //
                    await db.SaveChangesAsync();

                    return new JsonResult(new { s = 1, msg = "Petty-Cash Disbursement Deleted Successifully.." });
                }
                catch { }
            }
            return new JsonResult(new { s = 0, msg = "Operation failed.." });
        }
        #endregion

        #region PettyCash Payment Vouchers
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpcpvouchers", Name = FinanceControllerRoute.GetAllPettyCashPVouchers)]
        public IActionResult AllPettyCashPVouchers()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceControllerAction.AllPettyCashPVouchers);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addpcpvoucher", Name = FinanceControllerRoute.GetAddPettyCashPVoucher)]
        public IActionResult AddPettyCashPVoucher(int pid, string pcs)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (!db.FInvoices.Any())
            {
                return RedirectToRoute(FinanceControllerRoute.GetAddPettycashItem, new { msg = "Create an petty-cash request before creating a payment voucher" });
            }
            var model = new AddPaymentVoucherViewModel()
            {
                VoucherNo = Utils.GenerateVoucherNo(db).ToString()
            };
            //
            try
            {
                if (pid > 0)
                {
                    model.PCRequests.Add(db.FPettyCashReqs.Include(l => l.Ledger).Include(l => l.Payments).ThenInclude(l => l.CurrencyUsed).SingleOrDefault(l => l.Id == pid));
                    model.PCRequestIds = JsonConvert.SerializeObject(new int[] { pid });
                }
                else if (pcs != null)
                {
                    var list = JsonConvert.DeserializeObject<int[]>(pcs);
                    //
                    foreach (int y in list)
                    {
                        model.PCRequests.Add(db.FPettyCashReqs.Include(l => l.Ledger).Include(l => l.Payments).ThenInclude(l => l.CurrencyUsed).SingleOrDefault(l => l.Id == y));
                    }
                    //
                    model.PCRequestIds = pcs;
                }
            }
            catch { }
            //
            return this.View(FinanceControllerAction.CreatePettyCashPVoucher, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addpcpvoucher", Name = FinanceControllerRoute.GetAddPettyCashPVoucher)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPettyCashPVoucher(AddPaymentVoucherViewModel model, params int[] pcs)
        {
            if (ModelState.IsValid)
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                model.VoucherNo = Utils.GenerateVoucherNo(db).ToString();
                if (model.PCRequestIds != null)
                {
                    try
                    {
                        var l = JsonConvert.DeserializeObject<int[]>(model.PCRequestIds);
                        foreach (var s in l)
                            model.PCRequests.Add(db.FPettyCashReqs.Include(c => c.Payments).ThenInclude(c => c.CurrencyUsed).Single(p => p.Id == s));
                    }
                    catch { }
                }
                //
                if (pcs == null || pcs.Length == 0)
                {
                    ModelState.AddModelError("", "Please select a Petty-Cash request and try again...");
                    return this.View(FinanceControllerAction.CreatePettyCashPVoucher, model);
                }
                //Ledger
                var ledger = db.FLedgers.Include(l => l.PaymentAccounts).Single(l => l.Id == model.Ledger);
                //Payment mode
                var mode = ledger.PaymentAccounts.Single(p => p.Id == model.Mode);
                //Cashier
                var cashier = db.FCashierOffices.Single(l => l.Id == model.Cashier);
                //
                FProject project = null;
                //
                var list = new List<PettyCashReq>();
                //
                foreach (var j in pcs)
                {
                    var pc = db.FPettyCashReqs.SingleOrDefault(l => l.Id == j);
                    if (pc != null)
                        list.Add(pc);
                }
                //
                if (list.Count == 0)
                {
                    ModelState.AddModelError("", "No Petty-Cash request where selected.");
                }
                else
                {
                    //
                    await _service.DisbursePettyCahsReqs(list, ledger, project, cashier, mode, model.CheckNo, model.Description, User, ModelState);

                    if (ModelState.ErrorCount == 0)
                        //
                        return RedirectToRoute(FinanceControllerRoute.GetAllPettyCashPVouchers, new { msg = "Petty-Cash Payments where disbursed successfully" });
                }
                //TOO-BAD
                //We have errors
            }
            //
            return this.View(FinanceControllerAction.CreatePettyCashPVoucher, model);
        }

        #endregion

        #region Preview Payment Voucher
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpGet("prevpvoucher", Name = FinanceControllerRoute.GetPreviewPVoucher)]
        public IActionResult PreviewRegularPaymentVoucher(int pid, string returnurl)
        {
            if (pid == 0)
            {
                if (returnurl != null)
                    return Redirect(returnurl);
                else
                    return RedirectToRoute(FinanceControllerRoute.GetIndex, new { msg = "Please specify a payment voucher to be previewed." });
            }
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            var voucher = db.FPaymentVouchers.Include(k => k.Ledger).ThenInclude(l => l.PVoucherPrefixes)
                .Include(k => k.Ledger).ThenInclude(l => l.PaymentAccounts)
                .Include(k => k.Supplier)
                .Include(k => k.Cashier)
                .Include(l => l.Payments).ThenInclude(k => k.Taxes)
                .Include(l => l.Payments).ThenInclude(k => k.CurrencyUsed)
                .Include(l => l.Payments).ThenInclude(k => k.Invoice).ThenInclude(k => k.SupplierInvoiceWaivers)
                .Include(l => l.Payments).ThenInclude(k => k.PettyCashReq).ThenInclude(k => k.Staff)
                .Include(l => l.Payments).ThenInclude(k => k.ExpenseClaim).ThenInclude(k => k.Staff)
                .Include(l => l.Payments).ThenInclude(k => k.ImprestWarrant).ThenInclude(k => k.Staff)
                .Include(l => l.Personnel)
                .Include(l => l.PaymentAccount).SingleOrDefault(l => l.Id == pid);
            //
            if (voucher == null)
            {
                return new NotFoundResult();
            }
            //
            var total = 0.0; var tax = 0.0;
            foreach (var p in voucher.Payments)
            {
                if (p.Taxes.Count > 0)
                {
                    foreach (var t in p.Taxes)
                    {
                        var d = Utils.ApplyTax(p.GrossAmount, t);
                        tax += (p.GrossAmount - d);
                        total += d;
                    }
                }
                else
                {
                    total += p.GrossAmount;
                }
            }
            //continue
            var model = new PostPaymentVoucherViewModel()
            {
                VoucherId = voucher.Id,
                Voucher = voucher,
                NetAmount = total,
                NetTax = tax
            };
            //
            return this.View(FinanceControllerAction.PreviewPVouchers, model);
        }
        #endregion

        #region Payroll Journals
        [HttpGet("fallpyjournals", Name = FinanceControllerRoute.GetAllPayrollJournals)]
        public IActionResult AllPayrollJournals()
        {
            return this.View(FinanceControllerAction.AllPayrollJournals);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("faddpyjournal", Name = FinanceControllerRoute.GetImportPayrollJournals)]
        public async Task<IActionResult> ImportPayrollJournal()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new ImportPayrollJournalViewModel()
            {
                //Ref = (await Utils.GenerateJournalRefNo(db)).ToString("0000")
            };

            return this.View(FinanceControllerAction.ImportPayrollJournals, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("faddpyjournal", Name = FinanceControllerRoute.GetImportPayrollJournals)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ImportPayrollJournal(ImportPayrollJournalViewModel model)
        {
            if (ModelState.IsValid)
            {

            }
            //
            return this.View(FinanceControllerAction.ImportPayrollJournals, model);
        }
        #endregion

        #region Payable Journals
        [HttpGet("fallpbjournals", Name = FinanceControllerRoute.GetAllPayableJournals)]
        public IActionResult AllPayableJournals()
        {
            return this.View(FinanceControllerAction.AllPayableJournals);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("fpbjournal", Name = FinanceControllerRoute.GetAddPayablesJournal)]
        public async Task<IActionResult> AddPayableJournal()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new AddPayableJournalViewModel()
            {
                Ref = (await Utils.GenerateJournalRefNo(db)).ToString("0000")
            };
            return this.View(FinanceControllerAction.AddPayablesJournal, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("fpbjournal", Name = FinanceControllerRoute.GetAddPayablesJournal)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPayableJournal(AddPayableJournalViewModel model)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            if (ModelState.IsValid)
            {
                //
                var journal = new PaymentJournal()
                {
                    Amount = model.Amount,
                    Ref = await Utils.GenerateJournalRefNo(db),
                    Reference = model.Reference,
                    Revaluation = model.Revaluation,
                    Type = JournalType.Payables
                };
                //Ledger
                var lgr = db.FLedgers.Single(l => l.Id == model.LedgerId);
                journal.LedgerId = lgr.Id;

                //
                var dAcc = db.FinanceAccounts.Single(l => l.Id == model.DebitAccountId);
                journal.DebitAccountId = dAcc.Id;
                //
                var dDept = db.HrDepartments.Single(l => l.Id == model.DebitDeptId);
                journal.DebitDeptId = dDept.Id;
                //
                if (model.DebitProjectId > 0) {
                    var dProj = db.FProjects.Single(l => l.Id == model.DebitProjectId);
                    journal.DebitProjectId = dProj.Id;
                }

                //
                var cAcc = db.FinanceAccounts.Single(l => l.Id == model.CreditAccountId);
                journal.CreditAccountId = cAcc.Id;
                //
                var cDept = db.HrDepartments.Single(l => l.Id == model.CreditDeptId);
                journal.CreditDeptId = cDept.Id;
                //
                if (model.CreditProjectId > 0)
                {
                    var cProj = db.FProjects.Single(l => l.Id == model.CreditProjectId);
                    journal.CreditProjectId = cProj.Id;
                }

                //
                var baseCur = db.FCurrencies.Single(l => l.IsBase);

                //
                var payment = new FPayment()
                {
                    GrossAmount = model.Amount,
                    AccountId = dAcc.Id,
                    CurrencyId = baseCur.Id,
                    DeptId = dDept.Id
                };
                //Debit
                var trans1 = new GLTransaction()
                {
                    Amount = model.Amount,
                    AccountId = dAcc.Id,
                    Kind = GLTransactionKind.Journal,
                    Type = GLTransactionType.Debit,
                };
                payment.GLTransactions.Add(trans1);
                //Credit
                var trans2 = new GLTransaction()
                {
                    Amount = model.Amount,
                    AccountId = cAcc.Id,
                    Kind = GLTransactionKind.Journal,
                    Type = GLTransactionType.Credit,
                };
                payment.GLTransactions.Add(trans1);

                //
                journal.Payments.Add(payment);

                //
                db.FJournals.Add(journal);
                await db.SaveChangesAsync();

                return RedirectToRoute(FinanceControllerRoute.GetAllPayableJournals, new { msg = "Payables Journal was saved successfully." });
            }
            //
            return this.View(FinanceControllerAction.AddPayablesJournal, model);
        }
        #endregion


        #region Reports
        [HttpGet("freports", Name = FinanceControllerRoute.GetReports)]
        public IActionResult Reports()
        {
            return this.View(FinanceControllerAction.Reports);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("fbreports", Name = FinanceControllerRoute.GetBasicReports)]
        public async Task<IActionResult> BasicReports()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new ReportsViewModel()
            {
            };
            return this.View(FinanceControllerAction.BasicReports, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("fbreports", Name = FinanceControllerRoute.GetBasicReports)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ProcessBasicReport(ReportsViewModel model)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            if (ModelState.IsValid)
            {
            }
            return View(FinanceControllerAction.BasicReports, model);
        }

        [HttpGet("fbudreports", Name = FinanceControllerRoute.GetBudgetReports)]
        public async Task<IActionResult> BudgetReports()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new ReportsViewModel()
            {
            };
            return this.View(FinanceControllerAction.BudgetReports, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("fbudreports", Name = FinanceControllerRoute.GetBudgetReports)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ProcessBudgetReport(ReportsViewModel model)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            if (ModelState.IsValid)
            {
            }
            return View(FinanceControllerAction.BudgetReports, model);
        }
        [HttpGet("fstatements", Name = FinanceControllerRoute.GetFinanceStatements)]
        public async Task<IActionResult> FinanceStatements()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new ReportsViewModel()
            {
            };
            return this.View(FinanceControllerAction.FinanceStatements, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("fstatements", Name = FinanceControllerRoute.GetFinanceStatements)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ProcessFinanceStatement(ReportsViewModel model)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            if (ModelState.IsValid)
            {
            }
            return View(FinanceControllerAction.FinanceStatements, model);
        }
        [HttpGet("fqstatements", Name = FinanceControllerRoute.GetQuartelyFinanceStatements)]
        public async Task<IActionResult> QuartelyFinanceStatements()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new ReportsViewModel()
            {
            };
            return this.View(FinanceControllerAction.QuartelyFinanceStatements, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("fqstatements", Name = FinanceControllerRoute.GetQuartelyFinanceStatements)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ProcessQuartelyFinanceStatement(ReportsViewModel model)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            if (ModelState.IsValid)
            {
            }
            return View(FinanceControllerAction.QuartelyFinanceStatements, model);
        }
        [HttpGet("fbudstatus", Name = FinanceControllerRoute.GetBudgetStatus)]
        public async Task<IActionResult> BudgetStatus()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new BudgetStatusViewModel()
            {
            };
            return this.View(FinanceControllerAction.BudgetStatus, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("fbudstatus", Name = FinanceControllerRoute.GetBudgetStatus)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> BudgetStatus(BudgetStatusViewModel model)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            if (ModelState.IsValid)
            {
            }
            return View(FinanceControllerAction.BudgetStatus, model);
        }
        [HttpGet("fstudfeesum", Name = FinanceControllerRoute.GetStudentFeesSummary)]
        public async Task<IActionResult> StudentFeesSummary()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new StudentFeeSummaryViewModel()
            {
            };
            return this.View(FinanceControllerAction.StudentFees, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("fstudfeesum", Name = FinanceControllerRoute.GetStudentFeesSummary)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> StudentFeesSummary(StudentFeeSummaryViewModel model)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            if (ModelState.IsValid)
            {
            }
            return View(FinanceControllerAction.StudentFees, model);
        }
        #endregion
        #region Settings
        [HttpGet("fsettings", Name = FinanceControllerRoute.GetSettings)]
        public IActionResult Settings()
        {
            return this.View(FinanceControllerAction.Settings);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("fupdatesettings", Name = FinanceControllerRoute.GetUpdateSettings)]
        public IActionResult UpdateSettings()
        {
            return this.View(FinanceControllerAction.UpdateSettings);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("fupdatesettings", Name = FinanceControllerRoute.GetUpdateSettings)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateSettings(UpdateFinanceSettingsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                try
                {
                    var path = Path.Combine(hostingEnvironment.ContentRootPath, $"config.{this.hostingEnvironment.EnvironmentName}.json");

                    var cont = await System.IO.File.ReadAllTextAsync(path);
                    //
                    var doc = JToken.Parse(cont);
                    if(doc["AppSettings"] == null)
                    {
                        doc["AppSettings"] = JsonConvert.SerializeObject(appSettings.Value);
                    }
                    var settings = JsonConvert.DeserializeObject<AppSettings>((doc["AppSettings"]).ToString());
                    //
                    var accumFund = db.FinanceAccounts.Single(l=>l.Id == model.AccumulatedFund);
                    //
                    if(model.AccumulatedFund.ToString() != settings.Accounts.AccumFund)
                    {
                        settings.Accounts.AccumFund = model.AccumulatedFund.ToString();
                    }
                    //APCreditorAccs
                    if (model.APCreditors != null)
                    {
                        try
                        {
                            var apcreds = JsonConvert.DeserializeObject<List<JAccount>>(model.APCreditors);

                            if(apcreds != null)
                            {
                                foreach(var a in apcreds)
                                {
                                    //
                                    var acc = db.FinanceAccounts.Single(l => l.Id == a.AccId);
                                   if(acc.Type == FAccountType.Liability)
                                    {
                                        //
                                        if (!settings.Accounts.APCreditorAccs.Any(k => k.AccId == a.AccId))
                                        {
                                            settings.Accounts.APCreditorAccs.Add(a);
                                        }
                                    }
                                }
                            }
                            else { }
                        }
                        catch { }
                    }
                    //ARDebtorAccs
                    if (model.ARDebtors != null)
                    {
                        try
                        {
                            var ardebts = JsonConvert.DeserializeObject<List<JAccount>>(model.ARDebtors);

                            if (ardebts != null)
                            {
                                foreach (var a in ardebts)
                                {
                                    //
                                    var acc = db.FinanceAccounts.Single(l => l.Id == a.AccId);
                                    if(acc.Type == FAccountType.Asset)
                                    {
                                        //
                                        if (!settings.Accounts.ARDebtorAccs.Any(k => k.AccId == a.AccId))
                                        {
                                            settings.Accounts.ARDebtorAccs.Add(a);
                                        }
                                    }
                                }
                            }
                            else { }
                        }
                        catch { }
                    }
                    //BadDebtAcc
                    var baddebt = db.FinanceAccounts.Single(l => l.Id == model.BadDebtAccount);
                    //
                    if (baddebt.Id.ToString() != settings.Accounts.BadDebtAcc)
                    {
                        settings.Accounts.BadDebtAcc = baddebt.Id.ToString();
                    }
                    //BadDebtAcc
                    var cgacc = db.FinanceAccounts.Single(l => l.Id == model.CapitalGrantsAccount);
                    //
                    if (cgacc.Id.ToString() != settings.Accounts.CGAcc)
                    {
                        settings.Accounts.CGAcc = cgacc.Id.ToString();
                    }
                    //BadDebtAcc
                    var decacc = db.FinanceAccounts.Single(l => l.Id == model.DepreciationAccount);
                    //
                    if (decacc.Id.ToString() != settings.Accounts.DeprAcc)
                    {
                        settings.Accounts.DeprAcc = decacc.Id.ToString();
                    }
                    //BadDebtAcc
                    var obacc = db.FinanceAccounts.Single(l => l.Id == model.OpeningBalanceAccount);
                    //
                    if (obacc.Id.ToString() != settings.Accounts.OBAcc)
                    {
                        settings.Accounts.OBAcc = obacc.Id.ToString();
                    }
                    //BadDebtAcc
                    var pcacc = db.FinanceAccounts.Single(l => l.Id == model.PrePaymentCutomerAccount);
                    //
                    if (pcacc.Id.ToString() != settings.Accounts.PrePaymentCA)
                    {
                        settings.Accounts.PrePaymentCA = pcacc.Id.ToString();
                    }

                    //
                    if (model.PrePaymentReceiptAlias != settings.Accounts.PrePaymentRA)
                    {
                        settings.Accounts.PrePaymentRA = model.PrePaymentReceiptAlias;
                    }
                    //
                    if (model.EnableReceiptPrinting != settings.Accounts.ReceiptPrinting)
                    {
                        settings.Accounts.ReceiptPrinting = model.EnableReceiptPrinting;
                    }
                    //
                    if (model.PostCompletedPayments != settings.Accounts.PostCompletedPayments)
                    {
                        settings.Accounts.PostCompletedPayments = model.PostCompletedPayments;
                    }

                    //
                    doc["AppSettings"] = JToken.Parse(JsonConvert.SerializeObject(settings));
                    //
                    await System.IO.File.WriteAllTextAsync(path, JsonConvert.SerializeObject(doc));

                    //
                    return RedirectToRoute(FinanceControllerRoute.GetSettings, new { msg = "Your settings where applied successfully." });
                }
                catch
                {
                    ModelState.AddModelError("", "Settings could not be committed. Please try again..");
                }
            }
            //
            return this.View(FinanceControllerAction.UpdateSettings, model);
        }
        #endregion
       
    }
}