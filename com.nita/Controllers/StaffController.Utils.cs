﻿namespace com.nita.Controllers
{
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Boilerplate.AspNetCore;
    using Boilerplate.AspNetCore.Filters;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using com.nita.Constants;
    using com.nita.Services;
    using com.nita.Settings;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using System.Net.Http;
    using Newtonsoft.Json.Linq;
    using System;
    using Microsoft.Extensions.DependencyInjection;
    using com.nita.IdentityModels;
    using com.nita.ViewModels;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Logging;
    using System.Linq;
    
    public sealed partial class StaffController : Controller
    {

        #region Job Catalogue
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("alljob", Name = StaffUtilsControllerRoute.GetAllJobCatalogues)]
        public IActionResult AllJobCatalogues()
        {


            return this.View(StaffUtilsControllerAction.AllJobCatalogues);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addjob", Name = StaffUtilsControllerRoute.GetAddJobCatalogue)]
        public IActionResult AddJobCatalogue()
        {


            return this.View(StaffUtilsControllerAction.AddJobCatalogue);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addjob", Name = StaffUtilsControllerRoute.GetAddJobCatalogue)]
        [ValidateAntiForgeryToken]
        public IActionResult AddJobCatalogue(AddJobCatalogueViewModel model)
        {
            if (ModelState.IsValid)
            {
                //
                var db = provider.GetRequiredService<ApplicationDbContext>();

                
                //
                return RedirectToRoute(StaffUtilsControllerRoute.GetAllJobCatalogues, new { msg = "Job Catalogue was added successfully." });
            }

            return this.View(StaffUtilsControllerAction.AddJobCatalogue, model);
        }
        #endregion

        #region Employment Types
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allemptyps", Name = StaffUtilsControllerRoute.GetAllEmploymentTypes)]
        public IActionResult AllEmployementTypes()
        {


            return this.View(StaffUtilsControllerAction.AllEmploymentTypes);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addemptyp", Name = StaffUtilsControllerRoute.GetAddEmploymentType)]
        public IActionResult AddEmploymentType()
        {


            return this.View(StaffUtilsControllerAction.AddEmploymentType);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addemptyp", Name = StaffUtilsControllerRoute.GetAddEmploymentType)]
        [ValidateAntiForgeryToken]
        public IActionResult AddEmploymentType(AddEmploymentTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                //
                var db = provider.GetRequiredService<ApplicationDbContext>();


                //
                return RedirectToRoute(StaffUtilsControllerRoute.GetAllEmploymentTypes, new { msg = "Employment Type was added successfully." });
            }

            return this.View(StaffUtilsControllerAction.AddEmploymentType, model);
        }
        #endregion

        #region Job Category
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("alljobcat", Name = StaffUtilsControllerRoute.GetAllJobCategories)]
        public IActionResult AllJobCategories()
        {


            return this.View(StaffUtilsControllerAction.AllJobCategories);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addjobcat", Name = StaffUtilsControllerRoute.GetAddJobCategory)]
        public IActionResult AddJobCategory()
        {


            return this.View(StaffUtilsControllerAction.AddJobCategory);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addjobcat", Name = StaffUtilsControllerRoute.GetAddJobCategory)]
        [ValidateAntiForgeryToken]
        public IActionResult AddJobCategory(AddJobCatalogueViewModel model)
        {
            if (ModelState.IsValid)
            {
                //
                var db = provider.GetRequiredService<ApplicationDbContext>();


                //
                return RedirectToRoute(StaffUtilsControllerRoute.GetAllJobCategories, new { msg = "Job Category was added successfully." });
            }

            return this.View(StaffUtilsControllerAction.AddJobCategory, model);
        }
        #endregion

        #region Staff Designation
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allstaffdesg", Name = StaffUtilsControllerRoute.GetAllStaffDesignations)]
        public IActionResult AllStaffDesignations()
        {


            return this.View(StaffUtilsControllerAction.AllStaffDesignations);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addstaffdesg", Name = StaffUtilsControllerRoute.GetAddStaffDesignation)]
        public IActionResult AddStaffDesignation()
        {


            return this.View(StaffUtilsControllerAction.AddStaffDesignation);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addstaffdesg", Name = StaffUtilsControllerRoute.GetAddStaffDesignation)]
        [ValidateAntiForgeryToken]
        public IActionResult AddStaffDesignation(AddStaffDesignationViewModel model)
        {
            if (ModelState.IsValid)
            {
                //
                var db = provider.GetRequiredService<ApplicationDbContext>();


                //
                return RedirectToRoute(StaffUtilsControllerRoute.GetAllStaffDesignations, new { msg = "Staff Designation was added successfully." });
            }

            return this.View(StaffUtilsControllerAction.AddStaffDesignation, model);
        }
        #endregion

        #region Termination Types
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allstermtyp", Name = StaffUtilsControllerRoute.GetAllStaffTerminationTypes)]
        public IActionResult AllStaffTerminationTypes()
        {


            return this.View(StaffUtilsControllerAction.AllStaffTerminationTypes);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addstermtyp", Name = StaffUtilsControllerRoute.GetAddStaffTerminationType)]
        public IActionResult AddStaffTerminationType()
        {


            return this.View(StaffUtilsControllerAction.AddTerminationType);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addstermtyp", Name = StaffUtilsControllerRoute.GetAddStaffTerminationType)]
        [ValidateAntiForgeryToken]
        public IActionResult AddStaffTerminationType(AddTerminationTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                //
                var db = provider.GetRequiredService<ApplicationDbContext>();


                //
                return RedirectToRoute(StaffUtilsControllerRoute.GetAllStaffTerminationTypes, new { msg = "Staff Termination Type was added successfully." });
            }

            return this.View(StaffUtilsControllerAction.AddTerminationType, model);
        }
        #endregion
    }
}

