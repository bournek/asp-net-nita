﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.nita.IdentityModels;
using com.nita.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Nita.ViewModels.CycleViewModels;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;

namespace Nita.Controllers
{
    [Authorize]
    public class CyclesController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IOptions<AppSettings> appSettings;
        private readonly IServiceProvider provider;
        private readonly IHostingEnvironment hostingEnvironment;
        // GET: Cycles
        public CyclesController(
           IServiceProvider prov,
           IHostingEnvironment env,
           IOptions<AppSettings> appSettings, UserManager<ApplicationUser> userManager)
        {
            this.provider = prov;
            this.appSettings = appSettings;
            this.hostingEnvironment = env;
            this._userManager = userManager;
        }
        public ActionResult Index(int? errorCode )
        {
            if (errorCode != 0 && errorCode != null)
            {
                var error = Convert.ToInt32(errorCode);
                if (error == 1)
                {
                    ViewBag.Error = 1;
                    ViewBag.DateMessage = "Start Date must fall within the fiscal year dates";
                }
                if (error == 2)
                {
                    ViewBag.Error = 2;
                    ViewBag.DateMessage = "End Date must fall within the fiscal year";
                }
                if (error == 3)
                {
                    ViewBag.Error = 3;
                    ViewBag.DateMessage = "Cannot Delete cycle because it has some related records";
                }
            }

            var db = provider.GetRequiredService<ApplicationDbContext>();
            var cycles = db.Cycles.ToList();
            var fYears = db.FiscalYears.ToList();
            ViewBag.FYears = fYears;
            ViewBag.Cycles = cycles;
            return View();
        }

        // GET: Cycles/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Cycles/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        // POST: Cycles/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveData(CycleModel cycleModel)
        {
            try
            {
                // TODO: Add insert logic here
                var db = provider.GetRequiredService<ApplicationDbContext>();
                var cycles = new Cycle();
                var id = cycleModel.CycleId;
                var day = cycleModel.Day;
                var month = cycleModel.Month;
                var year = cycleModel.Year;
                var date = day + "/" + month + "/" + year;
                var day1 = cycleModel.Day1;
                var month1 = cycleModel.Month1;
                var year1 = cycleModel.Year1;
                var date1 = day1 + "/" + month1 + "/" + year1;
                var startDate = Convert.ToDateTime(date);
                var endDate = Convert.ToDateTime(date1);
                var fiscalYear = db.FiscalYears.Find(cycleModel.YearId);
                if (fiscalYear.BeginDate > startDate || fiscalYear.EndDate< startDate)
                {
                     return RedirectToAction("Index", new { errorCode = 1 });
                }

                if (fiscalYear.BeginDate > endDate || fiscalYear.EndDate < endDate)
                {
                    
                    return RedirectToAction("Index", new { errorCode = 2 });
                }

                //var duration = cycleModel.Duration;
                if (id == 0)
                {
                    cycles.CycleName = cycleModel.CycleName;
                    cycles.CycleCapacity = Convert.ToInt32(cycleModel.CycleCapacity);
                    cycles.CycleFrom = startDate;
                    cycles.CycleTo = endDate;
                    cycles.FYId = cycleModel.YearId;
                     await db.Cycles.AddAsync(cycles);
                  
                    db.SaveChanges();
                    var markFlags = db.MarkEntryFlags.Add(new MarkEntryFlag
                    {
                        CycleId = cycles.Id
                    });
                    db.SaveChanges();
                }
                else {
                    var cycle = db.Cycles.Find(id);
                    cycle.CycleName = cycleModel.CycleName;
                    cycle.CycleCapacity = Convert.ToInt32(cycleModel.CycleCapacity);
                    cycle.CycleFrom = startDate;
                    cycle.CycleTo = endDate;
                    cycle.FYId = cycleModel.YearId;
                    db.SaveChanges();
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // GET: Cycles/Edit/5
        [HttpGet]
        public JsonResult Edit(int id)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            var cycleData = db.Cycles.Find(id);
            var cid = cycleData.Id;
            var name = cycleData.CycleName;
            var capacity = cycleData.CycleCapacity;
            var day = cycleData.CycleFrom.Day.ToString();
            var month = cycleData.CycleFrom.Month.ToString();
            var year = cycleData.CycleFrom.Year.ToString();
            var day1 = cycleData.CycleTo.Day.ToString();
            var month1 = cycleData.CycleTo.Month.ToString();
            var year1 = cycleData.CycleTo.Year.ToString();
            return Json(new {cid=cid,name=name,day =day,capacity=capacity,month=month,year=year,day1 =day1,month1=month1,year1=year1});
        }

        // POST: Cycles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Cycles/Delete/5
        public ActionResult Delete(int id)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (db.FBudgets.Where(c=>c.CycleId == id).Any())
            {
                return RedirectToAction("Index", new { errorCode = 3 });
            }

            db.Remove(db.Cycles.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: Cycles/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}