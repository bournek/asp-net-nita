﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using com.nita.Constants;
using com.nita.IdentityModels;
using com.nita.Settings;
using com.nita.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Nita.Controllers
{
    [Authorize(DefaultRoles.NitaSysAdmin)]
    public class SettingsController : Controller
    {
        #region Fields
        private readonly IOptions<AppSettings> appSettings;
        private readonly IServiceProvider provider;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly ILogger logger;

        #endregion

        #region Constructors

        public SettingsController(
            IServiceProvider prov,
            IHostingEnvironment env,
            IOptions<AppSettings> appSettings)
        {
            this.provider = prov;
            this.appSettings = appSettings;
            this.hostingEnvironment = env;

            //
            var factory = prov.GetRequiredService<ILoggerFactory>();
            logger = factory.CreateLogger<SettingsController>();
        }

        #endregion
        [HttpGet("gsettings", Name = SettingsControllerRoute.GetIndex)]
        public IActionResult Index()
        {
            return View();
        }
        #region Departments
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("alllocs", Name = SettingsControllerRoute.GetAllLocations)]
        public IActionResult AllLocations()
        {


            return this.View(SettingsControllerAction.AllLocations);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addloc", Name = SettingsControllerRoute.GetAddLocation)]
        public IActionResult AddLocation()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new AddLocationViewModel()
            {
            };

            return this.View(SettingsControllerAction.AddLocation, model);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addloc", Name = SettingsControllerRoute.GetAddLocation)]
        [ValidateAntiForgeryToken]
        public IActionResult AddLocation(AddLocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                if (db.Locations.Any(k => k.Name == model.Name))
                {
                    ModelState.AddModelError("", "A location with a simillar name already exists.");
                    return this.View(SettingsControllerAction.AddLocation, model);
                }
                //Continue
                try
                {
                    var latlng = JsonConvert.DeserializeObject<JsonLatLng>(model.LatLng);
                    //
                    var loc = new Location()
                    {
                        Coodinates = new LatLng()
                        {
                            Latitude = latlng.lat,
                            Longitude = latlng.lng
                        },
                        Name = model.Name
                    };
                    //
                    var county = db.Counties.Single(l => l.Id == model.County);
                    loc.CountyId = county.Id;
                    //
                    db.Locations.Add(loc);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(SettingsControllerRoute.GetAllLocations, new { msg = "Location was added successfully" });

                }
                catch
                {
                    ModelState.AddModelError("", "Location could not be added. Try again..");
                }
            }

            return this.View(SettingsControllerAction.AddLocation, model);
        }
        #endregion

        #region Departments
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("alldepts", Name = SettingsControllerRoute.GetAllDepartments)]
        public IActionResult AllDepartments()
        {


            return this.View(SettingsControllerAction.AllDepartments);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("adddept", Name = SettingsControllerRoute.GetAddDepartment)]
        public IActionResult AddDepartment()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new AddDepartmentViewModel()
            {
            };

            return this.View(SettingsControllerAction.AddDepartment, model);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("adddept", Name = SettingsControllerRoute.GetAddDepartment)]
        [ValidateAntiForgeryToken]
        public IActionResult AddDepartment(AddDepartmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                if (db.HrDepartments.Any(k=>k.Name == model.Name))
                {
                    ModelState.AddModelError("", "A department with a simillar name already exists.");
                    return this.View(SettingsControllerAction.AddDepartment, model);
                }
                //Continue
                try
                {
                    var dept = new Department()
                    {
                        Name = model.Name,
                        Type = model.Type == 0 ? DeptType.Tution : DeptType.Administration
                    };
                    //
                    db.HrDepartments.Add(dept);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(SettingsControllerRoute.GetAllDepartments, new { msg = "Department was added successfully" });

                }
                catch
                {
                    ModelState.AddModelError("", "Department could not be added. Try again..");
                }
            }

            return this.View(SettingsControllerAction.AddDepartment, model);
        }
        #endregion

        #region Divisions
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("alldivs", Name = SettingsControllerRoute.GetAllDivisions)]
        public IActionResult AllDivisions(int sid)
        {


            return this.View(SettingsControllerAction.AllDivisions);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("adddiv", Name = SettingsControllerRoute.GetAddDivision)]
        public IActionResult AddDivision()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new AddDivisionViewModel()
            {
            };

            return this.View(SettingsControllerAction.AddDivision, model);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("adddiv", Name = SettingsControllerRoute.GetAddDivision)]
        [ValidateAntiForgeryToken]
        public IActionResult AddDivision(AddDivisionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                var sect = db.HrSections.Include(k => k.Divisions).Single(l => l.Id == model.Section);
                if (sect.Divisions.Any(k => k.Name == model.Name))
                {
                    ModelState.AddModelError("", "A division with a simillar name already exists in the specified section.");
                    return this.View(SettingsControllerAction.AddDivision, model);
                }
                //
                try
                {
                    var div = new Division()
                    {
                        Name = model.Name,
                        Status = model.Status ? EntityStatus.Active : EntityStatus.Inactive
                    };
                    div.SectionId = sect.Id;

                    //
                    db.HrDivisions.Add(div);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(SettingsControllerRoute.GetAllDivisions, new { msg = "Division was added successfully" });

                }
                catch
                {
                    ModelState.AddModelError("", "Division could not be added. Try again..");
                }
            }

            return this.View(SettingsControllerAction.AddDivision, model);
        }
        #endregion

        #region Sections
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allsects", Name = SettingsControllerRoute.GetAllSections)]
        public IActionResult AllSections(int did)
        {


            return this.View(SettingsControllerAction.AllSections);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addsect", Name = SettingsControllerRoute.GetAddSection)]
        public IActionResult AddSection()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new AddSectionViewModel()
            {
            };

            return this.View(SettingsControllerAction.AddSection, model);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addsect", Name = SettingsControllerRoute.GetAddSection)]
        [ValidateAntiForgeryToken]
        public IActionResult AddSection(AddSectionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                try
                {
                    //
                    var dept = db.HrDepartments.Include(k => k.Sections).Single(l => l.Id == model.Dept);
                    if (dept.Sections.Any(k => k.Name == model.Name))
                    {
                        ModelState.AddModelError("", "A section with a simillar name already exists in the specified department.");
                        return this.View(SettingsControllerAction.AddDivision, model);
                    }

                    //
                    var sect = new Section()
                    {
                        Name = model.Name,
                        Status = model.Status ? EntityStatus.Active : EntityStatus.Inactive
                    };
                    sect.DeptId = dept.Id;
                    //
                    db.HrSections.Add(sect);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(SettingsControllerRoute.GetAllSections, new { msg = "Section was added successfully" });

                }
                catch
                {
                    ModelState.AddModelError("", "Section could not be added. Try again..");
                }
            }

            return this.View(SettingsControllerAction.AddSection, model);
        }
        #endregion

        #region Configurations
        /// <summary>
        /// Get application configuration
        /// </summary>
        /// <returns></returns>
        [HttpGet("config", Name = SettingsControllerRoute.GetConfigurations)]
        public async Task<IActionResult> Config()
        {
            var path = Path.Combine(hostingEnvironment.ContentRootPath, $"config.{this.hostingEnvironment.EnvironmentName}.json");
            try
            {
                var cont = await System.IO.File.ReadAllTextAsync(path);
                //
                var doc = JToken.Parse(cont);
                var settings = JsonConvert.DeserializeObject<AppSettings>((doc["AppSettings"]).ToString());
                //
                return this.View(SettingsControllerAction.Configurations, settings);
            }
            catch (Exception e) { }
            //
            //return new JsonResult(new { s = 1, data = "", errors = "[]" });
            //return this.View(AbnAdminControllerAction.Configurations);
            return new NotFoundResult();
        }
        /// <summary>
        /// Get application configuration
        /// </summary>
        /// <returns></returns>
        [HttpGet("updateconfig", Name = SettingsControllerRoute.GetUpdateConfigurations)]
        public async Task<IActionResult> UpdateConfig()
        {
            var path = Path.Combine(hostingEnvironment.ContentRootPath, $"config.{this.hostingEnvironment.EnvironmentName}.json");
            try
            {
                var cont = await System.IO.File.ReadAllTextAsync(path);
                //
                var doc = JToken.Parse(cont);
                var settings = JsonConvert.DeserializeObject<AppSettings>((doc["AppSettings"]).ToString());
                //
                return this.View(SettingsControllerAction.UpdateConfigurations, settings);
            }
            catch (Exception e) { }
            //
            //return new JsonResult(new { s = 1, data = "", errors = "[]" });
            //return this.View(AbnAdminControllerAction.Configurations);
            return new NotFoundResult();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("updateconfig", Name = SettingsControllerRoute.GetUpdateConfigurations)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateConfig(AppSettings model)
        {
            //This file will override values in config.json on update...
            var path = Path.Combine(hostingEnvironment.ContentRootPath, $"config.{this.hostingEnvironment.EnvironmentName}.json");
            //
            try
            {
                var cont = await System.IO.File.ReadAllTextAsync(path);
                //
                var doc = JToken.Parse(cont);
                //
                if (doc["AppSettings"] == null)
                {
                    doc["AppSettings"] = JsonConvert.SerializeObject(appSettings);
                }
                //parse
                var settings = JsonConvert.DeserializeObject<AppSettings>((doc["AppSettings"]).ToString());
                //Site Tite
                if (model.SiteTitle != settings.SiteTitle)
                    settings.SiteTitle = model.SiteTitle;
                //Short Title
                if (model.SiteShortTitle != settings.SiteShortTitle)
                    settings.SiteShortTitle = model.SiteShortTitle;
               
                //Institution Address
                if (model.Address != settings.Address)
                    settings.Address = model.Address;
                
                //SMTP
                //acc
                if (model.Messaging.Smtp.Account != settings.Messaging.Smtp.Account)
                    settings.Messaging.Smtp.Account = model.Messaging.Smtp.Account;
                //
                if (model.Messaging.Smtp.Password != settings.Messaging.Smtp.Password)
                    settings.Messaging.Smtp.Password = model.Messaging.Smtp.Password;
                //
                if (model.Messaging.Smtp.Port != settings.Messaging.Smtp.Port)
                    settings.Messaging.Smtp.Port = model.Messaging.Smtp.Port;
                //
                if (model.Messaging.Smtp.EnableSsl != settings.Messaging.Smtp.EnableSsl)
                    settings.Messaging.Smtp.EnableSsl = model.Messaging.Smtp.EnableSsl;
                //
                if (model.Messaging.Smtp.Server != settings.Messaging.Smtp.Server)
                    settings.Messaging.Smtp.Server = model.Messaging.Smtp.Server;

               
                //Update
                doc["AppSettings"] = JToken.Parse(JsonConvert.SerializeObject(settings));
                //write
                await System.IO.File.WriteAllTextAsync(path, JsonConvert.SerializeObject(doc));
                //System is watching this file so changes should refect imediately

                //
                return RedirectToRoute(SettingsControllerRoute.GetConfigurations);
            }
            catch (Exception e)
            {
                logger.LogCritical(1, "Store Configuration Failed.", e);
                ModelState.AddModelError(string.Empty, "Store Configuration Failed.");
            }

            //
            //return new JsonResult(new { s = 1, data = "", errors = "[]" });
            return this.View(SettingsControllerAction.UpdateConfigurations, model);
            //return new NotFoundResult();
        }

        #endregion

        #region Utils
        [HttpGet("hrdepts")]
        public JsonResult GetDepartments()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();

            var depts = db.HrDepartments.ToList();
            var d = "[";
            if (depts != null && depts.Count > 0)
            {
                foreach (var dept in depts)
                {
                    d = d + "{\"id\":\"" + dept.Id + "\",\"name\":\"" + dept.Name + "\",\"typ\":\"" + (dept.Type) + "\"},";
                }
                //
                d = d.TrimEnd(',');
            }
            d = d + "]";
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("dsects")]
        public JsonResult GetSections(int did)
        {
            if (did == 0)
            {
                return new JsonResult(new { s = "0", err="[\"Bad Request\"]" });
            }
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();

            var dept = db.HrDepartments.Include(k=>k.Sections).SingleOrDefault(l=>l.Id == did);
            var d = "[";
            if (dept != null && dept.Sections.Count > 0)
            {
                foreach (var sect in dept.Sections)
                {
                    d = d + "{\"id\":\"" + sect.Id + "\",\"name\":\"" + sect.Name + "\"},";
                }
                //
                d = d.TrimEnd(',');
            }
            d = d + "]";
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = d });
        }
        #endregion
    }
}
