﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using com.nita.IdentityModels;
using com.nita.ViewModels;
using com.nita.Services;
using com.nita.Constants;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json.Linq;
using Nita.ViewModels;
using com.nita.Settings;

namespace com.nita.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class BeneficiaryController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public string Message;
        public int ResponseStatus;
        public UserManager<ApplicationUser> UserManager => _userManager;

        public BeneficiaryController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            this._userManager = userManager;
        }

        // [HttpGet("", Name = "Curriculum/GetIndex" /*CurriculumControllerRoute.GetIndex*/)]
        public IActionResult Index(string returnUrl = null)
        {
//            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }


     

        public JsonResult GetTradeAreas(int? categoryId)
        {
            var tradeAreaList = _context
                .TradeAreas
                .OrderBy(e => e.Name)
                .Join(_context.Categories,
                    tradeArea => tradeArea.CategoryId,
                    category => category.Id,
                    (tradeArea, category) =>
                        new {tradeArea.Id, tradeArea.CategoryId, tradeArea.Name, category.CategoryName})
                .ToList();
            var tAList = new List<dynamic>();

            if (tradeAreaList.Count > 0)
            {
                foreach (var ta in tradeAreaList)
                {
                    var smallTList = new
                    {
                        tradeAreaId = ta.Id,
                        categoryName = ta.CategoryName,
                        CategoryId = ta.CategoryId,
                        name = ta.Name,

                        totalExams = _context.NitaExams.Count(p => p.TradeAreaId == ta.Id)
                    };

                    tAList.Add(smallTList);
                }

                ;
            }


            return Json(new
                {
                    status = tAList.Count > 0 ? 1 : 0,
                    data = tAList
                }
            );
        }


        [HttpPost]
        public JsonResult AddCategory([FromBody] Category category)
        {
            _context.Categories.Add(category);
            _context.SaveChanges();

            return Json(new
                {
                    status = 1,
                    message = "Saved Successfully",
                    data = ""
                }
            );
        }

        public JsonResult AddAddress([FromBody] TraineeAddressViewModel userAddress)
        {
            // TraineeAddressViewModel userAddress = (TraineeAddressViewModel)traineeInfo;


            var address = new UserAddress
            {
                CountyId = userAddress.CountyId,
                District = userAddress.District,
                HomeTel = userAddress.HomeTel,
                PAddress = userAddress.PAddress,
                PostalCode = userAddress.PostalCode,
                City = userAddress.City
            };
            _context.Address.Add(address);
            _context.SaveChanges();

            return Json(new
                {
                    status = 1,
                    message = "Address  Saved Successfully",
                    data = address
                }
            );
        }

        public JsonResult SaveMultipleTraine([FromBody] TraineeViewModelTradeAreasMultiple traineeTradeArea)
        {

            string[] traineeIds = traineeTradeArea.TraineeIds.Split(',');
            int traines = 0;
            for (var i = 0; i < traineeIds.Length; i++)
            {
                traines++;
                var traineeTradeAreaModel = new TraineeTradeArea
                {
                    TrainerId = traineeTradeArea.TrainerId,
                    TradeAreaId = traineeTradeArea.TradeAreaId,
                    TraineeId = Convert.ToInt32(traineeIds[i])
                };

                _context.TraineeTradeAreas.Add(traineeTradeAreaModel);
                _context.SaveChanges();

                Beneficiary trainee = _context.Beneficiary.First(b => b.Id == Convert.ToInt32(traineeIds[i]));
                trainee.PlacedStatus = true;
            }
            
            _context.SaveChanges();
            ResponseStatus = 1;

            return Json(new
                {
                    status = 1,
                    message = traines + " Beneficiar(ies) Successfully Placed",
                    data = traineeTradeArea
            }
            );
        }
        public JsonResult AddTraineeTradeArea([FromBody] TraineeViewModelTradeAreas traineeTradeArea)
        {
            // TraineeAddressViewModel userAddress = (TraineeAddressViewModel)traineeInfo;


            var traineeTradeAreaModel = new TraineeTradeArea
            {
                TrainerId = traineeTradeArea.TrainerId,
                TradeAreaId = traineeTradeArea.TradeAreaId,
                TraineeId = traineeTradeArea.TraineeId,
                BeneficiaryId =  traineeTradeArea.TraineeId
            };
            _context.TraineeTradeAreas.Add(traineeTradeAreaModel);
            _context.SaveChanges();

            Beneficiary trainee = _context.Beneficiary.First(b => b.Id == traineeTradeArea.TraineeId);
            trainee.PlacedStatus = true;
            _context.SaveChanges();
            ResponseStatus = 1;

            return Json(new
                {
                    status = 1,
                    message = "Trade area successfully added To Trainee",
                    data = traineeTradeAreaModel
                }
            );
        }

        public int SaveTraineeApplication(ApplicationUser userData)
        {
            var user = new ApplicationUser()
            {
                UserName = "ABN_TP7",
                Email = "osoro@gmail.com",
                PhoneNumber = "14521254",
                AccountType = AccountType.Trainee
            };

            //Create USer Account
            var result = UserManager.CreateAsync(user, "123456789nita");

            return user.Id;
        }

        [HttpPost]
        public async Task<JsonResult> AddTrainee([FromBody] TraineePersonalViewModel traineProfile)
        {
            var user = new ApplicationUser()
            {
                UserName = traineProfile.AdminNo,
                Email = traineProfile.Email,
                PhoneNumber = "14521254",
                AccountType = AccountType.Trainee
            };

            //Create USer Account
            var result = await UserManager.CreateAsync(user, "123456789nita");


            var traineeProfile = new Beneficiary
            {
                AddressId = traineProfile.AddressId,
                AdminNo = traineProfile.AdminNo,
                ApplicationUserId = user.Id,
                DateOfBirth = traineProfile.DateOfBirth,
                Disability = traineProfile.Disability,
                Domicile = traineProfile.Domicile,
                FullName = traineProfile.FullName,
                Gender = traineProfile.Gender,
                Marital = traineProfile.Marital,
                CycleId = traineProfile.CycleId,
                PassportNo = traineProfile.PassportNo,
            };

            var traineeStageProgress = new BeneficiaryStage
            {

            };

            _context.Beneficiary.Add(traineeProfile);
            //checking some condition i dont know
           
             _context.SaveChanges();
            return Json(new
                {
                    status = 1,
                    message = " Trainee details  Saved Successfully",
                    data = traineeProfile
                }
            );
        }

        public JsonResult GetCounties()
        {
            var counties = _context.Counties.ToList();

            return Json(new
                {
                    status = counties.Count > 0 ? 1 : 0,
                    message = " County data retrieved Successfully",
                    data = counties
                }
            );
        }

        public JsonResult GetBanks()
        {
            var banks = _context.PBanks.ToList();

            return Json(new
                {
                    status = banks.Count > 0 ? 1 : 0,
                    message = " Bank data retrieved Successfully",
                    data = banks
            }
            );
        }

        public JsonResult GetBankBranches(int id)
        {
            var branches = _context.BankBranch.Where(b => b.BankId == id).ToList();


            return Json(new
                {
                    status = branches.Count > 0 ? 1 : 0,
                message = " Bank Branches data retrieved Successfully",
                    data = branches
            }
            );
        }
        public JsonResult AddAttendance([FromBody] List<TraineeAttendanceViewDetails> traineeAttendance)
        {
            if (traineeAttendance.Count > 0)
            {
                foreach (var traineeAtt in traineeAttendance)
                {
                    var trAttendance = new Attendance
                    {
                        BeneficiaryId = traineeAtt.BeneficiaryId,
                        NumberOfType = traineeAtt.NumberOfType,
                        Type = traineeAtt.Type,
                        DaysAttended = traineeAtt.DaysAttended

                    };
                    _context.Attendances.Add(trAttendance);
                }

                _context.SaveChanges();

                Message = "Attendance Saved Successfully";
                ResponseStatus = 1;
            }
            else
            {
                Message = "Attendance Could not be saved";
                ResponseStatus = 0;
            }

            return Json(new
                {
                    status = 1,
                    message = Message,
                    data = traineeAttendance
            }
            );
        }

        public JsonResult AddTraineeBank([FromBody] TraineeAccountViewDetails traineAccount)
        {
            var bank = new TraineeAccount
            {
                BankBranchId = traineAccount.BankBranch,
                BankId = traineAccount.Bank,
                AccountName = traineAccount.AccountName,
                AccountNumber = traineAccount.AccountNumber,
                TraineeId = traineAccount.TraineeId
            };
            if (true)
            {
                _context.TraineeAccount.Add(bank);
            }

            _context.SaveChanges();
            return Json(new
                {
                    status = 1,
                    message = " Bank account  Saved Successfully",
                    data = bank
                }
            );
        }

        public JsonResult GetProviders(int countyId, string searchString = "")
        {
            var providers = _context.Trainers
                .OrderBy(t => t.FullName)
                .Join(_context.Address,
                    trainers => trainers.AddressId,
                    userAddress => userAddress.Id,
                    (trainers, address) => new
                    {
                        trainers.FullName,
                        trainers.Id,
                        trainers.ContactPersonName,
                        address.CountyId
                    })
                .Where(t => (t.FullName.Contains(searchString)|| t.ContactPersonName.Contains(searchString)) && t.CountyId == countyId)
                .Take(1)
                .ToList();

            var providerWithCourse = new List<dynamic>();

            foreach (var provider in providers)
            {
                var courses = _context.TrainerContract
                    //.Where(p=>p.TrainerId==provider.Id)
                    .Join(_context.TradeAreaContract,
                        trainerContract => trainerContract.Id,
                        tradeAreaContract => tradeAreaContract.ContractId,
                        (tc, tac) =>
                            new {tc.TrainerId, tac.TradeAreaId}
                    )
                    .Join(_context.TradeAreas,
                        contractTradeAreas => contractTradeAreas.TradeAreaId,
                        tradeAreas => tradeAreas.Id,
                        (cta, ta) => new
                        {
                            cta.TradeAreaId,
                            cta.TrainerId,
                            ta.Name
                        }
                    )
                    .ToList();
                providerWithCourse.Add(new {provider, courses});
            }

            return Json(new
                {
                    status = providerWithCourse.Count > 0 ? 1 : 0,
                    message = "providers & courses fetched Successfully",
                    data = providerWithCourse
            }
            );
        }

        public JsonResult GetProviderWithTrainees(int trainerId)
        {
            var providers = _context.TraineeTradeAreas
                .Join(_context.Trainers,
                    traineeTradeArea => traineeTradeArea.TrainerId,
                    trainer => trainer.Id,
                    
                    (traineeTradeArea,trainer) => new
                    {
                        trainer.Id,trainer.FullName,traineeTradeArea.TradeAreaId,traineeTradeArea.TraineeId
                    }
                )
                .Where(t=>t.Id==trainerId)
                .Join(_context.TradeAreas,
                     providerTraineeTa=> providerTraineeTa.TradeAreaId,
                    tradeAreas=>tradeAreas.Id,
                    (providerTraineeTa, tradeAreas) => new
                    {
                        providerTraineeTa.Id,
                        providerTraineeTa.FullName,
                        providerTraineeTa.TradeAreaId,
                        providerTraineeTa.TraineeId,
                        tradeAreas.Name
                    }
                    )
                .Join(_context.Beneficiary,
                    providerTradeArea=> providerTradeArea.TraineeId,
                    trainee=>trainee.Id,
                    (providerTradeArea,trainee)=>new
                    {
                        
                        providerTradeArea.Id,
                        providerTradeArea.FullName,
                        providerTradeArea.TradeAreaId,
                        providerTradeArea.TraineeId,
                        providerTradeArea.Name,
                        trainee
                    })
                .ToList();


            return Json(new
                {
                    status = providers.Count > 0 ? 1 : 0,
                    message = "providers,beneficiary,trade areas fetched Successfully",
                    data = providers
            }
            );
        }




        public JsonResult GetAllProviders(int countyId, int type=445, string district = "", string searchString = "" )
        {
            var providers = _context.Trainers
                .OrderBy(t => t.FullName)
                .Join(_context.Address,
                    trainers => trainers.AddressId,
                    userAddress => userAddress.Id,
                    (trainers, address) => new
                    {
                        trainers.FullName,
                        trainers.Telephone,
                        trainers.Id,
                        trainers.Type,
                        trainers.ContactPersonName,
                        address.CountyId,
                        address.County.Name,
                        address.City
                    })
                .Where(t => (t.FullName.Contains(searchString)
                             || t.ContactPersonName.Contains(searchString))
                            && t.CountyId == countyId &&((int)t.Type)==type)
                .ToList();

            var providerWithCourse = new List<dynamic>();

            foreach (var provider in providers)
            {
                var courses = _context.TrainerContract
                    .Where(p => p.TrainerId == provider.Id)
                    .Join(_context.TradeAreaContract,
                        trainerContract => trainerContract.Id,
                        tradeAreaContract => tradeAreaContract.ContractId,
                        (tc, tac) =>
                            new {tc.TrainerId, tac.TradeAreaId}
                    )
                    .Join(_context.TradeAreas,
                        contractTradeAreas => contractTradeAreas.TradeAreaId,
                        tradeAreas => tradeAreas.Id,
                        (cta, ta) => new
                        {
                            cta.TradeAreaId,
                            cta.TrainerId,
                            ta.Name
                        }
                    )
                    .ToList();
                providerWithCourse.Add(new {provider, courses});
            }

            return Json(new
                {
                    status = 1,
                    message = "providers and courses fetched Successfully",
                    data = providerWithCourse
                }
            );
        }




        public JsonResult DeleteTrainee(int id)
        {
            try
            {
                Beneficiary trainee = _context.Beneficiary.First(b => b.Id == id);
                trainee.Status = 0;
                _context.SaveChanges();
                ResponseStatus = 1;
                this.Message = "Beneficiary Deleted successfully";
            }
            catch (DbUpdateException Ex)
            {
                Message = Ex.Message;
                ResponseStatus = 0;
            }

            return Json(new
            {
                status = ResponseStatus,
                message = Message
            });
        }

        public JsonResult ApproveTrainee(int id)
        {
            try
            {
                Beneficiary trainee = _context.Beneficiary.First(b => b.Id == id);
                trainee.ApprovedStatus = true;
                _context.SaveChanges();
                ResponseStatus = 1;
                this.Message = "Beneficiary Approved successfully";
            }
            catch (DbUpdateException Ex)
            {
                Message = Ex.Message;
                ResponseStatus = 0;
            }

            return Json(new
            {
                status = ResponseStatus,
                message = Message
            });
        }


        public JsonResult GetTrainees(string searchKey = "")
        {
            var beneficiaryList = _context
                .Beneficiary
                .Join(_context.Address,
                    beneficiary => beneficiary.AddressId,
                    userAddress => userAddress.Id,
                    (beneficiary, userAddress) =>
                        new {beneficiary, userAddress.County.Name, userAddress})
                .Where(beneficiary => beneficiary.beneficiary.Status != 0)
                .Where(beneficiary => beneficiary.beneficiary.FullName.Contains(searchKey))
                .OrderBy(e => e.beneficiary.PassportNo)
                .ToList();
            var finaList = new List<dynamic>();
            foreach (var beneficiary in beneficiaryList)
            {
                finaList.Add(new
                {
                    beneficiary = beneficiary.beneficiary,
                    userAddress = beneficiary.userAddress,
                    county = beneficiary.Name
                });
            }

            return Json(new
                {
                    status = finaList.Count > 0 ? 1 : 0,
                    data = finaList
                }
            );
        }

        public JsonResult SearchGetTrainees(int countyId, string district = "", string passportNo = "")
        {
            var beneficiaryList = _context
                .Beneficiary
                .Join(_context.Address,
                    beneficiary => beneficiary.AddressId,
                    userAddress => userAddress.Id,
                    (beneficiary, userAddress) =>
                        new {beneficiary, userAddress.County.Name, userAddress})
                .Where(beneficiary => beneficiary.beneficiary.Status != 0)
                .Where(county => county.userAddress.CountyId == countyId &&
                                 (county.userAddress.District.Contains(district) ||
                                  county.beneficiary.PassportNo.Contains(passportNo)))
                .OrderBy(e => e.beneficiary.PassportNo)
                .ToList();
            var finaList = new List<dynamic>();
            foreach (var beneficiary in beneficiaryList)
            {
                finaList.Add(new
                {
                    beneficiary = beneficiary.beneficiary,
                    userAddress = beneficiary.userAddress,
                    county = beneficiary.Name
                });
            }

            return Json(new
                {
                    status = finaList.Count > 0 ? 1 : 0,
                    data = finaList
                }
            );
        }

        public JsonResult SearchGetTraineesNarrow(int countyId,string trainerId="",string tradeAreaId="", string district = "")
        {
            var beneficiaryList = _context
                .Beneficiary
                .Join(_context.Address,
                    beneficiary => beneficiary.AddressId,
                    userAddress => userAddress.Id,
                    (beneficiary, userAddress) =>
                        new { beneficiary, userAddress.County.Name, userAddress })

                 .Join(_context.TraineeTradeAreas,
                         ben=> ben.beneficiary.Id,
                         traineeTradeArea=> traineeTradeArea.TraineeId,
                    (ben, traineeTradeArea) => new
                    {
                        ben.beneficiary.Id,
                        ben.beneficiary.FullName,
                        ben.userAddress.County.Name,
                        ben.userAddress.CountyId,
                        ben.userAddress.District,
                        ben.beneficiary.PassportNo,
                        ben.beneficiary.PlacedStatus,
                        ben.beneficiary.Status,
                        traineeTradeArea.TradeAreaId,
                        traineeTradeArea.DateCreated
                    }
                )
                .Where(beneficiary => beneficiary.Status != 0&&beneficiary.PlacedStatus==true)
                .Where(county => county.CountyId == countyId &&
                                 (county.District.Contains(district) ))
                
                .OrderBy(e => e.PassportNo)
                .ToList();
            var finaList = new List<dynamic>();
            foreach (var beneficiary in beneficiaryList)
            {
                finaList.Add(new
                {
                    beneficiary = beneficiary,
                    Attendance = 0
                });
            }

            return Json(new
                {
                    status = finaList.Count > 0 ? 1 : 0,
                    data = finaList
            }
            );
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(DefaultRoles.NitaCoordinator)]
        public IActionResult GetAttendance()
        {
            return View("Attendance");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(DefaultRoles.NitaCoordinator)]
        public IActionResult AddAttendance()
        {
            return View("AddAttendance");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(DefaultRoles.NitaCoordinator)]
        [ValidateAntiForgeryToken]
        public IActionResult AddAttendance(AttendanceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var cycle = _context.Cycles.SingleOrDefault(k => k.Id == model.Cycle);
                //
                if (cycle == null)
                {
                    ModelState.AddModelError("", "Cycle not found");
                    return View("AddAttendance",model);
                }
                else
                {
                    //Validate cycle state
                }
                //
                var county = _context.Counties.SingleOrDefault(k => k.Id == model.County);
                //
                var tp = _context.Trainers.SingleOrDefault(k => k.Id == model.Trainer);
                //
                var payperiod = _context.PIrrPaymentProjects.SingleOrDefault(l => (l.CloseDate.CompareTo(DateTime.Now) >= 0 && l.EffectiveDate.CompareTo(cycle.CycleFrom) >= 0 && l.EndDate.CompareTo(cycle.CycleTo) <= 0));
                //
                var personnel = _context.HrStaff.Include(l => l.ApplicationUser).Single(l => l.ApplicationUser.UserName == User.Identity.Name);
                //
                if(payperiod != null)
                {
                    var list = new List<TraineeAttendance>();
                    foreach(var i in model.Attendance)
                    {
                        var ben = _context.Trainees.Include(k=>k.Address).ThenInclude(l=>l.County).Include(l=>l.Trainer).SingleOrDefault(s => s.Id == i);
                        //
                        if(ben == null)
                        {
                            ModelState.AddModelError("", "Beneficiary with Id '" + 1 + "' was not found in the database.");
                            continue;
                        }
                        //
                        if (ben.Trainer == null || ben.Trainer.Id != tp.Id)
                        {
                            ModelState.AddModelError("", "Beneficiary with Id '" + 1 + "' was not found under '"+tp.FullName+"'.");
                            continue;
                        }
                        //
                        if (ben.Address.County.Id != county.Id)
                        {
                            ModelState.AddModelError("", "Beneficiary with Id '" + 1 + "' was not found in '"+county.Name+"' county.");
                            continue;
                        }

                        //
                        var at = new TraineeAttendance()
                        {
                            Date = model.Date,
                            Status = AttendanceState.Present,
                            Notes = model.Notes
                        };
                        at.PersonnelId = personnel.Id;
                        at.TraineeId = ben.Id;
                        //
                        list.Add(at);
                    }

                    //
                    if(list.Count > 0)
                    {
                        _context.AttendanceRegister.AddRange(list);
                        _context.SaveChanges();
                    }

                    //
                    if(ModelState.ErrorCount == 0)
                    {
                        return RedirectToAction(actionName: "Attendance");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "No Active Payment period was found in the specified cycle..");
                }

            }
            return View("AddAttendance");
        }
        [HttpGet]
        [Authorize(DefaultRoles.NitaCoordinator)]
        public IActionResult UpdateAttendance()
        {
            return View("UpdateAttendance");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(DefaultRoles.NitaCoordinator)]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateAttendance(AttendanceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var cycle = _context.Cycles.SingleOrDefault(k => k.Id == model.Cycle);
                //
                if(cycle == null)
                {
                    ModelState.AddModelError("", "Cycle not found");
                }
                else
                {
                    //Validate cycle state
                }

                //var payperiod = _context.PIrrPaymentProjects.SingleOrDefault(l=>(l.CloseDate.CompareTo(DateTime.Now) >= 0 && )

            }
            return View("UpdateAttendance");
        }
    }
}