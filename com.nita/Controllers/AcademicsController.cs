﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using com.nita.IdentityModels;
using com.nita.ViewModels;
using com.nita.Services;
using com.nita.Constants;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json.Linq;
using Nita.ViewModels;

namespace Nita.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class AcademicsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public string Message;
        public int ResponseStatus;
        public UserManager<ApplicationUser> UserManager => _userManager;

        public AcademicsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            this._userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }


        public JsonResult AddTraineeReporting([FromBody] List<AcademicReportStageViewModel> traineeAttendance)
        {
            if (traineeAttendance.Count > 0)
            {
                int noOfTrainees = 0;
                foreach (var traineeAtt in traineeAttendance)
                {
                    noOfTrainees++;
                    var traineeReporting = new BeneficiaryStage
                    {
                        BeneficiaryId = traineeAtt.BeneficiaryId,
                        ReportStage = true,
                        ReportStageDate = traineeAtt.ReportStageDate
                    };

                    _context.BeneficiaryStages.Add(traineeReporting);
                }

                _context.SaveChanges();
                Message = noOfTrainees+ " Beneficiar(ies) Reported Successfully";
                ResponseStatus = 1;
            }
            else

            {
                Message = "Attendance Could not be saved";
                ResponseStatus = 0;
            }
            return Json(new
                {
                    status = 1,
                    message = Message,
                    data = traineeAttendance
                }
            );
        }

        public JsonResult AssignTraineeToEmployer([FromBody] AcademicTraineeEmployerViewModel academicTraineeEmployerViewModel)
        {


            var academicTraineeEmployer = new TraineeProviderEmployer
            {
                BeneficiaryId = academicTraineeEmployerViewModel.BeneficiaryId,
                EmployerId = academicTraineeEmployerViewModel.EmployerId,
                ContactPerson = academicTraineeEmployerViewModel.ContactPerson,
                ContactPersonMobile = academicTraineeEmployerViewModel.ContactPersonMobile,
                ContactPersonEmail = academicTraineeEmployerViewModel.ContactPersonEmail
            };

            _context.TraineeProviderEmployers.Add(academicTraineeEmployer);

            var ben = _context.BeneficiaryStages
                .FirstOrDefault(b => b.BeneficiaryId == academicTraineeEmployerViewModel.BeneficiaryId);
            if (ben != null)
            {
                ben.InternshipStage = true;
                ben.InternshipStageDate = DateTime.Now;
            }

            _context.SaveChanges();

            return Json(new
                {
                    status = 1,
                    message = "Trainee has been successfully assigned an employer",
                    data = academicTraineeEmployer
            }
            );

        }

        public JsonResult SearchGetTraineesNarrow(int countyId, string trainerId = "", string tradeAreaId = "", string district = "")
        {
            var beneficiaryList = _context
                .Beneficiary
                .Join(_context.Address,
                    beneficiary => beneficiary.AddressId,
                    userAddress => userAddress.Id,
                    (beneficiary, userAddress) =>
                        new { beneficiary, userAddress.County.Name, userAddress })

                 .Join(_context.TraineeTradeAreas,
                         ben => ben.beneficiary.Id,
                         traineeTradeArea => traineeTradeArea.TraineeId,
                    (ben, traineeTradeArea) => new
                    {
                        ben.beneficiary.Id,
                        ben.beneficiary.FullName,
                        ben.userAddress.County.Name,
                        ben.userAddress.CountyId,
                        ben.userAddress.District,
                        ben.beneficiary.PassportNo,
                        ben.beneficiary.PlacedStatus,
                        ben.beneficiary.Status,
                        traineeTradeArea.TrainerId,
                        traineeTradeArea.TradeAreaId,
                        traineeTradeArea.DateCreated
                    }
                )
                .Where(beneficiary => beneficiary.Status != 0 && beneficiary.PlacedStatus == true)

                .Where(county => county.CountyId == countyId &&
                                 (county.District.Contains(district)))

                .OrderBy(e => e.PassportNo)
                .ToList();
            var finaList = new List<dynamic>();
            foreach (var beneficiary in beneficiaryList)
            {
                finaList.Add(new
                {
                    beneficiary = beneficiary,
                    Attendance = 0,
                    AlreadyReported = _context.BeneficiaryStages.FirstOrDefault(b => b.BeneficiaryId==beneficiary.Id)
                });
            }

            return Json(new
            {
                status = finaList.Count > 0 ? 1 : 0,
                data = finaList
            }
            );
        }

        public JsonResult GetTraineesForScoring(int countyId,string trainerId="",string district="",string tradeAreaId="")
        {
            var beneficiaries = _context
                .Beneficiary
                .Include(k=>k.BeneficiaryStage)
                .Include(m=>m.InternshipScore)
                .Include(t=>t.TraineeTradeArea.TradeArea)
               .Where(b=>b.Address.CountyId==countyId)
                .ToList();

            

            return Json(new
                {
                    status = beneficiaries.Count > 0 ? 1 : 0,
                    data = beneficiaries
            }
            );


        }

        public JsonResult SearchGetTrainees(int countyId, string district = "", string passportNo = "")
        {
            var beneficiaryList = _context
                .Beneficiary
                .Join(_context.Address,
                    beneficiary => beneficiary.AddressId,
                    userAddress => userAddress.Id,
                    (beneficiary, userAddress) =>
                        new { beneficiary, userAddress.County.Name, userAddress })
               // .Include(k=>k.beneficiary.BeneficiaryStage)
                .Where(beneficiary => beneficiary.beneficiary.Status != 0)
                .Where(county => county.userAddress.CountyId == countyId &&
                                 (county.userAddress.District.Contains(district) ||
                                  county.beneficiary.PassportNo.Contains(passportNo)))
                .OrderBy(e => e.beneficiary.PassportNo)
                .ToList();
            var finaList = new List<dynamic>();
            foreach (var beneficiary in beneficiaryList)
            {
                finaList.Add(new
                {
                    beneficiary = beneficiary.beneficiary,
                    userAddress = beneficiary.userAddress,
                    county = beneficiary.Name
                });
            }

            return Json(new
                {
                    status = finaList.Count > 0 ? 1 : 0,
                    data = finaList
                }
            );
        }

        public JsonResult GetTrainerEmployers(int id)
        {

            var employers = _context.TrainerEmployer.Where(e => e.TrainerId == id).ToList();
            return Json(new
                {
                    status = employers.Count > 0 ? 1 : 0,
                    data = employers
            }
            );


        }

        public JsonResult GetTraineeEmployer(int beneficiaryId)
        {

            var employers = _context
                .TraineeProviderEmployers
                .Join(_context.TrainerEmployer,
                      tpe=> tpe.EmployerId,
                      te=>te.Id,
                    (tpe,te)=>new
                    {
                        tpe.Id,tpe.DateCreated,tpe.BeneficiaryId,te.Name,te.Industry,tpe.ContactPerson,tpe.ContactPersonEmail,tpe.ContactPersonMobile
                    })
                .FirstOrDefault(e => e.BeneficiaryId == beneficiaryId)
                ;

            return Json(new
                {
                    status = 1,
                    data = employers
            }
            );

        }
    }




}