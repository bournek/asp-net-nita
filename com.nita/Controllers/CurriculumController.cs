﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using com.nita.IdentityModels;
using com.nita.ViewModels;
using com.nita.Services;
using com.nita.Constants;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json.Linq;
using Nita.ViewModels;

namespace com.nita.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class CurriculumController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CurriculumController(ApplicationDbContext context)
        {
            _context = context;
        }

        // [HttpGet("", Name = "Curriculum/GetIndex" /*CurriculumControllerRoute.GetIndex*/)]
        public IActionResult Index(string returnUrl = null)
        {
//            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        public IActionResult AddTradeArea()
        {
            return View();
        }


        public IActionResult Phases()
        {
            return View();
        }

        public JsonResult GetCategories()
        {
            var categoryList = _context
                .Categories
                .OrderBy(e => e.CategoryName)
                .ToList();

            return Json(new
                {
                    status = categoryList.Count > 0 ? 1 : 0,
                    data = categoryList
                }
            );
        }
        public JsonResult GetCycles()
        {
            var cycles = _context
                .Cycles
                .OrderBy(e => e.CycleFrom)
                .ToList();

            return Json(new
                {
                    status = cycles.Count > 0 ? 1 : 0,
                    data = cycles
            }
            );
        }
        public JsonResult GetTradeAreas(int? categoryId)
        {
            var tradeAreaList = _context
                .TradeAreas
                .OrderBy(e => e.Name)
                .Join(_context.Categories,
                    tradeArea => tradeArea.CategoryId,
                    category => category.Id,
                    (tradeArea, category) =>
                        new {tradeArea.Id, tradeArea.CategoryId, tradeArea.Name, category.CategoryName})
                .ToList();
            var tAList = new List<dynamic>();
           
            if (tradeAreaList.Count > 0)
            {
                
                foreach (var ta in tradeAreaList)
                {
                    
                    var smallTList = new
                    {
                        tradeAreaId = ta.Id,
                        categoryName = ta.CategoryName,
                        CategoryId = ta.CategoryId,
                        name = ta.Name,
                        
                        totalExams = _context.NitaExams.Count(p => p.TradeAreaId == ta.Id)
                    };
                    
                    tAList.Add(smallTList);
                }

                ;
            }


            return Json(new
                {
                    status = tAList.Count > 0 ? 1 : 0,
                    data = tAList
            }
            );
        }


        [HttpPost]
        public JsonResult AddCategory([FromBody] Category category)
        {
            _context.Categories.Add(category);
            _context.SaveChanges();

            return Json(new
                {
                    status = 1,
                    message = "Saved Successfully",
                    data = ""
                }
            );
        }

        public JsonResult AddCourseTradeArea([FromBody] TradeArea tradeArea)
        {
            _context.TradeAreas.Add(tradeArea);
            _context.SaveChanges();

            return Json(new
                {
                    status = 1,
                    message = " Tradea area  Saved Successfully",
                    data = ""
                }
            );
        }

        public JsonResult AddCourseExam([FromBody] ExamViewModel examViewModel)
        {
            var nitaExam = new NitaExam
            {
                TradeAreaId = examViewModel.TradeAreaId,
                CycleId = examViewModel.CycleId,
                Name = examViewModel.Name,
                TotalScore = examViewModel.TotalScore,
            };


            if (examViewModel.ExamIsDoneAcross == true)
            {
                var tradeAreas = _context.TradeAreas.ToList();
                foreach (var ta in tradeAreas)
                {
                    _context.NitaExams.Add(new NitaExam
                    {
                        TradeAreaId = ta.Id,
                        CycleId = examViewModel.CycleId,
                        Name = examViewModel.Name,
                        TotalScore = examViewModel.TotalScore,
                    });
                }
            }
            else
            {
                _context.NitaExams.Add(nitaExam);
            }

            _context.SaveChanges();

            return Json(new
                {
                    status = 1,
                    message = "Exam  Saved Successfully",
                    data = ""
                }
            );
        }
    }
}