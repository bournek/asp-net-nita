﻿namespace com.nita.Controllers
{
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Boilerplate.AspNetCore;
    using Boilerplate.AspNetCore.Filters;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using com.nita.Constants;
    using com.nita.Services;
    using com.nita.Settings;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using System.Net.Http;
    using Newtonsoft.Json.Linq;
    using System;
    using Microsoft.Extensions.DependencyInjection;
    using com.nita.IdentityModels;
    using com.nita.ViewModels;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Logging;
    using System.Linq;

    [Authorize]
    //[Route("[controller]/[action]")]
    public sealed partial class StaffController : Controller
    {
        #region Fields
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IOptions<AppSettings> appSettings;
        private readonly IServiceProvider provider;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly ILogger logger;
        
        #endregion

        #region Constructors

        public StaffController(
            UserManager<ApplicationUser> userManager,
            IServiceProvider prov,
            IHostingEnvironment env,
            IOptions<AppSettings> appSettings)
        {
            this.provider = prov;
            this.appSettings = appSettings;
            this.hostingEnvironment = env;
            this._userManager = userManager;

            //
            var factory = prov.GetRequiredService<ILoggerFactory>();
            logger = factory.CreateLogger<StaffController>();
        }

        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("staff", Name = StaffControllerRoute.GetIndex)]
        public IActionResult Index()
        {
            

            return this.View(StaffControllerAction.Index);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allstaff", Name = StaffControllerRoute.GetAllStaff)]
        public IActionResult AllStaff()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(StaffControllerAction.AllStaff);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("addstaff", Name = StaffControllerRoute.AddStaff)]
        public IActionResult AddStaff()
        {
            var model = new AddStaffViewModel();
            

            return this.View(StaffControllerAction.AddStaff,model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addstaff", Name = StaffControllerRoute.AddStaff)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddStaff(AddStaffViewModel model)
        {
            //
            if (ModelState.IsValid)
            {
                //App USer
                var user = new ApplicationUser()
                {
                    UserName = model.EmployeeNumber,
                    Email = model.PersonalEmail,
                    PhoneNumber = model.PhoneNumber,
                    AccountType = AccountType.NitaStaff
                };

                //Create USer Account
                var result = await _userManager.CreateAsync(user, appSettings.Value.DefaultPassword);
                logger.LogInformation(1, "Crated Account "+user.UserName);
                if (result.Succeeded)
                {
                    try
                    {
                        //
                        var db = provider.GetRequiredService<ApplicationDbContext>();

                        BankBranch branch = null;
                        var br = db.PBanks.Include(l => l.Branches).Select(p => p.Branches.Single(l => l.Id == model.BankBranchId));
                        if (!br.Any())
                        {
                            ModelState.AddModelError(string.Empty, "Please select a valid bank branch and try again.");
                            return this.View();
                        }
                        else
                        {
                            branch = br.First();
                        }
                        //County
                        var county = await db.Counties.SingleAsync(j => j.Id == model.County);
                        //address
                        var address = new UserAddress();
                        address.City = model.City;
                        address.CountyId = county.Id;
                        address.Nationality = model.Citizenship;
                        address.PAddress = model.Address;
                        address.PostalCode = model.PostalCode;
                        address.Street = model.Street;
                        address.WEmail = model.WorkEmail;
                        //Add
                        db.Addresses.Add(address);
                        db.SaveChanges();

                        //Bank Account
                        var bacc = new BankAccount()
                        {
                            Name = model.BankAccName,
                            Number = model.BankAccNo,
                            BranchId = branch.Id
                        };
                        db.PBankAccounts.Add(bacc);
                        db.SaveChanges();
                        //Staff
                        var staffProfile = new StaffProfile();
                        staffProfile.FullName = model.FullName;
                        //Salutation
                        staffProfile.Title = model.Title == (int)Title.Hon ? Title.Hon :
                            model.Title == (int)Title.Miss ? Title.Miss :
                            model.Title == (int)Title.Mr ? Title.Mr :
                            model.Title == (int)Title.Mrs ? Title.Mrs :
                            model.Title == (int)Title.Rev ? Title.Rev :
                            model.Title == (int)Title.Sr ? Title.Sr : Title.Other;

                        //HR Details;
                        //
                        staffProfile.EmplNo = model.EmployeeNumber;
                        //
                        staffProfile.KraPin = model.KraPIN;
                        staffProfile.LicClass = model.LicClass;
                        staffProfile.LicExp = model.LicExpirery;
                        staffProfile.LicNo = model.LicNo;
                        //Marital
                        staffProfile.Marital = model.MaritalStatus == (int)MaritalStatus.Diforced ? MaritalStatus.Diforced :
                            model.MaritalStatus == (int)MaritalStatus.Enganged ? MaritalStatus.Enganged :
                            model.MaritalStatus == (int)MaritalStatus.Married ? MaritalStatus.Married :
                            model.MaritalStatus == (int)MaritalStatus.Single ? MaritalStatus.Single :
                            model.MaritalStatus == (int)MaritalStatus.Windowed ? MaritalStatus.Windowed : MaritalStatus.Unspecified;
                        //
                        staffProfile.Nhif = model.NHIF;
                        staffProfile.Nssf = model.NSSF;
                        //Religiion
                        staffProfile.Religion = (model.ReligionId == (int)Religion.Christian ? Religion.Christian :
                            model.ReligionId == (int)Religion.Muslim ? Religion.Muslim :
                            model.ReligionId == (int)Religion.Hindu ? Religion.Hindu : Religion.Other);
                       
                        //
                        //REFRENCES
                        staffProfile.ApplicationUserId = user.Id;
                        //Section
                        var sect = await db.HrSections.SingleAsync(k => k.Id == model.SectionId);
                        staffProfile.SectionId = sect.Id;
                        //Employee Type
                        var empTyp = await db.HrEmployeeCategories.SingleAsync(k => k.Id == model.EmployeeCategory);
                        staffProfile.EmpCatId = empTyp.Id;
                        //Job catalogue
                        var jobCat = db.HrJobCatalogues.Single(p => p.Id == model.JobTitle);
                        staffProfile.JobCatId = jobCat.Id;
                        //Job Type
                        var lvGrp = db.HrLeaveGroups.First();
                        staffProfile.LeaveGroupId = lvGrp.Id;
                        //Designation
                        var desig = db.HrDesignations.Single(l => l.Id == model.Designation);
                        staffProfile.DesignationId = desig.Id;
                        //
                        if (model.Supervisor > 0)
                        {
                            var supervisor = db.HrStaff.SingleAsync(p => p.Id == model.Supervisor);
                            staffProfile.SupervisorId = supervisor.Id;
                        }
                        //
                        logger.LogInformation(1, "Updating user staff profile..");
                        user.StaffProfile = staffProfile;

                        //
                        staffProfile.BankAccId = bacc.Id;
                        staffProfile.AddressId = address.Id;
                        //
                        
                        //
                        result = await _userManager.UpdateAsync(user);
                        //
                        user = db.Users.Include(k=>k.StaffProfile).Single(p => p.Id == user.Id);
                        if(user.StaffProfile == null)
                        {
                            db.HrStaff.Add(staffProfile);
                            db.SaveChanges();
                        }
                        //
                        if (result.Succeeded)
                        {

                            logger.LogInformation(1, "User staff profile for " + staffProfile.FullName + " created.");

                            //Then
                            address.StaffId = staffProfile.Id;
                            db.SaveChanges();

                            //Send Email
                            var _emailSender = provider.GetRequiredService<IEmailSender>();
                            // Send an email with this link
                            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                            await _emailSender.SendEmailAsync(user.Email, "Confirm your NITA Staff account",
                                "Please confirm your Nita Staff account by clicking this link: <a href=\"" + callbackUrl + "\">link</a>");

                            //We Good
                            return RedirectToRoute(StaffControllerRoute.GetAllStaff,new { msg="Staff account for '"+staffProfile.FullName+"' was created sucessifuly and an account activation mail send to their personal email."});
                        }
                        else
                        {
                            logger.LogWarning(1, "Create Staff profile failed.");
                            AddErrors(result);
                        }
                    }catch(System.Data.SqlClient.SqlException ex)
                    {
                        logger.LogCritical(0, "Add Staff profile failed with "+ex.Message, ex);
                        ModelState.AddModelError(string.Empty, "Create Staff account failed with data-storage errors.");
                    }
                }
                else
                {
                    logger.LogWarning(1, "Create User Account failed. ");
                    AddErrors(result);
                }
            }

            //If we got this far....
            return this.View(StaffControllerAction.AddStaff,model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        [HttpGet("updatestaff", Name = StaffControllerRoute.UpdateStaff)]
        public IActionResult UpdateStaff(int sid)
        {


            return this.View(StaffControllerAction.Update);
        }
        /// <summary>
        /// Staff Termination
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        [HttpGet("termstaff", Name = StaffControllerRoute.TerminateStaff)]
        public IActionResult TerminateStaff(int sid)
        {
            var model = new AddStaffViewModel();


            return this.View(StaffControllerAction.AddStaff, model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        //[HttpGet("addstaff", Name = StaffControllerRoute.AddStaff)]
        //public IActionResult AddStaff()
        //{
        //    var model = new AddStaffViewModel();


        //    return this.View(StaffControllerAction.AddStaff, model);
        //}


        #region Helpers
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
        #endregion

        #region JSON REQs
        [HttpGet("stprof")]
        public async Task<JsonResult> GetStaffInfo(int sid)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (sid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }

            //
            var d = "{";
            var staff = await db.HrStaff.Include(l => l.Address).ThenInclude(k => k.County).Include(l => l.Termination).Include(l=>l.Section)
                .Include(k=>k.JobCatalogue).Include(k => k.ApplicationUser).SingleOrDefaultAsync(k => k.Id == sid && !(k.Terminated || k.Termination != null));
            //
            if (staff != null)
            {
                d = d + "\"name\":\"" + staff.FullName + "\",\"eno\":\"" + staff.EmplNo;
                d += "\",\"loc\":\"" + staff.Address.County.Name + "\",\"jtitle\":\"" + staff.JobCatalogue.Name + "\",\"sect\":\"" + staff.Section.Name;
                d += "\",\"jd\":\"" + staff.ApplicationUser.DateCreated.ToString("yyyy-MM-dd") + "\",\"cc\":\"\"";
            }
            else
            {

            }
            d = d + "}";
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("trprof")]
        public async Task<JsonResult> GetTraineeProfile(int tid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (tid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }

            //
            var d = "{";
            var trainee = await db.Trainees.Include(l => l.Address).ThenInclude(k => k.County).Include(k => k.Programme).Include(l => l.Trainer).Include(k => k.ApplicationUser).SingleOrDefaultAsync(k => k.Id == tid);
            if (trainee != null)
            {
                d = d + "\"name\":\"" + trainee.FullName + "\",\"prog\":\"" + trainee.Programme?.Name;
                d += "\",\"loc\":\"" + trainee.Address.County.Name + "\",\"trainer\":\"" + trainee.Trainer?.FullName;
                d += "\",\"jd\":\"" + trainee.ApplicationUser.DateCreated.ToString("yyyy-MM-dd") + "\",\"cc\":\"\"";
            }
            else
            {

            }
            d = d + "}";
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("mcprof")]
        public async Task<JsonResult> GetMCProfile(int mid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (mid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }

            //
            var d = "{";
            var trainer = await db.Trainers.Include(l => l.Address).ThenInclude(k => k.County).Include(k => k.Trainees).Include(l => l.TrainerContracts).Include(k => k.ApplicationUser).SingleOrDefaultAsync(k => k.Id == mid);
            if (trainer != null && trainer.Type == TrainerType.MasterCraftman)
            {
                d = d + "\"name\":\"" + trainer.FullName + "\",\"ent\":\"" + trainer.EnterpriseName;
                d += "\",\"loc\":\"" + trainer.Address.County.Name + "\",\"bef\":\"" + trainer.Trainees.Count;
                d += "\",\"jd\":\"" + trainer.ApplicationUser.DateCreated.ToString("yyyy-MM-dd") + "\",\"ta\":\"" + trainer.TrainerContracts.Count + "\"";
            }
            //
            d = d + "}";
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("tpprof")]
        public async Task<JsonResult> GetTPProfile(int mid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (mid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }

            //
            var d = "{";
            var trainer = await db.Trainers.Include(l => l.Address).ThenInclude(k => k.County).Include(k => k.Trainees).Include(l => l.TrainerContracts).Include(k => k.ApplicationUser).SingleOrDefaultAsync(k => k.Id == mid);
            if (trainer != null && trainer.Type == TrainerType.Institution)
            {
                d = d + "\"name\":\"" + trainer.FullName + "\",\"ent\":\"" + trainer.EnterpriseName + "\",\"cont\":\"" + trainer.ContactPersonName;
                d += "\",\"loc\":\"" + trainer.Address.County.Name + "\",\"bef\":\"" + trainer.Trainees.Count;
                d += "\",\"jd\":\"" + trainer.ApplicationUser.DateCreated.ToString("yyyy-MM-dd") + "\",\"ta\":\"" + trainer.TrainerContracts.Count + "\"";
            }
            //
            d = d + "}";
            return new JsonResult(new { s = "1", data = d });
        }
        #endregion
    }


}