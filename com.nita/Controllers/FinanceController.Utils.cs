﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using com.nita.Constants;
using com.nita.IdentityModels;
using com.nita.Settings;
using com.nita.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Nita.Controllers
{
    [Authorize]
    public partial class FinanceController : Controller
    {
        #region Currency
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allcur", Name = FinanceUtilsControllerRoute.GetAllCurrencies)]
        public IActionResult AllCurrencies()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceUtilsControllerAction.AllCurrencies);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addcur", Name = FinanceUtilsControllerRoute.AddCurrency)]
        public IActionResult AddCurrency()
        {

            //
            return this.View(FinanceUtilsControllerAction.AddCurrency);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addcur", Name = FinanceUtilsControllerRoute.AddCurrency)]
        [ValidateAntiForgeryToken]
        public IActionResult AddCurrency(AddCurrencyViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                if (db.FCurrencies.Any(k => k.Code.ToLower().Equals(model.Code.ToLower())))
                {
                    ModelState.AddModelError(string.Empty, "A currency with simillar code exist.");
                }
                else
                {
                    var cur = new Currency()
                    {
                        Name = model.Name,
                        Code = model.Code,
                        Alias = model.Alias
                    };
                    //
                    if (model.IsBase)
                    {
                        //enumerate exixting
                        foreach(var h in db.FCurrencies.ToList())
                        {
                            if (h.IsBase)
                            {
                                h.IsBase = false;//change
                                db.SaveChanges();
                            }
                        }

                        //
                        cur.IsBase = true;
                    }

                    //
                    db.FCurrencies.Add(cur);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceUtilsControllerRoute.GetAllCurrencies,new { msg= "Currency '"+model.Name+"' was added successfully"});
                }
            }
            //
            return this.View(FinanceUtilsControllerAction.AddCurrency,model);
        }

        #endregion

        #region Banks
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allbanks", Name = FinanceUtilsControllerRoute.GetAllBanks)]
        public IActionResult AllBanks()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceUtilsControllerAction.AllBanks);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addbank", Name = FinanceUtilsControllerRoute.GetAddBank)]
        public IActionResult AddBank()
        {

            //
            return this.View(FinanceUtilsControllerAction.AddBank);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addbank", Name = FinanceUtilsControllerRoute.GetAddBank)]
        [ValidateAntiForgeryToken]
        public IActionResult AddBank(AddBankViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                if (db.PBanks.Any(k => k.Code.ToLower().Equals(model.Code.ToLower())))
                {
                    ModelState.AddModelError(string.Empty, "A bank with simillar code already exist.");
                }
                else
                {
                    var bank = new FBank()
                    {
                        Name = model.Name,
                        Code = model.Code,
                        Address = model.Address,
                        ContactEmail = model.ContactEmail,
                        FaxNo = model.FaxNo,
                        SwiftCode = model.SwiftCode,
                        TelNo = model.TelNo,
                        Website = model.Website
                    };

                    //
                    db.PBanks.Add(bank);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceUtilsControllerRoute.GetAllBanks,new { msg = "Bank '"+model.Name+"' added successfully"});
                }
            }
            //
            return this.View(FinanceUtilsControllerAction.AddBank, model);
        }
        /// <returns></returns>
        [HttpGet("addbbranch", Name = FinanceUtilsControllerRoute.GetAddBankBranch)]
        public IActionResult AddBankBranch()
        {

            //
            return this.View(FinanceUtilsControllerAction.AddBankBranch);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addbbranch", Name = FinanceUtilsControllerRoute.GetAddBankBranch)]
        [ValidateAntiForgeryToken]
        public IActionResult AddBankBranch(AddBankBranchViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                var bank = db.PBanks.Include(l => l.Branches).Single(l=>l.Id == model.Bank);
                if (bank.Branches.Any(p=>p.Code.ToLower().Equals(model.Code.ToLower())))
                {
                    ModelState.AddModelError(string.Empty, "A branch with simillar code already exist for '"+bank.Name+"'.");
                }
                else
                {
                    var branch = new BankBranch()
                    {
                        Name = model.Name,
                        Code = model.Code,
                        Address = model.Address,
                        ContactEmail = model.ContactEmail,
                        FaxNo = model.FaxNo,
                        TelNo = model.TelNo
                    };

                    //
                    bank.Branches.Add(branch);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceUtilsControllerRoute.GetAllBanks, new { msg = bank.Name+" Branch '" + model.Name + "' added successfully" });
                }
            }
            //
            return this.View(FinanceUtilsControllerAction.AddBankBranch, model);
        }
        #endregion
        #region Projects
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allprojs", Name = FinanceUtilsControllerRoute.GetAllProjects)]
        public IActionResult AllProject()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceUtilsControllerAction.AllProjects);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addproj", Name = FinanceUtilsControllerRoute.GetAddProject)]
        public IActionResult AddProject()
        {

            //
            return this.View(FinanceUtilsControllerAction.AddProject);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addproj", Name = FinanceUtilsControllerRoute.GetAddProject)]
        [ValidateAntiForgeryToken]
        public IActionResult AddProject(AddProjectViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                if (db.FProjects.Any(k => k.Name.ToLower().Equals(model.Name.ToLower())))
                {
                    ModelState.AddModelError(string.Empty, "A currency with simillar code exist.");
                }
                else
                {
                    var proj = new FProject()
                    {
                        Name = model.Name,
                        Description = model.Description,
                        CompletedPercentage = model.CompletedPercentage,
                        ProjectNo = model.ProjectNo,
                        Amount = model.Amount,
                        EndDate = model.EndDate,
                        Location = model.Location,
                        StartDate = model.StartDate
                    };

                    var ledger = db.FLedgers.Single(k => k.Id == model.Ledger);
                    proj.LedgerId = ledger.Id;


                    //
                    db.FProjects.Add(proj);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceUtilsControllerRoute.GetAllProjects, new { msg = "Project '" + model.Name + "' was added successfully" });
                }
            }
            //
            return this.View(FinanceUtilsControllerAction.AddProject, model);
        }

        #endregion
        #region Accounts
        /// <summary>
        /// All Accounts
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allacc", Name = FinanceUtilsControllerRoute.GetAllAccounts)]
        public IActionResult AllAccounts()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceUtilsControllerAction.AllAccounts);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addacc", Name = FinanceUtilsControllerRoute.AddAccount)]
        public IActionResult AddAccount()
        {

            //
            return this.View(FinanceUtilsControllerAction.AddAccount);
        }
        /// <summary>
        /// Add Account
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addacc", Name = FinanceUtilsControllerRoute.AddAccount)]
        [ValidateAntiForgeryToken]
        public IActionResult AddAccount(AddFinanceAccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                if (db.FinanceAccounts.Any(p => p.Name.Equals(model.Name)))
                {
                    ModelState.AddModelError(string.Empty, "An Account with simillar name already exists");
                    //
                    return this.View(FinanceUtilsControllerAction.AddAccount, model);
                }
                //Finance
                var acc = new FinanceAccount()
                {
                    Name = model.Name,
                    Number = model.Number
                };
                //
                if (model.IsChild)
                {
                    if (model.Parent == 0)
                    {
                        ModelState.AddModelError("", "A Parent is required for child accounts");
                        //
                        return this.View(FinanceUtilsControllerAction.AddAccount, model);
                    }
                    var parent = db.FinanceAccounts.Single(p => p.Id == model.Parent);
                    acc.ParentId = parent.Id;
                    acc.Type = parent.Type;
                    acc.HasBudget = model.IsBudgetItem;
                    acc.Status = model.IsClosed ? EntityStatus.Closed : EntityStatus.Active;
                    //
                }
                else
                {
                    acc.Type = (model.Type == (int)FAccountType.Asset ? FAccountType.Asset :
                    model.Type == (int)FAccountType.Equity ? FAccountType.Equity :
                    model.Type == (int)FAccountType.Expense ? FAccountType.Expense :
                    model.Type == (int)FAccountType.Income ? FAccountType.Income : FAccountType.Liability);
                }

                //
                db.FinanceAccounts.Add(acc);
                db.SaveChanges();

                //
                return RedirectToRoute(FinanceUtilsControllerRoute.GetAllAccounts, new { msg = "Account created succesifully." });
            }
            //
            return this.View(FinanceUtilsControllerAction.AddAccount, model);
        }
        #endregion
        #region Ledgers
        /// <summary>
        /// All financial Ledgers
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("alllgr", Name = FinanceUtilsControllerRoute.GetAllLedgers)]
        public IActionResult AllLedgers()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceUtilsControllerAction.AllLedgers);
        }
        /// <summary>
        /// Add Ledger
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addlgr", Name = FinanceUtilsControllerRoute.AddLedger)]
        public IActionResult AddLedger()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (!db.FCurrencies.Any())
            {
                return RedirectToRoute(FinanceUtilsControllerRoute.AddCurrency,new { msg="A currency is required to add a ledger."});
            }
            //
            return this.View(FinanceUtilsControllerAction.AddLedger);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addlgr", Name = FinanceUtilsControllerRoute.AddLedger)]
        [ValidateAntiForgeryToken]
        public IActionResult AllAccounts(AddFinanceLedgerViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                if (db.FLedgers.Any(p => p.Name.Equals(model.Name)))
                {
                    ModelState.AddModelError(string.Empty, "A Legder with simillar name already exists");
                    //
                    //
                    return this.View(FinanceUtilsControllerAction.AddAccount, model);
                }
                //Finance
                var ledger = new FinanceLedger()
                {
                    Name = model.Name
                };
                var cur = db.FCurrencies.Single(l => l.Id == model.Currency);
                ledger.CurrencyId = cur.Id;

                //
                db.FLedgers.Add(ledger);
                db.SaveChanges();

                //
                return RedirectToRoute(FinanceUtilsControllerRoute.GetAllLedgers, new { msg = "Ledger created succesifully." });
            }
            //
            return this.View(FinanceUtilsControllerAction.AddAccount, model);
        }
        #endregion
        #region Payment Mode
        /// <summary>
        /// All Payment Modes
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpmode", Name = FinanceUtilsControllerRoute.GetAllPaymentAccounts)]
        public IActionResult AllPaymentAccounts()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceUtilsControllerAction.AllPaymentAccounts);
        }
        /// <summary>
        /// Add Payment Mode
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addpmode", Name = FinanceUtilsControllerRoute.AddPaymentAccount)]
        public IActionResult AddPaymentAccount()
        {

            //
            return this.View(FinanceUtilsControllerAction.AddPaymentAccount);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addpmode", Name = FinanceUtilsControllerRoute.AddPaymentAccount)]
        [ValidateAntiForgeryToken]
        public IActionResult AddPaymentAccount(AddPaymentAccountViewModel model, params int[] actions)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                if (db.FPaymentAccounts.Any(p => p.Name.ToLower().Equals(model.Name.ToLower())))
                {
                    ModelState.AddModelError(string.Empty, "A Payment Mode with simillar name already exists");
                    //
                    return this.View(FinanceUtilsControllerAction.AddPaymentAccount, model);
                }
                //Payment
                var mode = new PaymentAccount()
                {
                    Name = model.Name,
                    AccountNumber = model.AccNo,
                    BankTransferPayeeName = model.PayeeName,
                    Description = model.Description,
                    EFTEnabled = model.HasEFT,
                    Mode = model.Mode == (int)PayMode.Cash ? PayMode.Cash : 
                    model.Mode == (int)PayMode.Cheque ? PayMode.Cheque :
                    model.Mode == (int)PayMode.EFT ? PayMode.EFT :
                    model.Mode == (int)PayMode.MobileBanking ? PayMode.MobileBanking :
                    model.Mode == (int)PayMode.FOSA ? PayMode.FOSA :PayMode.Other
                };
                //
                var ledger = db.FLedgers.Single(l => l.Id == model.Ledger);
                mode.LedgerId = ledger.Id;
                //
                var acc = db.FinanceAccounts.Single(l => l.Id == model.Account);
                mode.AccountId = acc.Id;

                //
                var cred = new SOAPServiceCredentials()
                {
                    Name = model.SOAPUserId,
                    Password = model.SOAPPassword,
                    ModeId = mode.Id
                };
                mode.SOAPCredentials = cred;
                //
                db.FPaymentAccounts.Add(mode);
                db.SaveChanges();
                //

                //
                foreach (var i in actions)
                {
                    PaymentType typ;
                    switch (i)
                    {
                        case (int)PaymentType.AvailbleForBanking:
                            //
                            typ = PaymentType.AvailbleForBanking;
                            break;
                        case (int)PaymentType.MakePayments:
                            //
                            typ = PaymentType.MakePayments;
                            break;
                        case (int)PaymentType.ReceivePayments:
                            //
                            typ = PaymentType.ReceivePayments;
                            break;
                        default:
                            //Error
                            ModelState.AddModelError("", "Invalid Payment Action");
                            return View();
                    }
                    //
                    //
                    if (i == (int)typ)
                    {
                        var act = db.FPaymentActions.SingleOrDefault(p => p.Type == typ);
                        //
                        if (act != null)
                        {
                            mode.ApprovedActions.Add(act);
                        }
                        else
                        {
                            //Create it
                            act = new PaymentAction();
                            act.Type = typ;
                            act.Caption = Regex.Replace(typ.ToString(), "(\\B[A-Z])", " $1");
                            //
                            mode.ApprovedActions.Add(act);
                        }
                    }
                    //
                }
                //
                db.SaveChanges();
                //
                return RedirectToRoute(FinanceUtilsControllerRoute.GetAllPaymentAccounts, new { msg = "Payment Mode created succesifully." });
            }
            //
            return this.View(FinanceUtilsControllerAction.AddPaymentAccount, model);
        }
        #endregion
        #region Fiscal Years
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allfyrs", Name = FinanceUtilsControllerRoute.GetAllFiscalYears)]
        public IActionResult AllFiscalYears()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceUtilsControllerAction.AllFiscalYears);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addfyr", Name = FinanceUtilsControllerRoute.AddFiscalYear)]
        public IActionResult AddFiscalYear()
        {

            //
            return this.View(FinanceUtilsControllerAction.AddFiscalYear);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addfyr", Name = FinanceUtilsControllerRoute.AddFiscalYear)]
        [ValidateAntiForgeryToken]
        public IActionResult AddFiscalYear(AddFiscalYearViewModel model, params int[] actions)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                if (db.FiscalYears.Any(p => p.Name.Equals(model.Name)))
                {
                    ModelState.AddModelError(string.Empty, "A Fiscal Year with simillar name already exists");
                    //
                    return this.View(FinanceUtilsControllerAction.AddFiscalYear, model);
                }

                //
                var valid = true;
                //Validate End
                if (model.Ends.CompareTo(model.Begins) < 0)
                {
                    //
                    ModelState.AddModelError(string.Empty, "The year end date should be GREATER than year begin date");
                    valid = false;
                }
                //Validate Ends
                if (model.Closed.CompareTo(model.Ends) < 0)
                {
                    //
                    ModelState.AddModelError(string.Empty, "The year close date should be after the year has ended");
                    valid = false;
                }
                //
                if (!valid)
                    return this.View(FinanceUtilsControllerAction.AddFiscalYear, model);

                //continue
                var yr = new FiscalYear()
                {
                    Name = model.Name,
                    BeginDate = model.Begins,
                    EndDate = model.Ends,
                    CloseDate = model.Closed
                };
                //
                db.FiscalYears.Add(yr);
                db.SaveChanges();
                //
                return RedirectToRoute(FinanceUtilsControllerRoute.GetAllFiscalYears, new { msg = "Fiscal Year " + yr.Name + " was added succesifully" });
            }

            //
            return this.View(FinanceUtilsControllerAction.AddFiscalYear, model);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addfyrq", Name = FinanceUtilsControllerRoute.AddFiscalYearQuarter)]
        public IActionResult AddFiscalYearQuarter()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (!db.FiscalYears.Any())
            {
                return RedirectToRoute(FinanceUtilsControllerRoute.GetAllFiscalYears,new { msg="Add fiscal years first before you create a quarter."});
            }
            //
            return this.View(FinanceUtilsControllerAction.AddFiscalYearQuarter);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addfyrq", Name = FinanceUtilsControllerRoute.AddFiscalYearQuarter)]
        [ValidateAntiForgeryToken]
        public IActionResult AddFiscalYearQuarter(AddFiscalYearQuarterViewModel model, params int[] actions)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                var yr = db.FiscalYears.Include(f=>f.Quarters).Single(l => l.Id == model.Year);
                //
                var valid = true;
                //Validate Name
                if (yr.Quarters.Any(p => p.Name.Equals(model.Name)))
                {
                    ModelState.AddModelError(string.Empty, "A Fiscal Year Quarter with simillar name already exists for this fiscal year");

                    //
                    valid = false;
                }
                //Validate Begin
                var g = model.Begin.CompareTo(yr.BeginDate);var h = model.Begin.CompareTo(yr.EndDate);
                if ( g<0 || h > 0)
                {
                    //
                    ModelState.AddModelError(string.Empty, "The quarter start date is outside selected year");
                    valid = false;
                }
                //Validate Ends
                g = model.Ends.CompareTo(yr.BeginDate); h = model.Ends.CompareTo(yr.EndDate);
                if (g < 0 || h > 0)
                {
                    //
                    ModelState.AddModelError(string.Empty, "The quarter end date is outside selected year");
                    valid = false;
                }
                //Check overlap
                if(yr.Quarters.Count > 0)
                {
                    foreach(var q in yr.Quarters)
                    {
                        //Check Begin falls in another q
                        g = model.Begin.CompareTo(q.BeginDate); h = model.Begin.CompareTo(q.EndDate);
                        if ( g >= 0 && h <= 0)
                        {
                            //
                            ModelState.AddModelError(string.Empty, "This begin date specified falls in an existing quarter");
                            valid = false;
                            break;
                        }
                        //Check End falls in another q
                        g = model.Ends.CompareTo(q.BeginDate); h = model.Ends.CompareTo(q.EndDate);
                        if (g >= 0 && h <= 0)
                        {
                            //
                            ModelState.AddModelError(string.Empty, "This begin date specified falls in an existing quarter");
                            valid = false;
                            break;
                        }
                    }
                }
                //
                if (!valid)
                    return this.View(FinanceUtilsControllerAction.AddFiscalYearQuarter, model);
                //
                var qrt = new FiscalYearQuarter()
                {
                    Name = model.Name,
                    BeginDate = model.Begin,
                    EndDate = model.Ends,
                    FiscalYearId = yr.Id
                };

                //
                yr.Quarters.Add(qrt);
                db.SaveChanges();
                //
                return RedirectToRoute(FinanceUtilsControllerRoute.GetAllFiscalYears, new { msg = "Fiscal Year Quarter added successifully" });
            }

            //
            return this.View(FinanceUtilsControllerAction.AddFiscalYearQuarter, model);
        }
        #endregion
        #region Taxes
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("alltaxes", Name = FinanceUtilsControllerRoute.GetAllFinanceTaxes)]
        public IActionResult AllTaxes()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceUtilsControllerAction.AllFinanceTaxes);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addtax", Name = FinanceUtilsControllerRoute.AddFinanceTax)]
        public IActionResult AddTax()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (!db.FinanceAccounts.Any()) {
                return RedirectToRoute(FinanceUtilsControllerRoute.AddAccount, new { msg = "Account is required before a tax can be created." });
            }
            //
            return this.View(FinanceUtilsControllerAction.AddFinanceTax);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("addtax", Name = FinanceUtilsControllerRoute.AddFinanceTax)]
        [ValidateAntiForgeryToken]
        public IActionResult AddTax(AddTaxViewModel model, params int[] types)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                var acc = db.FinanceAccounts.Single(h=>h.Id == model.Account);
                //
                if (db.FTaxes.Any(l => l.Name.ToLower().Equals(model.Name.ToLower())))
                {
                    ModelState.AddModelError(string.Empty,"A tax with simmilar name already exist.");
                    return this.View(FinanceUtilsControllerAction.AddFinanceTax, model);
                }

                //
                var tax = new FinanceTax()
                {
                    Name = model.Name,
                    AccountId = acc.Id,
                    Rate = model.Rate,
                };
                //
                switch (model.RoundType) {
                    case (int)RoundOffType.Round:
                        //
                        tax.RoundType = RoundOffType.Round;
                        break;
                    case (int)RoundOffType.Round_Down:
                        //
                        tax.RoundType = RoundOffType.Round_Down;
                        break;
                    case (int)RoundOffType.Round_Up:
                        //
                        tax.RoundType = RoundOffType.Round_Up;
                        break;
                    default:
                        ModelState.AddModelError(string.Empty, "Unknown Round-Off Type selected.");
                        return this.View(FinanceUtilsControllerAction.AddFinanceTax, model);
                }
                //
                db.FTaxes.Add(tax);
                db.SaveChanges();
                //
                foreach(var t in types)
                {
                    TaxScheme scheme = null;
                    foreach(var f in db.FTaxes.Include(k => k.TaxSchemes).ToList())
                    {
                        if(f.TaxSchemes.Any(j=>((int)j.Type == t))){
                            scheme = f.TaxSchemes.First(j => ((int)j.Type == t));
                            break;
                        }
                    }
                    TaxType type = TaxType.Retention;
                    var name = "";
                    //
                    switch (t)
                    {
                        case (int)TaxType.Full_V_A_T:
                            //
                            name = "Full V.A.T";
                            type = TaxType.Full_V_A_T;
                            break;
                        case (int)TaxType.IncomeTaxWithholding:
                            //
                            name = "Income Tax Withholding";
                            type = TaxType.IncomeTaxWithholding;
                            break;
                        case (int)TaxType.Retention:
                            //
                            name = "Retention";
                            type = TaxType.Retention;
                            break;
                        case (int)TaxType.V_A_T_Withholding:
                            //
                            name = "V.A.T Withholding";
                            type = TaxType.V_A_T_Withholding;
                            break;
                        default:
                            ModelState.AddModelError(string.Empty, "Unknown Tax Type selected.");
                            return this.View(FinanceUtilsControllerAction.AddFinanceTax, model);
                    }
                    //
                    if (scheme == null)
                    {
                        scheme = new TaxScheme()
                        {
                            Name = name,
                            Type = type
                        };
                    }
                    //

                    tax.TaxSchemes.Add(scheme);
                    db.SaveChanges();
                }
                //
                return RedirectToRoute(FinanceUtilsControllerRoute.GetAllFinanceTaxes, new { msg="Tax was added succesifully"});
            }
            //
            return this.View(FinanceUtilsControllerAction.AddFinanceTax,model);
        }
        #endregion
        #region Vendor Types
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allvtyps", Name = FinanceUtilsControllerRoute.GetAllVendorTypes)]
        public IActionResult AllVendors()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceUtilsControllerAction.AllVendorTypes);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addvtyp", Name = FinanceUtilsControllerRoute.AddVendorType)]
        public IActionResult AddVendor()
        {

            //
            return this.View(FinanceUtilsControllerAction.AddVendorType);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("addvtyp", Name = FinanceUtilsControllerRoute.AddVendorType)]
        [ValidateAntiForgeryToken]
        public IActionResult AddVendor(AddVendorViewModel model)
        {
            //
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                var vendor = new FVendorType()
                {
                    Name = model.Name
                };
                //
                db.FVendorTypes.Add(vendor);
                db.SaveChanges();

                //
                return RedirectToRoute(FinanceUtilsControllerRoute.GetAllVendorTypes, new { msg="Vendor "+vendor.Name+" added succesifully."});
            }
            //
            return this.View(FinanceUtilsControllerAction.AddVendorType,model);
        }
        #endregion
        #region Voucher Prefixes
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allvprefixes", Name = FinanceUtilsControllerRoute.GetAllVoucherPrefixes)]
        public IActionResult AllVouchers()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceUtilsControllerAction.AllVoucherPrefixes);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addvprefix", Name = FinanceUtilsControllerRoute.AddVoucherPrefix)]
        public IActionResult AddVoucher()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if (!db.FLedgers.Any())
            {
                return RedirectToRoute(FinanceUtilsControllerRoute.AddLedger, new {msg="Ledger is required before a voucher can be added." });
            }
            //
            return this.View(FinanceUtilsControllerAction.AddVoucherPrefix);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("addvprefix", Name = FinanceUtilsControllerRoute.AddVoucherPrefix)]
        [ValidateAntiForgeryToken]
        public IActionResult AddVoucher(AddVoucherViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                var lgr = db.FLedgers.Include(j => j.PVoucherPrefixes).Single(p=>p.Id == model.Ledger);
                if (lgr.PVoucherPrefixes.Any())
                {
                    ModelState.AddModelError(string.Empty, "This ledger already defines a prefix. Please Edit the existing one");
                    return this.View(FinanceUtilsControllerAction.AddVoucherPrefix, model);
                }
                //
                var voucher = new PVoucherPrefix()
                {
                    Code = model.Code,
                    LedgerId = lgr.Id
                };
                //
                lgr.PVoucherPrefixes.Add(voucher);
                db.SaveChanges();
                //
                return RedirectToRoute(FinanceUtilsControllerRoute.GetAllVoucherPrefixes, new { msg="Voucher prefix '"+model.Code+"' added successfully. "});
            }
            //
            return this.View(FinanceUtilsControllerAction.AddVoucherPrefix, model);
        }
        #endregion
        #region Product Categories
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allfprodcat", Name = FinanceUtilsControllerRoute.GetAllProductCategories)]
        public IActionResult AllProductCategories()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceUtilsControllerAction.AllProductCategories);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addfprodcat", Name = FinanceUtilsControllerRoute.GetAddProductCategory)]
        public IActionResult AddProductCategory()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
           
            //
            return this.View(FinanceUtilsControllerAction.AddProductCategory);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("addfprodcat", Name = FinanceUtilsControllerRoute.GetAddProductCategory)]
        [ValidateAntiForgeryToken]
        public IActionResult AddProductCategory(AddProductCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                if(db.FProductCategories.Any(l=>l.Code == model.Code))
                {
                    ModelState.AddModelError("", "A product category with simillar code already exist..");
                }
                else{
                    //
                    var cat = new FProductCategory()
                    {
                        Name = model.Name,
                        Code = model.Code,
                        Status = model.Status ? EntityStatus.Active : EntityStatus.Inactive
                    };
                    //
                    db.FProductCategories.Add(cat);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceUtilsControllerRoute.GetAllProductCategories, new { msg = "Product category '" + model.Code + "' was added successfully. " });
                }
                
            }
            //
            return this.View(FinanceUtilsControllerAction.AddProductCategory, model);
        }
        #endregion
        #region Customer Types
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allfcusttyp", Name = FinanceUtilsControllerRoute.GetAllCustomerTypes)]
        public IActionResult AllCustomerTypes()
        {
            if (Request.Query.Any(p => p.Key.Equals("msg")))
            {
                ViewData["Message"] = Request.Query.First(k => k.Key.Equals("msg")).Value;
            }
            //
            return this.View(FinanceUtilsControllerAction.AllCustomerTypes);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addfcusttyp", Name = FinanceUtilsControllerRoute.GetAddCustomerType)]
        public IActionResult AddCustomerType()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            //
            return this.View(FinanceUtilsControllerAction.AddCustomerType);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("addfcusttyp", Name = FinanceUtilsControllerRoute.GetAddCustomerType)]
        [ValidateAntiForgeryToken]
        public IActionResult AddCustomerType(AddCustomerTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();
                if (db.FProductCategories.Any(l => l.Code == model.Code))
                {
                    ModelState.AddModelError("", "A customer type with simillar code already exist..");
                }
                else
                {
                    //
                    var cust = new FCustomerType()
                    {
                        Name = model.Name,
                        Code = model.Code,
                        Status = model.Status ? EntityStatus.Active : EntityStatus.Inactive
                    };
                    //
                    db.FCustomerTypes.Add(cust);
                    db.SaveChanges();
                    //
                    return RedirectToRoute(FinanceUtilsControllerRoute.GetAllCustomerTypes, new { msg = "Customer Type '" + model.Name + "' was added successfully. " });
                }
            }
            //
            return this.View(FinanceUtilsControllerAction.AddCustomerType, model);
        }
        #endregion
    }
}