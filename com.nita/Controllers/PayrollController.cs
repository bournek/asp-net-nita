﻿namespace Nita.Controllers
{
    using Boilerplate.AspNetCore;
    using Microsoft.AspNetCore.Mvc;
    using com.nita.Constants;
    using Microsoft.AspNetCore.Authorization;
    using com.nita.Settings;
    using Microsoft.Extensions.Options;
    using System;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;
    using Microsoft.Extensions.DependencyInjection;
    using com.nita.IdentityModels;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using Newtonsoft.Json.Linq;
    using com.nita.ViewModels;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using System.Text;

    /// <summary>
    /// Provides methods that respond to HTTP requests with HTTP errors.
    /// </summary>
    [Authorize(DefaultRoles.NitaSysAdmin)]
    public sealed partial class PayrollController : Controller
    {
        private readonly IOptions<AppSettings> appSettings;
        private readonly IServiceProvider provider;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly ILogger logger;

        public PayrollController(IServiceProvider prov,
            IHostingEnvironment env,
            IOptions<AppSettings> appSettings)
        {
            this.provider = prov;
            this.appSettings = appSettings;
            this.hostingEnvironment = env;
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("payroll", Name = PayrollControllerRoute.GetIndex)]
        public IActionResult Index()
        {


            return this.View(PayrollControllerAction.Index);
        }

        #region Irregular Payment Vouchers
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("allppvouchers", Name = PayrollControllerRoute.GetAllIrregularPaymentVouchers)]
        public IActionResult AllIrregularPaymentVouchers()
        {


            return this.View(PayrollControllerAction.AllIrregularPaymentVouchers);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addppvoucher", Name = PayrollControllerRoute.GetAddIrregularPaymentVoucher)]
        public IActionResult AddIrregularPaymentVoucher(string p)
        {
            if(p == null)
            {
                return RedirectToRoute(PayrollControllerRoute.GetProcessIrregularPayment,new { msg="Please specify the Earnings to be paid and try again."});
            }
            var encoding = Encoding.UTF8;
            var list = JsonConvert.DeserializeObject<List<int>>(encoding.GetString(Convert.FromBase64String(p)));
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new AddIrregularPVViewModel();
            //
            model.VoucherNo = Utils.GenerateVoucherNo(db).ToString();
            //
            foreach (var i in list) {
                var er = db.PIrrEarnings.Include(k=>k.Payment).ThenInclude(k=>k.CurrencyUsed).Include(k => k.Payment).ThenInclude(k => k.GLAccount).Include(l=>l.Trainee).ThenInclude(l=>l.BankAccount)
                    .Include(l => l.Trainer).ThenInclude(l => l.BankAccount).Include(l=>l.Project).Include(k=>k.PayAccount).Single(l => l.Id == i);
                //
                if (er.Payment.Voucher == null)
                    model.UnpaidEarnings.Add(er);
            }
            //
            return this.View(PayrollControllerAction.AddIrregularPaymentVoucher,model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        [HttpPost("addppvoucher", Name = PayrollControllerRoute.GetAddIrregularPaymentVoucher)]
        [ValidateAntiForgeryToken]
        public IActionResult AddIrregularPaymentVoucher(AddIrregularPVViewModel model)
        {
            if (ModelState.IsValid)
            {

            }

            //
            return this.View(PayrollControllerAction.AddIrregularPaymentVoucher,model);
        }
        #endregion

        #region Process Irregular Payment
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allproirrp", Name = PayrollControllerRoute.GetAllProcessedIrregularPayments)]
        public IActionResult AllProcessedIrregularPayments()
        {

            return this.View(PayrollControllerAction.AllProcessedIrregularPayments);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("proirrp", Name = PayrollControllerRoute.GetProcessIrregularPayment)]
        public IActionResult ProcessIrregularPayment(int? eid)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var model = new ProcessIrregularPaymentViewModel();
            //
            if(eid > 0)
            {
                model.PendingEarnings = db.PIrrEarnings.Include(p => p.Payment).ThenInclude(k => k.Voucher).Include(p => p.Payment).ThenInclude(k => k.PaymentAccounts).Include(l => l.Project).Include(l => l.Trainee)
                .Include(l => l.Trainer).Include(p => p.Payment).ThenInclude(k => k.CurrencyUsed).Include(p => p.Payment).ThenInclude(k => k.GLAccount)
                .Include(p => p.Payment).ThenInclude(k => k.Department).Where(k => k.Id == eid).ToList();
            }
            else
            {
                model.PendingEarnings = db.PIrrEarnings.Include(p => p.Payment).ThenInclude(k => k.Voucher).Include(p => p.Payment).ThenInclude(k => k.PaymentAccounts).Include(l => l.Project).Include(l => l.Trainee)
               .Include(l => l.Trainer).Include(p => p.Payment).ThenInclude(k => k.CurrencyUsed).Include(p => p.Payment).ThenInclude(k => k.GLAccount)
               .Include(p => p.Payment).ThenInclude(k => k.Department).ToList();
            
            }
                //
            return this.View(PayrollControllerAction.ProcessIrregularPayment,model);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("proirrp", Name = PayrollControllerRoute.GetProcessIrregularPayment)]
        [ValidateAntiForgeryToken]
        public IActionResult ProcessIrregularPayment(ProcessIrregularPaymentViewModel model)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (ModelState.IsValid)
            {
                if(model.earnings.Length == 0)
                {
                    ModelState.AddModelError("", "Please select at least one earning and try again.");
                    return this.View(PayrollControllerAction.ProcessIrregularPayment, model);
                }
                //
                //var proj = db.PIrrPaymentProjects.Single(l => l.Id == model.ProjectId);
                //
                //var voucher = new FPaymentVoucher()
                //{
                //    Description = proj.Description,
                //    k
                var list = new List<int>();
                //}
                //
                foreach (var h in model.earnings)
                {
                    var earning = db.PIrrEarnings.Single(k=>k.Id == h);
                    list.Add(earning.Id);
                }
                var encoding = Encoding.UTF8;
                //
                return RedirectToRoute(PayrollControllerRoute.GetAddIrregularPaymentVoucher, new { p = Convert.ToBase64String(encoding.GetBytes(JsonConvert.SerializeObject(list))) });
            }
            if (model.ProjectId > 0)
            {

                model.PendingEarnings = db.PIrrEarnings.Include(p => p.Payment).ThenInclude(k => k.Voucher).Include(p => p.Payment).ThenInclude(k => k.PaymentAccounts).Include(l => l.Project).Include(l => l.Trainee)
                .Include(l => l.Trainer).Include(p => p.Payment).ThenInclude(k => k.CurrencyUsed).Include(p => p.Payment).ThenInclude(k => k.GLAccount)
                .Include(p => p.Payment).ThenInclude(k => k.Department).Where(k => k.ProjectId == model.ProjectId).ToList();
            }
            return this.View(PayrollControllerAction.ProcessIrregularPayment, model);
        }
        #endregion

        #region Irregular Payment Earning
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpirrear", Name = PayrollControllerRoute.GetAllIrregularEarnings)]
        public IActionResult AllIrregularPayment()
        {


            return this.View(PayrollControllerAction.AllIrregularEarnings);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="earnings"></param>
        /// <returns></returns>
        [HttpPost("allpirrear", Name = PayrollControllerRoute.GetAllIrregularEarnings)]
        [ValidateAntiForgeryToken]
        public IActionResult AllIrregularPayment(params int[] earnings)
        {

            return this.View(PayrollControllerAction.AllIrregularEarnings);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addirrear", Name = PayrollControllerRoute.GetAddIrregularEarning)]
        public IActionResult CreateIrregularPayment()
        {


            return this.View(PayrollControllerAction.AddIrregularEarning);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addirrear", Name = PayrollControllerRoute.GetAddIrregularEarning)]
        [ValidateAntiForgeryToken]
        public IActionResult CreateIrregularPayment(AddIrregularEarningViewModel model)
        {
            if (ModelState.IsValid)
            {
                //
                //
                if (model.TraineeId > 0 && model.TrainerId > 0)
                {
                    ModelState.AddModelError(string.Empty, "Please choose either an Beneficiary or a Master Craftman. Not both.");
                    return this.View(PayrollControllerAction.AddIrregularEarning, model);
                }

                //
                var db = provider.GetRequiredService<ApplicationDbContext>();

                //
                var proj = db.PIrrPaymentProjects.Include(k=>k.Earnings).Single(p => p.Id == model.ProjectId);

                //
                var earning = new IrregularEarning()
                {
                    IsConsultancy = model.IsConsultancy
                };
                //Is Trainee
                if(model.TraineeId > 0)
                {
                    var trainee = db.Trainees.Single(j=>j.Id == model.TraineeId);
                    //
                    if (proj.Earnings.Any(l => l.TraineeId == model.TraineeId))
                    {
                        ModelState.AddModelError(string.Empty, "This project already contains a Earning for the selected Beneficiary.");
                        return this.View(PayrollControllerAction.AddIrregularEarning, model);
                    }

                    //
                    earning.TraineeId = trainee.Id;

                }else if(model.TrainerId > 0)//MC
                {
                    var trainer = db.Trainers.Single(l => l.Id == model.TrainerId);
                    //
                    if(trainer.Type != TrainerType.MasterCraftman)
                    {
                        ModelState.AddModelError(string.Empty, "Create Irregular Project Earning failed. Bad data received");
                        return this.View(PayrollControllerAction.AddIrregularEarning, model);
                    }
                    //
                    if (proj.Earnings.Any(l => l.TrainerId == model.TrainerId))
                    {
                        ModelState.AddModelError(string.Empty, "This project already contains a Earning for the selected Master Craftman.");
                        return this.View(PayrollControllerAction.AddIrregularEarning, model);
                    }

                    //
                    earning.TrainerId = trainer.Id;
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Create Irregular Project Earning failed. Bad data received");
                    return this.View(PayrollControllerAction.AddIrregularEarning, model);
                }
                //
                var pacc = db.PPayAccounts.Include(l=>l.Account).Single(j => j.Id == model.AccountId);
                //Pay Account
                earning.PayAccountId = pacc.Id;
                //
                var payment = new FPayment()
                {
                    GrossAmount = proj.Duration * proj.Rate,
                    AccountId = pacc.AccountId
                };
                //
                var dept = db.HrDepartments.Single(l=>l.Id == model.Dept);
                payment.DeptId = dept.Id;
                //
                payment.CurrencyId = db.FCurrencies.Single(k=>k.IsBase).Id;
                //
                earning.Payment = payment;


                //Project
                earning.ProjectId = proj.Id;
                //Save
                db.PIrrEarnings.Add(earning);
                db.SaveChanges();
                //
                return RedirectToRoute(PayrollControllerRoute.GetAllIrregularEarnings,new { msg = "Irregular Payment Earning was added successfully"});
            }

            //
            return this.View(PayrollControllerAction.AddIrregularEarning,model);
        }
        #endregion

        #region Irregular Payment Profile
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpirrprof", Name = PayrollControllerRoute.GetAllIrregularPaymentProfiles)]
        public IActionResult AllIrregularPaymentProfiles()
        {


            return this.View(PayrollControllerAction.AllIrregularPaymentProfiles);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addpirrprof", Name = PayrollControllerRoute.GetAddIrregularPaymentProfile)]
        public IActionResult CreateIrregularPaymentProfile()
        {


            return this.View(PayrollControllerAction.AddIrregularPaymentProfile);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addpirrprof", Name = PayrollControllerRoute.GetAddIrregularPaymentProfile)]
        [ValidateAntiForgeryToken]
        public IActionResult CreateIrregularPaymentProfile(AddIrregularPaymentProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                //
                if (model.TraineeId > 0 && model.TrainerId > 0)
                {
                    ModelState.AddModelError(string.Empty, "Please select either an Beneficiary or a Master Craftman. Not both.");
                    return this.View(PayrollControllerAction.AddIrregularEarning, model);
                }

                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                //
                var prof = new IrregularPaymentProfile()
                {
                    ApplyPayeRelief = model.ApplyPayeRelief,
                    EndDate = model.EndDate,
                    ExemptTax = model.ExemptTax,
                    OtherIncome = model.OtherIncome,
                    PayMode = model.PayMode == (int)PayMode.Cash ? PayMode.Cash : model.PayMode == (int)PayMode.Cheque ? PayMode.Cheque : model.PayMode == (int)PayMode.FOSA ? PayMode.FOSA : PayMode.EFT,
                    StartDate = model.StartDate
                };

                if (model.TraineeId > 0)
                {
                    //Check Trainee
                    var trainee = db.Trainees.Include(k=>k.IrPaymentProfile).Single(l => l.Id == model.TraineeId);
                    //
                    if(trainee.IrPaymentProfile != null)
                    {
                        //
                        ModelState.AddModelError(string.Empty, "The selected Beneficiary account already as an Irregular Payment Profile");
                        return this.View(PayrollControllerAction.AddIrregularPaymentProfile, model);
                    }

                    //
                    prof.TraineeId = trainee.Id;
                    
                }
                else if(model.TrainerId > 0)
                {
                    //Check Trainer
                    var trainer = db.Trainers.Include(k => k.IrrPaymentProfile).Single(l => l.Id == model.TrainerId);
                    //
                    if (trainer.Type != TrainerType.MasterCraftman)
                    {
                        //
                        ModelState.AddModelError(string.Empty, "Irregular Payment Profile could not be created. Bad data received.");
                        return this.View(PayrollControllerAction.AddIrregularPaymentProfile, model);
                    }
                    //
                    if (trainer.IrrPaymentProfile != null)
                    {
                        //
                        ModelState.AddModelError(string.Empty, "The selected Master Craftman account already as an Irregular Payment Profile");
                        return this.View(PayrollControllerAction.AddIrregularPaymentProfile, model);
                    }

                    //
                    prof.TrainerId = trainer.Id;
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "The select either a Beneficiary or a Master Craftman and try again.");
                    return this.View(PayrollControllerAction.AddIrregularPaymentProfile, model);
                }

                //
                db.PIrrPaymentProfiles.Add(prof);
                db.SaveChanges();

                //
                return RedirectToRoute(PayrollControllerRoute.GetAllIrregularPaymentProfiles,new { msg = "Irregular payment profile was added successfully"});
            }

            return this.View(PayrollControllerAction.AddIrregularPaymentProfile,model);
        }
        #endregion

        #region Irregular Payment Projects
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpirrproj", Name = PayrollControllerRoute.GetAllIrregularPaymentProjects)]
        public IActionResult AllIrregularPaymentProjects()
        {


            return this.View(PayrollControllerAction.AllIrregularPaymentProjects);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addpirrproj", Name = PayrollControllerRoute.GetAddIrregularPaymentProject)]
        public IActionResult CreateIrregularPaymentProjects()
        {


            return this.View(PayrollControllerAction.CreateIrregularPaymentProject);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addpirrproj", Name = PayrollControllerRoute.GetAddIrregularPaymentProject)]
        [ValidateAntiForgeryToken]
        public IActionResult CreateIrregularPaymentProject(AddIrregularPaymentProjectViewModel model)
        {
            if (ModelState.IsValid)
            {
                var db = provider.GetRequiredService<ApplicationDbContext>();
                //
                if(db.PIrrPaymentProjects.Any(l=>l.Name == model.Name))
                {
                    ModelState.AddModelError("", "A Project with simillar name already exist.");
                    return this.View(PayrollControllerAction.CreateIrregularPaymentProject, model);
                }

                //
                //if (db.PIrrPaymentProjects.Any(j => (j.EndDate.CompareTo(model.EndDate) <= 0)))
                //{
                //    ModelState.AddModelError(string.Empty, "The specified P.A.Y.E period overlaps an existing P.A.Y.E period. Review the dates and try again.");
                //    return this.View(PayrollUtilsControllerAction.AddSalaryPeriod, model);
                //}

                var proj = new IrregularPaymentProject()
                {
                    Name = model.Name,
                    EffectiveDate = model.EffectiveDate,
                    EndDate = model.EndDate,
                    CloseDate = model.CloseDate,
                    Description = model.Description,
                    Duration = model.Duration,
                    Rate = model.Rate,
                    Unit = model.DurationUnit == (int)DurationUnit.Day ? DurationUnit.Day :
                    model.DurationUnit == (int)DurationUnit.Month ? DurationUnit.Month :
                    model.DurationUnit == (int)DurationUnit.Week ? DurationUnit.Week : DurationUnit.Year,
                    Taxable = model.Taxable
                };
                //
                db.PIrrPaymentProjects.Add(proj);
                db.SaveChanges();

                //
                return RedirectToRoute(PayrollControllerRoute.GetAllIrregularPaymentProjects, new { msg = "Irregular Payment Project '"+model.Name+"' was created successfully"});
            }

            return this.View(PayrollControllerAction.CreateIrregularPaymentProject, model);
        }
        #endregion
        #region Settings
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("psettings", Name = PayrollControllerRoute.GetSettings)]
        public IActionResult Settings()
        {


            return this.View(PayrollControllerAction.Settings);
        }
        #endregion

        #region  Utils
        [HttpGet("bbranches")]
        public async Task<JsonResult> GetBankBranches(int bid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (bid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }

            //
            var d = "[";
            try
            {
                var bank = db.PBanks.Include(l => l.Branches).SingleOrDefault(k => k.Id == bid);
                if (bank != null)
                {
                    foreach (var b in bank.Branches)
                    {
                        d += "{\"txt\":\"" + b.Name + "\",\"val\":\""+b.Id+"\"},";
                    }
                    d = d.TrimEnd(',');
                }
            }
            catch
            {
                d = "[";
            }
            d = d + "]";
            var h = JToken.Parse(d);
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = h.ToString() });
        }
        
        [HttpGet("projear")]
        public async Task<JsonResult> GetProjectPaymentAmount(int pid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (pid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }

            //
            var d = "{";
            var proj = db.PIrrPaymentProjects.SingleOrDefault(p => p.Id == pid);
            if (proj != null)
            {
                d = d + "\"amt\":\"" + (proj.Rate * proj.Duration) + "\"";
            }
            else
            {

            }
            d = d + "}";
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("deptbudget")]
        public async Task<JsonResult> GetDepartmentBudget(/*int? pid,*/int aid, int did)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (/*pid == 0 ||*/ aid ==0 || did == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }

            //
            try
            {
                var d = "{";
                //var proj = await db.PIrrPaymentProjects.SingleOrDefaultAsync(p => p.Id == pid);
                var acc = await db.FinanceAccounts.SingleAsync(l => l.Id == aid);

                //
                var dept = db.HrDepartments.Include(k => k.Budgets).ThenInclude(l=>l.Cycle).Include(k => k.Budgets).ThenInclude(l => l.Period).Include(l => l.Payments).SingleOrDefault(l => l.Id == did);

                // 
                var budget = await Utils.GetCurrentDeptBudget(dept, acc);
                //
                if(budget != null && budget.Cycle != null)
                {
                    var cycle = budget.Cycle;
                    //Get all complete payments within the cycle
                    var totalSpent = await Utils.GetSpentBudgetAmount(budget);
                    //
                    var rem = budget.Amount - totalSpent;
                    //

                    d = d + "\"btyp\":\"0\",\"bamt\":\"" + budget.Amount + "\",\"spent\":\"" + totalSpent + "\",\"rem\":\"" + rem + "\"";
                }
                else if (budget != null && budget.Period != null)
                {
                    var period = budget.Period;
                    //Get all complete payments within the cycle
                    var totalSpent = await Utils.GetSpentBudgetAmount(budget);
                    //
                    var rem = budget.Amount - totalSpent;
                    //

                    d = d + "\"btyp\":\"1\",\"bamt\":\"" + budget.Amount + "\",\"spent\":\"" + totalSpent + "\",\"rem\":\"" + rem + "\"";
                }
                else
                {
                    d = d + "\"bamt\":\"-1\"";
                }
                d = d + "}";
                return new JsonResult(new { s = "1", data = d });
            }
            catch
            {

            }
            //
            return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
        }
        [HttpGet("paccs")]
        public async Task<JsonResult> GetPayAccounts()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            //
            var d = "[";
            var accs = db.PPayAccounts.Include(l => l.Account).ToList();
            if (accs != null && accs.Count > 0)
            {
                foreach (var acc in accs)
                {
                    d = d + "{\"id\":\"" + acc.Id + "\",\"code\":\"" + acc.Code + "\",\"aid\":\"" + (acc.AccountId) + "\",\"atyp\":\""+acc.AccountType+"\",\"ptyp\":\""+acc.PayType+"\"},";
                }
                //
                d = d.TrimEnd(',');
            }
            d = d + "]";
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = d });
        }
        #endregion
    }
}