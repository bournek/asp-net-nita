﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using com.nita.Constants;
using com.nita.IdentityModels;
using com.nita.Services;
using com.nita.Settings;
using com.nita.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Nita.Controllers
{
    [Authorize(DefaultRoles.NitaSysAdmin)]
    public partial class FinanceController : Controller
    {
        
        #region JSON Utils


        [HttpGet("projs")]
        public async Task<JsonResult> GetProjects(int aid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if(aid == 0)
            {
                return new JsonResult(new {s="0",errs="[\"Bad Request\"]"});
            }

            //
            var d = "[";
            var list = (await db.FLedgers.Include(l=>l.Projects).SingleAsync(k => k.Id == aid))?.Projects;
            if(list!=null && list.Count > 0)
            {
                foreach(var p in list)
                {
                    d = d + "{\"txt\":\""+p.Name+"\",\"val\":\""+p.Id+"\"},";
                }
                //
                d = d.TrimEnd(',');
            }
            d = d + "]";
            return new JsonResult(new {s="1",data=d});
        }
        [HttpGet("falllgr")]
        public async Task<JsonResult> GetAllFinanceLedgers()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            //
            var d = "[";
            var list = db.FLedgers.Include(k=>k.PVoucherPrefixes).Include(l=>l.PaymentAccounts).ThenInclude(k=>k.ApprovedActions).ToList();
            if (list != null && list.Count > 0)
            {
                foreach (var p in list)
                {
                    d = d + "{\"id\":\"" + p.Id + "\",\"name\":\"" + p.Name + "\",\"cur\":\"" + p.CurrencyId + "\",\"prefs\":[";
                    foreach(var h in p.PVoucherPrefixes)
                    {
                        d += "{\"id\":\""+h.Id+"\",\"code\":\""+ h.Code + "\"},";
                    }
                    //
                    d = d.TrimEnd(',');
                    d = d + "],";
                    //
                    d = d + "\"pmodes\":[";
                    foreach(var m in p.PaymentAccounts)
                    {
                        d = d + "{\"id\":\"" + m.Id + "\",\"name\":\"" + m.Name + "\",\"payee\":\"" + m.BankTransferPayeeName + "\",\"typ\":\"" + m.Mode.ToString().ToLower() + "\",\"eft\":\""+m.EFTEnabled+"\",";
                        //
                        d = d + "\"acts\":[";
                        foreach(var act in m.ApprovedActions)
                        {
                            d = d + "{\"id\":\""+act.Id+"\",\"cap\":\""+act.Caption+"\",\"typ\":\""+act.Type+"\"},";
                        }
                        d = d.TrimEnd(',');
                        d= d+"]},";
                    }
                    //
                    d = d.TrimEnd(',');
                    //
                    d +="]},";
                }
                //
                d = d.TrimEnd(',');
            }
            d = d + "]";
            var e = JToken.Parse(d);
            return new JsonResult(new { s = "1", data = e.ToString()});
        }
        [HttpGet("alltxs")]
        public async Task<JsonResult> GetAllTaxes()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            
            //
            var d = "[";
            var list = db.FTaxes.ToList();
            if (list != null && list.Count > 0)
            {
                foreach (var p in list)
                {
                    d = d + "{\"id\":\"" + p.Id + "\",\"name\":\"" + p.Name + "\",\"rate\":\""+p.Rate+"\",\"rtyp\":\""+(int)p.RoundType+"\"},";
                }
                //
                d = d.TrimEnd(',');
            }
            d = d + "]";
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("supinfo")]
        public async Task<JsonResult> GetSupplierProfile(int sid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            if(sid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }
            //
            var d = "{";
            var sup = db.FSuppliers.Include(k=>k.VendorType).SingleOrDefault(k=>k.Id == sid);
            if (sup != null)
            {
                d = d + "\"id\":\"" + sup.Id + "\",\"name\":\"" + sup.Name + "\",\"ref\":\"" + sup.RefNo + "\",\"addr\":\"" + sup.Address + "\",\"typ\":\""+sup.VendorType.Name+"\"";
                //
            }
            d = d + "}";
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("sinvoices")]
        public async Task<JsonResult> GetSupplierInvoices(int sid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (sid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }

            //
            var d = "[";
            var list = db.FInvoices.Include(l=>l.Payments).ThenInclude(l=>l.Taxes).Include(l => l.Payments).ThenInclude(l => l.Voucher).Where(k => k.SupplierId == sid && k.Payments.Any(l=>l.Voucher == null)).ToList();
            if (list != null && list.Count > 0)
            {
                foreach (var p in list)
                {
                    d = d + "{\"ref\":\"" + p.DocNo + "\",\"id\":\"" + p.Id + "\",\"pps\":[";
                    foreach(var i in p.Payments)
                    {
                        d = d + "{";
                        var payment = db.FWallet.Include(j=>j.GLAccount).Include(k=>k.Department).Include(l=>l.PaymentAccounts).Single(l => l.Id == i.Id);
                        //
                        var taxes = "[";
                        var tax = 0.0;
                        foreach (var t in payment.Taxes)
                        {
                            tax += Utils.ApplyTax(payment.GrossAmount, t);
                            taxes += "\""+t.Name+"\",";
                        }
                        //
                        taxes = taxes.TrimEnd(',');
                        taxes = taxes + "]";
                        //
                        d = d + "\"amt\":\"" + payment.GrossAmount+ "\",\"dis\":\"" + payment.Discount+ "\",\"tax\":\"" + tax + "\",\"taxes\":"+taxes+ ",\"net\":\"" + await Utils.ComputeAmountPayable(payment) + "\"";
                        //
                        //if (payment.PaymentAccount == null)
                        //{
                            d = d + ",\"pid\":\"" + payment.Id + "\",\"did\":\"" + payment.DeptId + "\",\"dname\":\"" + payment.Department.Name + "\",\"aid\":\"" + payment.GLAccount.Id + "\",\"aname\":\"" + payment.GLAccount.Name + "\"";
                        //}
                        //
                        d = d + "},";
                    }
                    //
                    d = d.TrimEnd(',');
                    //
                    d = d + "],\"idate\":\""+p.InvoiceDate.ToString("yyyy-MM-dd")+"\",\"idue\":\""+p.DueDate.ToString("yyyy-MM-dd")+"\"},";
                }
                //
                d = d.TrimEnd(',');
            }
            d = d + "]";
            var h = JToken.Parse(d);
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("staxinvoices")]
        public async Task<JsonResult> GetSupplierTaxableInvoices(int sid, int tid)
        {
            //
            if (sid == 0 || tid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var tax = db.FTaxes.Single(k => k.Id == tid);
            //
            var sup = await db.FSuppliers.Include(k => k.Invoices).ThenInclude(k => k.Payments).ThenInclude(l=>l.CurrencyUsed).Include(k => k.Invoices).ThenInclude(k => k.Payments).ThenInclude(k => k.GLTransactions)
                .Include(k => k.Invoices).ThenInclude(k=>k.Payments).ThenInclude(k => k.Taxes).Include(k => k.Invoices).ThenInclude(k => k.Payments).ThenInclude(k => k.Voucher)
                .SingleOrDefaultAsync(l => l.Id == sid && l.Invoices.Any(m=>m.Payments.Any(n=>n.Taxes.Any(u=>u.Id == tax.Id))));
            //
            var d = "[";
            //
            if (sup != null)
            {
                //all invoices with a payment payments without an active voucher
                foreach (var inv in sup.Invoices.Where(g=>g.Payments.Any(p =>p.Taxes.Any(x=>x.Id == tid) && (p.Voucher == null || p.Voucher.Status == PVStatus.Cancelled || !p.GLTransactions.Any(l=>l.Kind == GLTransactionKind.Tax && l.Type == GLTransactionType.Debit)))).ToList())
                {
                    d += "{\"id\":\"" + inv.Id + "\",\"dno\":\"" + inv.DocNo + "\",\"ord\":\"" + inv.OrderNo + "\",\"idate\":\"" + inv.InvoiceDate.ToString("dd MMM yyyy") + "\",\"idue\":\"" + inv.DueDate.ToString("dd MMM yyyy") + "\",\"pps\":[";
                    //those with payments
                    foreach (var py in inv.Payments.Where(l=>l.Taxes.Any(s=>s.Id == tax.Id)).ToList())
                    {
                        var tt = 0.0;
                        if(tax.Rate > 0)
                            tt = (py.GrossAmount - py.Discount) * tax.Rate / 100;
                        //
                        d += "{\"pid\":\""+py.Id+"\",\"cur\":\""+py.CurrencyUsed.Alias+"\",\"pamt\":\""+py.GrossAmount.ToString("#,##0") + "\",\"pdis\":\""+py.Discount.ToString("#,##0") + "\",\"tax\":\""+tt.ToString("#,##0")+"\"},";
                    }
                    //
                    d = d.TrimEnd(',');
                    //
                    d+="]},";
                }
                d = d.TrimEnd(',');
            }
            d += "]";
            var h = JToken.Parse(d);
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("getvoucherno")]
        public async Task<JsonResult> GetVoucherNumber(int lid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (lid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }

            //
            var d = "{";
            try
            {
                var lgr = db.FLedgers.Include(l => l.PVoucherPrefixes).SingleOrDefault(k => k.Id == lid);
                if (lgr != null)
                {
                    var s = lgr.PVoucherPrefixes.OrderByDescending(k => k.DateCreated).First()?.Code;
                    d = d + "\"s\":\"1\",\"vp\":\"" + (s + Utils.GenerateVoucherNo(db)) + "\"";
                }
            }
            catch
            {
                d = d + "\"s\":\"0\"";
            }
            d = d + "}";
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("faccs")]
        public async Task<JsonResult> GetFinanceAccounts()
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();
            
            //
            var d = "[";
            var parents = db.FinanceAccounts.Include(l=>l.Parent).Include(l=>l.Children).Where(l=>l.Parent == null).ToList();
            if (parents != null && parents.Count > 0)
            {
                foreach (var parent in parents)
                {
                    d = d + "{\"id\":\"" + parent.Id + "\",\"name\":\"" + parent.Name + "\",\"typ\":\"" + (parent.Type) + "\",\"childs\":[";
                    foreach(var child in parent.Children)
                    {
                        d = d + "{\"id\":\""+child.Id+"\",\"name\":\""+child.Name+"\",\"typ\":\""+(child.Type)+"\",\"bitem\":\""+(child.HasBudget?"1":"0")+"\"},";
                    }
                    //
                    d = d.TrimEnd(',');
                    //
                    d = d + "]},";
                }
                //
                d = d.TrimEnd(',');
            }
            d = d + "]";
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("lpaccs")]
        public async Task<JsonResult> GetLedgerPaymentAccounts(int lid)
        {
            ////
            var db = provider.GetRequiredService<ApplicationDbContext>();

            //
            var d = "[";
            var lgr = db.FLedgers.Include(l => l.PaymentAccounts).SingleOrDefault(l=>l.Id == lid);
            if (lgr != null)
            {
                foreach (var acc in lgr.PaymentAccounts)
                {
                    d = d + "{\"id\":\"" + acc.Id + "\",\"name\":\"" + acc.Name + "\",\"acno\":\"" + (acc.AccountNumber) + "\"},";
                }
                //
                d = d.TrimEnd(',');
            }
            d = d + "]";
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("basecur")]
        public JsonResult GetBaseCurrency()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();

            //
            var d = "{";
            var cur = db.FCurrencies.SingleOrDefault(l=>l.IsBase);
            if (cur != null)
            {
                d = d + "\"id\":\"" + cur.Id + "\",\"name\":\"" + cur.Name + "\",\"alias\":\"" + (cur.Alias) + "\"";
            }
            d = d + "}";
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("warinfo")]
        public JsonResult GetImprestDetails(int wid)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if(wid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }
            //
            var d = "{";
            var war = db.FImprestWarrants.Include(l=>l.Staff).Include(k=>k.Ledger).ThenInclude(l=>l.PaymentAccounts).Include(k=>k.Project).Include(l=>l.Payments).SingleOrDefault(l => l.Id == wid);
            if (war != null)
            {
                d = d + "\"id\":\"" + war.Id + "\",\"sname\":\"" + war.Staff.FullName + "\",\"sno\":\"" + war.Staff.EmplNo +
                    "\",\"proj\":\"" + war.Project?.Description + "\",\"desc\":\"" + war.Description + "\",\"impd\":\"" + war.CreateDate.ToString("yyyy-MM-dd")
                    + "\",\"surd\":\"" + war.SurrenderDate.ToString("yyyy-MM-dd");
                //
                var cur = db.FCurrencies.Single(p => p.IsBase);
                var total = 0.0;
                foreach (var p in war.Payments)
                {
                    var payment = db.FWallet.Include(l => l.CurrencyUsed).Single(l => l.Id == p.Id);
                    total += p.GrossAmount;
                    if (cur.Id != payment.CurrencyUsed.Id)
                    {
                        cur = payment.CurrencyUsed;
                    }
                }
                //
                d +="\",\"amt\":\""+ total.ToString("#,##0") + "\",\"cur\":\""+cur.Alias+ "\",\"lgr\":\"" + war.Ledger.Name + "\"";
            }
            d = d + "}";
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = d });
        }
        [HttpGet("staffwars")]
        public JsonResult GetStaffImprestDisbursements(int sid, int typ = 0)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (sid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }
            //
            var d = "{";
            var wars = db.FImprestWarrants.Include(l => l.Staff).Include(k => k.Disbursement)
                .Include(k => k.Disbursement).ThenInclude(k=>k.Surrender).Include(k => k.Payments)
                .Include(l=>l.Ledger).ThenInclude(l=>l.PaymentAccounts).Where(l => l.Staff.Id == sid).ToList();
            //
            if (wars != null && wars.Count > 0)
            {
                var staff = wars.First().Staff;
                d = d + "\"sid\":\"" + staff.Id + "\",\"sname\":\"" + staff.FullName + "\",\"sno\":\"" + staff.EmplNo+ "\",";

                d = d + "\"winf\":[";
                foreach(var war in wars)
                {
                    d += "{\"wdate\":\"" + war.CreateDate.ToString("yyyy-MM-dd") + "\",\"surd\":\"" + war.SurrenderDate.ToString("yyyy-MM-dd") + "\",";
                    if (war.Disbursement != null && typ == 0)
                    {
                        d += "\"dis\":{\"did\":\"" + war.Disbursement.Id + "\",\"ddate\":\"" + war.Disbursement.DateCreated.ToString("yyyy-MM-dd") + "\",\"dsid\":\"" + war.Disbursement.SurrenderId + "\",\"dsdate\":\"" + war.Disbursement.Surrender?.DateSurrendered.ToString("yyyy-MM-dd") + "\"},";
                    }
                    //
                    var cur = db.FCurrencies.Single(p => p.IsBase);
                    var total = 0.0;
                    //
                    d = d + "\"accs\":[";
                    foreach (var p in war.Payments)
                    {
                        var payment = db.FWallet.Include(l => l.CurrencyUsed).Include(l=>l.GLAccount).Include(l=>l.Department).Single(l => l.Id == p.Id);
                        total += p.GrossAmount;
                        if (cur.Id != payment.CurrencyUsed.Id)
                        {
                            cur = payment.CurrencyUsed;
                        }
                        d = d + "{\"aid\":\""+payment.AccountId+ "\",\"aname\":\"" + payment.GLAccount.Name + "\",\"dname\":\"" + payment.Department.Name+ "\",\"did\":\"" + payment.DeptId + "\",\"amt\":\""+payment.GrossAmount.ToString("#,##0") + "\"},";
                    }
                    //
                    d = d.TrimEnd(',');
                    //
                    d += "],\"damt\":\"" + total.ToString("#,##0") + "\",\"cur\":\"" + cur.Alias + "\",\"lgr\":\"" + war.Ledger.Name + "\",\"war\":\""+war.Ref+ "\",\"wid\":\"" + war.Id + "\",";
                    //
                    d = d + "\"pmodes\":[";
                    foreach (var p in war.Ledger.PaymentAccounts)
                    {
                        d = d + "{\"pid\":\""+p.Id+"\",\"pname\":\""+p.Name+"\"},";
                    }
                    //
                    d = d.TrimEnd(',');
                    //
                    d = d + "]";
                    //
                    d = d + "},";
                }
                //
                d = d.TrimEnd(',');
                //
                d += "]";              
            }
            //
            d = d + "}";
            //
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = JToken.Parse(d).ToString() });
        }

        [HttpGet("staffec")]
        public JsonResult GetStaffExpenseClaims(int sid, int typ = 0)
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();

            if (sid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }
            //
            var d = "{";
            List<ExpenseClaim> list = null;
            //
            if(typ == 1)
                list = db.FExpenseClaims.Include(l => l.Staff).Include(k => k.Disbursement).Include(k => k.Payments).ThenInclude(l => l.CurrencyUsed)
                .Include(l => l.Ledger).ThenInclude(l => l.PaymentAccounts).Include(k => k.ClaimEntries).Where(l => l.Staff.Id == sid && l.Disbursement == null).ToList();
            else
                list = db.FExpenseClaims.Include(l => l.Staff).Include(k => k.Disbursement).Include(k => k.Payments).ThenInclude(l => l.CurrencyUsed)
                .Include(l => l.Ledger).ThenInclude(l => l.PaymentAccounts).Include(k => k.ClaimEntries).Where(l => l.Staff.Id == sid).ToList();
            //
            if (list != null && list.Count > 0)
            {
                var staff = list.First().Staff;
                d = d + "\"sname\":\"" + staff.FullName + "\",\"sno\":\"" + staff.EmplNo + "\",\"ecs\":[";
                foreach (var claim in list)
                {
                    d += "{\"cdate\":\"" + claim.DateCreated.ToString("yyyy-MM-dd") + "\",\"duty\":\"" + claim.DutyCalled + "\",";
                    if (claim.Disbursement != null)
                    {
                        d += "\"dis\":{\"did\":\"" + claim.Disbursement.Id + "\",\"ddate\":\"" + claim.DateCreated.ToString("yyyy-MM-dd") + "\",\"ddis\":\"" + claim.Disbursement.DateDisbursed.ToString("yyyy-MM-dd") + "\"},";
                    }
                    //
                    var cur = db.FCurrencies.Single(p => p.IsBase);
                    var total = 0.0;
                    //
                    d = d + "\"accs\":[";
                    foreach (var p in claim.Payments.Distinct())
                    {
                        var payment = db.FWallet.Include(l => l.CurrencyUsed).Include(l => l.GLAccount).Include(l => l.Department).Single(l => l.Id == p.Id);

                        d = d + "{\"gl\":\""+payment.GLAccount.Name+ "\",\"id\":\"" + payment.AccountId + "\",\"pys\":[";
                        //
                        foreach(var f in claim.Payments.Where(o=>o.AccountId == payment.AccountId))
                        {
                            total += p.GrossAmount;
                            //
                            d = d + "{\"id\":\"" + payment.Id + "\",\"aname\":\"" + payment.GLAccount.Name + "\",\"dname\":\"" + payment.Department.Name + "\",\"did\":\"" + payment.DeptId + "\",\"amt\":\"" + payment.GrossAmount.ToString("#,##0") + "\"},";
                        }
                        //
                        d = d.TrimEnd(',');
                        d = d + "]},";
                    }
                    //
                    d = d.TrimEnd(',');
                    //
                    d += "],\"camt\":\"" + total.ToString("#,##0") + "\",\"cur\":\"" + cur.Alias + "\",\"lgr\":\"" + claim.Ledger.Name + "\",\"lid\":\"" + claim.LedgerId + "\",\"cref\":\"" + claim.Ref + "\",\"cid\":\"" + claim.Id + "\",";
                    //
                    d = d + "\"pmodes\":[";
                    foreach (var p in claim.Ledger.PaymentAccounts)
                    {
                        d = d + "{\"pid\":\"" + p.Id + "\",\"pname\":\"" + p.Name + "\"},";
                    }
                    //
                    d = d.TrimEnd(',');
                    d = d + "],";
                    //Millage
                    if (claim.Mileage != null)
                    {
                        d = d + "\"mlg\":{\"frm\":\""+claim.Mileage.DestFrom+ "\",\"to\":\""+claim.Mileage.DesTo+"\",\"dist\":\""+claim.Mileage.Distance+ "\",\"regno\":\""+claim.Mileage.RegNo+"\",\"id\":\""+claim.Mileage.Id+"\"},";
                    }
                    //Substainace
                    //if (claim.Millage != null)
                    //{
                    //    d = d + "\"mlg\":{\"frm\":\"" + claim.Millage.DestFrom + "\",\"to\":\"" + claim.Millage.DesTo + "\",\"dist\":\"" + claim.Millage.Distance + "\",\"regno\":\"" + claim.Millage.RegNo + "\",\"id\":\"" + claim.Millage.Id + "\"}";
                    //}
                    //Entries
                    d = d + "\"ents\":[";
                    foreach (var ent in claim.ClaimEntries)
                    {
                        d = d + "{\"id\":\"" + ent.Id + "\",\"qty\":\"" + ent.Quantity + "\",\"cost\":\"" + ent.UnitCost + "\",\"name\":\"" + ent.UnitName + "\",\"date\":\"" + ent.DateIncured?.ToString("yyyy-MM-dd") + "\"},";
                    }
                    d = d.TrimEnd(',');
                    //
                    d = d + "]";
                    //
                    d = d + "},";
                }
                //
                d = d.TrimEnd(',');
                //
                d += "]";
            }
            //
            d = d + "}";
            //
            //logger.LogInformation(8, "Pending Supplier Invoices = " + d);
            return new JsonResult(new { s = "1", data = JToken.Parse(d).ToString() });
        }

        [HttpGet("invinfo")]
        public JsonResult GetInvoiceDetails(int id)
        {
            if (id == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }
            //
            var d = "{";

            var db = provider.GetRequiredService<ApplicationDbContext>();

            var inv = db.FInvoices.Include(l=>l.Payments).Include(k=>k.Supplier).ThenInclude(l=>l.VendorType).Include(k=>k.SupplierInvoiceWaivers).SingleOrDefault(l => l.Id == id && l.SupplierInvoiceWaivers.Count >=0);
            if(inv != null)
            {
                d = d + "\"dno\":\""+inv.DocNo+ "\",\"idate\":\""+inv.InvoiceDate.ToString("yyyy-MM-dd")+"\",\"idue\":\""+inv.DueDate.ToString("yyyy-MM-dd")+"\",\"amt\":\""+inv.Amount.ToString("#,##0")+"\",\"ord\":\""+inv.OrderNo+"\",\"proj\":\""+inv.Project?.Name+ "\",";
                //
                d = d + "\"sup\":{\"name\":\"" + inv.Supplier.Name + "\",\"id\":\"" + inv.SupplierId + "\",\"typ\":\""+inv.Supplier.VendorType.Name+"\",\"addr\":\""+inv.Supplier.Address+"\"},";
                //
                d += "\"iws\":[";
                foreach(var waiver in inv.SupplierInvoiceWaivers)
                {
                    d = d + "{\"wid\":\""+waiver.Id+"\",\"wamt\":\""+waiver.GrossAmount+"\",\"ref\":\""+waiver.RefNo+"\"},";
                }
                //
                d = d.TrimEnd(',');
                //
                d = d + "],";
                //
                d = d + "\"pys\":[";
                foreach(var p in inv.Payments.Select(l=>l.AccountId).Distinct())
                {
                    var payment = db.FWallet.Include(j=>j.GLAccount).Include(l=>l.Department).First(l=>l.AccountId == p);
                    d = d + "{\"aid\":\""+p+"\",\"aname\":\""+payment.GLAccount.Name+"\",\"amt\":\""+payment.GrossAmount.ToString("#,##0")+"\",\"did\":\""+payment.DeptId+"\",\"dname\":\""+payment.Department.Name+"\"},";
                }
                d = d.TrimEnd(',');
                //
                d = d + "]";
            }
            d = d + "}";
            return new JsonResult(new { s = "1", data = JToken.Parse(d).ToString() });
        }

        [HttpGet("fcinfo")]
        public JsonResult GetCashierOfficeInfo(int oid, int sid)
        {
            if (oid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }
            //
            var d = "{";

            var db = provider.GetRequiredService<ApplicationDbContext>();

            var office = db.FCashierOffices.Include(j=>j.PaymentAccounts).Include(l=>l.Attendants).SingleOrDefault(k=>k.Id == oid);

            if(office != null)
            {
                d = d + "\"name\":\""+office.Name+ "\",\"loc\":\""+office.Location+ "\",\"ref\":\""+office.RefNo+ "\",\"accs\":\""+office.PaymentAccounts.Count+ "\",\"dc\":\"" + office.DateCreated.ToString("dd MMM yyyy") + "\",\"status\":\"" + office.Status+"\"";
            }

            d = d + "}";
            return new JsonResult(new { s = "1", data = JToken.Parse(d).ToString() });
        }
        [HttpGet("staffpc")]
        public JsonResult GetStaffPettyCashReqs(int sid, int typ=0)
        {
            if (sid == 0)
            {
                return new JsonResult(new { s = "0", errs = "[\"Bad Request\"]" });
            }
            //
            var d = "[";

            var db = provider.GetRequiredService<ApplicationDbContext>();

            List<PettyCashReq> list;
            //
            if(typ == 1)
                list = db.FPettyCashReqs.Include(j => j.Staff).Include(l => l.Disbursement).Include(l => l.Ledger).Include(l => l.Payments).Include(l => l.Project).Where(k => k.StaffId == sid && k.Disbursement == null).ToList();
            else
                list = db.FPettyCashReqs.Include(j => j.Staff).Include(l => l.Disbursement).Include(l => l.Ledger).Include(l => l.Payments).Include(l => l.Project).Where(k => k.StaffId == sid).ToList();
            //
            if (list.Count > 0)
            {
                foreach(var pc in list)
                {
                    d = d + "{\"id\":\""+pc.Id+ "\",\"sname\":\"" + pc.Staff.FullName + "\",\"sno\":\"" + pc.Staff.EmplNo + "\",\"cdate\":\"" + pc.DateCreated.ToString("yyyy-MM-dd") + "\",\"desc\":\"" + pc.Description + "\",\"ref\":\"PC" + pc.Ref.ToString("0000") + "\",\"pid\":\"" + pc.Project?.Id + "\",\"proj\":\"" + pc.Project?.Name + "\",\"lgr\":\"" + pc.Ledger.Name + "\",\"status\":\"" + pc.Status + "\",";
                    //
                    if (pc.Disbursement != null)
                    {
                        d = d + "\"pcw\":{\"id\":\"" + pc.Disbursement .Id+ "\",\"wdate\":\""+pc.Disbursement.DateDisbursed.ToString("dd MMM yyyy")+"\"},";
                    }
                    //
                    d = d + "\"pcpys\":[";
                    if (pc.Payments.Count > 0)
                    {
                       foreach(var h in pc.Payments)
                        {
                            d = d + "{\"aid\":\"" + h.AccountId + "\",\"did\":\"" + h.DeptId + "\",\"amt\":\""+h.GrossAmount.ToString("#,##0")+"\",\"pdate\":\""+h.DateCreated.ToString("dd MMM yyyy")+"\"},";
                        }

                        d = d.TrimEnd(',');
                    }
                    d = d + "]},";
                }
                d = d.TrimEnd(',');
            }

            d = d + "]";
            return new JsonResult(new { s = "1", data = JToken.Parse(d).ToString() });
        }

        #endregion
    }
}