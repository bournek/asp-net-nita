﻿namespace Nita.Controllers
{
    using Boilerplate.AspNetCore;
    using Microsoft.AspNetCore.Mvc;
    using com.nita.Constants;
    using Microsoft.AspNetCore.Authorization;
    using com.nita.Settings;
    using com.nita.ViewModels;
    using Microsoft.Extensions.DependencyInjection;
    using com.nita.IdentityModels;
    using System.Linq;
    using System;

    /// <summary>
    /// Provides methods that respond to HTTP requests with HTTP errors.
    /// </summary>
    [Authorize(DefaultRoles.NitaSysAdmin)]
    public sealed partial class PayrollController : Controller
    {

        #region Pay Grades
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("alljobgrd", Name = PayrollUtilsControllerRoute.GetAllJobGroups)]
        public IActionResult AllPayGrades()
        {


            return this.View(PayrollUtilsControllerAction.AllJobGroups);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addjobgrd", Name = PayrollUtilsControllerRoute.GetAddJobGroup)]
        public IActionResult AddPayGrade()
        {


            return this.View(PayrollUtilsControllerAction.AddJobGroup);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addjobgrd", Name = PayrollUtilsControllerRoute.GetAddJobGroup)]
        [ValidateAntiForgeryToken]
        public IActionResult AddPayGrade(AddJobGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                //
                var db = provider.GetRequiredService<ApplicationDbContext>();

                if (model.StartDate.CompareTo(model.EndDate) > 0)
                {
                    ModelState.AddModelError(string.Empty, "Effective date cannot be after End date.");
                    return this.View(PayrollUtilsControllerAction.AddJobGroup, model);
                }
                //
                var grp = new JobGroup()
                {
                    Name = model.Name,
                    Rank = model.Rank,
                    Increment = model.Increment,
                    MaxPay = model.MaxPay,
                    MinPay = model.MinPay,
                    EndDate = model.EndDate,
                    StartDate = model.StartDate
                };
                //

                db.PJobGroups.Add(grp);
                db.SaveChanges();
                //
                return RedirectToRoute(PayrollUtilsControllerRoute.GetAllJobGroups, new { msg = "Job group '" + model.Name + "' was successfully." });
            }

            return this.View(PayrollUtilsControllerAction.AddJobGroup, model);
        }
        #endregion

        #region Pay Accounts
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpayacc", Name = PayrollUtilsControllerRoute.GetAllPayAccounts)]
        public IActionResult AllPayTypes()
        {


            return this.View(PayrollUtilsControllerAction.AllPayAccounts);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addpayacc", Name = PayrollUtilsControllerRoute.GetAddPayAccount)]
        public IActionResult AddPayAccount()
        {


            return this.View(PayrollUtilsControllerAction.AddPayAccount);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addpayacc", Name = PayrollUtilsControllerRoute.GetAddPayAccount)]
        [ValidateAntiForgeryToken]
        public IActionResult AddPayAccount(AddPayAccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                //
                var db = provider.GetRequiredService<ApplicationDbContext>();

                var acc = new PayAccount()
                {
                    Code = model.Code,
                    Description = model.Description
                };
                try
                {
                    //
                    var gl = db.FinanceAccounts.Single(p => p.Id == model.Account);
                    acc.AccountId = gl.Id;
                    //
                    if (model.IsRegular)
                    {
                        acc.AccountType = PayAccountType.Regular;
                        switch (model.PayType)
                        {
                            case (int)PayType.Deduction:
                                //
                                acc.PayType = PayType.Deduction;
                                break;
                            case (int)PayType.Earning:
                                //
                                acc.PayType = PayType.Earning;
                                break;
                            case (int)PayType.Loan:
                                //
                                acc.PayType = PayType.Loan;
                                break;
                            case (int)PayType.Pension:
                                //
                                acc.PayType = PayType.Pension;
                                break;
                            case (int)PayType.Union:
                                //
                                acc.PayType = PayType.Union;
                                break;
                            default:
                                //???
                                ModelState.AddModelError(string.Empty, "Invalid 'Pay Type' selected for regular pay account.");
                                return this.View(PayrollUtilsControllerAction.AddPayAccount, model);

                        }
                        //
                        if (model.ContributionAccount > 0)
                        {
                            var ecgl = db.FinanceAccounts.Single(l => l.Id == model.ContributionAccount);
                            acc.ECAccountId = ecgl.Id;
                        }
                    }
                    else
                    {
                        acc.AccountType = PayAccountType.Irregular;

                        //
                        switch (model.PayType)
                        {
                            case (int)PayType.Deduction:
                                //
                                acc.PayType = PayType.Deduction;
                                break;
                            case (int)PayType.Earning:
                                //
                                acc.PayType = PayType.Earning;
                                break;

                            default:
                                //???
                                ModelState.AddModelError(string.Empty, "Invalid 'Pay Type' selected for Irregular pay account.");
                                return this.View(PayrollUtilsControllerAction.AddPayAccount, model);

                        }
                        //
                        acc.PAYEThreshold = model.PayeThreshold;
                    }
                    //
                    if (model.Deductions != null && model.Deductions.Length > 0)
                    {
                        foreach (var d in model.Deductions)
                        {
                            var ded = db.PDeductions.SingleOrDefault(l => ((int)l.Type == d));
                            if (ded == null)
                            {
                                ded = new PayrollDeduction();
                                switch (d)
                                {
                                    case (int)DeductionType.AllowableDeduction:
                                        //
                                        ded.Type = DeductionType.AllowableDeduction;
                                        ded.Name = "Allowable Deduction";
                                        break;
                                    case (int)DeductionType.NHIF:
                                        //
                                        ded.Type = DeductionType.NHIF;
                                        ded.Name = "NHIF";
                                        break;
                                    case (int)DeductionType.PAYE:
                                        //
                                        ded.Type = DeductionType.PAYE;
                                        ded.Name = "P.A.Y.E";
                                        break;
                                    default:
                                        //
                                        ModelState.AddModelError(string.Empty, "Invalid Deduction specified.");
                                        return this.View(PayrollUtilsControllerAction.AddPayAccount, model);
                                }
                            }
                            //
                            db.PDeductions.Add(ded);
                            db.SaveChanges();
                            //
                            acc.Deductions.Add(ded);
                        }
                    }
                    //
                    acc.Status = model.IsActive ? EntityStatus.Active : EntityStatus.Inactive;

                    //
                    db.PPayAccounts.Add(acc);
                    db.SaveChanges();

                    //
                    return RedirectToRoute(PayrollUtilsControllerRoute.GetAllPayAccounts, new { msg="Pay Account was added successfully."});
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Pay Account could NOT be created.");
                }
            }

            //
            return this.View(PayrollUtilsControllerAction.AddPayAccount,model);
        }
        #endregion

        #region Salary Periods
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allsalperiod", Name = PayrollUtilsControllerRoute.GetAllSalaryPeriods)]
        public IActionResult AllSalaryPeriods()
        {


            return this.View(PayrollUtilsControllerAction.AllSalaryPeriods);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addsalperiod", Name = PayrollUtilsControllerRoute.GetAddSalaryPeriod)]
        public IActionResult AddSalaryPeriod()
        {


            return this.View(PayrollUtilsControllerAction.AddSalaryPeriod);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addsalperiod", Name = PayrollUtilsControllerRoute.GetAddSalaryPeriod)]
        [ValidateAntiForgeryToken]
        public IActionResult AddSalaryPeriod(AddSalaryPeriodViewModel model)
        {
            if (ModelState.IsValid)
            {
                //
                var db = provider.GetRequiredService<ApplicationDbContext>();

                DateTime yr;
                try
                {
                    yr = DateTime.Parse("01-01-" + model.Year);
                }
                catch
                {
                    ModelState.AddModelError("", "Invalid year selected.");
                    return this.View(PayrollUtilsControllerAction.AddSalaryPeriod, model);
                }

                //
                try
                {
                    if (db.PSalaryPeriods.Any(p => p.Year.Year == model.Year))
                    {
                        ModelState.AddModelError(string.Empty, "A Salary period with simillar year already exists.");
                        return this.View(PayrollUtilsControllerAction.AddSalaryPeriod, model);
                    }

                    //
                    if (db.PSalaryPeriods.Any(j => (j.BeginDate.CompareTo(model.BeginDate) >= 0 && j.EndDate.CompareTo(model.EndDate) <= 0)))
                    {
                        ModelState.AddModelError(string.Empty, "The specified salary period overlaps an existing period.");
                        return this.View(PayrollUtilsControllerAction.AddSalaryPeriod, model);
                    }

                    //
                    var period = new SalaryPeriod()
                    {
                        Name = model.Name,
                        BeginDate = model.BeginDate,
                        EndDate = model.EndDate,
                        Year = yr
                    };
                    //
                    period.PayFreq = model.PayFreq == (int)PeriodFrequency.Monthly ? PeriodFrequency.Monthly :
                        model.PayFreq == (int)PeriodFrequency.Weekly ? PeriodFrequency.Weekly : PeriodFrequency.Irregular;
                    //

                    db.PSalaryPeriods.Add(period);
                    db.SaveChanges();

                    return RedirectToRoute(PayrollUtilsControllerRoute.GetAllSalaryPeriods, new { msg = "Salary period was created successfully." });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Add salary failed.");
                }
            }
            //
            return this.View(PayrollUtilsControllerAction.AddSalaryPeriod,model);
        }
        #endregion

        #region Paye Rates
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpyrates", Name = PayrollUtilsControllerRoute.GetAllPayeRates)]
        public IActionResult AllPayeRates()
        {


            return this.View(PayrollUtilsControllerAction.AllPayeRates);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addpyrate", Name = PayrollUtilsControllerRoute.GetAddPayeRate)]
        public IActionResult AddPayeRate()
        {


            return this.View(PayrollUtilsControllerAction.AddPayeRate);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addpyrate", Name = PayrollUtilsControllerRoute.GetAddPayeRate)]
        [ValidateAntiForgeryToken]
        public IActionResult AddPayeRate(AddPayeRateViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();

                if(db.PPayeRates.Any(k=>k.Code == model.Code))
                {
                    ModelState.AddModelError(string.Empty, "A P.A.Y.E rate with simillar code already exist.");
                    return this.View(PayrollUtilsControllerAction.AddPayeRate, model);
                }
                //
                //
                if (db.PPayeRates.Any(j => (j.StartDate.CompareTo(model.StartDate) >= 0 && j.EndDate.CompareTo(model.EndDate) <= 0)))
                {
                    ModelState.AddModelError(string.Empty, "The specified P.A.Y.E period overlaps an existing P.A.Y.E period. Review the dates and try again.");
                    return this.View(PayrollUtilsControllerAction.AddSalaryPeriod, model);
                }
                //
                var rate = new PayeRate()
                {
                    Code = model.Code,
                    Description = model.Description,
                    EndDate = model.EndDate,
                    StartDate = model.StartDate,
                    PersonalRelief = model.PersonalRelief
                };

                db.PPayeRates.Add(rate);
                db.SaveChanges();

                //
                return RedirectToRoute(PayrollUtilsControllerRoute.GetAllPayeRates, new { msg = "P.A.Y.E rate was sadded successfully." });
            }

            //Invalid model.
            return this.View(PayrollUtilsControllerAction.AddPayeRate,model);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addpyrrange", Name = PayrollUtilsControllerRoute.GetAddPayeRateRange)]
        public IActionResult AddPayeRateRange()
        {


            return this.View(PayrollUtilsControllerAction.AddPayeTaxRange);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addpyrrange", Name = PayrollUtilsControllerRoute.GetAddPayeRateRange)]
        [ValidateAntiForgeryToken]
        public IActionResult AddPayeRateRange(AddDeductionRangeViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();

                var rate = db.PPayeRates.Single(l => l.Id == model.RateId);

                if(rate.Rates.Any(j=>j.Min >= model.Min && j.Max <= model.Max))
                {
                    ModelState.AddModelError("", "The specified bracket '" + model.Min + "' <->'" + model.Max + "' overlaps an existing bracket for this P.A.Y.E Rate.");
                    return this.View(PayrollUtilsControllerAction.AddPayeTaxRange, model);
                }

                //
                var range = new TaxRate()
                {
                    Min = model.Min,
                    Max = model.Max,
                    DeductionAmount = model.Amount
                };

                rate.Rates.Add(range);
                db.SaveChanges();

                //
                return RedirectToRoute(PayrollUtilsControllerRoute.GetAllPayeRates, new { msg="P.A.Y.E Rate Bracket was succesifully added to the system."});
            }

            return this.View(PayrollUtilsControllerAction.AddPayeTaxRange,model);
        }
        #endregion

        #region NHIF Rates
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allnhrates", Name = PayrollUtilsControllerRoute.GetAllNhifRates)]
        public IActionResult AllNhifRates()
        {


            return this.View(PayrollUtilsControllerAction.AllNhifRates);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addnhrate", Name = PayrollUtilsControllerRoute.GetAddNhifRate)]
        public IActionResult AddNhifRate()
        {


            return this.View(PayrollUtilsControllerAction.AddNhifRate);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addnhrate", Name = PayrollUtilsControllerRoute.GetAddNhifRate)]
        [ValidateAntiForgeryToken]
        public IActionResult AddNhifRate(AddNhifRateViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();

                //
                if (db.PNhifRates.Any(j => (j.StartDate.CompareTo(model.StartDate) >= 0 && j.EndDate.CompareTo(model.EndDate) <= 0)))
                {
                    ModelState.AddModelError(string.Empty, "The specified NHIF period overlaps an existing NHIF period. Review the dates and try again.");
                    return this.View(PayrollUtilsControllerAction.AddSalaryPeriod, model);
                }

                //
                var rate = new NhifRate()
                {
                    Description = model.Description,
                    EndDate = model.EndDate,
                    StartDate = model.StartDate
                };

                //
                db.PNhifRates.Add(rate);
                db.SaveChanges();

                //
                return RedirectToRoute(PayrollUtilsControllerRoute.GetAllNhifRates, new { msg = "NHIF Rate was added successfully." });
            }

            return this.View(PayrollUtilsControllerAction.AddNhifRate);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addnhrrange", Name = PayrollUtilsControllerRoute.GetAddNhifRateRange)]
        public IActionResult AddNhifRateRange()
        {


            return this.View(PayrollUtilsControllerAction.AddNhifRateRange);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addnhrrange", Name = PayrollUtilsControllerRoute.GetAddNhifRateRange)]
        [ValidateAntiForgeryToken]
        public IActionResult AddNhifRateRange(AddDeductionRangeViewModel model)
        {
            if (ModelState.IsValid)
            {
                ////
                var db = provider.GetRequiredService<ApplicationDbContext>();

                var rate = db.PNhifRates.Single(l => l.Id == model.RateId);

                if (rate.Ranges.Any(j => j.Min >= model.Min && j.Max <= model.Max))
                {
                    ModelState.AddModelError("", "The specified bracket '" + model.Min + "' <->'" + model.Max + "' overlaps an existing bracket for this NHIF Rate.");
                    return this.View(PayrollUtilsControllerAction.AddPayeTaxRange, model);
                }

                //
                var range = new NhifRange()
                {
                    Min = model.Min,
                    Max = model.Max,
                    DeductionAmount = model.Amount
                };

                rate.Ranges.Add(range);
                db.SaveChanges();

                //
                return RedirectToRoute(PayrollUtilsControllerRoute.GetAllNhifRates, new { msg = "NHIF Rate Bracket was succesifully added to the system." });
            }
            //
            return this.View(PayrollUtilsControllerAction.AddNhifRateRange,model);
        }
        #endregion

        #region Renumeration Schemes
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("allrenumschs", Name = PayrollUtilsControllerRoute.GetAllRenumSchemes)]
        public IActionResult AllRenumSchemes()
        {


            return this.View(PayrollUtilsControllerAction.AllRenumSchemes);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <returns></returns>
        [HttpGet("addrenumschs", Name = PayrollUtilsControllerRoute.GetAddRenumScheme)]
        public IActionResult AddRenumScheme()
        {


            return this.View(PayrollUtilsControllerAction.AddRenumScheme);
        }
        /// <summary>
        /// @BourneK
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("addrenumschs", Name = PayrollUtilsControllerRoute.GetAddRenumScheme)]
        [ValidateAntiForgeryToken]
        public IActionResult AddRenumScheme(AddRenumSchemeViewModel model)
        {
            if (ModelState.IsValid)
            {
                //
                var db = provider.GetRequiredService<ApplicationDbContext>();


                //
                return RedirectToRoute(PayrollUtilsControllerRoute.GetAllRenumSchemes, new { msg = "Employment Type was added successfully." });
            }

            return this.View(PayrollUtilsControllerAction.AddRenumScheme, model);
        }
        #endregion
    }
}