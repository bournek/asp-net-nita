﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace com.nita
{
    public class Program
    {
        private const string HostingJsonFileName = "hosting.json";

        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            //WebHost.CreateDefaultBuilder(args)
            //    .UseStartup<Startup>()
            //    .Build();

            //
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("config.json", optional: true, reloadOnChange: true)
                .AddJsonFile(HostingJsonFileName, optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();
            //
            IHostingEnvironment hostingEnvironment = null;
            //
            var hostUrl = configuration["urls"];
            //var regEx = new Regex(@"((http[s]?):\/\/)([\w\d\.]*)(?:\:\d+)");
            //var rootUrl = regEx.Match(configuration["urls"]).Value;
            //Uri httpsRootUrl = null;
            //Uri httpRootUrl = null;
            int defaultHttpsPort = 443;//new Uri(rootUrl).Port
            int defaultHttpPort = 5000;

            //  var g = WebHost.CreateDefaultBuilder(args)
            //.UseStartup<Startup>()
            //.Build();

            var host = new WebHostBuilder()
                //.ConfigureLogging((_, factory) =>
                //{
                //    factory.AddConsole();
                //})
                .UseConfiguration(configuration)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseSetting("DesignTime", "true")
                .ConfigureServices(
                        services =>
                        {
                            hostingEnvironment = services
                                .Where(x => x.ServiceType == typeof(IHostingEnvironment))
                                .Select(x => (IHostingEnvironment)x.ImplementationInstance)
                                .First();

                            //
                            if (hostingEnvironment.IsDevelopment())
                            {
                                //
                                if(args.Length > 0)
                                {
                                    foreach (var p in args)
                                    {

                                        if (p.Contains("http"))
                                        {
                                            var regEx = new Regex(@"((http[s]?):\/\/)([\w\d\.]*)(?:\:\d+)");
                                            var rootUrl = regEx.Match(p).Value;

                                            //
                                            //rootUrlPort = new Uri(rootUrl).Port;

                                            var pp = p.Split(";");
                                            foreach (var k in pp)
                                            {
                                                if (k.StartsWith("https"))
                                                {
                                                    int.TryParse(k.Split(":").Last(), out defaultHttpsPort);
                                                }
                                                else if (k.StartsWith("http"))
                                                {
                                                    int.TryParse(k.Split(":").Last(), out defaultHttpPort);
                                                }
                                            }
                                            //
                                            break;
                                        }
                                    }
                                }
                                else
                                {

                                }                                
                            }

                            //LOGGING
                            //
                            var path = Path.Combine(hostingEnvironment.WebRootPath, "logs/nita-mis-{Date}.txt");
                            //Seri-Logging_to-File
                            Log.Logger = new LoggerConfiguration().MinimumLevel.Verbose()
                                .WriteTo.RollingFile(pathFormat: path, restrictedToMinimumLevel: hostingEnvironment.IsDevelopment()?LogEventLevel.Debug : LogEventLevel.Error, outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] {Message}{NewLine}{Exception}", shared: false)
                                .CreateLogger();
                            //
                            Log.Logger.Information("\n\r<<<<<<<<<🍎 Logger Created :: " + DateTime.Now.ToString() + " 🍎>>>>>>>>>>\n\r");
                        });
            //Kestrel
            host.UseKestrel(
                options =>
                {

                    //The maximum number of connections is unlimited (null) by default.
                    //options.Limits.MaxConcurrentConnections = 100;

                    //MaxConcurrentUpgradedConnections
                    //options.Limits.MaxConcurrentUpgradedConnections = 100;

                    //The default maximum request body size is 30,000,000 bytes, which is approximately 28.6MB
                    //options.Limits.MaxRequestBodySize = 10 * 1024;

                    //MinRequestBodyDataRate
                    options.Limits.MinRequestBodyDataRate = new MinDataRate(bytesPerSecond: 100, gracePeriod: TimeSpan.FromSeconds(10));

                    //MinRequestBodyDataRate
                    options.Limits.MinResponseDataRate = new MinDataRate(bytesPerSecond: 100, gracePeriod: TimeSpan.FromSeconds(10));
                    //
                    if (hostingEnvironment.IsDevelopment())
                    {
                        //http
                        options.Listen(System.Net.IPAddress.Loopback, defaultHttpPort);
                        //https
                        //options.Listen(System.Net.IPAddress.Loopback, defaultHttpsPort, listenOptions =>
                        //{
                        //    //
                        //    listenOptions.UseConnectionLogging();
                        //    //
                        //listenOptions.UseHttps(new X509Certificate2("DevelopmentCertificate.pfx", "password"));
                        //    //
                        //    Log.Logger.Information("Loaded Devt. SSL Certificate.");
                        //});
                    }
                    else
                    {
                        //Default Ports
                        //http
                        options.Listen(System.Net.IPAddress.Any, 5000);
                        //https
                        //options.Listen(System.Net.IPAddress.Any, 443, listenOptions =>
                        //{
                        //    if (!string.IsNullOrEmpty(hostUrl))
                        //    {
                        //        host.UseUrls(hostUrl);
                        //    }
                        //    //Replace with production certificate
                        //listenOptions.UseHttps(new X509Certificate2("DevelopmentCertificate.pfx", "password"));
                        //    Log.Logger.Information("Loaded Production SSL Certificate.");
                        //});
                        //
                        host.PreferHostingUrls(true);
                    }
                    // Run callbacks on the transport thread
                    options.ApplicationSchedulingMode = Microsoft.AspNetCore.Server.Kestrel.Transport.Abstractions.Internal.SchedulingMode.Inline;

                    //
                    options.UseSystemd();
                    // The following section should be used to demo sockets
                    //options.ListenUnixSocket("/tmp/kestrel-test.sock");
                })
                //
                .UseLibuv(options =>
                {
                    // Uncomment the following line to change the default number of libuv threads for all endpoints.
                    // options.ThreadCount = 4;
                });

            //
            host.UseAzureAppServices()
            .UseIISIntegration()
            .ConfigureLogging((hostingContext, factory) => {
                    //factory.UseConfiguration(hostingContext.Configuration.GetSection("Logging"));                    
                    factory.AddConfiguration(configuration.GetSection("Logging"));
                factory.AddConsole();
                factory.AddDebug();
                factory.AddSerilog(dispose: true);
                //
                Log.Logger.Information("Logging configuration loaded.");
            })
            .UseStartup<Startup>();

            //
            return host.Build();
        }
    }
}
