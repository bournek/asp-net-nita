﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace com.nita.Services
{
    public interface ICryptoService
    {
        string EncryptDataBase64(string data);
        string DecryptDataBase64(string input);
        Task<string> EncryptDataRSA(string data, X509Certificate2 certificate);
        Task<string> DecryptDataRSA(string input, X509Certificate2 cert);
    }
}
