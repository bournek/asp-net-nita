﻿
using System;
using System.Linq;
using com.nita.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace com.nita.Services
{
    public class CryptoService : ICryptoService
    {

        private readonly IHostingEnvironment hostingEnvironment;
        private readonly ILogger logger;

        public CryptoService(IServiceProvider provider)
        {
            var settings = provider.GetRequiredService<IOptions<AppSettings>>();
            //
            this.hostingEnvironment = provider.GetRequiredService<IHostingEnvironment>();

            //
            var factory = provider.GetRequiredService<ILoggerFactory>();
            this.logger = factory.CreateLogger<CryptoService>();
        }

        /// <summary>
        /// Simple algorithm to encode a string to Base64
        /// Valid output should not have the trailing '=='
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string EncryptDataBase64(string data)
        {
            if (string.IsNullOrEmpty(data))
                throw new ArgumentNullException("Data cannot be null.");
            //
            try
            {
                Encoding iso = Encoding.GetEncoding("ISO-8859-1");
                Encoding utf8 = Encoding.UTF8;
                //string msg = iso.GetString(utf8.GetBytes(appKeySecret));
                byte[] bytes = iso.GetBytes(data).Reverse().ToArray();//Reverse Once

                string x = Convert.ToBase64String(bytes);//Base64.encode(bytes);

                //Trim ending '=='
                //x = x.Substring(0, x.Length - 2);
                logger.LogInformation("Deserialized = " + DecryptDataBase64(x));
                //
                return x;
            }
            catch (Exception e)
            {
                logger.LogCritical(1, "Encrypt Base64 Failed!!", e);
            }
            //
            return null;
        }
        /// <summary>
        /// Simple algorithm to decypher a Base64 encoded string
        /// Note: Valid input should not have the trailling '=='
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public string DecryptDataBase64(string input)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentNullException("Input cannot be null.");
            //if (input.EndsWith("=="))
            //    throw new ArgumentException("Invalid input for decryption");
            //
            try
            {
                Encoding iso = Encoding.GetEncoding("ISO-8859-1");
                Encoding utf8 = Encoding.UTF8;
                //string msg = iso.GetString(utf8.GetBytes(appKeySecret));

                //Append ending '=='
                //input = input + "==";
                //
                byte[] bytes = Convert.FromBase64String(input).Reverse().ToArray();
                //
                return iso.GetString(bytes);
            }
            catch (Exception e)
            {
                logger.LogCritical(1, "Decrypt Base64 Failed!!", e);
            }
            //
            return null;
        }

        /// <summary>
        /// Encodes a tring with the specified certificate
        /// </summary>
        /// <param name="data"></param>
        /// <param name="certificate"></param>
        /// <returns></returns>
        public async Task<string> EncryptDataRSA(string data, X509Certificate2 certificate)
        {
            //
            if (string.IsNullOrEmpty(data))
                throw new ArgumentNullException("Data cannot be null.");
            if (certificate == null)
                throw new ArgumentNullException("certificate cannot be null.");
            //
            string output = "";
            try
            {
                using (RSA csp = certificate.GetRSAPublicKey())
                {
                    byte[] bytesData = Encoding.UTF8.GetBytes(data);
                    //
                    //byte[] bytesEncrypted = csp.Encrypt(bytesData, RSAEncryptionPadding.OaepSHA1);
                    byte[] bytesEncrypted = csp.Encrypt(bytesData, RSAEncryptionPadding.Pkcs1);//PKCS#1.5
                                                                                               //Change to base64
                    output = Convert.ToBase64String(bytesEncrypted);
                }
            }
            catch (Exception e)
            {
                logger.LogCritical(1, "Encryption Failure!!", e);
            }
            return output;
        }
        /// <summary>
        /// Decrypts a string with the specified certificate
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cert"></param>
        /// <returns></returns>
        public async Task<string> DecryptDataRSA(string input, X509Certificate2 cert)
        {
            //
            if (string.IsNullOrEmpty(input))
                throw new ArgumentNullException("Input cannot be null.");
            if (cert == null)
                throw new ArgumentNullException("certificate cannot be null.");
            //
            string output = "";
            try
            {

                using (RSA rsa = cert.GetRSAPublicKey())
                {
                    byte[] data = Convert.FromBase64String(input);
                    //
                    byte[] decrypted = rsa.Decrypt(data, RSAEncryptionPadding.Pkcs1);
                    //
                    output = Encoding.UTF8.GetString(decrypted);
                }
            }
            catch (Exception e)
            {
                logger.LogCritical(1, "Decrytion Failure!!", e);
            }
            return output;
        }
    }
}
