﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Hubs;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.Services
{
    public enum NotificationType
    {
        Response,Result,Error
    }
    public interface INitaClient
    {
        [HubMethodName("onResult")]
        Task OnResult(string data);
        [HubMethodName("onError")]
        Task OnError(string data);
        [HubMethodName("onResponse")]
        Task OnResponse(string data);
    }
    [HubName("nitaHub")]
    //[Authorize]
    public class NitaHub:Hub<INitaClient>
    {
        private const string TAG = "NitaHub > ";
        public static NitaHub Instance
        {
            get;private set;
        }
        private static readonly ConcurrentDictionary<string, HubUser> HubUsers = new ConcurrentDictionary<string, HubUser>();
        public NitaHub()
        {
            System.Diagnostics.Debug.WriteLine(TAG + " Init()...");
            //
            Instance = this;
        }
        public override async Task OnConnected()
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                System.Diagnostics.Debug.WriteLine(TAG + " User " + Context.User.Identity.Name + " Connected...");
                //
                var user = HubUsers.GetOrAdd(Context.User.Identity.Name, _ => new HubUser()
                {
                    Name = Context.User.Identity.Name,
                    ConnectionIds = new HashSet<string>()
                });

                lock (user.ConnectionIds)
                {
                    user.ConnectionIds.Add(Context.ConnectionId);
                }
                //
                //await Task.Run(async () =>
                //{
                //    await Clients.Clients(user.ConnectionIds.ToList()).OnError("{\"action\":\"test\",\"cmd\":\"error\"}");
                //    await Clients.Clients(user.ConnectionIds.ToList()).OnResult("{\"action\":\"test\",\"cmd\":\"Result\"}");
                //    await Clients.Clients(user.ConnectionIds.ToList()).OnResponse("{\"action\":\"test\",\"cmd\":\"Response\"}");
                //});
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(TAG + " User " + Context.ConnectionId + " Connected.");
            }
            //
            await base.OnConnected();
        }
        public override async Task OnDisconnected(bool stopCalled)
        {
            if (Context.User.Identity.Name != null)
            {
                HubUser user;
                HubUsers.TryGetValue(Context.User.Identity.Name, out user);
                System.Diagnostics.Debug.WriteLine(TAG + " User " + Context.User.Identity.Name + " DIS-Connected...");
                //
                if (user != null)
                {
                    lock (user.ConnectionIds)
                    {
                        user.ConnectionIds.RemoveWhere(p => p.Equals(Context.ConnectionId));

                        if (!user.ConnectionIds.Any())
                        {
                            HubUser removedUser;
                            HubUsers.TryRemove(Context.User.Identity.Name, out removedUser);
                        }
                    }
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(TAG + " User " + Context.ConnectionId + " DIS-Connected...");
            }
            //
            await base.OnDisconnected(stopCalled);
        }
        public override async Task OnReconnected()
        {
            System.Diagnostics.Debug.WriteLine(TAG + " User " + Context.User.Identity.Name + " RE-Connected...");

            //
            await OnConnected();
            //
            await base.OnReconnected();
        }

        public async Task<bool> Notify(NotificationType type, string data, string username)
        {
            try
            {
                data = JToken.Parse(data).ToString();
            }
            catch
            {
            }
            if(username != null)
            {
                //
                HubUser user;
                HubUsers.TryGetValue(username, out user);

                if(user != null)
                {
                    //
                    switch (type)
                    {
                        case NotificationType.Result:
                            await Clients.Clients(user.ConnectionIds.ToList()).OnResult(data);
                            break;
                        case NotificationType.Response:

                           await Clients.Clients(user.ConnectionIds.ToList()).OnResponse(data);
                            break;
                        default:
                            //
                            await Clients.Clients(user.ConnectionIds.ToList()).OnError(data);
                            break;
                    }
                }else{
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
    }

    public class HubUser
    {
        public string Name { get; set; }
        public HashSet<string> ConnectionIds{get;set;}
    }
}
