﻿using com.nita.Settings;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace com.nita.Services
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link http://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        private readonly IServiceProvider provider;
        private readonly ILogger logger;
        
        private readonly IOptionsSnapshot<AppSettings> appSettings;

        public AuthMessageSender(IServiceProvider prov)
        {
            this.provider = prov;
            this.appSettings = prov.GetRequiredService<IOptionsSnapshot<AppSettings>>();
            
            //this.hostingEnvironment = prov.GetRequiredService<IHostingEnvironment>();

            var factory = prov.GetRequiredService<ILoggerFactory>();
            this.logger = factory.CreateLogger<AuthMessageSender>();

        }
        public Task SendEmailAsync(string email, string subject, string message)
        {
            var config = appSettings.Value.Messaging.Smtp;
            //Validate
            if (string.IsNullOrEmpty(config.Server) ||
                string.IsNullOrEmpty(config.Server) ||
                string.IsNullOrEmpty(config.Account) ||
                string.IsNullOrEmpty(config.Password) ||
                config.Port == 0)
            {
                logger.LogCritical(0, "Mis-Configured SMTP Settings.");
                throw new InvalidOperationException("Invalid SMTP Settings");
            }

            //Continue
            MailAddress sentFrom = new MailAddress(config.Account, appSettings.Value.SiteTitle);
            MailAddress sentTo = new MailAddress(email, appSettings.Value.SiteShortTitle);
            var pwd = config.Password;

            // Configure the client:
            SmtpClient client = new SmtpClient(config.Server);

            client.Port = config.Port;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;

            // Create the credentials:
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(config.Account, pwd);

            client.EnableSsl = false;
            client.Credentials = credentials;

            // Create the message:
            MailMessage mail = new MailMessage(sentFrom, sentTo);

            mail.Subject = subject;
            mail.Body = message;

            // Send:
            logger.LogInformation(2, "Sending mail to " + email);
            return client.SendMailAsync(mail);

        }

        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}
