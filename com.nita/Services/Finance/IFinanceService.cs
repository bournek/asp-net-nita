﻿using com.nita.IdentityModels;
using com.nita.ViewModels;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Threading.Tasks;

namespace com.nita.Services
{
    public interface IFinanceService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="payables"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        Task<List<FPayment>> GetAccountsPayable(List<JsonAccountPayable> payables, PayablesType type, ModelStateDictionary ModelState);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="payables"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        Task<List<ClaimEntry>> GetClaimEntries(List<JsonClaimItem> payables, ModelStateDictionary ModelState);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="voucher"></param>
        /// <param name="pmode"></param>
        /// <param name="CheckNo"></param>
        /// <param name="User"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        Task<int> PostPaymentVoucher(FPaymentVoucher voucher, PaymentAccount pmode, string CheckNo, IPrincipal User, ModelStateDictionary ModelState);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vouchers"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        Task<int> PostPaymentVouchers(IList<FPaymentVoucher> vouchers, PaymentAccount pmode, string CheckNo, IPrincipal User, ModelStateDictionary ModelState);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reqs"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        Task<int> DisbursePettyCahsReqs(IList<PettyCashReq> reqs, FinanceLedger Ledger, FProject Project, FCashier Cashier, PaymentAccount mode, string ChequeNo, string Description, IPrincipal User, ModelStateDictionary ModelState);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="warrants"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        Task<int> DisburseImprestWarrants(IList<ImprestWarrant> warrants, string Description, IPrincipal User, ModelStateDictionary ModelState);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="claims"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        Task<int> DisburseExpenseClaims(ExpenseClaim claim, List<JsonPaymentEntry> entries, IPrincipal User, ModelStateDictionary ModelState);
    }
}
