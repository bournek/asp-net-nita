﻿
using System;
using System.Linq;
using com.nita.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using com.nita.IdentityModels;
using com.nita.ViewModels;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Security.Principal;
using Newtonsoft.Json;

namespace com.nita.Services
{
    public class FinanceService : IFinanceService
    {

        private readonly IHostingEnvironment hostingEnvironment;
        private readonly ILogger logger;
        private readonly IServiceProvider provider;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_prov"></param>
        public FinanceService(IServiceProvider _prov)
        {
            this.provider = _prov;
            var settings = _prov.GetRequiredService<IOptions<AppSettings>>();
            //
            this.hostingEnvironment = _prov.GetRequiredService<IHostingEnvironment>();

            //
            var factory = _prov.GetRequiredService<ILoggerFactory>();
            this.logger = factory.CreateLogger<CryptoService>();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entries"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        public async Task<List<ClaimEntry>> GetClaimEntries(List<JsonClaimItem> entries, ModelStateDictionary ModelState)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var list = new List<ClaimEntry>();
            //
            await Task.Run(() => {

                foreach (var p in entries)
                {
                    try
                    {
                        var entry = new ClaimEntry()
                        {
                            Quantity = p.Qty,
                            UnitCost = p.UnitPrice,
                            UnitName = p.UnitName,
                            DateIncured = p.Date
                        };
                        //
                        var cur = db.FCurrencies.Single(l => l.IsBase);
                        entry.CurrencyId = cur.Id;
                        //
                        list.Add(entry);
                    }
                    catch
                    {
                        ModelState.AddModelError("", "Claim Entry '" + p.UnitName + "' could not be parsed..");
                    }
                }
            });

            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="payables"></param>
        /// <param name="hasCreditorAcc"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        public async Task<List<FPayment>> GetAccountsPayable(List<JsonAccountPayable> payables, PayablesType type, ModelStateDictionary ModelState)
        {
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var list = new List<FPayment>();
            //
            foreach (var p in payables)
            {
                //
                var acc = db.FinanceAccounts.Single(l => l.Id == p.AccId);
                //
                var dept = db.HrDepartments.Include(k => k.Budgets).ThenInclude(l => l.Cycle).Include(k => k.Budgets).ThenInclude(l => l.Period).Include(l => l.Payments).SingleOrDefault(l => l.Id == p.Dept);
                //Budget validations
                if (acc.HasBudget)
                {
                    FinanceBudget budget = await Utils.GetCurrentDeptBudget(dept, acc);

                    //
                    if (budget == null)
                    {
                        ModelState.AddModelError("", "GL Account '" + acc.Name + "' requires a budgeting, but a budget allocation was found in department '" + dept.Name + "'.");
                        continue;
                    }

                    //Used funds
                    if (budget.Amount - await Utils.GetSpentBudgetAmount(budget) <= 0)
                    {
                        ModelState.AddModelError("", "GL Account '" + acc.Name + "' budget has been exhausted in department '" + dept.Name + "'. Please request for more funds and try again..");
                        continue;
                    }

                }
                //
                //continue
                var payment = new FPayment()
                {
                    AccountId = acc.Id,
                    GrossAmount = p.GrossAmount,
                    Discount = p.Discount
                };
                //
                var taxamt = 0.0;//Total tax...
                                 //
                if (p.Taxes != null && p.Taxes.Length > 0)
                {
                    foreach (var t in p.Taxes)
                    {
                        var tax = db.FTaxes.Include(l => l.GLAccount).SingleOrDefault(l => l.Id == t);
                        if (tax != null)
                        {
                            payment.Taxes.Add(tax);
                            //
                            var amt = (p.GrossAmount - p.Discount) * tax.Rate / 100;
                            //GL Transaction
                            var tn = new GLTransaction()
                            {
                                AccountId = tax.GLAccount.Id,////Double Entry acc.
                                Amount = amt,
                                Kind = GLTransactionKind.Tax,
                                Type = GLTransactionType.Debit
                            };
                            //
                            payment.GLTransactions.Add(tn);
                            //
                            taxamt += amt;
                        }
                    }
                }
                payment.DeptId = dept.Id;
                //
                var cur = db.FCurrencies.Single(l => l.IsBase);
                //
                payment.CurrencyId = cur.Id;
                //
                list.Add(payment);
                //has AP(Crediros) account
                switch (type)
                {
                    case PayablesType.AccPayables:
                        //
                        //GL Transactions
                        var ap = db.FinanceAccounts.Single(k => k.Id == p.ApAcc);//Double Entry acc.
                                                                                 //
                        var aptrans = new GLTransaction()
                        {
                            AccountId = ap.Id,// Increase Debt
                            Type = GLTransactionType.Debit,
                            Kind = GLTransactionKind.Regular,
                            Amount = p.GrossAmount - (p.Discount - taxamt)//total ded
                        };
                        //
                        payment.GLTransactions.Add(aptrans);
                        break;
                    case PayablesType.AccReceivables:
                        //
                        //GL Transactions
                        var ar = db.FinanceAccounts.Single(k => k.Id == p.ArAcc);//Double Entry acc.
                                                                                 //
                        var artrans = new GLTransaction()
                        {
                            AccountId = ar.Id,// Decrease Debt
                            Type = GLTransactionType.Debit,
                            Kind = GLTransactionKind.Regular,
                            Amount = p.GrossAmount - (p.Discount - taxamt)//total ded
                        };
                        //
                        payment.GLTransactions.Add(artrans);
                        break;
                    default:
                        break;
                }
            }
            //
            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vouchers"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        public async Task<int> PostPaymentVouchers(IList<FPaymentVoucher> vouchers, PaymentAccount pmode, string CheckNo,IPrincipal User, ModelStateDictionary ModelState)
        {
            var count = 0;

            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            foreach (var voucher in vouchers)
            {
                try
                {
                
                        //
                        voucher.ModeId = pmode.Id;
                        //
                        if (CheckNo!=null && voucher.CheckNo != CheckNo)
                            voucher.CheckNo = CheckNo;
                        //
                        var valid = true;
                        //
                        var total = 0.0;
                        for (var j = 0; j < voucher.Payments.Count; j++)
                        {
                            var payment = voucher.Payments.ElementAt(j);
                            if (payment.GLTransactions.Count != 0)
                            {

                                //Get First Entry transaction
                                var tn = payment.GLTransactions.Single(l => l.Kind == GLTransactionKind.Regular && l.Type == GLTransactionType.Debit);

                                //Check if a 2nd entry has been made
                                if (!payment.GLTransactions.Any(k => k.Kind == GLTransactionKind.Regular && k.Type == GLTransactionType.Credit && k.AccountId == tn.AccountId))
                                {
                                    //No Double Entry found

                                    //
                                    var pamt = await Utils.ComputeAmountPayable(payment);

                                    //
                                    total += pamt;
                                    //var t1 = p.GLTransactions.Where(l=>l.AccountAId == p.AccountId &&);
                                    //
                                    var trans = new GLTransaction()
                                    {
                                        AccountId = pmode.AccountId,//Double Entry
                                        Kind = GLTransactionKind.Regular,
                                        Type = GLTransactionType.Credit //We are reducing
                                    };
                                    //
                                    trans.Amount = pamt;
                                    trans.PaymentId = payment.Id;
                                    //
                                    payment.GLTransactions.Add(trans);
                                }
                                else
                                {
                                    logger.LogWarning(2, "An attempt to do a 'Double Posting' for payment id = " + payment.Id);
                                    ModelState.AddModelError("", "Regular Voucher Payment was processed with errors. Some payments seem to have been posted already..");
                                    valid = false;
                                }
                            }
                            else
                            {
                                //No debit transaction
                                ModelState.AddModelError("", "No Debit GL entry found for some payments in the voucher.");
                                valid = false;
                            }

                        }
                        
                        //Save GL Transactions
                        //db.SaveChanges();
                        //
                        voucher.Status = PVStatus.Posted;
                        //
                        voucher.PayDate = DateTime.Now;
                        //
                        var pp = db.HrStaff.Include(l => l.ApplicationUser).Single(l => l.ApplicationUser.UserName == User.Identity.Name);
                        //
                        voucher.PostingPersonnelId = pp.Id;
                        //
                        await db.SaveChangesAsync();

                        //
                        foreach (var p in voucher.Payments)
                        {
                            var payment = db.FWallet.Include(l => l.PaymentAccounts).Single(l => l.Id == p.Id);
                            if (!payment.PaymentAccounts.Any(i => i.Id == pmode.Id))
                            {
                                //
                                payment.PaymentAccounts.Add(pmode);
                                //
                                await db.SaveChangesAsync();
                            }
                        }
                        //
                        count++;

                    }
                catch
                {
                    ModelState.AddModelError("", "Post voucher " + voucher.VoucherNo + " failed.");
                }
            }
            //
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reqs"></param>
        /// <param name="Ledger"></param>
        /// <param name="Project"></param>
        /// <param name="Cashier"></param>
        /// <param name="mode"></param>
        /// <param name="ChequeNo"></param>
        /// <param name="Description"></param>
        /// <param name="User"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        public async Task<int> DisbursePettyCahsReqs(IList<PettyCashReq> reqs, FinanceLedger Ledger, FProject Project, FCashier Cashier, PaymentAccount mode, string ChequeNo, string Description, IPrincipal User, ModelStateDictionary ModelState)
        {
            var count = 0;
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var pv = new FPaymentVoucher()
            {
                CheckNo = ChequeNo,
                VoucherDate = DateTime.Now,
                Description = Description,
                Type = PVType.PettyCash,
                Status = PVStatus.Pending
            };
            //Ledger
            pv.LedgerId = Ledger.Id;
            //Project
            if (Project != null)
            {
                pv.ProjectId = Project.Id;
            }
            //
            var personnel = db.Users.Include(j => j.StaffProfile).Single(l => l.UserName == User.Identity.Name).StaffProfile;
            //Personnel
            pv.PersonnelId = personnel.Id;
            //
            pv.VoucherNo = Utils.GenerateVoucherNo(db).ToString();
            //
            if (!Cashier.PaymentAccounts.Any())
                Cashier = db.FCashierOffices.Include(p => p.PaymentAccounts).Single(l => l.Id == Cashier.Id);
            //
            foreach (var jc in reqs)
            {
                try
                {

                    var pc = db.FPettyCashReqs.Include(l => l.Payments).ThenInclude(l => l.CurrencyUsed)
                                .Include(l => l.Payments).ThenInclude(l => l.GLTransactions).Single(k => k.Id == jc.Id);
                    //
                    foreach (var p in pc.Payments)
                    {
                        pv.Payments.Add(p);

                        //GT Transactions
                        //Credit
                        var trans = new GLTransaction()
                        {
                            Amount = p.GrossAmount,
                            DateEntered = DateTime.Now,
                            AccountId = Cashier.PaymentAccounts.First().AccountId,//Cashier
                            Type = GLTransactionType.Credit,
                        };
                        p.GLTransactions.Add(trans);
                        //Debit
                        //var trans1 = new GLTransaction()
                        //{
                        //    Amount = p.GrossAmount,
                        //    DateEntered = DateTime.Now,
                        //    AccountId = acc.Id,
                        //    Type = GLTransactionType.Debit,
                        //};
                        //p.GLTransactions.Add(trans1);

                    }
                    //
                    await db.SaveChangesAsync();
                    
                    //                
                }
                catch
                {
                    ModelState.AddModelError(String.Empty, "Disburse Petty-Cash request \"PC"+jc.Ref.ToString("0000")+"\" failed.");
                }

                //
                count++;
            }
            //
            db.FPaymentVouchers.Add(pv);
            await db.SaveChangesAsync();
            //
            ////////////////////////////////////////////////////////////////////
            // POST
            ////////////////////////////////////////////////////////////////////

            try
            {
                //
                pv.ModeId = mode.Id;
                //
                foreach (var d in pv.Payments)
                {
                    //Debit
                    var trans1 = new GLTransaction()
                    {
                        Amount = d.GrossAmount,
                        DateEntered = DateTime.Now,
                        AccountId = mode.AccountId,
                        Type = GLTransactionType.Debit,
                    };
                    d.GLTransactions.Add(trans1);
                }
                pv.PayDate = DateTime.Now;
                //
                pv.PostingPersonnelId = personnel.Id;
                pv.Status = PVStatus.Posted;
                //
                await db.SaveChangesAsync();
            }
            catch
            {
                ModelState.AddModelError("", "Posting Petty-Cash voucher has failed. Please try again...");
            }
            //
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="warrants"></param>
        /// <param name="Description"></param>
        /// <param name="User"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        public async Task<int> DisburseImprestWarrants(IList<ImprestWarrant> warrants,string Description,IPrincipal User, ModelStateDictionary ModelState)
        {
            int count = 0;

            //
            var db = provider.GetRequiredService<ApplicationDbContext>();

            foreach (var warrant in warrants)
            {
                //
                try
                {
                    //
                    var dis = new ImprestDisbursement()
                    {
                        DateCreated = DateTime.Now,
                        Description = Description,
                    };
                    //
                    var war = db.FImprestWarrants.Single(p => p.Id == warrant.Id);
                    dis.WarrantId = war.Id;
                    //
                    var personnel = db.Users.Include(k => k.StaffProfile).Single(l => l.UserName == User.Identity.Name);
                    dis.PersonnelId = personnel.Id;
                    //
                    dis.Ref = await Utils.GenerateDisbursementRef(db);
                    //
                    db.FImprestDisbursements.Add(dis);
                    await db.SaveChangesAsync();
                }
                catch
                {
                    ModelState.AddModelError("", "Diburse Imprest warrant \"IW"+warrant.Ref.ToString("0000")+"\" failed.");
                }
            }
            //
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="claim"></param>
        /// <param name="entries"></param>
        /// <param name="User"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        public async Task<int> DisburseExpenseClaims(ExpenseClaim claim, List<JsonPaymentEntry> entries,IPrincipal User, ModelStateDictionary ModelState)
        {
            var count = 0;
            //
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            try
            {
                var dis = new ExpenseClaimDisbursement()
                {
                    ClaimId = claim.Id,
                    Status = EntityStatus.Active
                };
                //
                var staff = db.HrStaff.Single(l => l.Id == claim.StaffId);
                //
                var personnel = db.HrStaff.Include(l => l.ApplicationUser).Single(l => l.ApplicationUser.UserName == User.Identity.Name);
                dis.PersonnelId = personnel.Id;
                //
                foreach (var ent in entries)
                {
                    //
                    var voucher = new FPaymentVoucher()
                    {
                        CheckNo = ent.Cheque,
                        Description = "Expense Claim Disbursement to " + staff.FullName,
                        LedgerId = claim.LedgerId,
                        VoucherNo = Utils.GenerateVoucherNo(db) + "",
                        Type = PVType.ExpenseClaim,
                        Status = PVStatus.Pending
                    };
                    //
                    voucher.PersonnelId = personnel.Id;
                    //
                    var tamt = 0.0;
                    //
                    var gl = db.FinanceAccounts.Single(k => k.Id == ent.AccId);
                    //All Payments for GL Account
                    foreach (var py in claim.Payments.Where(p => (p.GLAccount.Type == gl.Type || p.GLAccount.Type == gl.Type)))
                    {
                        var payment = db.FWallet.Include(j => j.GLTransactions).Include(k => k.GLAccount).Include(j => j.ExpenseClaim).Single(l => l.Id == py.Id && py.ClaimId == claim.Id);

                        var payable = await Utils.ComputeAmountPayable(payment);
                        //Withdraw
                        var trans = new GLTransaction()
                        {
                            AccountId = gl.Id,//Get Money from.
                            Type = GLTransactionType.Credit,
                            Kind = GLTransactionKind.ExpenseClaimDisbursement,
                            Amount = payable
                        };
                        //
                        payment.GLTransactions.Add(trans);

                        //
                        voucher.Payments.Add(payment);

                        tamt += payable;
                    }
                    //
                    if(voucher.Payments.Count == 0)
                    {
                        ModelState.AddModelError("", "Expense claim disbursement could not be prepared. Try again...");
                        return count;
                    }
                    //
                    await db.SaveChangesAsync();

                    //////////////////////////////////////////////////////////////////////////////////////////////////////////
                    // POST PAYMENT
                    /////////////////////////////////////////////////////////////////////////////////////

                    //
                    var pmode = db.FPaymentAccounts.Include(l => l.GLAccount).Include(l => l.Ledger).Single(l => l.Id == ent.Mode);
                    //
                    foreach (var p in voucher.Payments)
                    {
                        var trans1 = new GLTransaction()
                        {
                            AccountId = pmode.AccountId,//Pay Money to(Debtor).
                            Type = GLTransactionType.Debit,
                            Kind = GLTransactionKind.ExpenseClaimDisbursement,
                            Amount = tamt
                        };
                        //
                        p.GLTransactions.Add(trans1);
                    }
                    //
                    voucher.ModeId = pmode.Id;
                    voucher.PayDate = DateTime.Now;
                    voucher.PaymentAccNo = pmode.AccountNumber;
                    voucher.PostingPersonnelId = personnel.Id;
                    voucher.Status = PVStatus.Posted;
                    voucher.VoucherDate = DateTime.Now;

                    //
                    dis.Vouchers.Add(voucher);
                }

                if (dis.Vouchers.Count == 0)
                {
                    ModelState.AddModelError(string.Empty, "Errors while processing payments...");
                }
                //
                dis.DateDisbursed = DateTime.Now;
                //
                db.FEDisbursements.Add(dis);
                await db.SaveChangesAsync();
                //
                count++;
            }
            catch
            {
                ModelState.AddModelError("", "Expense claim(s) could not be disbursed. Try again..");
            }
            //
            return count;
        }
        /// <summary>
        /// Unary call...
        /// </summary>
        /// <param name="voucher"></param>
        /// <param name="pmode"></param>
        /// <param name="CheckNo"></param>
        /// <param name="User"></param>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        public async Task<int> PostPaymentVoucher(FPaymentVoucher voucher, PaymentAccount pmode, string CheckNo, IPrincipal User, ModelStateDictionary ModelState)
        {
            var list = new List<FPaymentVoucher>();
            list.Add(voucher);
            return await PostPaymentVouchers(list,pmode,CheckNo,User,ModelState);
        }
    }
}
