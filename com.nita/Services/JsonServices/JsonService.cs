﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using com.nita.IdentityModels;
using com.nita.ViewModels;
using com.nita.Services;
using com.nita.Constants;

namespace Nita.Services.JsonServices
{
    public class JsonService  : Controller
    {
        public JsonResult ReturnJsonData(List<Category> ListOfData)
        {


            return Json(ListOfData);
        }


    }
}
