﻿namespace com.nita.Constants
{
    public static class FinanceControllerRoute
    {
        //Dashboard
        public const string GetIndex = ControllerName.Finance + "GetIndex";
        //Payments Registry
        public const string GetPaymentsRegistry = ControllerName.Finance + "GetPaymentsRegistry";
        public const string GetUnpostedPayments = ControllerName.Finance + "GetUnpostedPayments";
        //Supplier Invoices
        public const string GetAddInvoice = ControllerName.Finance + "GetAddInvoice";
        public const string GetAllInvoices = ControllerName.Finance + "GetAllInvoices";
        public const string UpdateInvoice = ControllerName.Finance + "GetUpdateInvoice";
        public const string PreviewInvoice = ControllerName.Finance + "GetPreviewInvoice";
        public const string GetDropInvoice = ControllerName.Finance + "GetDropInvoice";

        //Invoices Waivers
        public const string GetAddInvoiceWaiver = ControllerName.Finance + "GetAddInvoiceWaiver";
        public const string GetAllInvoiceWaivers = ControllerName.Finance + "GetAllInvoiceWaivers";
        public const string UpdateInvoiceWaiver = ControllerName.Finance + "GetUpdateInvoiceWaiver";
        public const string DropInvoiceWaiver = ControllerName.Finance + "GetDropInvoiceWaiver";
        public const string PreviewInvoiceWaiver = ControllerName.Finance + "GetPreviewInvoiceWaiver";

        //Regular Payment Vouchers
        public const string GetAddRegularPVoucher= ControllerName.Finance + "GetAddRegularPVoucher";
        public const string GetAllRegularPVouchers = ControllerName.Finance + "GetAllRegularPVoucher";
        public const string GetUpdateRegularPVoucher = ControllerName.Finance + "GetUpdateRegularPVoucher";
        public const string GetDropPVoucher = ControllerName.Finance + "GetDropPVoucher";
        public const string GetPreviewPVoucher = ControllerName.Finance + "GetPreviewPVoucher";

        //Tax Payment Vouchers
        public const string GetAddTaxPVoucher = ControllerName.Finance + "GetAddTaxPVoucher";
        public const string GetAllTaxPVouchers = ControllerName.Finance + "GetAllTaxPVoucher";
        public const string GetUpdateTaxPVoucher = ControllerName.Finance + "GetUpdateTaxPVoucher";
        //public const string GetPreviewTaxPVoucher = ControllerName.Finance + "GetPreviewTaxPVoucher";

        //PettyCash Payment Vouchers
        public const string GetAddPettyCashPVoucher = ControllerName.Finance + "GetAddPettyCashPVoucher";
        public const string GetAllPettyCashPVouchers = ControllerName.Finance + "GetAllPettyCashPVoucher";
        public const string GetUpdatePettyCashPVoucher = ControllerName.Finance + "GetUpdatePettyCashPVoucher";
        //public const string GetPreviewPettyCashPVoucher = ControllerName.Finance + "GetPreviewPettyCashPVoucher";

        //ExpenseClaim Payment Vouchers
        public const string GetAddExpenseClaimPVoucher = ControllerName.Finance + "GetAddExpenseClaimPVoucher";
        public const string GetAllExpenseClaimPVouchers = ControllerName.Finance + "GetAllExpenseClaimPVoucher";
        public const string GetUpdateExpenseClaimPVoucher = ControllerName.Finance + "GetUpdateExpenseClaimPVoucher";
        //public const string GetPreviewExpenseClaimPVoucher = ControllerName.Finance + "GetPreviewExpenseClaimPVoucher";

        //Imprest Payment Vouchers
        public const string GetAddImprestPVoucher = ControllerName.Finance + "GetAddImprestPVoucher";
        public const string GetAllImprestPVouchers = ControllerName.Finance + "GetAllImprestPVoucher";
        public const string GetUpdateImprestPVoucher = ControllerName.Finance + "GetUpdateImprestPVoucher";
        //public const string GetPreviewImprestPVoucher = ControllerName.Finance + "GetPreviewImprestPVoucher";
        //Supplier
        public const string GetAddSupplier = ControllerName.Finance + "GetAddSupplier";
        public const string GetAllSuppliers = ControllerName.Finance + "GetAllSuppliers";
        public const string UpdateSupplier = ControllerName.Finance + "GetUpdateSupplier";
        public const string DropSupplier = ControllerName.Finance + "GetDropSupplier";

        //ImprestWarrant
        public const string GetAddImprestWarrant = ControllerName.Finance + "GetAddImprestWarrant";
        public const string GetAllImprestWarrants = ControllerName.Finance + "GetAllImprestWarrants";
        public const string UpdateImprestWarrant = ControllerName.Finance + "GetUpdateImprestWarrant";
        public const string DropImprestWarrant = ControllerName.Finance + "GetDropImprestWarrant";

        //ImprestDisbursement
        public const string GetAddImprestDisbursement = ControllerName.Finance + "GetAddImprestDisbursement";
        public const string GetAllImprestDisbursements = ControllerName.Finance + "GetImprestDisbursements";
        public const string UpdateImprestDisbursement = ControllerName.Finance + "GetUpdateImprestDisbursement";
        public const string PreviewImprestDisbursement = ControllerName.Finance + "GetPreviewImprestDisbursement";

        //ImprestSurrender
        public const string GetAddImprestSurrender = ControllerName.Finance + "GetAddImprestSurrender";
        public const string GetAllImprestSurrenders = ControllerName.Finance + "GetImprestSurrenders";
        public const string UpdateImprestSurrender = ControllerName.Finance + "GetUpdateImprestSurrender";

        //ExpenseClaim
        public const string GetAddExpenseClaim = ControllerName.Finance + "GetAddExpenseClaim";
        public const string GetAllExpenseClaims = ControllerName.Finance + "GetExpenseClaims";
        public const string UpdateExpenseClaim = ControllerName.Finance + "GetUpdateExpenseClaim";
        public const string DropExpenseClaim = ControllerName.Finance + "GetDropExpenseClaim";

        //ExpenseDisbursement
        public const string GetAddExpenseDisbursement = ControllerName.Finance + "GetAddExpenseDisbursement";
        public const string GetAllExpenseDisbursements = ControllerName.Finance + "GetExpenseDisbursements";
        public const string UpdateExpenseDisbursement = ControllerName.Finance + "GetUpdateExpenseDisbursement";

        //PettycashItem
        public const string GetAddPettycashItem = ControllerName.Finance + "GetAddPettycashItem";
        public const string GetAllPettycashItems = ControllerName.Finance + "GetPettycashItems";
        public const string UpdatePettycashItem = ControllerName.Finance + "GetUpdatePettycashItem";
        public const string DropPettyCashReq = ControllerName.Finance + "GetDropPettycashItem";

        //PettycashItem Disbursement
        public const string GetAddPettycashDisbursement = ControllerName.Finance + "GetAddPettycashDisbursement";
        public const string GetAllPettycashDisbursements = ControllerName.Finance + "GetPettycashDisbursements";
        public const string UpdatePettycashDisbursement = ControllerName.Finance + "GetUpdatePettycashDisbursement";
        public const string DropPettyCashDisbursement = ControllerName.Finance + "DropPettyCashDisbursement";

        //GL Regular PV Posting
        public const string GetPostPaymentVoucher = ControllerName.Finance + "GetPostPaymentVoucher";
        public const string GetAllPostedRegularPVs = ControllerName.Finance + "GetAllPostedRegularPVs";

        //GL Tax PV Posting
        public const string GetPostTaxPV = ControllerName.Finance + "GetPostTaxPV";
        public const string GetAllPostedTaxPVs = ControllerName.Finance + "GetAllPostedTaxPVs";
        public const string GetPostAllPayments = ControllerName.Finance + "GetPostAllPayments";
        //
        //PC Settings
        public const string GetAllPCCashierOffices = ControllerName.Finance + "GetAllPCCashierOffices";
        public const string GetAddPCCashierOffice = ControllerName.Finance + "GetAddPCCashierOffice";
        public const string DropPCCashierOffice = ControllerName.Finance + "DropPCCashierOffice";
        public const string GetAddPCCashierOfficeAttendant = ControllerName.Finance + "GetAddPCCashierOfficeAttendant";

        //Payables Journals
        public const string GetAllPayableJournals = ControllerName.Finance + "GetAllPayableJournals";
        public const string GetAddPayablesJournal = ControllerName.Finance + "GetAddPayablesJournal";
        public const string GetUpdatePayablesJournal = ControllerName.Finance + "GetUpdatePayablesJournal";

        //Payroll Journals
        public const string GetAllPayrollJournals = ControllerName.Finance + "GetAllPayrollJournals";
        public const string GetImportPayrollJournals = ControllerName.Finance + "GetImportPayrollJournals";

        //
        public const string GetReports = ControllerName.Finance + "GetReports";
        //
        public const string GetBasicReports = ControllerName.Finance + "GetBasicReports";
        //
        public const string GetBudgetReports = ControllerName.Finance + "GetBudgetReports";
        //
        public const string GetFinanceStatements = ControllerName.Finance + "GetFStatements";
        public const string GetQuartelyFinanceStatements = ControllerName.Finance + "GetQuartelyFStatements";
        //
        public const string GetBudgetStatus = ControllerName.Finance + "GetBudgetStatus";
        //
        public const string GetStudentFeesSummary = ControllerName.Finance + "GetStudentFeesSummary";

        //Finance Settings
        public const string GetSettings = ControllerName.Finance + "GetSettings";
        public const string GetUpdateSettings = ControllerName.Finance + "GetUpdateSettings";
    }


    public static class FinanceUtilsControllerRoute
    {
        //Currency
        public const string AddCurrency = ControllerName.Finance + "AddCurrency";//
        public const string GetAllCurrencies = ControllerName.Finance + "AllCurrencies";//
        public const string UpdateCurrency = ControllerName.Finance + "UpdateCurrency";//
        //Accounts
        public const string GetAccountPreview = ControllerName.Finance + "GetPreview";
        public const string AddAccount = ControllerName.Finance + "AddAccount";//
        public const string GetAllAccounts = ControllerName.Finance + "AllAccounts";//
        public const string UpdateAccount = ControllerName.Finance + "UpdateAccount";//
        //Ledgers
        public const string AddLedger = ControllerName.Finance + "AddLedger";
        public const string GetAllLedgers = ControllerName.Finance + "GetAllLedgers";
        public const string UpdateLedger = ControllerName.Finance + "UpdateLedger";
        //Payment Modes
        public const string AddPaymentAccount = ControllerName.Finance + "AddPaymentAccount";
        public const string GetAllPaymentAccounts = ControllerName.Finance + "GetPaymentAccounts";
        public const string UpdatePaymentAccount = ControllerName.Finance + "UpdatePaymentAccount";
        //Years
        public const string AddFiscalYear = ControllerName.Finance + "AddFiscalYear";
        public const string AddFiscalYearQuarter = ControllerName.Finance + "AddFiscalYearQuarter";
        public const string GetAllFiscalYears = ControllerName.Finance + "GetFiscalYears";
        public const string UpdateFiscalYear = ControllerName.Finance + "UpdateFiscalYear";
        //FinanceTax
        public const string AddFinanceTax = ControllerName.Finance + "AddFinanceTax";
        public const string GetAllFinanceTaxes = ControllerName.Finance + "GetFinanceTaxes";
        public const string UpdateFinanceTax = ControllerName.Finance + "UpdateFinanceTax";
        //Vender
        public const string AddVendorType = ControllerName.Finance + "AddVendorType";
        public const string GetAllVendorTypes = ControllerName.Finance + "GetVendorTypes";
        public const string UpdateVendorType = ControllerName.Finance + "UpdateVendorType";
        //Voucher
        public const string AddVoucherPrefix = ControllerName.Finance + "AddVoucherPrefix";
        public const string GetAllVoucherPrefixes = ControllerName.Finance + "GetVoucherPrefixes";
        public const string UpdateVoucherPrefix = ControllerName.Finance + "UpdateVoucherPrefix";
        //Project
        public const string GetAddProject = ControllerName.Finance + "AddProject";
        public const string GetAllProjects = ControllerName.Finance + "GetAllProjects";
        public const string UpdateProject = ControllerName.Finance + "UpdateProject";
        //Bank
        public const string GetAddBank = ControllerName.Finance + "GetAddBank";
        public const string GetAllBanks = ControllerName.Finance + "GetBanks";
        public const string UpdateBank = ControllerName.Finance + "UpdateBank";
        public const string GetAddBankBranch = ControllerName.Finance + "GetAddBankBranch";

        //Product categories 
        public const string GetAllProductCategories = ControllerName.Finance + "AllProductCategories";
        public const string GetAddProductCategory = ControllerName.Finance + "GetAddProductCategory";
        public const string UpdateProductCategory = ControllerName.Finance + "UpdateProductCategory";
        //Customer type
        public const string GetAllCustomerTypes = ControllerName.Finance + "AllCustomerTypes";
        public const string GetAddCustomerType = ControllerName.Finance + "GetAddCustomerType";
        public const string UpdateCustomerType = ControllerName.Finance + "UpdateCustomerType";
    }
}