﻿namespace com.nita.Constants
{
    public static class FinanceControllerAction
    {
        public const string Index = "Index";
        //Registry
        public const string PaymentsRegistry = "PaymentsRegistry";
        public const string UnpostedPayments = "UnpostedPayments";
        //Supplier
        public const string AddSupplier = "AddSupplier";
        public const string AllSuppliers = "AllSuppliers";
        public const string UpdateSupplier = "UpdateSupplier";
        //Invoice
        public const string CreateInvoice = "CreateInvoice";
        public const string AllInvoices = "AllInvoices";
        public const string UpdateInvoice = "UpdateInvoice";

        //Invoice Waiver
        public const string AddInvoiceWaiver = "AddSIWaiver";
        public const string AllInvoiceWaivers = "AllSIWaivers";
        public const string UpdateInvoiceWaiver = "UpdateSIWaiver";

        //Payment Regular Vouchers
        public const string CreateRegularPVoucher = "CreateRegularPVoucher";
        public const string AllRegularPVouchers = "AllRegularPVouchers";
        public const string UpdateRegularPVouchers = "UpdateRegularPVoucher";
        //public const string PreviewRegularPVouchers = "PreviewRegularPVoucher";
        public const string PreviewPVouchers = "PreviewPVoucher";

        //Payment Tax Vouchers
        public const string CreateTaxPVoucher = "CreateTaxPVoucher";
        public const string AllTaxPVouchers = "AllTaxPVouchers";
        public const string UpdateTaxPVouchers = "UpdateTaxPVoucher";
        public const string PreviewTaxPVouchers = "PreviewTaxPVoucher";

        //ImprestWarrant
        public const string AddImprestWarrant = "AddImprestWarrant";
        public const string AllImprestWarrants = "AllImprestWarrants";
        public const string UpdateImprestWarrant = "UpdateImprestWarrant";

        //ImprestDisbursement
        public const string AddImprestDisbursement = "AddImprestDisbursement";
        public const string AllImprestDisbursements = "AllImprestDisbursements";
        public const string UpdateImprestDisbursement = "UpdateImprestDisbursement";

        //ImprestSurrender
        public const string AddImprestSurrender = "AddImprestSurrender";
        public const string AllImprestSurrenders = "AllImprestSurrenders";
        public const string UpdateImprestSurrender = "UpdateImprestSurrender";

        //Imprest P Voucher
        public const string CreateImprestPVoucher = "CreateImprestPVoucher";
        public const string AllImprestPVouchers = "AllImprestPVouchers";
        public const string UpdateImprestPVoucher = "UpdateImprestPVoucher";
        //public const string PreviewImprestPVoucher = "PreviewImprestPVoucher";

        //Expense Claim
        public const string AddExpenseClaim = "AddExpenseClaim";
        public const string AllExpenseClaims = "AllExpenseClaims";
        public const string UpdateExpenseClaim = "UpdateExpenseClaim";

        //Expense Claim Disbursement
        public const string AddExpenseDisbursement = "AddEDisbursement";
        public const string AllExpenseDisbursements = "AllEDisbursements";
        public const string UpdateExpenseDisbursement = "UpdateEDisbursement";

        //Expense Claim P Voucher
        public const string CreateExpenseClaimPVoucher = "CreateECPVoucher";
        public const string AllExpenseClaimPVouchers = "AllECPVouchers";
        public const string UpdateExpenseClaimPVoucher = "UpdateECPVoucher";
        //public const string PreviewExpenseClaimPVoucher = "PreviewECPVoucher";

        //Petty cash Item
        public const string AddPettycashItem = "AddPCItem";
        public const string AllPettycashItems = "AllPCItems";
        public const string UpdatePettycashItem = "UpdatePCItem";

        //Petty cash Disbursement
        public const string AddPettycashDisbursement = "AddPCDisbursement";
        public const string AllPettycashDisbursements = "AllPCDisbursements";
        public const string UpdatePettycashDisbursement = "UpdatePCDisbursement";

        //petty cash P Voucher
        public const string CreatePettyCashPVoucher = "CreatePCPVoucher";
        public const string AllPettyCashPVouchers = "AllPCPVouchers";
        public const string UpdatePettyCashPVoucher = "UpdatePCPVoucher";
        //public const string PreviewPettyCashPVoucher = "PreviewPCPVoucher";

        //GL Regular PV Poting
        public const string AllPostedRegularPVs = "AllPostedPVouchers";
        public const string PostRegularPV = "PostPVoucher";

        //GL Tax PV Poting
        public const string AllPostedTaxPVs = "AllPostedPVouchers";
        public const string PostTaxPV = "PostPVoucher";

        //PC Cashier Offices
        public const string AllPCCashierOffices = "AllPCCashierOffices";
        public const string AddPCCashierOffice = "AddPCCashierOffice";
        public const string AddPCCashierOfficeAttendant = "AddPCCashierOfficeAttendant";

        //Payables Journals
        public const string AllPayableJournals = "AllPayableJournals";
        public const string AddPayablesJournal = "AddPayablesJournal";

        //Payroll Journals
        public const string AllPayrollJournals = "AllPayrollJournals";
        public const string ImportPayrollJournals = "ImportPayrollJournals";

        //Finance Reports
        public const string Reports = "Reports";
        //
        public const string BasicReports = "BasicReports";
        //
        public const string BudgetReports = "BudgetReports";
        //
        public const string FinanceStatements = "FStatements";
        public const string QuartelyFinanceStatements = "QuartelyFStatements";
        //
        public const string BudgetStatus = "BudgetStatus";
        //
        public const string StudentFees = "StudentFeesSummary";

        //Admin
        public const string Settings = "Settings";
        public const string UpdateSettings = "UpdateSettings";
    }

    public static class FinanceUtilsControllerAction
    {
        //Dashboard
        public const string Index = "Index";//Dashboard
        //Accounts 
        public const string AccountPreview = "AccountPreview";
        public const string AllAccounts = "AllAccounts";
        public const string UpdateAccount = "UpdateAccount";
        //Currency
        public const string AddCurrency = "AddCurrency";
        public const string AllCurrencies = "AllCurrencies";
        public const string UpdateCurrency = "UpdateCurrency";
        //Ledgers
        public const string AddAccount = "AddAccount";
        public const string AddLedger = "AddLedger";
        public const string AllLedgers = "AllLedgers";
        public const string UpdateLedger = "UpdateLedger";

        //Payment Mode
        public const string AddPaymentAccount = "AddPaymentAccount";
        public const string AllPaymentAccounts = "AllPaymentAccounts";
        public const string EditPaymentAccount = "EditPaymentAccount";
        //FiscalYEar
        public const string AddFiscalYear = "AddFiscalYear";
        public const string AddFiscalYearQuarter = "AddFiscalYearQuarter";
        public const string AllFiscalYears = "AllFiscalYears";
        public const string UpdateFiscalYear = "UpdateFiscalYear";
        //Taxxes
        public const string AddFinanceTax = "AddFinanceTax";
        public const string AllFinanceTaxes = "AllFinanceTaxes";
        public const string UpdateFinanceTax = "UpdateFinanceTax";
        //Venders
        public const string AddVendorType = "AddVendorType";
        public const string AllVendorTypes = "AllVendorTypes";
        public const string UpdateVendorType = "UpdateVendorType";
        //Vouchers
        public const string AddVoucherPrefix = "AddVoucherPrefix";
        public const string AllVoucherPrefixes = "AllVoucherPrefixes";
        public const string UpdateVoucherPrefix = "UpdateVoucherPrefix";
        //
        //Project
        public const string AddProject = "AddProject";
        public const string AllProjects = "AllProjects";
        public const string UpdateProject = "UpdateProject";
        //Product Category 
        public const string AllProductCategories = "AllProductCategories";
        public const string AddProductCategory = "AddProductCategory";
        public const string UpdateProductCategory = "UpdateProductCategory";
        //Customer Date
        public const string AllCustomerTypes = "AllCustomerTypes";
        public const string AddCustomerType = "AddCustomerType";
        public const string UpdateCustomerType = "UpdateCustomerType";
        //Banks
        public const string AddBank = "AddBank";
        public const string AllBanks = "AllBanks";
        public const string UpdateBank = "UpdateBank";
        public const string AddBankBranch = "AddBankBranch";
        public const string UpdateBankBranch = "UpdateBankBranch";
        //
        public const string AddSponsor = "AddSponsor";
    }
}