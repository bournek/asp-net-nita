﻿namespace com.nita.Constants
{
    public static class AdminControllerRoute
    {
        public const string GetConfigurations = ControllerName.Admin + "GetConfig";
        public const string GetIndex = ControllerName.Admin + "GetIndex";
    }
}