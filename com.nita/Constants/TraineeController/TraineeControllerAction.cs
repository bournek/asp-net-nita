﻿namespace com.nita.Constants
{
    public static class TraineeControllerAction
    {
        public const string Events = "Events";
        public const string Profile = "Profile";
        public const string Feed = "Feed";
        public const string Index = "Index";//Dashboard
        public const string Tution = "Tution";
        public const string Finance = "Finance";
        public const string Fora = "AlmniFora";
        public const string MedicalHist = "MedicalHistory";
        public const string Repository = "Repository";
    }
}