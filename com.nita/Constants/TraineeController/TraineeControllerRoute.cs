﻿namespace com.nita.Constants
{
    public static class TraineeControllerRoute
    {
        public const string GetEvents = ControllerName.Trainer + "GetEmployers";
        public const string GetProfile = ControllerName.Trainer + "GetProfile";
        public const string GetTution = ControllerName.Trainer + "GetAreas";
        public const string GetIndex = ControllerName.Trainer + "GetIndex";
    }
}