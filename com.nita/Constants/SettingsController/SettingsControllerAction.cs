﻿namespace com.nita.Constants
{
    public static class SettingsControllerAction
    {
        //
        public const string Index = ControllerName.Staff + "Index";

        //Division
        public const string AllLocations = "AllLocations";
        public const string AddLocation = "AddLocation";
        public const string UpdateLocation= "UpdateLocation";

        //Departments
        public const string AllDepartments = "AllDepartments";
        public const string AddDepartment = "AddDepartment";
        public const string UpdateDepartment = "UpdateDepartment";

        //Section
        public const string AllSections = "AllSections";
        public const string AddSection = "AddSection";
        public const string UpdateSection = "UpdateSection";

        //Division
        public const string AllDivisions = "AllDivisions";
        public const string AddDivision = "AddDivision";
        public const string UpdateDivision = "UpdateDivision";

        //Configurations
        public const string Configurations = "Configs";
        public const string UpdateConfigurations = "UpdateConfigs";
    }
}