﻿namespace com.nita.Constants
{
    public static class SettingsControllerRoute
    {
        //
        public const string GetIndex = ControllerName.Settings + "GetIndex";

        //
        public const string GetAllLocations = ControllerName.Settings + "GetAllLocs";
        public const string GetAddLocation = ControllerName.Settings + "GetAddLoc";
        public const string GetUpdateLocation = ControllerName.Settings + "GetUpdateLoc";
        public const string DropLocation = ControllerName.Settings + "GetDropLoc";
        //
        public const string GetAllDepartments = ControllerName.Settings + "GetAllDepartments";
        public const string GetAddDepartment = ControllerName.Settings + "GetAddDepartment";
        public const string GetUpdateDepartment = ControllerName.Settings + "GetUpdateDepartment";
        public const string DropDepartment = ControllerName.Settings + "GetDropDepartment";
        //
        public const string GetAllSections = ControllerName.Settings + "GetAllSections";
        public const string GetAddSection = ControllerName.Settings + "GetAddSection";
        public const string GetUpdateSection = ControllerName.Settings + "GetUpdateSection";
        public const string DropSection = ControllerName.Settings + "GetDropSection";
        //
        public const string GetAllDivisions = ControllerName.Settings + "GetAllDivisions";
        public const string GetAddDivision = ControllerName.Settings + "GetAddDivision";
        public const string GetUpdateDivision = ControllerName.Settings + "GetUpdateDivision";
        public const string GetDropDivision = ControllerName.Settings + "GetDropDivision";

        //
        public const string GetConfigurations = ControllerName.Settings + "GetConfigurations";
        public const string GetUpdateConfigurations = ControllerName.Settings + "GetUpdateConfigurations";
    }
}