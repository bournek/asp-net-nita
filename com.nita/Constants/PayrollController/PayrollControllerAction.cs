﻿namespace com.nita.Constants
{
    public static class PayrollControllerAction
    {
        public const string Index = "Index";

        //Irregular Earning
        public const string AllIrregularEarnings = "AllIrrEarnings";
        public const string AddIrregularEarning = "CreateIrrEarning";
        public const string UpdateIrregularEarning = "UpdateIrrEarning";

        //Irregular Payment Profile
        public const string AllIrregularPaymentProfiles = "AllIrrPaymentProfiles";
        public const string AddIrregularPaymentProfile = "CreateIrrPaymentProfile";
        public const string UpdateIrregularPaymentProfile = "UpdateIrrPaymentProfile";

        //Process Irregular Payment
        public const string AllProcessedIrregularPayments = "AllProIrrPayments";
        public const string ProcessIrregularPayment = "ProcessIrrPayment";
        public const string PreviewProcessedIrregularPayment = "PreviewProIrrPayment";

        //Irregular Payment Projects
        public const string AllIrregularPaymentProjects = "AllIrrPaymentProjects";
        public const string CreateIrregularPaymentProject = "CreateIrrPaymentProject";
        public const string PreviewIrregularPaymentProject = "PreviewIrrPaymentProject";

        //Irregular Earning
        public const string AllIrregularPaymentVouchers = "AllIrrPaymentVouchers";
        public const string AddIrregularPaymentVoucher = "CreateIrrPaymentVoucher";
        public const string UpdateIrregularPaymentVoucher = "UpdateIrrPaymentVoucher";
        //Settings
        public const string Settings = "Settings";

    }
    public static class PayrollUtilsControllerAction
    {
        //Pay Grade
        public const string AllJobGroups = "AllJobGroups";
        public const string AddJobGroup = "AddJobGroup";
        public const string UpdateJobGroup = "UpdateJobGroup";

        //Pay Account
        public const string AllPayAccounts = "AllPayAccounts";
        public const string AddPayAccount = "AddPayAccount";
        public const string UpdatePayAccount = "UpdatePayAccount";
        //Pay Rate
        public const string AllPayRates = "AllPayRates";
        public const string AddPayRate = "AddPayAccount";
        public const string UpdatePayRate = "UpdatePayRate";
        //Salary Period
        public const string AllSalaryPeriods = "AllSalaryPeriods";
        public const string AddSalaryPeriod = "AddSalaryPeriod";
        public const string UpdateSalaryPeriod = "UpdateSalaryPeriod";
        //Paye Rates
        public const string AllPayeRates = "AllPayeRates";
        public const string AddPayeRate = "AddPayeRate";
        public const string AddPayeTaxRange = "AddPayeTaxRange";
        public const string UpdatePayeRate = "UpdatePayeRate";
        //Nhif Rates
        public const string AllNhifRates = "AllNhifRates";
        public const string AddNhifRate = "AddNhifRate";
        public const string AddNhifRateRange = "AddNhifRateRange";
        public const string UpdateNhifRate = "UpdateNhifRate";

        //Renumeration Schemes
        public const string AllRenumSchemes = "AllRenumSchemes";
        public const string AddRenumScheme = "AddRenumScheme";
        public const string UpdateRenumScheme = "UpdateRenumScheme";
    }
}