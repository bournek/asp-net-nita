﻿namespace com.nita.Constants
{
    public static class PayrollControllerRoute
    {
        public const string GetIndex = ControllerName.Payroll + "GetIndex";

        //Irregular Earning
        public const string GetAllIrregularEarnings = ControllerName.Payroll + "GetAllIrregularEarnings";
        public const string GetAddIrregularEarning = ControllerName.Payroll + "GetAddIrregularEarning";
        public const string GetUpdateIrregularEarning = ControllerName.Payroll + "GetUpdateIrregularEarning";

        //Irregular Payment Profile
        public const string GetAllIrregularPaymentProfiles = ControllerName.Payroll + "GetAllIrregularPaymentProfiles";
        public const string GetAddIrregularPaymentProfile = ControllerName.Payroll + "GetAddIrregularPaymentProfile";
        public const string GetUpdateIrregularPaymentProfile = ControllerName.Payroll + "GetUpdateIrregularPaymentProfile";

        //Process Irregular Payment
        public const string GetAllProcessedIrregularPayments = ControllerName.Payroll + "GetAllProcessedIrregularPayments";
        public const string GetProcessIrregularPayment = ControllerName.Payroll + "GetProcessIrregularPayment";
        public const string GetPreviewProcessedIrregularPayment = ControllerName.Payroll + "GetPreviewProcessedIrregularPayment";

        //Irregular Payment Project
        public const string GetAllIrregularPaymentProjects = ControllerName.Payroll + "GetAllIrregularPaymentProjects";
        public const string GetAddIrregularPaymentProject = ControllerName.Payroll + "GetAddIrregularPaymentProject";
        public const string GetUpdateIrregularPaymentProject = ControllerName.Payroll + "GetUpdateIrregularPaymentProject";
        public const string GetPreviewIrregularPaymentProject = ControllerName.Payroll + "GetPreviewIrregularPaymentProject";

        //Irregular Payment Voucher
        public const string GetAllIrregularPaymentVouchers = ControllerName.Payroll + "GetAllIrregularPaymentVouchers";
        public const string GetAddIrregularPaymentVoucher = ControllerName.Payroll + "GetAddIrregularPaymentVoucher";
        public const string GetUpdateIrregularPaymentVoucher = ControllerName.Payroll + "GetUpdateIrregularPaymentVoucher";
        public const string GetPreviewIrregularPaymentVoucher = ControllerName.Payroll + "GetPreviewIrregularPaymentVoucher";

        //Settings
        public const string GetSettings = ControllerName.Payroll + "GetSettings";//
    }

    public static class PayrollUtilsControllerRoute
    {
        //
        public const string GetAllJobGroups = ControllerName.Payroll + "GetAllJobGroups";
        public const string GetAddJobGroup = ControllerName.Payroll + "GetAddJobGroup";
        public const string GetUpdateJobGroup = ControllerName.Payroll + "GetUpdateJobGroup";
        //Pay Accounts
        public const string GetAllPayAccounts = ControllerName.Payroll + "GetAllPayAccounts";
        public const string GetAddPayAccount = ControllerName.Payroll + "GetAddPayAccount";
        public const string GetUpdatePayAccount = ControllerName.Payroll + "GetUpdatePayAccount";
        //Salary Periods
        public const string GetAllSalaryPeriods = ControllerName.Payroll + "GetAllSalaryPeriods";
        public const string GetAddSalaryPeriod = ControllerName.Payroll + "GetAddSalaryPeriod";
        public const string GetUpdateSalaryPeriod = ControllerName.Payroll + "GetUpdateSalaryPeriod";
        //Paye Rates
        public const string GetAllPayeRates = ControllerName.Payroll + "GetAllPayeRates";
        public const string GetAddPayeRate = ControllerName.Payroll + "GetAddPayeRate";
        public const string GetAddPayeRateRange = ControllerName.Payroll + "GetAddPayeRateRange";
        public const string GetUpdatePayeRate = ControllerName.Payroll + "GetUpdatePayeRate";
        //NHIF Rates
        public const string GetAllNhifRates = ControllerName.Payroll + "GetAllNhifRates";
        public const string GetAddNhifRate = ControllerName.Payroll + "GetAddNhifRate";
        public const string GetAddNhifRateRange = ControllerName.Payroll + "GetAddNhifRateRange";
        public const string GetUpdateNhifRate = ControllerName.Payroll + "GetUpdateNhifRate";

        //Renumeration Schemes
        public const string GetAllRenumSchemes = ControllerName.Payroll + "GetAllRenumSchemes";
        public const string GetAddRenumScheme = ControllerName.Payroll + "GetAddRenumScheme";
        public const string GetUpdateRenumScheme = ControllerName.Staff + "GetUpdateRenumScheme";
    }
}