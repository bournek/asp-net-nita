﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.Constants
{
    public class CurriculumControllerRoute
    {
        public const string GetConfigurations = ControllerName.Curriculum + "GetConfig";
        public const string GetIndex = ControllerName.Curriculum + "GetIndex";
    }
}

