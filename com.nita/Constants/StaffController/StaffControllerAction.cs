﻿namespace com.nita.Constants
{
    public static class StaffControllerAction
    {
        //Normal 
        public const string StaffProfile = "StaffPreview";
        public const string AllStaff = "AllStaff";
        public const string Update = "UpdateStaff";
        public const string Index = "Index";//Dashboard

        //Admin Employee
        public const string AddStaff = "AddStaff";
        public const string TerminateStaff = "TerminateStaff";
        public const string Settings = "Settings";
    }
    public static class StaffUtilsControllerAction
    {
        
        //Job Gatalogue
        public const string AllJobCatalogues = "AllJobCatalogues";
        public const string AddJobCatalogue = "AddJobCatalogue";
        public const string UpdateJobCatalogue = "UpdateJobCatalogue";


        //Job Category
        public const string AllJobCategories = "AllJobCategories";
        public const string AddJobCategory = "AddJobCategory";
        public const string UpdateJobCategory = "UpdateJobCategory";

        //
        //Employment Type
        public const string AllEmploymentTypes = "AllEmploymentTypes";
        public const string AddEmploymentType = "AddEmploymentType";
        public const string UpdateEmploymentType = "UpdateEmploymentType";

        //Staff Designation
        public const string AllStaffDesignations = "AllStaffDesignations";
        public const string AddStaffDesignation = "AddStaffDesignation";
        public const string UpdateStaffDesignation = "UpdateStaffDesignation";

        //Staff Termination Type
        public const string AllStaffTerminationTypes = "AllStaffTermTypes";
        public const string AddTerminationType = "AddStaffTermType";
        public const string UpdateStaffTerminationType = "UpdateStaffTermType";
    }
}