﻿namespace com.nita.Constants
{
    public static class StaffControllerRoute
    {
        public const string GetProfile = ControllerName.Staff + "GetProfile";
        public const string GetIndex = ControllerName.Staff + "GetIndex";
        public const string AddStaff = ControllerName.Staff + "AddStaff";//
        public const string GetAllStaff = ControllerName.Staff + "AllStaff";//
        public const string UpdateStaff = ControllerName.Staff + "UpdateStaff";//
        public const string TerminateStaff = ControllerName.Staff + "TerminateStaff";
    }
    public static class StaffUtilsControllerRoute
    {
        //
        //
        public const string GetAllEmploymentTypes = ControllerName.Staff + "GetAllEmploymentTypes";
        public const string GetAddEmploymentType = ControllerName.Staff + "GetAddEmploymentType";
        public const string GetUpdateEmploymentType = ControllerName.Staff + "GetUpdateEmploymentType";
        //Job Gatalogue
        public const string GetAllJobCatalogues= ControllerName.Staff + "GetAllJobCatalogues";
        public const string GetAddJobCatalogue = ControllerName.Staff + "GetAddJobCatalogue";
        public const string GetUpdateJobCatalogue = ControllerName.Staff + "GetUpdateJobCatalogue";
        //Job Category
        public const string GetAllJobCategories = ControllerName.Staff + "GetAllJobCategories";
        public const string GetAddJobCategory = ControllerName.Staff + "GetAddJobCategory";
        public const string GetUpdateJobCategory = ControllerName.Staff + "GetUpdateJobCategory";

        //Staff Designation
        public const string GetAllStaffDesignations = ControllerName.Staff + "GetAllStaffDesignations";
        public const string GetAddStaffDesignation = ControllerName.Staff + "GetAddStaffDesignation";
        public const string GetUpdateStaffDesignation = ControllerName.Staff + "GetUpdateStaffDesignation";

        //Staff Termination Type
        public const string GetAllStaffTerminationTypes = ControllerName.Staff + "GetAllStaffTerminationTypes";
        public const string GetAddStaffTerminationType = ControllerName.Staff + "GetAddStaffTerminationType";
        public const string GetUpdateStaffTerminationType = ControllerName.Staff + "GetUpdateStaffTerminationType";
    }
}