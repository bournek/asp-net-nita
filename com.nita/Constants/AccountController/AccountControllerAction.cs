using System;

namespace com.nita.Constants
{
    public static class AccountControllerAction
    {
        public const string Login = "Login";

        public const string LogOff = "LogOff";

        public const string Register = "Register";

        public const string ConfrimEmail = "ConfirmEmail";

        public const string DsiplayEmail = "DisplayEmail";

        public const string ExternalLoginConfimation = "ExternalLoginConfirmation";

        public const string ExternalLoginFailure = "ExternalLoginFailure";

        public const string FogotPassword = "FogotPassword";

        public const string ForgotPasswordConfirmed = "ForgotPasswordConfirmed";

        public const string ResetPassword = "ResetPassword";

        public const string ResetPasswordConfirmed = "ResetPasswordConfirmed";

        public const string SendCode = "SendCode";

        public const string VerifyCode = "VerifyCode";

        public const string LinkLoginCallback = "LinkLoginCallback";

        public const string ExternalLogin = "ExternalLogin";

        public const string ExternalLoginCallback = "ExternalLoginCallback";

        public const string SendVerifyEmail = "SendVerifyEmail";
    }
}
