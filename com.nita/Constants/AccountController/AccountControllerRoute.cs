using System;

namespace com.nita.Constants
{
    public static class AccountControllerRoute
    {
        public const string GetLogin = ControllerName.Account + "Get" + AccountControllerAction.Login;
        public const string PostLogin = ControllerName.Account + "Post" + AccountControllerAction.Login;
        public const string GetRegister = ControllerName.Account + "Get" + AccountControllerAction.Register;
        public const string PostRegister = ControllerName.Account + "Post" + AccountControllerAction.Register;
        public const string GetVerifyCode = ControllerName.Account + "Get" + AccountControllerAction.VerifyCode;
        public const string PostVerifyCode = ControllerName.Account + "Post" + AccountControllerAction.VerifyCode;
        public const string GetConfirmEmail = ControllerName.Account + "Get" + AccountControllerAction.ConfrimEmail;
        public const string GetForgotPassword = ControllerName.Account + "Get" + AccountControllerAction.FogotPassword;
        public const string PostForgotPassword = ControllerName.Account + "Post" + AccountControllerAction.FogotPassword;
        public const string GetForgotPasswordConfirmation = ControllerName.Account + "Get" + AccountControllerAction.ForgotPasswordConfirmed;
        public const string PostForgotPasswordConfirmation = ControllerName.Account + "Post" + AccountControllerAction.ForgotPasswordConfirmed;
        public const string PostSendVerifyEmail = ControllerName.Account + "Post" + AccountControllerAction.SendVerifyEmail;
        public const string GetResetPassword = ControllerName.Account + "Get" + AccountControllerAction.ResetPassword;
        public const string PostResetPassword = ControllerName.Account + "Post" + AccountControllerAction.ResetPassword;
        public const string GetResetPasswordConfirmation = ControllerName.Account + "Get" + AccountControllerAction.ResetPasswordConfirmed;
        public const string PostExternalLogin = ControllerName.Account + "Get" + AccountControllerAction.ExternalLogin;
        public const string PostSendCode = ControllerName.Account + "Post" + AccountControllerAction.SendCode;
        public const string GetSendCode = ControllerName.Account + "Get" + AccountControllerAction.SendCode;
        public const string GetExternalLoginCallback = ControllerName.Account + "Get" + AccountControllerAction.ExternalLoginCallback;
        public const string GetELC = ControllerName.Account + "Get" + AccountControllerAction.ExternalLoginConfimation;
        public const string PostLogOff = ControllerName.Account + "Post" + AccountControllerAction.LogOff;
        public const string GetExternalLoginFailure = ControllerName.Account + "Get" + AccountControllerAction.ExternalLoginFailure;
        public const string GetLinkLoginCallback = ControllerName.Account + "Get" + AccountControllerAction.LinkLoginCallback;
    }
}
