﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.Constants
{
    public static class CorsPolicyName
    {
        public const string AllowAny = nameof(AllowAny);
    }
}
