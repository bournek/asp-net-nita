﻿namespace com.nita.Constants
{
    public static class ControllerName
    {
        public const string Settings = "Settings";
        public const string Error = "Error";
        public const string Home = "Home";
        public const string Account = "Account";
        public const string Admin = "Admin";
        public const string Staff = "Staff";
        public const string Trainer = "Trainer";

        public const string Finance = "Finance";
        public const string Payroll = "Payroll";
        public const string Curriculum = "Curriculum";
        public const string Providers = "Trainer";
        public const string Beneficiaries = "Trainer";
        public const string Institution = "Trainer";
        
    }
}