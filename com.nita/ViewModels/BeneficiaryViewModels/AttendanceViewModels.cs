﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AttendanceViewModel
    {
        [Required]
        [Range(1,int.MaxValue,ErrorMessage = "Please specify a valid cycle")]
        public int Cycle { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please specify a valid county")]
        public int County { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please specify a valid Training provider/Master Cracfman")]
        public int Trainer { get; set; }
        public DateTime Date { get; set; }
        public int[] Attendance { get; set; }
        public string Notes { get; set; }
    }
}
