﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nita.ViewModels
{
    public class ExamViewModel
    {
        public int Id { get; set; }
        public int TradeAreaId { get; set; }
        public int CycleId { get; set; }
        public string Name { get; set; } //End
        public double TotalScore { get; set; } //30

        public bool ExamIsDoneAcross { get; set; }
    }
}
