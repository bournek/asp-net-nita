﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class JsonResponse
    {
        public string status { get; set; }
        public Data data { get; set; }
        public List<string> errors { get; set; }
    }
    public class Data
    {
        public int id { get; set; }
        public string msg { get; set; }
    }
}
