﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nita.ViewModels.CycleViewModels
{
    public class CycleModel
    {
        public int CycleId { get; set; }
        public string CycleName { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string Day1 { get; set; }
        public string Month1 { get; set; }
        public string Year1 { get; set; }
        public string CycleCapacity { get; set; }
        public int YearId { get; set; }
    }
}
