﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddJobGroupViewModel
    {
        public string Name { get; set; }
        public int Rank { get; set; }
        [Display(Name = "Min. Pay")]
        public double MinPay { get; set; }
        [Display(Name = "Max. Pay")]
        public double MaxPay { get; set; }
        public double Increment { get; set; }
        //
        [Display(Name = "Effective Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        public bool Status { get; set; }
    }
}
