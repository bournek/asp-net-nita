﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddStaffViewModel
    {
        //Employee Info
        [Required]
        public string EmployeeNumber { get; set; }
        [Required]
        [Display(Name ="Job Title")]
        public int JobTitle { get; set; }//Position Hired

        public int Title { get; set; }//Salutation
        [Required]
        public string FullName { get; set; }
        public int Gender { get; set; }
        [Display(Name="Date of Birth")]
        public DateTime Dob { get; set; }
        [Display(Name ="Section")]
        [Range(1, int.MaxValue, ErrorMessage = "Section field is required")]
        public int SectionId { get; set; }
        public int Disability { get; set; }
        [Display(Name ="National ID")]
        public string NationalId { get; set; }
        [Display(Name ="Date Appointed")]
        public DateTime AppointmentDate { get; set; }
        [Display(Name ="Date Hired")]
        public DateTime HireDate { get; set; }

        //Personal
        [Display(Name ="Marital Status")]
        public int MaritalStatus { get; set; }
        [Display(Name ="KRA PIN")]
        public string KraPIN { get; set; }
        [Display(Name ="NHIF")]
        public string NHIF { get; set; }
        public string NSSF { get; set; }
        public string Spouse { get; set; }
        [Display(Name ="Religion")]
        public int ReligionId { get; set; }
        [Display(Name = "Location")]
        public int Location { get; set; }
        //Imigration
        [Display(Name ="Visa No.")]
        public string VisaNo { get; set; }
        [Display(Name ="Visa Exp.")]
        public DateTime VisaExpirery { get; set; }
        [Display(Name ="Passport No.")]
        public string PassportNo { get; set; }
        [Display(Name ="Passport Exp.")]
        public DateTime PassportExpirery { get; set; }
        public string Citizenship { get; set; }
        //Drivers Info
        [Display(Name ="License No.")]
        public string LicNo { get; set; }
        [Display(Name ="License Class")]
        public string LicClass { get; set; }
        [Display(Name ="License Exp.")]
        public DateTime LicExpirery { get; set; }
        //Contacts
        [Display(Name ="Address To")]
        public string Address { get; set; }
        public string PostalCode { get; set; }
        [Display(Name="Town/City")]
        public string City { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "County field is required")]
        public int County { get; set; }
        public string Street { get; set; }
        public string HouseNo { get; set; }
        [Required]
        [Display(Name ="Phone Number")]
        public string PhoneNumber { get; set; }
        [Required]
        [Display(Name ="Personal Email")]
        [EmailAddress]
        public string PersonalEmail { get; set; }
        [Display(Name = "Work Email")]
        [EmailAddress]
        public string WorkEmail { get; set; }
        //Hire Info
        [Display(Name ="Employee Category")]
        //[Range(1, int.MaxValue, ErrorMessage = "Employee Category field is required")]
        public int EmployeeCategory { get; set; }
        [Display(Name ="Job Category")]
        //[Range(1,int.MaxValue, ErrorMessage = "Job Category field is required")]
        public int JobCategory { get; set; }
        public int Supervisor { get; set; }
        public int Designation { get; set; }
        //
        [Display(Name = "Branch Name")]
        public int BankBranchId { get; set; }
        [Display(Name = "Acc. No.")]
        public string BankAccNo { get; set; }
        [Display(Name = "Acc. Name")]
        public string BankAccName { get; set; }
    }
}
