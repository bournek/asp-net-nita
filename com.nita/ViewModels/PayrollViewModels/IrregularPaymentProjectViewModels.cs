﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddIrregularPaymentProjectViewModel
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        [Display(Name = "Compensation Rate")]
        public double Rate { get; set; }
        [Range(1,double.MaxValue,ErrorMessage ="Duration should be a value grater than 1")]
        public double Duration { get; set; }
        [Display(Name = "Period Unit")]
        public int DurationUnit { get; set; }
        [Display(Name = "Effective Date")]
        public DateTime EffectiveDate { get; set; }
        [Display(Name ="End Date")]
        public DateTime EndDate { get; set; }
        [Display(Name = "Close Date")]
        public DateTime CloseDate { get; set; }
        [Display(Name = "Is Taxable ?")]
        public bool Taxable { get; set; }
    }
}
