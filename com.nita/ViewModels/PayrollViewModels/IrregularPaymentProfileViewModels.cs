﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddIrregularPaymentProfileViewModel
    {
        //[Display(Name = "Training Provider")]
        //public int? TrainerId { get; set; }
        [Display(Name = "Master Craftman")]
        public int? TrainerId { get; set; }
        [Display(Name = "Beneficiary")]
        public int? TraineeId { get; set; }
        [Display(Name = "Other Income")]
        public double OtherIncome { get; set; }
        [Display(Name = "Pay Mode")]
        public int PayMode { get; set; }
        [Display(Name = "Effective Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        [Display(Name = "Excempt from Taxation ?")]
        public bool ExemptTax { get; set; }
        [Display(Name = "Apply P.A.Y.E Releif ?")]
        public bool ApplyPayeRelief { get; set; }
    }
}
