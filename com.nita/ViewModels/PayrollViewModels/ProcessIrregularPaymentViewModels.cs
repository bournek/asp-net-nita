﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class ProcessIrregularPaymentViewModel
    {
        [Display(Name = "Project")]
        public int? ProjectId { get; set; }
        public List<IrregularEarning> PendingEarnings {get;set; }
        [Display(Name = "Unpaid Earnings")]
        public int[] earnings { get; set; }
        public ProcessIrregularPaymentViewModel()
        {
            PendingEarnings = new List<IrregularEarning>();
        }
    }
}
