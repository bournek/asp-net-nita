﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddIrregularPVViewModel
    {
        public int Ledger { get; set; }
        [Display(Name = "Payable To")]
        public string PayableTo { get; set; }
        [Display(Name = "Gross Amount")]
        public double GrossAmount { get; set; }
        public double Discount { get; set; }
        [Display(Name = "Management Amount")]
        public double MgmtAmount { get; set; }
        public string Description { get; set; }
        [Display(Name = "Cheque No.")]
        public string CheckNo { get; set; }//
        public string VoucherNo { get; set; }
        public int[] earnings { get; set; }
        [Display(Name = "Payee Name")]
        public string PayeeName { get; set; }
        [Display(Name = "Sponsor Name")]
        public int Sponsor { get; set; }//
        public List<IrregularEarning> UnpaidEarnings = new List<IrregularEarning>();
    }
}
