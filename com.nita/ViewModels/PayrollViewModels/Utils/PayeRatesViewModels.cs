﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddPayeRateViewModel
    {
        [Required]
        public string Code { get; set; }
        public string Description { get; set; }
        [Display(Name = "Personal Relief")]
        public double PersonalRelief { get; set; }
        //
        [Display(Name = "Effective Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
    }
}
