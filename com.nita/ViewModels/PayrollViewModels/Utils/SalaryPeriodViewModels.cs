﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddSalaryPeriodViewModel
    {
        [Required]
        public string Name { get; set; }
        [Range(2010,int.MaxValue,ErrorMessage ="Year should be at least after 2010")]
        public int Year { get; set; }
        [Display(Name = "Effective Date")]
        public DateTime BeginDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        [Display(Name = "Payment Frequency")]
        public int PayFreq { get; set; }
        [Display(Name = "Close Date")]
        public DateTime CloseDate { get; set; }
    }
}
