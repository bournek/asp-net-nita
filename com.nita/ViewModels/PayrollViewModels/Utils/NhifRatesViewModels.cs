﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddNhifRateViewModel
    {
        public string Description { get; set; }
        [Display(Name="Effective Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

    }
    public class AddDeductionRangeViewModel
    {
        [Display(Name = "Rate")]
        public int RateId { get; set; }
        public NhifRate NhifRate { get; set; }
        public PayeRate PayeRate { get; set; }
        [Display(Name = "Starting From")]
        public double Min { get; set; }
        [Display(Name = "To a Max. of")]
        public double Max { get; set; }
        [Display(Name = "Deduction Amount")]
        public double Amount { get; set; }
    }
}
