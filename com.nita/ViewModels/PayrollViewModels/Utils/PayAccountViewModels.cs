﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddPayAccountViewModel
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Pay Type")]
        public int PayType { get; set; }
        [Display(Name = "Is Regular Pay Account ?")]
        public bool IsRegular { get; set; }
        [Display(Name = "Applicable Deductions")]
        public int[] Deductions { get; set; }
        [Display(Name= "GL Account")]
        public int Account { get; set; }
        [Display(Name = "Department")]
        public int Dept { get; set; }
        [Display(Name = "Employer Contibution GL Account")]
        public int? ContributionAccount { get; set; }
        [Display(Name = "iTax Mapping Field")]
        public string ITaxField { get; set; }
        [Display(Name = "Closed/Inactive")]
        public bool IsActive { get; set; }
        [Display(Name = "P.A.Y.E Threshold")]
        public double PayeThreshold{ get; set; }
    }
}
