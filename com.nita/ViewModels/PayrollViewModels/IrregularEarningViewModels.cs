﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddIrregularEarningViewModel
    {
        [Display(Name = "Beneficiary")]
        public int? TraineeId { get; set; }
        [Display(Name = "Master Craftman")]
        public int? TrainerId { get; set; }
        [Display(Name = "Pay Account")]
        public int AccountId { get; set; }
        [Display(Name = "Linked Payment Project")]
        public int ProjectId { get; set; }
        [Display(Name = "Department")]
        public double Dept { get; set; }
        [Display(Name = "Is Consultancy Payment ?")]
        public bool IsConsultancy { get; set; }
    }
}
