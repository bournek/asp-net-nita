﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class UpdateFinanceSettingsViewModel
    {
        //Prepayemnt Settings
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Customer Account is required.")]
        [Display(Name = "Customer Account")]
        public int PrePaymentCutomerAccount { get; set; }
        [Required]
        [Display(Name = "Receipt Alias")]
        public string PrePaymentReceiptAlias { get; set; }
        //
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Bad Debt GL Account is required.")]
        [Display(Name = "Bad Debt Account")]
        public int BadDebtAccount { get; set; }
        //
        [Required]
        [Range(1,int.MaxValue,ErrorMessage = "Accumulated Fund GL Account is required.")]
        [Display(Name = "Fiscal Year Accumulated Funds")]
        public int AccumulatedFund { get; set; }
        //
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Depreciation GL Account is required.")]
        [Display(Name = "Depreciation Account")]
        public int DepreciationAccount { get; set; }
        //
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Capital Grants GL Account is required.")]
        [Display(Name = "Capital Grants Account")]
        public int CapitalGrantsAccount { get; set; }
        //
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Opening Balance GL Account is required.")]
        [Display(Name = "Opening Balance Account")]
        public int OpeningBalanceAccount  { get; set; }

        [Display(Name = "Accounts Receivable (Debtors) Accounts")]
        public string ARDebtors { get; set; }//JSON
        [Display(Name = "Accounts Payable (Creditors) Accounts")]
        public string APCreditors { get; set; }//JSON
        [Display(Name = "Enable Printing Receipts ?")]
        public bool EnableReceiptPrinting { get; set; }
        [Display(Name = "Post a payment once completed ?")]
        public bool PostCompletedPayments { get; set; }
    }
}
