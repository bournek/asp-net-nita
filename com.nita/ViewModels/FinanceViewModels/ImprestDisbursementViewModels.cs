﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddImprestDisbursementViewModel
    {
        public ImprestWarrant Imprest { get; set; }
        [Display(Name = "Ref. No.")]
        public int? Ref { get; set; } = 0;
        [Display(Name = "Imprest Warrant")]
        [Range(1,int.MaxValue, ErrorMessage = "A Imprest Warrant Required")]
        public int WarrantId { get; set; }
        [Display(Name = "Disbursement Notes")]
        public string Description { get; set; }
    }
}
