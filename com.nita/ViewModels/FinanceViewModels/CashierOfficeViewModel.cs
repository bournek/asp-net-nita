﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddCashierOfficeViewModel
    {
        [Required]
        public string Name { get; set; }
        [Display(Name = "Ref. No.")]
        public string RefNo { get; set; }
        [Required]
        [Display(Name = "Location")]
        public int LocationId { get; set; }
        [Display(Name = "Department")]
        public int DeptId { get; set; }
        [Display(Name = "Ledger")]
        public int LedgerId { get; set; }
        [Display(Name = "Ledger Cash Acc.")]
        public int Mode { get; set; }
        [Display(Name = "GL Account")]
        public int Account { get; set; }
        //       
        [Display(Name = "Closed/In-Active")]
        public bool IsActive { get; set; }
    }

    public class AddCashierOfficeAttendantViewModel
    {
        public StaffProfile Staff { get; set; }
        public FCashier Office { get; set; }
        [Display(Name = "Staff")]
        public int StaffId { get; set; }
        [Display(Name = "Cashier Office")]
        public int OfficeId { get; set; }
        //       
        [Display(Name = "Closed/In-Active")]
        public bool IsActive { get; set; }
    }
}
