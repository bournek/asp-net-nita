﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddPaymentAccountViewModel
    {
        [Required]
        [Display(Name ="GL Account")]
        public int Account { get; set; }
        [Required]
        [Display(Name ="Ledger")]
        public int Ledger { get; set; }
        public string Name { get; set; }
        [Display(Name= "Bank EFT Payee Name")]
        public string PayeeName { get; set; }
        [Display(Name="Acc. Number")]
        public string AccNo { get; set; }
        [Display(Name = "More Details")]
        public string Description { get; set; }
        [Display(Name ="API UserID")]
        [Required]
        public string SOAPUserId { get; set; }
        [Display(Name ="API Password")]
        [Required]
        public string SOAPPassword { get; set; }
        public int Mode { get; set; }
        [Display(Name = "Has EFT ?")]
        public bool HasEFT { get; set; }
    }

    
}
