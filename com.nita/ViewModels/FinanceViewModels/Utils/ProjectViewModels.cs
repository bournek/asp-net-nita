﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddProjectViewModel
    {
        public int Ledger { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string ProjectNo { get; set; }
        public double Amount { get; set; }
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        [Display(Name = "Completed Percentage")]
        public double CompletedPercentage { get; set; }

        public int[] Coordinators { get; set; }
        public int[] Beneficiaries { get; set; }
    }
}
