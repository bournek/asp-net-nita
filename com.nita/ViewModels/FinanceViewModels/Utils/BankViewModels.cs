﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddBankViewModel
    {
        [Required]
        [Display(Name = "Bank Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Bank Code")]
        public string Code { get; set; }
        [Display(Name = "Switf Code")]
        public string SwiftCode { get; set; }
        public string Address { get; set; }
        [Phone]
        [Display(Name = "Tel. No.")]
        public string TelNo { get; set; }
        [Display(Name = "Contact Email")]
        public string ContactEmail { get; set; }
        public string Website { get; set; }
        [Display(Name = "Fax No.")]
        public string FaxNo { get; set; }
    }
    public class AddBankBranchViewModel
    {
        public int Bank { get; set; }
        [Required]
        [Display(Name ="Branch Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Branch Code")]
        public string Code { get; set; }
        public string Address { get; set; }
        [Phone]
        [Display(Name = "Tel. No.")]
        public string TelNo { get; set; }
        [Display(Name = "Contact Email")]
        public string ContactEmail { get; set; }
        [Display(Name = "Fax No.")]
        public string FaxNo { get; set; }
    }
}
