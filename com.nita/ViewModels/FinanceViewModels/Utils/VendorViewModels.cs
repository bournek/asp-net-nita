﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddVendorViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}
