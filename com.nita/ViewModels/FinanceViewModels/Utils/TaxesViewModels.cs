﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddTaxViewModel
    {
        [Display(Name="Account")]
        public int Account { get; set; }
        [Display(Name= "Round-Off Type")]
        public int RoundType { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public double Rate { get; set; }
    }
}
