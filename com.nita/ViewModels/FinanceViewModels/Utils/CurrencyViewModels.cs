﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddCurrencyViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        public string Alias { get; set; }
        [Display(Name="Mark as Base Currency")]
        public bool IsBase { get; set; }
    }
}
