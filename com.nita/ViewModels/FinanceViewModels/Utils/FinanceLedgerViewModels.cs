﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddFinanceLedgerViewModel
    {
        [Required]
        public int Currency { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Closed/In-Active")]
        public bool IsClosed { get; set; }
    }
}
