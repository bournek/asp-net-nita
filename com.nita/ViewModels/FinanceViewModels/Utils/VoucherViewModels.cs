﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddVoucherViewModel
    {
        public int Ledger { get; set; }
        [Required]
        public string Code { get; set; }
    }
}
