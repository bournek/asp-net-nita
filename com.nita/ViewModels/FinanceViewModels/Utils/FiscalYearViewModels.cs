﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddFiscalYearViewModel
    {
        public string Name { get; set; }
        public DateTime Begins { get; set; }
        public DateTime Ends { get; set; }
        public DateTime Closed { get; set; }
        public AddFiscalYearViewModel()
        {
            Begins = DateTime.Now;
            Ends = DateTime.Now;
            Closed = DateTime.Now;
        }
    }
    public class AddFiscalYearQuarterViewModel
    {
        public int Year { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public DateTime Begin { get; set; }
        public DateTime Ends { get; set; }
        public AddFiscalYearQuarterViewModel()
        {
            Begin = DateTime.Now;
            Ends = DateTime.Now.AddMonths(3);
        }
    }
}
