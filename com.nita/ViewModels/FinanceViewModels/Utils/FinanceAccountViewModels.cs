﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddFinanceAccountViewModel
    {
        [Required]
        [Range(1,int.MaxValue,ErrorMessage ="A GL Account Type is required")]
        [Display(Name = "GL Account Type")]
        public int Type { get; set; }
        [Required]
        [Display(Name = "GL Account Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "GL Account No.")]
        public string Number { get; set; }
        [Display(Name = "Is Child Account ?")]
        public bool IsChild { get; set; }
        [Display(Name = "Is Budget Item ?")]
        public bool IsBudgetItem { get; set; }
        [Display(Name = "InActive/Closed")]
        public bool IsClosed { get; set; }
        [Display(Name = "Parent GL Account")]
        public int? Parent { get; set; } = 0;
    }
}
