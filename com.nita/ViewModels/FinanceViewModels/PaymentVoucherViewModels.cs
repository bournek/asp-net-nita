﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddPaymentVoucherViewModel
    {
        public string InvoiceIds { get; set; }
        public IList<FInvoice> SelectedInvoices { get; set; }
        //
        public string ImprestIds { get; set; }
        public IList<ImprestWarrant> ImprestWarrants { get; set; }
        //
        public string ClaimIds { get; set; }
        public IList<ExpenseClaim> ExpenseClaims { get; set; }
        //
        public string PCRequestIds { get; set; }
        public IList<PettyCashReq> PCRequests { get; set; }

        //
        [Display(Name = "Department")]
        public int? Dept { get; set; } = 0;
        [Display(Name = "GL Account")]
        public int? GLAccount { get; set; } = 0;
        [Display(Name = "A/P(Creditors) Account")]
        public int? APAccount { get; set; } = 0;
        public int Ledger { get; set; }
        [Display(Name = "Tax Type")]
        public int? Tax { get; set; }
        public int? Project { get; set; }
        public int? Supplier { get; set; }
        [Display(Name ="Staff Account(s)")]
        public int? PVStaff { get; set; }
        [Display(Name = "Cashier Office")]
        public int? Cashier { get; set; }
        [Display(Name = "Payable To")]
        public string PayableTo { get; set; }
        [Display(Name = "Gross Amount")]
        public double GrossAmount { get; set; }
        public double Discount { get; set; }
        [Display(Name = "Management Amount")]
        public double MgmtAmount { get; set; }
        public string Description { get; set; }
        [Display(Name = "Cheque No.")]
        public string CheckNo { get; set; }//
        [Display(Name = "Voucher No.")]
        public string VoucherNo { get; set; }
        public DateTime VoucherDate { get; set; }
        public int[] Invoices { get; set; }
        public int? Mode { get; set; }
        //
        public bool Registered { get; set; }
        public string PayeeName { get; set; }
        [Display(Name ="Sponsor Name")]
        public int Sponsor { get; set; }//
        public string Payables { get; set; }
        public AddPaymentVoucherViewModel()
        {
            GLAccount = 0;
            SelectedInvoices = new List<FInvoice>();
            ImprestWarrants = new List<ImprestWarrant>();
            ExpenseClaims = new List<ExpenseClaim>();
            PCRequests = new List<PettyCashReq>();
        }
    }

    public class PostPaymentVoucherViewModel
    {
        public int VoucherId { get; set; }
        [Display(Name = "Payment Account")]
        public int BankMode { get; set; }
        [Display(Name = "Cheque No.")]
        public string CheckNo { get; set; }
        public FPaymentVoucher Voucher { get; set; }
        public double NetAmount { get; set; }
        public double NetTax { get; set; }
        [Display(Name = "Pay via EFT ? ")]
        public bool PayEFT { get; set; }
    }

    public class PostAllPaymentsViewModel
    {
        public bool PostAll { get; set; }
        public int[] ecs { get; set; }
        public int[] pcs { get; set; }
        public int[] pvs { get; set; }
        public int[] iws { get; set; }
    }
}
