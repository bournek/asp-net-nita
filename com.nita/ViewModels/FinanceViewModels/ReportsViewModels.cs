﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public enum FinanceReports
    {
        DailyCashReturn,SummarizedIncome,FeesColection,SponsorshipAllocation,
        SummarizedSponsorshipAllocations,UnderspentImprest,DetailedPayments,PaymentVouchers,
        UnpostedPaymentVouchers,CashPaymentVouchers,ImprestWarrant,PettyCash,PendingPettyCash,
        CashierBalance,ImprestBalance,CustomerInvoices,CustomerBalances,SupplierStatement,
        StudentFeePaymentPerAcc,FeesInvoicedPerSession,UnbankedCash,PendingImprest, SummarizedPayments,
        ImprestSurrender, UnSurrenseredImprest
    }
    public enum FinanceStatements
    {
        Income_Expenditure,TrailBalance,FinancialPosition,CashFlow,ChangeInNetAsset,FinancialPerformance,
        ComparisonOfBudgetAndActualAmount,NotesToFinancialStatement,GeneralLedger,AgedReceivables,AgedPayables,
        CashflowAnalysis,FixedAssetMovementSchedule,SummarizedTrialBalance,StudentSchedules,GeneralLedgerSummary,
        ProjectProgressSummary,ProjectIncomeAndExpenditure
    }
    public enum QuartelyFinancialStatements
    {
        FinancialPerformance,FinancialPosition,Cashflow,BudgetAndActualComparison,FinancialStatementNotes,CashFlowNotes
    }
    public enum BudgetaryReports
    {
        ComparisonOfBudgetActualAmounts,FinancialStatementBudgetActualNotes
    }
    public enum MimeType
    {
        xls,xlsx,cvs,pdf
    }
    public enum Comparator
    {
        EQUALTO=8,LESSTHAN=7,GREATERTHAN=6,BETWEEN=5,ALL=2
    }
    public class ReportsViewModel
    {
        public FinanceReports FinanceReport { get; set; }
        public FinanceStatements Statement { get; set; }
        public QuartelyFinancialStatements QStatement { get; set; }
        public BudgetaryReports BudgetReport { get; set; }

        public Comparator Comparator { get; set; }
        public DateTime? Begin { get; set; }
        public DateTime? End { get; set; }

        //Customer
        public int? CustomerRef { get; set; }
        public int? CustomerType { get; set; }

        //Supplier
        public int? PayeeRef { get; set; }
        public int? SupplierRef { get; set; }

        //Student
        public int? RegNo { get; set; }
        public int? ClassCode { get; set; }
        public int? Programme { get; set; }
        public int? School { get; set; }
        public int? StudentDomnicile { get; set; }
        public int? StudyMode { get; set; }
        public int? StudyType { get; set; }
        public int? FinancialAid { get; set; }
        public int? Sponsorship { get; set; }
        public int? Session { get; set; }

        //Staff
        public int? Staff { get; set; }
        public int? StaffType { get; set; }
        public int? JobTitle { get; set; }
        public int? StaffLocation { get; set; }
        //GL Info
        public int? Dept { get; set; }
        public int? Ledger { get; set; }
        public int? Project { get; set; }
        public int? Campus { get; set; }
        public int? GLAccount { get; set; }
        public int? PaymentAccount { get; set; }
        public int? Currency { get; set; }
        public int? FiscalYear { get; set; }

        //Personnel
        public int? CPersonnel { get; set; }
        public int? APersonnel { get; set; }

        //
        public bool IncludeOld { get; set; }
        public bool IncludeZeroBalances { get; set; }
        public bool StoreDebtorsBalances { get; set; }

        public MimeType Export { get; set; }
    }

    public class BudgetStatusViewModel
    {
        public int GLAccount { get; set; }
        public DateTime? Date { get; set; }
        public int Dept { get; set; }
        [Display(Name ="Show Budgeted Departments Only ?")]
        public bool BudgetedDeptsOnly { get; set; }
        [Display(Name="Show Budgeted GL Accounts Only ?")]
        public bool BudgetedAccountOnly { get; set; }
        public MimeType Export { get; set; }
    }
    public class StudentFeeSummaryViewModel
    {
        public DateTime? Begin { get; set; }
        public DateTime? End { get; set; }
        public int? StudyMode { get; set; }
        public int? StudyType { get; set; }
        public int? Campus { get; set; }
        public int? ClassCode { get; set; }
        public int? School { get; set; }
        public int? Programme { get; set; }
        public MimeType Export { get; set; }
    }
}
