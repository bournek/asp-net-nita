﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddInvoiceViewModel
    {
        //public string DocNo { get; set; }
        [Required]
        [Display(Name = "Order No.")]
        public string OrderNo { get; set; }
        public int Ledger { get; set; }
        public int Project { get; set; }
        public int Supplier { get; set; }
        [Display(Name="Accounts Payables")]
        public string Payables { get; set; }
        [Display(Name = "Invoice Date")]
        public DateTime InvoiceDate { get; set; }
        [Display(Name = "Billing Date")]
        public DateTime? BillDate { get; set; }
        [Display(Name ="Invoicing Terms")]
        public int Term { get; set; }
        public DateTime DueDate { get; set; }
    }
}
