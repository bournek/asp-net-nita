﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddExpenseClaimViewModel
    {
        public ImprestWarrant Warrant { get; set; }
        [Display(Name = "Staff")]
        public int Staff { get; set; }
        public int Ledger { get; set; }
        public int? Project { get; set; }

        [Display(Name = "Associated Imprest")]
        public int? Imprest { get; set; }
        //
        [Display(Name = "Ref. No.")]
        public string Ref { get; set; }
        [Display(Name = "Order No.")]
        public string OrderNo { get; set; }
        [Display(Name = "Nature of duty")]
        public string DutyCalled { get; set; }
        [Display(Name = "Amount Claiming")]
        public double Amount { get; set; }
        [Display(Name = "GL Account Payables")]
        public string Payables { get; set; }
        [Display(Name = "Claim Entries")]
        [Required]
        public string ClaimEntries { get; set; }
        public MileageExpense Mileage { get; set; }
        public Substainance Substainance { get; set; }
        public AddExpenseClaimViewModel()
        {
            Mileage = new MileageExpense();
            Substainance = new Substainance();
        }
    }
}
