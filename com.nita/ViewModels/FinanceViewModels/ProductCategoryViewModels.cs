﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddProductCategoryViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        [Display(Name = "Closed/Incative")]
        public bool Status { get; set; }
    }
}
