﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddSupplierInvoiceWaiverViewModel
    {
        //UPDATE FIELDS
        public SupplierInvoiceWaiver Waiver { get; set; }
        public int? WaiverId { get; set; }

        //
        public FInvoice Invoice { get; set; }
        [Display(Name = "Unpaid Invoice")]
        [Range(1,int.MaxValue,ErrorMessage = "Invoice is required.")]
        public int InvoiceId { get; set; }
        [Display(Name = "Department")]
        public int DeptId { get; set; }
        [Display(Name = "GL Account")]
        public int AccountId { get; set; }
        public int? Project { get; set; }
        [Display(Name = "Ref.")]
        public string Ref { get; set; }
        [Display(Name = "Waiver Amount")]
        public double Amount { get; set; }
        [Display(Name = "Additional Details")]
        public string Details { get; set; }
    }
}
