﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public enum PayablesType
    {
        Unspecified,AccPayables,AccReceivables
    }
    public class JsonAccountPayable
    {
        public int AccId { get; set; }
        public double GrossAmount { get; set; }
        public double SpentAmount { get; set; }
        public double Discount { get; set; }
        public int PayTo { get; set; }
        public int Dept { get; set; }
        public int ApAcc { get; set; }
        public int ArAcc { get; set; }
        public int[] Taxes { get; set; }
    }
    public class JsonClaimItem
    {
        public double Qty { get; set; }
        public double UnitPrice { get; set; }
        public string UnitName { get; set; }
        public DateTime Date { get; set; }
    }
}
