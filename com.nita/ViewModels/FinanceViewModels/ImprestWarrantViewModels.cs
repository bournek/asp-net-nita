﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddImprestWarrantViewModel
    {
        [Display(Name = "Staff")]
        public int Staff { get; set; }
        [Display(Name = "Ledger")]
        public int Ledger { get; set; }
        [Display(Name = "Project")]
        public int? Project { get; set; }
        [Display(Name = "REF. NO.")]
        public string Ref { get; set; }
        [Required]
        [Display(Name = "Order No.")]
        public string OrderNo { get; set; }
        [Display(Name = "Nature of Duty")]
        public string NatureOfDuty { get; set; }
        [Display(Name = "Proposed Itinerary")]
        public string ProposedItinerary { get; set; }
        [Display(Name = "Follow-up notes")]
        public string Description { get; set; }
        [Display(Name = "Estimated Days Away")]
        public int EstimatedDaysAway { get; set; }
        [Display(Name = "Surrender Date")]
        public DateTime SurrenderDate { get; set; }
        [Display(Name = "Create Date")]
        public DateTime CreateDate { get; set; }
        public int Department { get; set; }
        [Display(Name = "Receiving Payee")]
        //[Range(1,int.MaxValue,ErrorMessage ="Receiving Payee is required")]
        public int? PayeeNo { get; set; }
        [Display(Name = "GL Account Payables")]
        public string Payables { get; set; }
    }
}
