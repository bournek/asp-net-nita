﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddPettycashDisbursementViewModel
    {
        public PettyCashReq Request { get; set; }
        public string Ref { get; set; }
        [Display(Name = "Petty Cash Entitled User :")]
        public int Staff { get; set; }
        [Display(Name = "Petty Cash Req.")]
        public int RequestId { get; set; }
        [Display(Name = "Description")]
        public string Desc { get; set; }
        [Display(Name = "Closed/Inactive")]
        public bool Active { get; set; }
    }
}
