﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddPettyCashItemViewModel
    {
        [Display(Name = "Staff")]
        public int? StaffId { get; set; }
        [Display(Name = "Beneficiary")]
        public int? TraineeId { get; set; }
        [Display(Name = "Master Craftman")]
        public int? TrainerId { get; set; }
        [Display(Name = "Project")]
        public int? ProjectId { get; set; }
        [Display(Name = "Ledger")]
        public int LedgerId { get; set; }
        //
        [Display(Name = "Ref. No.")]
        public string Ref { get; set; }
        public string Description { get; set; }
        public string Payables { get; set; }
    }
}
