﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddSupplierViewModel
    {
        //EDIT FLAGS
        public FSupplier Supplier { get; set; }
        public int? SupplierId { get; set; }

        //
        [Required]
        public string Name { get; set; }
        [Display(Name="Vendor Type")]
        public int VendorType { get; set; }
        public string Address { get; set; }
        [Display(Name = "Tel. No.")]
        public string TelNo{get;set;}
        [Display(Name = "Ext.")]
        public string Ext { get; set; }
        [Phone]
        [Display(Name = "Mobile Phn.")]
        public string CellNo { get; set; }
        [Display(Name = "Fax No.")]
        public string FaxNo { get; set; }
        [EmailAddress]
        public string Email { get; set; }

        //
        [Display(Name = "KRA PIN")]
        public string KRAPIN { get; set; }
        [Display(Name = "VAT No.")]
        public string VATNo { get; set; }

        //
        [Display(Name = "Bank")]
        public int BankCode { get; set; }
        [Display(Name = "Bank Acc.")]
        public string BankAccount { get; set; }
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
        //Options
        [Display(Name = "Discounts(%)")]
        public double Discount { get; set; }
        [Display(Name = "Exempt From Tax")]
        public bool ExemptFromTax { get; set; }
        [Display(Name = "Hold Payments")]
        public bool HoldPayments { get; set; }
        //
        [Display(Name = "Payment Term")]
        public bool PaymentTerm { get; set; }
        [Display(Name = "Address")]
        public string LatLng { get; set; }
        [Display(Name = "Location")]
        public string Loc { get; set; }
        [Range(0,int.MaxValue,ErrorMessage ="County is required")]
        public int County { get; set; }
    }
}
