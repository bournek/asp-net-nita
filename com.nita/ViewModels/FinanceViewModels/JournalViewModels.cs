﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddPayableJournalViewModel
    {
        [Display(Name = "Ledger")]
        public int LedgerId { get; set; }

        //Debit
        [Display(Name = "Debit Project")]
        public int? DebitProjectId { get; set; }
        [Display(Name = "Debit Dept.")]
        public int DebitDeptId { get; set; }
        [Display(Name = "Debit Account.")]
        public int DebitAccountId { get; set; }

        //Credit
        [Display(Name = "Credit Project")]
        public int? CreditProjectId { get; set; }
        [Display(Name = "Credit Dept.")]
        public int CreditDeptId { get; set; }
        [Display(Name = "Credit Account")]
        public int CreditAccountId { get; set; }

        //
        [Display(Name = "Ref.")]
        public string Ref { get; set; }
        public double Amount { get; set; }
        public string Reference { get; set; }
        [Display(Name = "Is Revaluation ? ")]
        public bool Revaluation { get; set; }
    }
    public class ImportPayrollJournalViewModel
    {
        public IFormFile[] Files { get; set; }
    }
}
