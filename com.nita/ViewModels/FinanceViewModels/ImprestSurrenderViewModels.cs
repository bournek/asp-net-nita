﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddImprestSurrenderViewModel
    {
        public ImprestDisbursement Disbursement { get; set; }
        [Display(Name = "Staff")]
        [Range(1,int.MaxValue,ErrorMessage = "Staff is required")]
        [Required]
        public int Staff { get; set; }
        [Display(Name = "Imprest Disbursement")]
        public int IWDisbursementId{ get; set; }
        [Display(Name = "Payment Mode")]
        public int? ModeId { get; set; }
        //[Display(Name = "Amount Spent")]
        //public double AmountSpent { get; set; }
        [Display(Name ="Imprest Expense Breakdown")]
        public string Payables { get; set; }
        
        [Display(Name = "Payment Mode Number")]
        public string ModeAccNumber { get; set; }
        [Display(Name = "Settlement Payment Mode")]
        public int? RecoveryPaymentAccount { get; set; }
    }
}
