﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddExpenseDisbursementViewModel
    {
        [Display(Name ="Staff")]
        public int Staff { get; set; }
        [Display(Name = "Claim To Disburse")]
        public int ClaimId { get; set; }
        public ExpenseClaim Claim { get; set; }
        [Display(Name = "Payment Details")]
        public string PaymentEntries { get; set; }
    }
    public class JsonPaymentEntry
    {
        public int AccId { get; set; }
        public bool EFT { get; set; }
        public int Mode { get; set; }
        public string Cheque { get; set; }
    }
}
