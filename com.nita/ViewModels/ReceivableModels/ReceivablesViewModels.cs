﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nita.ViewModels
{
    public class ReceivablesViewModels
    {


    }

    public class ReceivableProductViewModel
    {
        public double SellPrice { get; set; }
        public double? CostPrice { get; set; } = null;
        public double? MarkUp { get; set; }
        public string Code { get; set; }
        public string ProductName { get; set; }
        public int FProductCategoryId { get; set; }
        public int FTaxesId { get; set; }
       public bool DiscountStatus { get; set; }

    }

    public class ReceivableCustomerViewModel
    {
        public int? FCurrencyId { set; get; }
        public string CustomerName { set; get; }
        public string CustomerCode { set; get; }
        public string CustomerMobile { set; get; }
        public string CustomerEmail { set; get; }
        public string CustomerAddress { set; get; }
        public int CustomerType { set; get; }
        public string RegNo { set; get; }
        public double? CustomerBalance { set; get; }
        public int FCustomerTypeId { get; set; }
        public bool ActiveStatus { get; set; }
    }

   
    public class ReceivableInvoiceViewModel
    {
        public int Discount { get; set; }
        public int FTaxesId { get; set; }
        public int ProductId{ get; set; }
        public int Quantity { get; set; }
        public double SellPrice { get; set; }
    }

    public class InvoiceProductModel
    {

    }
    public class ViewProductInvoiceModel
    {
        public List<ReceivableInvoiceViewModel> InvoiceProducts { get; set; }
        public string Terms { get; set; }
        
        public int FCurrencyId { get; set; }

        public int CustomerId { get; set; }

        public DateTime DueDate { get; set; }
        public int Recurrency { get; set; }

    }

    public class ReceivableProductPricesViewModel
    {

    }

    public class ReceivableCustomerInvoiceProductModel
    {
       
        public int InvoiceId { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public double ProductSellPrice { get; set; }
        public int? FTaxesId { get; set; }
        public DateTime DateCreated { get; set; }
        public int ItemStatus { get; set; }
        public int Status { get; set; } = 1;
        public int Quantity { get; set; }
        public double Discount { get; set; } = 0.00;

      

    }

    public class ReceivablesCustomerInvoiceModelView
    {
        public int CustomerId { get; set; }
        public int InvoiceNo { get; set; }
        public int PaidAmount { get; set; }
        public bool InvoiceStatus { get; set; } = false;
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public string Terms { get; set; }
        public int Recurrency { get; set; }
        public DateTime DateDue { get; set; }
        public int FCurrencyId { get; set; }
        public int PendingAmount { get; set; }
        

    }

  


}
