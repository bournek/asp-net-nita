﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddDepartmentViewModel
    {
        [Required]
        public string Name { get; set; }
        [Display(Name = "Closed/Inactive")]
        public bool Status { get; set; }
        [Display(Name = "Department Type")]
        [Range(1,int.MaxValue,ErrorMessage = "Department Type is required")]
        public int Type { get; set; }
    }
}
