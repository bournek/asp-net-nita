﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddLocationViewModel
    {
        public string Name { get; set; }
        [Display(Name = "Co-ordinates")]
        public string LatLng { get; set; }
        public string Address { get; set; }
        public int County { get; set; }
        [Display(Name = "InActive/Closed")]
        public bool IsActive { get; set; }
    }

    public class JsonLatLng {
        public double lat { get; set; }
        public double lng { get; set; }
    }
}
