﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddSectionViewModel
    {
        [Display(Name = "Department")]
        [Range(1,int.MaxValue,ErrorMessage = "Department is required")]
        public int Dept { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Closed/Inactive")]
        public bool Status { get; set; }
    }
}
