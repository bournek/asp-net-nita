﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class AddDivisionViewModel
    {
        public Department Department { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Section is required")]
        public int Section { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Closed/Inactive")]
        public bool Status { get; set; }
    }
}
