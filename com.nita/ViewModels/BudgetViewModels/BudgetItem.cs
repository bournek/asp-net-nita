﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nita.ViewModels.BudgetViewModels
{
    public class BudgetItem
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int DeptId { get; set; }
        public int CycleId { get; set; }
        public int PeriodId { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public bool Enforce { get; set; }
    }
}
