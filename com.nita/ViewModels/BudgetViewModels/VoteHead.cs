﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nita.ViewModels.BudgetViewModels
{
    public class VoteHead
    {
        public int Number { get; set; }
        public string AccName { get; set; }
        public double Amount { get; set; }
        public bool Enforced { get; set; }
    }
}
