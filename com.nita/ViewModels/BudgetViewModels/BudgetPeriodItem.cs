﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nita.ViewModels.BudgetViewModels
{
    public class BudgetPeriodItem
    {
        public int PeriodId { get; set; }
        public string PeriodName { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string Day1 { get; set; }
        public string Month1 { get; set; }
        public string Year1 { get; set; }
        public int YearId { get; set; }
    }
}
