﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class PrintChequeModel
    {
        public int Id { get; set; }
        public string PayeeName { get; set; }
        public List<FPayment> Payments { get; set; } = new List<FPayment>();
    }
}
