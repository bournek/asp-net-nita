﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nita.ViewModels.ContractViewModels
{
    public class ContractModel
    {
        public string ContractName { get; set; }
        public int TrainerId { get; set; }
        public string ContractNo { get; set; }
        public int CycleId { get; set; }
        public bool TerminationStatus { get; set; }
        public string ContractStatus { get; set; }
        //public List<TradeAreaItem> TradeAreasList { get; set; }
    }
}
