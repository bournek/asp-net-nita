﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nita.ViewModels.ContractViewModels
{
    public class TradeAreaSave
    {
       public int ContractId { get; set; }
       public int TradeAreaId { get; set;} 
        public int Capacity { get; set; }
    }
}
