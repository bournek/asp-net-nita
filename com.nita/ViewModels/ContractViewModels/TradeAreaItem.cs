﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nita.ViewModels.ContractViewModels
{
    public class TradeAreaItem
    {
        public int TradeAreaId { get; set; }
        public string TradeAreaName { get; set; }
        public bool Selected { get; set; }
    }
}
