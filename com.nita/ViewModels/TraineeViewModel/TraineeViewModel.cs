﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.nita.IdentityModels;

namespace Nita.ViewModels
{
    public class TraineeViewModel
    {
        public int Id { get; set; }
        public int ApplicationUserId { get; set; }
        public int AddressId { get; set; }
        public int TrainerId { get; set; }

        public int ProgrammeId { get; set; }

        //
        public string AdminNo { get; set; }
        public string FullName { get; set; }
        public string PassportNo { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        public MaritalStatus Marital { get; set; }
        public bool Closed { get; set; }
        public DateTime DateClosed { get; set; }
        public string CloseReason { get; set; }
        public string Disability { get; set; }

        public int TraineeAccountId { get; set; }
        public string Domicile { get; set; }

        public int AccountName { get; set; }
        public string Bank { get; set; }
        public string AccountNumber { get; set; }
        public string BankBranch { get; set; }
        public bool Status { get; set; } = true;

        public int CountyId { get; set; }
        public int StaffId { get; set; }
        public int TraineeId { get; set; }

        //
        public string Nationality { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
        public string BuildingName { get; set; }
        public string NearbyLandmark { get; set; }
        public string City { get; set; }
        public string Street1 { get; set; }
        public string PostalCode { get; set; }
        public string PAddress { get; set; }
        public string House { get; set; }
        public string Pager { get; set; }
        public string HomeTel { get; set; }
        public string HomeFax { get; set; }
        public string Web { get; set; }
        public string WTel { get; set; }
        public string ContactNote { get; set; }
        public string WFax { get; set; }
        public string WEmail { get; set; }


    }

    public class TraineeAddressViewModel
    {
        public int CountyId { get; set; }
        public string District { get; set; }
        public string HomeTel { get; set; }
        public string PAddress { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }

    public class TraineePersonalViewModel
    {
        public int AddressId { get; set; }
        public string AdminNo { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Disability { get; set; }
        public string Domicile { get; set; }
        public string FullName { get; set; }
        public Gender Gender { get; set; }
        public MaritalStatus Marital { get; set; }
        public string PassportNo { get; set; }
        public string Email { get; set; }
        public int CycleId;

    }

    public class TraineeAccountViewDetails
    {
        public int TraineeId { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public int BankBranch { get; set; }
        public int Bank { get; set; }


    }

    public class TraineeAttendanceViewDetails
    {
        public int BeneficiaryId { get; set; }
        public int NumberOfType { get; set; }
        
        public int DaysAttended { get; set; }
        public string Type { get; set; }
    }

    public class TraineeViewModelTradeAreas
    {
        public int TrainerId { get; set; }
        public int TraineeId { get; set; }
        public int TradeAreaId { get; set; }
    }

    public class TraineeViewModelTradeAreasMultiple
    {
        public int TrainerId { get; set; }
        public string TraineeIds { get; set; }
        public int TradeAreaId { get; set; }
    }

}