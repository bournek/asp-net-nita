﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nita.ViewModels.ProviderViewModels
{
    public class Employer
    {
        public int Id { get; set; }
        public string BusinessName { get; set; }
        public string Industry { get; set; }
        public string Notes { get; set; }
        public int CountyId { get; set; }
    }
}
