﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nita.ViewModels.ProviderViewModels
{
    public class McModel
    {
        public string EnterpriseName { get; set; }
        public string PostalAddressBiz { get; set; }
        public int NoSkilledEmployees { get; set; }
        public int CountyId { get; set; }
        public int SubCountyId { get; set; }
        public string PhysicalLocation { get; set; }
        public FullName McFullName { get; set; }
        public string Email { get; set; }
        public string IdNo { get; set; }
        public string Telephone { get; set; }
        public string Position { get; set; }
        public string PostalAddressMc { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public int BankId { get; set; }
        public int BranchId { get; set; }

    }

    public class FullName {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SurName { get; set; }
    }
}
