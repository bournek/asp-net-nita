﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nita.ViewModels.ProviderViewModels
{
    public class BankAcc
    {
        public int BranchId { get; set; }
        public DateTime DateCreated { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
    }
}
