﻿using com.nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.ViewModels
{
    public class ProviderModels
    {
        public string FullName { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonPosition { get; set; }
        public string ContactEmail { get; set; }
        public string Telephone { get; set; }
        public string PostalAddress { get; set; }
        public string StreetRoad { get; set; }
        public string BuildingName { get; set; }
        public int CountyId { get; set; }
        public string LandMark { get; set; }
        public string TrainerType { get; set; }
        public int? Id { get; set; }
        public int? ApplicationUserId { get; set; }
        public int? AddressId { get; set; }
    }
}
