﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.nita.IdentityModels;

namespace Nita.ViewModels
{
    public class AcademicReportStageViewModel
    {
        public int BeneficiaryId { get; set; }
        
        public DateTime ReportStageDate { get; set; }
    }

    public class AcademicTraineeEmployerViewModel
    {
        public int BeneficiaryId;
        public int EmployerId;
        public string ContactPerson;
        public string ContactPersonMobile;
        public string ContactPersonEmail;
    }
}