var NITA = {},JQuery;
NITA.Nav = {},
NITA.FAccounts = [],
    NITA.Departments = [],
    NITA.PayAccounts = [],
    NITA.BaseCurrency = {}, NITA.FAPAccounts = [], NITA.ImprestWarrants = [], NITA.StaffWarrants = [], NITA.Invoices = [], NITA.FLedgers = [];
//
(function (document) {
    typeof (jQuery) !== "undefined" && (JQuery = jQuery),
        NITA.convertDate = function (date) {
            console && console.log('Format Date = ' + date)
            var yyyy = date.getFullYear().toString();
            var mm = (date.getMonth() + 1).toString();
            var dd = date.getDate().toString();

            var mmChars = mm.split('');
            var ddChars = dd.split('');

            return yyyy + '-' + (mmChars[1] ? mm : "0" + mmChars[0]) + '-' + (ddChars[1] ? dd : "0" + ddChars[0]);
        },
    console && console.log('NITA > Type of JQuery() = ' + typeof (JQuery)),
    jQuery.getJSON('/faccs', function (res) {
        console && console.log(res)
        typeof (res.s) !== "undefined" && res.s === "1" && (NITA.FAccounts = JSON.parse(res.data));
    }),
        jQuery.getJSON('/hrdepts', function (res) {
            console && console.log(res)
            typeof (res.s) !== "undefined" && res.s === "1" && (NITA.Departments = JSON.parse(res.data));
        }),
        jQuery.getJSON('/basecur', function (res) {
            console && console.log(res)
            typeof (res.s) !== "undefined" && res.s === "1" && (NITA.BaseCurrency = JSON.parse(res.data));
        }),
        jQuery.getJSON('/paccs', function (res) {
            console && console.log(res)
            typeof (res.s) !== "undefined" && res.s === "1" && (NITA.PayAccounts = JSON.parse(res.data));
        }), jQuery.getJSON("/alltxs", function (res) {
        console && console.log(JSON.stringify(res)), res.s === "1" && (NITA.Taxes = JSON.parse(res.data))
        }),
        jQuery.getJSON("/falllgr", function (res) {
            console && console.log(res),res.s === "1" && (NITA.FLedgers = JSON.parse(res.data))
        }),
    //
        jQuery(document).ready(function () {
        //
        jQuery('body').on('change', '.select-all-chks', function () {
            if (jQuery(this).is(':checked')) {
                jQuery('.select-chks').prop('checked', true)
                jQuery('.select-all-chks').prop('checked', true)
            } else {
                jQuery('.select-chks').prop('checked', false)
                jQuery('.select-all-chks').prop('checked', false)
            }
        }),
            jQuery('body').on('change', '.select-chks', function () {
                var all = true
                jQuery.each(jQuery('.select-chks'), function (s, obj) {
                    if (!jQuery(obj).is(':checked')) {
                        all = false
                    }
                })
                if (all)
                    jQuery('.select-all-chks').prop('checked', true)
                else
                    jQuery('.select-all-chks').prop('checked', false)
            })
            //ar
            jQuery('#add-ar').click(function () {
                //
                var b = '<li style="margin-top:5px" id="" ><div class="row acc-row-cont"><div class="input-group col-sm-8">';
                b += '<select class="select2 form-control custom-select ar-acc" data-placeholder="-- Select An Asset GL Account --">', wrapper = jQuery('#ar-accs'),
                    wrapper.children("li").children('p').length > 0 && (wrapper.empty()),
                    jQuery.each(NITA.FAccounts, function (index, item) {
                        item.typ === "Assets" && (b += '<optgroup label="🏠 ' + item.typ + ' Accounts" style="font-weight:bold"><optgroup label="⤵  ' + item.name + '">',
                            jQuery.each(item.childs, function (i, child) {
                                b += '<option value="' + child.Id + '">    ↪ ' + child.name + '</option>'
                            }), b += '</optgroup></optgroup>')
                    }),
                    b += '</select></div><div class="input-group col-sm-3"><div class="form-check bd-example-indeterminate"><label class="custom-control custom-checkbox">',
                    b += '<input type="checkbox" class="custom-control-input ar-chks" name="actions" value="" data-accid=""><span class="custom-control-indicator"></span><span class="custom-control-description">Default</span>',
                    b += '</label></div></div><button class="bnt btn-sm btn-danger btn-rounded btn-rm-ar" type="button" data-toggle="tooltip" style="padding:2px;margin:2px;height:22px" data-original-title="Remove Entry"><i class="ti-close"></i></button></div></li>',
                    //jQuery('#ar-accs').empty(),
                    wrapper.append(b), $(".select2").select2();
            }),
            jQuery('body').on('change', '.ar-chks', function () {
                jQuery.each(jQuery('.ar-chks'), function (j, k) {
                    jQuery(k).prop('checked', false)
                })
                jQuery(this).prop('checked', true)
            }), jQuery('body').on('click', '.btn-rm-ar', function () {
                jQuery(this).closest("li").remove(),
                    jQuery('#ar-accs').children('li').length == 0 && (jQuery('#ar-accs').append('<li><p class="" id="hld" style="text-align:center;line-height:100px">No Entries....</p></li>'))
            }),
            //ap
            jQuery('#add-ap').click(function () {
                //
                var b = '<li style="margin-top:5px" id="" ><div class="row acc-row-cont"><div class="input-group col-sm-8">';
                b += '<select class="select2 form-control custom-select ap-acc" data-placeholder="-- Select A Liability GL Account --">', wrapper = jQuery('#ap-accs'), wrapper.children("li").children('p').length > 0 && (wrapper.empty()),
                    jQuery.each(NITA.FAccounts, function (index, item) {
                        item.typ === "Liability" && (b += '<optgroup label="🏠 ' + item.typ + ' Accounts" style="font-weight:bold"><optgroup label="⤵  ' + item.name + '">',
                            jQuery.each(item.childs, function (i, child) {
                                b += '<option value="' + child.id + '">    ↪ ' + child.name + '</option>'
                            }), b += '</optgroup></optgroup>')
                    }),
                    b += '</select></div><div class="input-group col-sm-3"><div class="form-check bd-example-indeterminate"><label class="custom-control custom-checkbox">',
                    b += '<input type="checkbox" class="custom-control-input ap-chks" name="actions" value="" data-accid=""><span class="custom-control-indicator"></span><span class="custom-control-description">Default</span>',
                    b += '</label></div></div><button class="bnt btn-sm btn-danger btn-rounded btn-rm-ap" type="button" data-toggle="tooltip" style="padding:2px;margin:2px;height:22px" data-original-title="Remove Entry"><i class="ti-close"></i></button></div></li>',
                    //jQuery('#ar-accs').empty(),
                    wrapper.append(b), $(".select2").select2();
            }),
            jQuery('body').on('change', '.ap-chks', function () {
                jQuery.each(jQuery('.ap-chks'), function (j, k) {
                    jQuery(k).prop('checked', false)
                })
                jQuery(this).prop('checked', true)
            }), jQuery('body').on('click', '.btn-rm-ap', function () {
                jQuery(this).closest("li").remove(),
                    jQuery('#ap-accs').children('li').length == 0 && (jQuery('#ap-accs').append('<li><p class="" id="hld" style="text-align:center;line-height:100px">No Entries....</p></li>'))
            }),
            jQuery('body').on('click', '#save-settings', function () {
                var apaccs = [], araccs = [];
                jQuery('#ap-accs').children("li").children('p').length == 0 && (jQuery.each(jQuery('#ap-accs').children('li'), function (i, o) {
                    var g = jQuery(o).find("select").val();
                    typeof (g) !== "undefined" && (Number(g) > 0 && (
                        apaccs.push({ 'AccId': jQuery(o).find("select").val(), 'Name': '', 'IsDefault': jQuery(o).find('input[type=checkbox]').is(":checked") })))
                })),
                    jQuery('#ar-accs').children("li").children('p').length == 0 && (jQuery.each(jQuery('#ar-accs').children('li'), function (i, o) {
                        var g = jQuery(o).find("select").val();
                        typeof (g) !== "undefined" && (Number(g) > 0 && (
                            araccs.push({ 'AccId': jQuery(o).find("select").val(), 'Name': '', 'IsDefault': jQuery(o).find('input[type=checkbox]').is(":checked") })))
                    })), jQuery('#APCreditors').val(JSON.stringify(apaccs)), jQuery('#ARDebtors').val(JSON.stringify(araccs)), jQuery('#settings-form').submit()
            }),
                typeof (NITA.StaffWarrants) === "undefined" && (NITA.StaffWarrants = []), jQuery('body').on('change', '#Staff', function () {
                sid = jQuery('#Staff').val(), sid && (NITA.StaffWarrants.length > 0 ? (d = {}, jQuery.each(NITA.StaffWarrants, function (index, item) {
                    item.sid === sid && (d = item.winf);
                }), d.length && (loadDis(d))) : jQuery.getJSON("/staffwars?sid=" + this.value, function (res) {
                    console && console.log(JSON.stringify(res))
                    var data = JSON.parse(res.data);
                    res.s === "1" && (loadDis(data.winf), NITA.StaffWarrants.push(data))
                    //
                }))

            }),
            jQuery('body').on('change', '#IWDisbursementId', function () {
                var dis = this.value, sid = jQuery('#Staff').val();
                console && console.log('Load prev for ' + dis + ', Staff = ' + sid + ', cahced warrants = ' + NITA.StaffWarrants.length)
                jQuery.each(NITA.StaffWarrants, function (index, item) {
                    if (item.sid === sid) {
                        var obj;
                        for (var g in item.winf) {
                            if (typeof (item.winf[g].dis) !== 'undefined' && (item.winf[g].dis).did === dis) {
                                obj = item.winf[g];
                            }
                        }
                        //
                        typeof (obj) !== "undefined" ? loadPrev(item, obj) : console && console.log('Dibursement with id ' + dis + ' NOT FOUND IN CACHE')
                    } else {
                        console && console.log('Staff with id ' + sid + ' NOT FOUND IN CACHE')
                    }
                })
            }),
            jQuery('body').on('keyup', '.amt-spent', function () {
                var tt = 0.0;
                jQuery.each(jQuery('.amt-spent'), function (u, i) {
                    var s = jQuery(i).val();
                    tt += Number(s)
                }),
                    jQuery('#amt-spent').val(tt.toFixed(2)), x = jQuery('#amt-dis').val(), jQuery('#amt-rem').val((Number(x.replace(',', '')) - tt).toFixed(2))
            }),
            jQuery('body').on('click', '#surrender', function () {
                var impaccs = [], a = Number(jQuery('#amt-rem').val()), b = Number(jQuery('#amt-dis').val().replace(',', ''));
                (a >= 0) ? (jQuery('#payables-wrapper').children("li").children('p').length == 0 && (jQuery.each(jQuery('#payables-wrapper').children('li'), function (i, o) {
                    var g = jQuery(o).find("input.acc-id").data('id'), h = jQuery(o).find("input.acc-dept").data('id'), samt = jQuery(o).find('input.amt-spent').val();
                    typeof (samt) !== "undefined" && (Number(samt) > 0 && (
                        impaccs.push({ 'AccId': g, 'Dept': h, 'SpentAmount': samt })))
                })), alert('Payables = ' + JSON.stringify(impaccs)), jQuery('#Payables').val(JSON.stringify(impaccs)), jQuery('#surrender-form').submit()) : (alert('Amount Spent cannot be more than amount disbursed.'))
            })
            loadDis = function (data) {
            jQuery('#IWDisbursementId').empty(), jQuery("#IWDisbursementId").append('<option value="0"> -- Select Disbursement -- </option>'),
                jQuery.each(data, function (index, item) {
                //filter where surrender == 0
                item.dis && item.dis.did && !item.dis.dsid && (jQuery('#IWDisbursementId').append(jQuery('<option>', { value: item.dis.did, text: "IWD" + ("0000" + item.war).slice(-4) })));
                })

            },
            //
                loadPrev = function (data, winf) {
                console && console.log('Update prev with ' + JSON.stringify(data) + ', Dis - ' + JSON.stringify(winf.dis))
                jQuery('#staff-name').val(data.sname), jQuery('#staff-no').val(data.sno), jQuery('#lgr').val(winf.lgr),
                    jQuery('#proj').val(winf.proj), jQuery('#desc').val(winf.desc), jQuery('#amt-dis').val(winf.damt),
                    jQuery('#imp-date').val(winf.dis.ddate), jQuery('#sur-date').val(winf.dis.surd), jQuery('.cur').text(winf.cur)
                jQuery('#payables-wrapper').empty(), winf.accs.length > 0 ? (jQuery.each(winf.accs, function (index, item) {
                    var w = '<li style="margin-top:5px" id=""><div class="row acc-row-cont"><div class="input-group col-sm-3"><div class="input-group-addon"><i class="ti-key"></i></div>';
                    w += '<input class="form-control acc-dept" value="' + item.dname + '" data-id="' + item.did + '" placeholder="Department..." style="height:38px" readonly /></div><div class="input-group col-sm-4"><div class="input-group-addon"><i class="ti-key"></i></div>';
                    w += '<input class="form-control acc-id" value="' + item.aname + '" data-id="' + item.aid + '" placeholder="GL Account Name..." style="height:38px" readonly /></div><div class="input-group col-sm-3"><div class="input-group-addon"><i class="cur">' + winf.cur + '</i></div>';
                    w += '<input class="form-control" value="' + item.amt + '" placeholder="Amount Allocated..." style="height:38px" readonly /></div><div class="input-group col-sm-2">';
                    w += '<input class="form-control amt-spent" value="" type="number" placeholder="Amount Spent...(' + winf.cur + ')" style="height:38px" /></div></div></li>',
                        jQuery('#payables-wrapper').append(w)
                })) : (jQuery('#payables-wrapper').append('<li style=""><p class="" id="hld" style="text-align:center;line-height:100px">No Entries...</p></li>')),
                    jQuery('#ModeId').empty(), jQuery("#ModeId").append('<option value="0"> -- Select Payment Mode -- </option>'), winf.pmodes.length > 0 &&  jQuery.each(winf.pmodes, function (index, item) {
                    jQuery('#ModeId').append(jQuery('<option>', { value: item.pid, text: item.pname }));
                    })
            }
    })
    
})(document);

//
//String.prototype.endsWith = function (s) {
//    return this.length >= s.length && this.substr(this.length - s.length) == s;
//}

// Performs tests to see if a stylesheet was loaded successfully, if the stylesheet failed to load, appends a new
// link tag pointing to the local copy of the stylesheet before performing the next check.
(function (document) {
    "use strict";

    var fallbacks = [
        {
            // metaName - The name of the meta tag that the test is performed on. The meta tag must have a class from the
            //            relevant stylesheet on it so it is styled and a test can be performed against it. E.g. for
            //            font awesome the <meta name="x-font-awesome-stylesheet-fallback-test" class="fa"> meta tag is
            //            added. The 'fa' class causes the font awesome style to be applied to it.
            metaName: "x-font-awesome-stylesheet-fallback-test",
            // test - The test to perform against the meta tag. Checks to see if the Font awesome styles loaded
            //        successfully by checking that the font-family of the meta tag is 'FontAwesome'.
            test: function (meta) { return meta.style.fontFamily === "FontAwesome"; },
            // href - The URL to the fallback stylesheet.
            href: "/css/font-awesome.css"
        }
    ];

    var metas = document.getElementsByTagName("meta");

    for (var i = 0; i < fallbacks.length; ++i) {
        var fallback = fallbacks[i];

        for (var j = 0; j < metas.length; ++j) {
            var meta = metas[j];
            if (meta.getAttribute("name") === fallback.metaName) {
                if (!fallback.test(meta)) {
                    var link = document.createElement("link");
                    link.href = fallback.href;
                    link.rel = "stylesheet";
                    document.getElementsByTagName("head")[0].appendChild(link);
                }
                break;
            }
        }

    }

})(document);
// Performs tests to see if a script was loaded successfully, if the script failed to load, appends a new script tag
// pointing to the local copy of the script and then waits for it to load before performing the next check.
// Example: Bootstrap is dependant on jQuery. If loading jQuery from the CDN fails, this script loads the jQuery
//          fallback and waits for it to finish loading before attempting the next fallback test.
(function (document) {
    "use strict";

    var fallbacks = [
        // test - Tests whether the script loaded successfully or not. Returns true if the script loaded successfully or
        //        false if the script failed to load and the fallback is required.
        // src - The URL to the fallback script.
        { test: function () { return window.Modernizr; }, src: "//lib/js/modernizr.js" },
        { test: function () { return window.jQuery; }, src: "//lib/js/jquery.js" },
        { test: function () { return window.jQuery.validator; }, src: "//lib/js/jquery-validate.js" },
        { test: function () { return window.jQuery.validator.unobtrusive; }, src: "//lib/js/jquery-validate-unobtrusive.js" },
        { test: function () { return window.jQuery.fn.modal; }, src: "//lib/js/bootstrap.js" }
    ];

    var check = function (fallbacks, i) {
        if (i < fallbacks.length) {
            var fallback = fallbacks[i];
            if (fallback.test()) {
                check(fallbacks, i + 1);
            }
            else {
                var script = document.createElement("script");
                script.onload = function () {
                    check(fallbacks, i + 1);
                };
                script.src = fallback.src;
                document.getElementsByTagName("body")[0].appendChild(script);
            }
        }
    };
    check(fallbacks, 0);

})(document);
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Calculator = (function () {
    function Calculator() {
    }
    Calculator.prototype.add = function (a, b) {
        return a + b;
    };
    Calculator.prototype.subtract = function (a, b) {
        return a - b;
    };
    Calculator.prototype.multiply = function (a, b) {
        return a * b;
    };
    Calculator.prototype.divide = function (a, b) {
        this.checkDivideByZero(b);
        return a / b;
    };
    Calculator.prototype.checkDivideByZero = function (x) {
        if ((x === 0) && this.throwOnDivideByZero) {
            throw new Error("Divide by Zero.");
        }
    };
    return Calculator;
}());
exports.Calculator = Calculator;

//# sourceMappingURL=site.js.map
