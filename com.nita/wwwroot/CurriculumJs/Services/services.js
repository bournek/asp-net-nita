﻿var SmsServices = angular.module('App');

SmsServices.service('Services', function () {
    this.SmsServicesObject = {};
    this.SmsServicesObject.loaderStatus = {};
    var itemsPerPage = 10;
    var alphaNumHyDashRegExp = /^[a-zA-Z0-9\-\/]+$/;
    var numericRegex = /^[0-9]*$/;
    var emailRegex = '';
    var alphaNumHyDashSpaceRegExp = /^[a-zA-Z0-9](?:[_\-. /]?[a-zA-Z0-9]+)*$/; ///^[a-zA-Z0-9. _\-\/]+$/;
    this.SmsServicesObject.loaderStatus = {

        fbMessage: '',
        fbStatus: false,
        fbbgColor: 'alert-success'
    }


    this.SmsServicesObject.returnServerSyntaxErrorAlert = function (message) {

        iziToast.error({
            title: 'Server Error',
            message: angular.isDefined(message) ? message : 'Server error occurred,please try again.',
            position: 'topCenter'
        });
    }
    this.SmsServicesObject.returnNotFoundServerErrorAlert = function (message) {
        iziToast.error({
            title: 'Server Error',
            message: angular.isDefined(message) ? message : 'Server error occurred,please try again.',
            position: 'topRight'
        });
    }
    this.SmsServicesObject.wrongUsernameAlert = function (message) {

        iziToast.error({
            title: 'Error',
            message: angular.isDefined(message) ? message : ' Wrong username or password/Inactive account,please try again.',
            position: 'topRight',
            timeout: 60000
        });
    }
    this.SmsServicesObject.unErrorAlert = function (message) {
        iziToast.error({
            title: 'Server Error',
            message: angular.isDefined(message) ? message : 'Server error occurred,please try again.',
            position: 'topCenter'
        });
    }
    this.SmsServicesObject.returnProgressAlert = function () {
        //        var jsAlert = $.alert({title: '<i class="fa fa-2x fa-spin fa-spinner fa-2x"></i> PROGRESS',
        //            content: "action in progress, please wait ...</div>"}
        //        );
        //        return jsAlert;
        iziToast.destroy();
        iziToast.info({
            title: '<i class="fa fa-2x fa-spin fa-spinner fa-2x"></i>',
            message: 'action in progress, please wait ...',
            position: 'topRight',
            timeout: 15000
        });

    }

    this.SmsServicesObject.returnNotificationAlert = function (message) {
        //        var jsAlert = $.alert({title: '<i class="fa fa-2x fa-spin fa-spinner fa-2x"></i> PROGRESS',
        //            content: "action in progress, please wait ...</div>"}
        //        );
        //        return jsAlert;
        iziToast.destroy();
        iziToast.info({
            title: '<i class="fa fa-2x fa-smile-o"></i>',
            message: angular.isDefined(message) ? message : '1 new message received',
            position: 'topRight'
        });


    }

    this.SmsServicesObject.returnNoRecordsFoundAlert = function (message) {
        //        var jsAlert = $.alert({title: '<i class="fa fa-2x fa-exclamation-triangle text-warning"></i> NO DATA',
        //            content: angular.isDefined(message) ? message : 'No Records found.'});
        //        return jsAlert;
        iziToast.destroy();
        iziToast.warning({
            title: 'OK',
            message: angular.isDefined(message) ? message : 'No Records found.',
            position: 'topCenter',
            timeout: 30000
        });
    }
    this.SmsServicesObject.returnActionSuccessAlert = function (message) {
        //        var jsAlert = $.alert({title: '<i class="fa fa-2x fa-check-circle text-success"></i> Success',
        //            content: angular.isDefined(message) ? message : 'Action was successful.'});
        //        return jsAlert;
        iziToast.destroy();
        iziToast.success({
            title: 'OK',
            message: angular.isDefined(message) ? message : 'Action was successful.',
            position: 'topCenter',
            timeout: 25000
        });


    }
    this.SmsServicesObject.returnMissingFieldsAlert = function (message) {
        //        var jsAlert = $.alert({title: '<i class="fa fa-2x fa-check-circle text-danger"></i> Error',
        //            content: 'Please fill all required fields.'});
        //        return jsAlert;


        iziToast.error({
            title: 'Error',
            message: angular.isDefined(message) ? message : 'Please fill all required fields.',
            position: 'topCenter'
        });

    }
    this.SmsServicesObject.returnrecordSavedSuccessAlert = function (message) {
        //        var jsAlert = $.alert({title: '<i class="fa fa-2x fa-check-circle text-success"> Success</i>',
        //            content: angular.isDefined(message) ? message : 'Record saved succesfully.'});
        //        return jsAlert;


        iziToast.success({
            title: 'OK',
            message: angular.isDefined(message) ? message : 'Record saved succesfully.',
            position: 'topCenter'
        });

    }

    this.SmsServicesObject.returnrCodeCouldNotExecuteAlert = function (message) {
        //        var jsAlert = $.alert({title: '<i class="fa fa-2x fa-exclamation-triangle text-danger"> Success</i>',
        //            content: angular.isDefined(message) ? message : 'Code couldn\'t Execute.'});
        //        return jsAlert;
        //        
        iziToast.error({
            title: 'ERROR',
            message: angular.isDefined(message) ? message : 'Code couldn\'t Execute.',
            position: 'topCenter'
        });

    }
    var notificationPermission = '';

    function authorizeNotification() {
        if (notificationPermission !== 'granted') {
            Notification.requestPermission(function (perm) {
                notificationPermission = perm;
            });
        }
    }

    authorizeNotification();
    this.SmsServicesObject.showDesktopNotification = function (message) {
        if (notificationPermission !== 'granted') {
            authorizeNotification();
        }
        var isActive = false;

        window.onfocus = function () {
            isActive = true;
        };

        window.onblur = function () {
            isActive = false;
        };
        //        alert(message)
        if (!isActive) {
            var notification = new Notification("New 360 Notification", {
                dir: "auto",
                lang: "",
                body: angular.isDefined(message) ? message : "One new notification received",
                tag: '',
            });
            setTimeout(function () {
                notification.close();
            }, 10000);

        }
    }


    this.SmsServicesObject.returnNameSymbols = function (name) {
        var str = name;
        var matches = str.match(/\b(\w)/g);              // ['J','S','O','N']
        var acronym = matches.join('');

        return acronym.toUpperCase();
    }
    this.SmsServicesObject.showErrorType = function showErrorType(res) {

        if (res.status == 2) {
            this.returnServerSyntaxErrorAlert(res.message);

        } else if (res.status == 0) {
            this.returnNoRecordsFoundAlert(res.message);

        } else {
            this.returnServerSyntaxErrorAlert();
        }
    }


    this.SmsServicesObject.returnTaskColor = function (status) {
        switch (status) {
            case '0':
                return 'label-danger';
                break;
            case '1':
                return 'label-warning';
                break;
            case '2':
                return 'label-success';
                break;
            case '3':
                return 'label-default';
                break;
            case '4':
                return 'label-info';
                break;


            default:
                return 'label-danger';
                break;
        }


    }
    this.SmsServicesObject.returnTaskStatus = function (status) {
        switch (status) {
            case '0':
                return 'open';
                break;
            case '1':
                return 'assigned';
                break;
            case '2':
                return 'closed';
                break;
            case '3':
                return 'suspended';
                break;
            case '4':
                return 'unknown';
                break;


            default:
                return 'label-danger';
                break;
        }


    }


    this.SmsServicesObject.getRandomColor = function () {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    };


    this.SmsServicesObject.getUrlSection = function getAllUrlParams(url) {

        // get query string from url (optional) or window
        var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

        // we'll store the parameters here
        var obj = {};

        // if query string exists
        if (queryString) {

            // stuff after # is not part of query string, so get rid of it
            queryString = queryString.split('#')[0];

            // split our query string into its component parts
            var arr = queryString.split('&');

            for (var i = 0; i < arr.length; i++) {
                // separate the keys and the values
                var a = arr[i].split('=');

                // in case params look like: list[]=thing1&list[]=thing2
                var paramNum = undefined;
                var paramName = a[0].replace(/\[\d*\]/, function (v) {
                    paramNum = v.slice(1, -1);
                    return '';
                });

                // set parameter value (use 'true' if empty)
                var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

                // (optional) keep case consistent
                paramName = paramName.toLowerCase();
                paramValue = paramValue.toLowerCase();

                // if parameter name already exists
                if (obj[paramName]) {
                    // convert value to array (if still string)
                    if (typeof obj[paramName] === 'string') {
                        obj[paramName] = [obj[paramName]];
                    }
                    // if no array index number specified...
                    if (typeof paramNum === 'undefined') {
                        // put the value on the end of the array
                        obj[paramName].push(paramValue);
                    }
                    // if array index number specified...
                    else {
                        // put the value at that index number
                        obj[paramName][paramNum] = paramValue;
                    }
                }
                // if param name doesn't exist yet, set it
                else {
                    obj[paramName] = paramValue;
                }
            }
        }

        return obj;
    }


    var location = {};

    function showPosition(position) {
        var loc = {};
        loc.latitude = position.coords.latitude;
        loc.longitude = position.coords.longitude;
        location = loc;

    }

    this.SmsServicesObject.returnCurrentLocationObject = function () {
        return location;
    }
    this.SmsServicesObject.returnCurrentLocation = function () {
        if (navigator.geolocation) {
            return navigator.geolocation.getCurrentPosition(showPosition);
            //            return location;
        } else {
            return "Geolocation is not supported by this browser.";
        }
    }


})
SmsServices.factory('HttpRequest', function ($http, $q, Upload) {
    var HttpHolder = {};

    var url = SERVER_URL.Main + 'testHttpService';
    HttpHolder.MakeGetDataHttp = function (filterData) {
        var url = filterData.url;
        delete filterData.url;

        var request = $http(
            {
                method: 'post',
                url: url,
                headers: { 'Content-Type': 'application/json' },
                data: {
                    filterData: filterData
                }
            });
        return (request.then(handleSuccess, handleError));
    }


    HttpHolder.MakePostHttp = function (saveData, callback) {
        var url = saveData.url;
        delete saveData.url;

        var request = $http(
            {
                method: 'post',
                url: url,
                headers: { 'Content-Type': 'application/json' },
                data: {
                    saveData: saveData
                }
            });
        return (request.then(handleSuccess, handleError));
    }


    HttpHolder.MakeGetHttpTest = function (callback) {
        var request = $http({
            method: "get",
            url: url,
            params: {
                action: "get"
            }
        });
        return (request.then(handleSuccess, handleError));
    }


    function handleError(response) {

        // The API response from the server should be returned in a
        // nomralized format. However, if the request was not handled by the
        // server (or what not handles properly - ex. server error), then we
        // may have to normalize it on our end, as best we can.
        var jsAlert = $.alert({
            title: '<i class="fa fa-2x  fa-exclamation-triangle text-danger"></i> SERVER ERROR',
            content: 'Server  couldn\'t be reached or an error occured,please try again.'
        });
        if (
            !angular.isObject(response.data) ||
            !response.data.message
        ) {
            return ($q.reject("An unknown error occurred."));
        }
        // Otherwise, use expected error message.
        return ($q.reject(response.data.message));
    }

    // I transform the successful response, unwrapping the application data
    // from the API response payload.
    function handleSuccess(response) {

        return (response.data);

    }


    return HttpHolder;
});