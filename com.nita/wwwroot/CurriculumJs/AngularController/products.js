﻿//(function() {
//    'use strict';

var appModule = angular.module('App',
    [
        // Angular modules 
        'ngRoute'

        // Custom modules 

        // 3rd Party Modules
    ]);

appModule.controller('ProductsController',
    function($scope, $http, Services) {
        $scope.products = {};
        $scope.products.currentPage = 'products';
        $scope.products.searchCustomer = "";
        $scope.products.searchProduct = "";
        $scope.products.productsList = {};
        $scope.products.returnSearchString = function(searchString) {
            if (angular.isUndefined(searchString)) {
                return '';
            } else {
                if (searchString == "") {
                    return "";
                } else {
                    return "?searchString=" + searchString;
                }
            }
        }
        $scope.products.getAllProducts = function(searchString) {

            try {

                if (true) {

                    var url = "/receviables/GetProducts" + $scope.products.returnSearchString(searchString);
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            $scope.products.productsList = response.data.data;

                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }
        $scope.products.CustomerTypesList = {};
        $scope.products.getCustomerTypes = function(searchString) {
            try {

                if (true) {

                    var url = "/receviables/GetAllCustomerCategories" +
                        $scope.products.returnSearchString(searchString);
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            $scope.products.CustomerTypesList = response.data.data;

                        },
                        function errorCallback(error) {
                            $scope.products.HTTPStatus = false;
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }

        $scope.products.fProductCategories = {};
        $scope.products.getFProductCategories = function(searchString) {

            try {

                if (true) {

                    var url = "/receviables/getFProductCategories" +
                        (angular.isDefined(searchString) ? "?searchString=" + searchString : '');
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            $scope.products.fProductCategories = response.data.data;

                        },
                        function errorCallback(error) {
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                            $scope.products.HTTPStatus = false;
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }
        $scope.products.ProductFormData = {}


        $scope.products.saveProduct = function(productData) {

            try {
                console.log(productData);

                if (true) {

                    var url = '/Receviables/AddProduct';
                    if (angular.isDefined(productData.Id)) {
                        url = '/Receviables/UpdateProduct';
                    }

                    $scope.products.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(productData)
                    }).then(function successCallback(response) {
                            console.log(response);

                            $scope.products.HTTPStatus = false;

                            if (response.data.status == 1) {
                                $scope.products.ProductFormData = {};
                                $scope.products.getAllProducts($scope.products.searchProduct);
                                $("#addProduct").modal('hide');
                                Services.SmsServicesObject.returnrecordSavedSuccessAlert(response.data.message);
                            } else {
                                Services.SmsServicesObject.unErrorAlert(response.data.message);
                            }


                        },
                        function errorCallback(error) {
                            alert(error);
                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        }

        $scope.products.currentInvoiceCustomer = {};
        $scope.products.pickCustomerForInvoice = function(customer) {
            $scope.products.currentInvoiceCustomer = customer;
        }
        $scope.products.invoiceProducts = [];
        $scope.products.invoiceTerms = {};
        $scope.products.saveInvoiceProduct = function(product) {
            console.log(product);

            var productAdd = {
                id: product.id,
                productName: product.productName,
                productCode: product.productCode,
                sellPrice: product.sellPrice,
                quantity: product.quantity,
                discount: product.discount,
                fTaxesId: product.fTaxesId,
                VAT: product.fTaxes

            }
            var addedProducts = $scope.products.invoiceProducts;
            var exists = false;
            for (var i = 0; i < addedProducts.length; i++) {
                if (addedProducts[i].id == product.id) {
                    addedProducts[i] = {
                        id: product.id,
                        productName: product.productName,
                        productCode: product.productCode,
                        sellPrice: product.sellPrice,
                        quantity: product.quantity,
                        discount: product.discount,
                        fTaxesId: product.fTaxesId,
                        VAT: product.fTaxes

                    }
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                addedProducts.push(productAdd);
            }
            $scope.products.invoiceProducts = addedProducts;
            console.log($scope.products.invoiceProducts);
            $scope.products.returnSum();
        }
        $scope.products.removeItems = function(id) {

            var products = $scope.products.invoiceProducts;
            for (var i = 0; i < products.length; i++) {
                if (products[i].id == id) {

                    products.splice(i, 1);
                    break;
                }
            }
            $scope.products.invoiceProducts = products;
            $scope.products.returnSum();
        }
        $scope.products.invoiceTotal = {
            totalAmount: 0,
            totalTaxes: 0,
            totalDiscount: 0

        }

<<<<<<< HEAD
        $scope.products.returnSumCurrentSum = function (products) {

          //  var products = $scope.products.invoiceProducts;
=======
        $scope.products.returnSumCurrentSum = function(products) {

            //  var products = $scope.products.invoiceProducts;
>>>>>>> bb29540261fcc55104fe6c2e3fa6114c425a9380
            var sm = {
                totalAmount: 0,
                totalTaxes: 0,
                totalDiscount: 0

            };
            if (products.length > 0) {
                for (var i = 0; i < products.length; i++) {

                    sm.totalAmount += (products[i].productSellPrice * products[i].quantity);

<<<<<<< HEAD
                    sm.totalTaxes += (products[i].productSellPrice * products[i].quantity * products[i].financeTax.rate) / 100;
=======
                    sm.totalTaxes += (products[i].productSellPrice *
                            products[i].quantity *
                            products[i].financeTax.rate) /
                        100;
>>>>>>> bb29540261fcc55104fe6c2e3fa6114c425a9380
                    sm.totalDiscount += products[i].discount;
                }
            }
            $scope.products.invoiceTotal = sm;
        }


        $scope.products.returnSum = function() {

            var products = $scope.products.invoiceProducts;
            var sm = {
                totalAmount: 0,
                totalTaxes: 0,
                totalDiscount: 0

            };
            if (products.length > 0) {
                for (var i = 0; i < products.length; i++) {

                    sm.totalAmount += (products[i].sellPrice * products[i].quantity);

                    sm.totalTaxes += (products[i].sellPrice * products[i].quantity * products[i].VAT.rate) / 100;
                    sm.totalDiscount += products[i].discount;
                }
            }
            $scope.products.invoiceTotal = sm;
        }

        $scope.products.returnCompressedObject = function(addedProducts) {
            for (var i = 0; i < addedProducts.length; i++) {

                addedProducts[i] = {
                    ProductId: addedProducts[i].id,

                    SellPrice: addedProducts[i].sellPrice,
                    Quantity: addedProducts[i].quantity,
                    Discount: addedProducts[i].discount,
                    FTaxesId: addedProducts[i].fTaxesId
                }
            }
            return addedProducts;
        }
        $scope.products.InvoiceFormData = {}
        $scope.products.saveInvoiceProducts = function(invoiceData) {

            try {

                invoiceData.InvoiceProducts = $scope.products.returnCompressedObject($scope.products.invoiceProducts);
                invoiceData.CustomerId = $scope.products.currentInvoiceCustomer.id;
                console.log(invoiceData);
                if (true) {

                    var url = '/Receviables/AddCustomerInvoice';
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(invoiceData)
                    }).then(function successCallback(response) {
                            console.log(response);

                            $scope.products.HTTPStatus = false;

                            if (response.data.status == 1) {
                                $scope.products.invoiceProducts = [];
                                console.log($scope.products.invoiceProducts);
                                $scope.products.InvoiceFormData = {};
                                $scope.products.currentPage = 'invoices';
                                Services.SmsServicesObject.returnrecordSavedSuccessAlert(response.data.message);
                            } else {
                                Services.SmsServicesObject.unErrorAlert(response.data.message);
                            }


                        },
                        function errorCallback(error) {
                            console.log(error);
                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        }

        $scope.products.currentInvoice = {}
        $scope.products.currentInvoiceInfo = function(invoice) {

            $scope.products.currentInvoice = invoice;
            $scope.products.currentInvoice.cI.customerInvoiceProducts = {};
            try {

                if (true) {

                    var url = "/receviables/getInvoiceProducts?id=" + invoice.cI.id;
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            if (response.data.status == 0) {
                                Services.SmsServicesObject.returnNoRecordsFoundAlert();
                            }
                            $scope.products.currentInvoice.cI.customerInvoiceProducts = response.data.data;
<<<<<<< HEAD
                            $scope.products.returnSumCurrentSum($scope.products.currentInvoice.cI.customerInvoiceProducts)
=======
                            $scope.products.returnSumCurrentSum($scope.products.currentInvoice.cI
                                .customerInvoiceProducts)
>>>>>>> bb29540261fcc55104fe6c2e3fa6114c425a9380
                        },
                        function errorCallback(error) {
                            $scope.products.HTTPStatus = false;
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }

        $scope.products.CustomersList = {};


        $scope.products.removeProductFromSavedInvoice = function(product) {
            var inProducts = $scope.products.currentInvoice.cI.customerInvoiceProducts;
            for (var i = 0; i < inProducts.length; i++) {
                if (product.id == inProducts[i].id) {
                    inProducts.splice(i, 1);
                }
            }
            $scope.products.currentInvoice.cI.customerInvoiceProducts = inProducts;
            try {

                if (true) {

                    var url = "/receviables/RemoveProductFromInvoice?id=" + product.id;
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            if (response.data.status == 1) {
                                Services.SmsServicesObject.returnrecordSavedSuccessAlert();
                            } else {
                                Services.SmsServicesObject.returnrCodeCouldNotExecuteAlert(response.data.message);
                            }


                        },
                        function errorCallback(error) {
                            $scope.products.HTTPStatus = false;
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                        });
                }
            } catch (ex) {
                console.log(ex);
            }

        }

        $scope.products.getAllCustomers = function(searchString) {

            try {

                if (true) {

                    var url = "/receviables/GetCustomers" + $scope.products.returnSearchString(searchString);
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            $scope.products.CustomersList = response.data.data;

                        },
                        function errorCallback(error) {
                            $scope.products.HTTPStatus = false;
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }


        $scope.products.customerInvoicesList = {};


        $scope.products.getAllCustomerInvoices = function(searchString) {

            try {

                if (true) {

                    var url = "/receviables/GetAllCustomerInvoices" + $scope.products.returnSearchString(searchString);
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            $scope.products.customerInvoicesList = response.data.data;

                            if (response.data.status == 0) {
                                Services.SmsServicesObject.returnNoRecordsFoundAlert();
                            }

                        },
                        function errorCallback(error) {
                            $scope.products.HTTPStatus = false;
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }


        $scope.products.taxes = {};
        $scope.products.getAllTaxes = function() {
            try {

                if (true) {

                    var url = "/receviables/GetTaxes";
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            $scope.products.taxes = response.data.data;

                        },
                        function errorCallback(error) {
                            $scope.products.HTTPStatus = false;
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }

        $scope.products.currencies = {};
        $scope.products.getAllCurrencies = function() {
            try {

                if (true) {

                    var url = "/receviables/GetCurrencies";
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            $scope.products.currencies = response.data.data;

                        },
                        function errorCallback(error) {
                            $scope.products.HTTPStatus = false;
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }

<<<<<<< HEAD
        $scope.products.getAllInvoicePayments= function(searchString) {


=======
        $scope.products.getAllInvoicePayments = function(searchString) {

            try {

                if (true) {

                    var url = "/receviables/GetCustomerInvoices?CustomerCode=";
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            $scope.products.currencies = response.data.data;

                        },
                        function errorCallback(error) {
                            $scope.products.HTTPStatus = false;
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                        });
                }
            } catch (ex) {
                console.log(ex);
            }

        }
        $scope.products.currentReceiptCustomer = {};
        $scope.products.currentCustomerInvoices = {};
        $scope.products.currentCustomerCode = "";
        $scope.products.pickCustomerForReceipt = function(customer) {

            $scope.products.currentReceiptCustomer = customer;
            console.log(customer);
            $scope.products.currentCustomerCode = customer.customerCode;
            $scope.products.getCustomerInvoices(customer.id);
            $("#receiptCustomers").modal('hide');
        }
        $scope.products.showCustomerModal = function() {
            $("#receiptCustomers").modal('show');
        }
        $scope.products.getCustomerInvoices = function(customerCode) {
            try {

                if (true) {
                    $scope.products.currentCustomerInvoices = {};
                    var url = "/receviables/GetCustomerInvoices?customerId=" + customerCode;
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            $scope.products.currentCustomerInvoices = response.data.data;

                        },
                        function errorCallback(error) {
                            $scope.products.HTTPStatus = false;
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
>>>>>>> bb29540261fcc55104fe6c2e3fa6114c425a9380

        }

        $scope.products.invoiceTerms = {}
        $scope.products.getInvoiceTerms = function() {
            try {

                if (true) {

                    var url = "/receviables/GetInvoiceTerms";
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            $scope.products.invoiceTerms = response.data.data;

                        },
                        function errorCallback(error) {
                            $scope.products.HTTPStatus = false;
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }

        $scope.products.glAccounts = {};
        $scope.products.getGlAccounts = function() {


            try {

                if (true) {

                    var url = "/receviables/getGlAccounts";
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            $scope.products.glAccounts = response.data.data;

                        },
                        function errorCallback(error) {
                            $scope.products.HTTPStatus = false;
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                        });
                }
            } catch (ex) {
                console.log(ex);
            }

        }
        $scope.products.glAccountPayAccounts = {};
        $scope.products.getPaymentAccounts = function(glAccountId) {
            try {

                if (true) {

                    var url = "/receviables/getGlAccountsPayAccount?legderId=" + glAccountId;
                    console.log(url);
                    $scope.products.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.products.HTTPStatus = false;
                            $scope.products.glAccountPayAccounts = response.data.data;

                        },
                        function errorCallback(error) {
                            $scope.products.HTTPStatus = false;
                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }

        $scope.products.getAllTaxes();
        $scope.products.CustomerFormData = {}

        $scope.products.saveCustomer = function(customerData) {

            try {
                customerData.CustomerBalance = 0.00;
                console.log(customerData);

                if (true) {

                    var url = '/Receviables/AddCustomer';
                    if (angular.isDefined(customerData.Id)) {
                        url = '/Receviables/UpdateCustomer';
                    }

                    $scope.products.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(customerData)
                    }).then(function successCallback(response) {
                            console.log(response);

                            $scope.products.HTTPStatus = false;

                            if (response.data.status == 1) {
                                $scope.products.CustomerFormData = {};
                                $scope.products.getAllCustomers($scope.products.searchCustomer);
                                $("#addCustomer").modal('hide');
                                Services.SmsServicesObject.returnrecordSavedSuccessAlert(response.data.message);
                            } else {
                                Services.SmsServicesObject.unErrorAlert(response.data.message);
                            }


                        },
                        function errorCallback(error) {
                            alert(error);
                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        }

<<<<<<< HEAD
        $scope.products.printHtmlArea = function (htmlId) {


            var innerContents = document.getElementById(htmlId).innerHTML;
            var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWinindow.document.open();
            popupWinindow.document.write('' +
                '<html><head><link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />' +
                '</head><body onload="window.print()">' + innerContents + '</html>');
            popupWinindow.document.close();
//            jQuery(document).ready(function () {
                //jQuery("#print").click(function () {
=======
        $scope.products.printHtmlArea = function(htmlId) {


            var innerContents = document.getElementById(htmlId).innerHTML;
            var popupWinindow = window.open('',
                '_blank',
                'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWinindow.document.open();
            popupWinindow.document.write('' +
                '<html><head><link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />' +
                '</head><body onload="window.print()">' +
                innerContents +
                '</html>');
            popupWinindow.document.close();
//            jQuery(document).ready(function () {
            //jQuery("#print").click(function () {
>>>>>>> bb29540261fcc55104fe6c2e3fa6114c425a9380
//                    var mode = 'iframe'; //popup
//                    var close = mode == "popup";
//                    var options = {
//                        mode: mode,
//                        popClose: close,
//                        popHt: 500,   // popup window height
//                        popWd: 400,  // popup window width
//                    };
//                    jQuery("#"+htmlId).printArea(options);
//                });
<<<<<<< HEAD
           // });
=======
            // });
>>>>>>> bb29540261fcc55104fe6c2e3fa6114c425a9380

        }

        $scope.products.fillProductEditInfo = function(product) {

            $scope.products.ProductFormData = {
                SellPrice: product.sellPrice,
                CostPrice: product.costPrice,
                ProductName: product.productName,
                Code: product.code,
                FTaxesId: product.fTaxesId,
                FProductCategoryId: product.fProductCategoryId,
                Id: product.id,
                MarkUp: product.markUp
            }
            console.log($scope.products.ProductFormData);
            $("#addProduct").modal('show');
        }
        $scope.products.deleteProduct = function(product) {

            $.confirm({
                title: 'Confirm!',
                theme: 'light',
                content: 'Are you sure you want to delete this product!',
                buttons: {
                    'confirm': {
                        btnClass: 'btn-blue',
                        action: function() {

                            product.deleted = 1;


                            try {

                                if (angular.isDefined(product.id)) {

                                    var url = '/Receviables/deleteProduct/?id=' + product.id;
                                    $scope.products.HTTPStatus = true;
                                    $http({
                                        method: "GET",
                                        contentType: "application/json; charset=utf-8",
                                        url: url
                                    }).then(function successCallback(response) {
                                            console.log(response)


                                            if (response.data.status == 1) {
                                                $scope.products.getAllProducts();
                                                Services.SmsServicesObject.returnrecordSavedSuccessAlert(response
                                                    .data
                                                    .message);
                                            } else {
                                                Services.SmsServicesObject.unErrorAlert(response.data.message);
                                            }


                                        },
                                        function errorCallback(error) {
                                            Services.SmsServicesObject.returnServerSyntaxErrorAlert();
                                        });
                                }
                            } catch (ex) {
                                console.log(ex);
                            }


                        }
                    },
                    cancel:
                    {
                        btnClass: 'btn-danger',
                        action: function() {

                        }
                    }
                }
            });


        }


    }
);