﻿//(function() {
//    'use strict';

var appModule = angular.module('TraineesApp',
    [
        // Angular modules 
        'ngRoute'

        // Custom modules 

        // 3rd Party Modules
    ]);

//appModule.config(function ($routeProvider) {
//    $routeProvider
//        .when('/',
//            {
//                templateUrl: 'app/SharedFiles/login.html'
//            }
//    );
//    $routeProvider
//        .when('/courses',
//            {
//                templateUrl: 'app/SharedFiles/login.html'
//            }
//        );
//
//});
appModule.controller('TraineesController',
    function($scope, $http) {


        /*=======================================================================================================*/
        /***************************Declare variables here***********************/
        /*=======================================================================================================*/
        $scope.trainee = {};
        $scope.trainee.testData = 'angular loader';
        $scope.trainee.curriculumList = {};
        $scope.trainee.currentPage = 'currentStudents';
        $scope.trainee.CategoryFormData = {};

        /*-----------------------------------------END----------------------------------------------------------*/


        /*=======================================================================================================*/
        /***************************method to alert errors,will later on be moved to services***********************/
        /*=======================================================================================================*/
        $scope.trainee.showResponseMessages = function(data) {
            if (data.status == 0)
                $.alert('error occured,could not save data');
            if (data.status == 1)
                $.alert(data.message)


        }


        /**
        _____________________________________________________________________________________________
        |* *******                                                                                   |
        | * Category methods and properties begin here                                               |
        |____________________________________________________________________________________________*/


        /*=======================================================================================================*/
        /***************************save category via add category on curriculum controller***********************/
        /*=======================================================================================================*/


        $scope.trainee.saveTrainee = function(addressData) {

            console.log(addressData);

            try {

                if (angular.isDefined(addressData)) {

                    var url = '/beneficiary/AddAddress';
                    addressData.AccountName = addressData.Fullname;
                    var addressModel = {
                        CountyId: addressData.CountyId,
                        District: addressData.District,
                        HomeTel: addressData.HomeTel,
                        PAddress: addressData.PAddress,
                        PostalCode: addressData.PostalCode,
                        City: addressData.City
                    };

                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(addressModel)
                    }).then(function successCallback(response) {
                            console.log(response)
                            $scope.trainee.showAddCategory = false;
                            if (response.data.status == 1) {

                                addressData.AddressId = response.data.data.id;
                                $scope.trainee.saveTraineeProfile(addressData);
                                $scope.trainee.userAddressSuccess = true;


                            }
                            $scope.trainee.showResponseMessages(response.data);
                        },
                        function errorCallback(error) {
                            $scope.trainee.userAddressSuccess = true;
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }

        $scope.trainee.saveTraineeBankAccount = function(bankAccount) {
            try {

                if (angular.isDefined(bankAccount)) {

                    var url = '/beneficiary/AddTraineeBank';

                    var bankModel = {
                        Bank: bankAccount.Bank,
                        BankBranch: bankAccount.BankBranch,
                        TraineeId: bankAccount.TraineeId,
                        AccountName: bankAccount.AccountName,
                        AccountNumber: bankAccount.AccountNumber
                    };

                    console.log(bankModel);

                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(bankModel)
                    }).then(function successCallback(response) {
                            console.log(response)
                            $scope.trainee.showAddCategory = false;
                            if (response.data.status == 1) {

                                $scope.trainee.currentForm = 'firstForm';
                                $scope.trainee.currentPage = 'currentStudents';
                                $scope.trainee.userBankSuccess = true;
                                $scope.trainee.getTrainees();

                            }
                            $scope.trainee.showResponseMessages(response.data);
                        },
                        function errorCallback(error) {
                            $scope.trainee.userBankSuccess = true;
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }


        $scope.trainee.saveTraineeProfile = function(traineProfile) {

            console.log(traineProfile);

            try {

                if (angular.isDefined(traineProfile)) {

                    var url = '/beneficiary/AddTrainee';

                    var traineeModelInfo = {
                        AddressId: traineProfile.AddressId,
                        AdminNo: traineProfile.AdminNo,
                        DateOfBirth: traineProfile.DOB,
                        Disability: traineProfile.Disability,
                        Domicile: traineProfile.docimile,
                        FullName: traineProfile.Fullname,
                        Gender: traineProfile.Gender,
                        Marital: traineProfile.Marital,
                        PassportNo: traineProfile.PassportNo,
                        CycleId: traineProfile.CycleId,
                        Email: traineProfile.Email
                    };
                    console.log(traineeModelInfo)
                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(traineeModelInfo)
                    }).then(function successCallback(response) {
                            console.log(response.data.data);
                            $scope.trainee.showAddCategory = false;
                            if (response.data.status == 1) {
                                 $scope.trainee.traineeFormData = {};
                                $scope.trainee.userProfileSuccess = true;
                                traineProfile.TraineeId = response.data.data.id;
                                $scope.trainee.saveTraineeBankAccount(traineProfile);
                            }
                            $scope.trainee.showResponseMessages(response.data);
                        },
                        function errorCallback(error) {
                            $scope.trainee.userProfileSuccess = true;
                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }
        /*-----------------------------------------END----------------------------------------------------------*/


        /*=======================================================================================================*/
        /***************************save trade area/course of the curriculum***********************/
        /*=======================================================================================================*/
        $scope.trainee.saveCourse = function(courseFormData) {


            try {
                console.log(courseFormData)
                if (true) {

                    var url = '/curriculum/AddCourseTradeArea';

                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(courseFormData),
                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.trainee.getCourses();
                            $scope.trainee.showAddCategory = false;
                            if (response.data.status == 1) {
                                $scope.trainee.CourseFormData = {};
                                $("#addCourse").modal('hide');
                            }
                            $scope.trainee.showResponseMessages(response.data);
                        },
                        function errorCallback(error) {
                            $.alert("An error occured while making the request");
                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        }


        /*=======================================================================================================*/


        $scope.trainee.fillCourseFormData = function(course) {

            $scope.trainee.CourseFormData = {
                Name: course.name,
                CategoryID: course.categoryId
            }
            console.log($scope.trainee.CourseFormData);


        }

        /*=======================================================================================================*/
        /* ********************check if category name exist before saving*****************************************/
        /*=======================================================================================================*/

        $scope.trainee.checkCategoryName = function() {


        }


        /*=======================================================================================================*/
        /* ******************Delete category from database(change flag on database)******************************/
        /*=======================================================================================================*/
        $scope.trainee.approveTrainee = function(beneficiary) {
            console.log(beneficiary.approvedStatus);
            $.confirm({
                title: 'Confirm!',
                theme: 'light',
                content: 'Are you sure you wan to approve this trainee!',
                buttons: {
                    'confirm': {
                        btnClass: 'btn-blue',
                        action: function() {

                            beneficiary.approvedStatus = true;


                            try {

                                if (angular.isDefined(beneficiary.beneficiary.id)) {

                                    var url = '/beneficiary/ApproveTrainee/?id=' + beneficiary.beneficiary.id;
                                    var data = { id: beneficiary.beneficiary.id }
                                    $scope.trainee.HTTPStatus = true;
                                    $http({
                                        method: "GET",
                                        contentType: "application/json; charset=utf-8",
                                        url: url,
                                        dataType: "json",
                                        data: JSON.stringify(data)
                                    }).then(function successCallback(response) {
                                            console.log(response)

                                            if (response.data.status == 1) {
                                                $scope.trainee.getTrainees();

                                            }
                                            $scope.trainee.showResponseMessages(response.data);
                                        },
                                        function errorCallback(error) {
                                            $scope.trainee.userAddressSuccess = true;
                                        });
                                }
                            } catch (ex) {
                                console.log(ex);
                            }


                        }
                    },
                    cancel:
                    {
                        btnClass: 'btn-danger',
                        action: function() {

                        }
                    }
                }
            });
        }


        /*-----------------------------------------------------------------------------------------------------*/
        $scope.trainee.counties = {};

        $scope.trainee.getCounties = function() {
            try {
                if (true) {

                    var url = '/beneficiary/getCounties';
                    console.log(url);
                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.trainee.counties = response.data.data;

                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        }

        $scope.trainee.getCounties();
        $scope.trainee.traineesCompressedList = {};

        $scope.trainee.getSearchTraineeAttendance = function(traineeFilters) {
            var searchString = '?';

            try {
                if (angular.isDefined(traineeFilters.countyId))
                    searchString += 'countyID=' + traineeFilters.countyId;
                if (angular.isDefined(traineeFilters.trainerId) && traineeFilters.trainerId !== "")
                    searchString += '&trainerId=' + traineeFilters.trainerId;
                if (angular.isDefined(traineeFilters.tradeAreaId) && traineeFilters.tradeAreaId !== "")
                    searchString += '&type=' + traineeFilters.tradeAreaId;
                if (angular.isDefined(traineeFilters.district) && traineeFilters.district !== "")
                    searchString += '&searchString=' + traineeFilters.searchString;
                var url = '/beneficiary/SearchGetTraineesNarrow' + searchString;


                console.log(url);

                $scope.trainee.HTTPStatus = true;
                $http({
                    method: 'get',
                    url: url

                }).then(function successCallback(response) {
                        if (response.data.status == 1)
                            $scope.trainee.traineesCompressedList = response.data.data;
                        else {
                            $scope.trainee.traineesCompressedList = {};

                            $.alert("No Benefeciaries available");
                        }

                    },
                    function errorCallback(error) {

                    });
            } catch (ex) {
                alert(ex)
            }
        }

        /*=======================================================================================================*/
        /* ******************Delete category from database(change flag on database)******************************/
        /*=======================================================================================================*/

        $scope.trainee.deleteTrainee = function(id) {


            $.confirm({
                title: 'Confirm!',
                theme: 'light',
                content: 'Are you sure you wan to delete this trainee,this action cannot be undone!',
                buttons: {
                    'confirm': {
                        btnClass: 'btn-blue',
                        action: function() {
                            try {
                                alert(id)
                                if (angular.isDefined(id)) {

                                    var url = '/beneficiary/deleteTrainee/?id=' + id;
                                    var data = { id: id }
                                    $scope.trainee.HTTPStatus = true;
                                    $http({
                                        method: "GET",
                                        contentType: "application/json; charset=utf-8",
                                        url: url,
                                        dataType: "json",
                                        data: JSON.stringify(data)
                                    }).then(function successCallback(response) {
                                            console.log(response)

                                            if (response.data.status == 1) {
                                                $scope.trainee.getTrainees();

                                            }
                                            $scope.trainee.showResponseMessages(response.data);
                                        },
                                        function errorCallback(error) {
                                            $scope.trainee.userAddressSuccess = true;
                                        });
                                }
                            } catch (ex) {
                                console.log(ex);
                            }
                        }
                    },
                    cancel:
                    {
                        btnClass: 'btn-danger',
                        action: function() {

                        }
                    }
                }
            });

        }

        /*---------------------------------------------END--------------------------------------------------*/


        /*=======================================================================================================*/
        /* **************************Get all course categories registerd on the DB *********************************/
        /*=======================================================================================================*/

        $scope.trainee.traineesList = {};
        $scope.trainee.getTrainees = function() {

            try {

                if (true) {

                    var url = '/beneficiary/getTrainees';
                    console.log(url);
                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
//                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
//                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.trainee.traineesList = response.data.data;

                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        };
        /*---------------------------------------------END--------------------------------------------------*/


        /*=======================================================================================================*/
        /* **************************Get all courses/trade ares  Available *********************************/
        /*======================================================================================================*/
        $scope.trainee.currentTrainee = {}
        $scope.trainee.selectTrainee = function(trainee) {

            $scope.trainee.currentTrainee = trainee.beneficiary;
            $scope.trainee.currentTrainee.county = trainee.county;
            $scope.trainee.currentTrainee.countyId = trainee.userAddress.countyId;
        }


        $scope.trainee.saveTradeArea = function(traineeTradeArea) {


            try {
                console.log(traineeTradeArea)
                if (true) {

                    traineeTradeArea.TraineeId = $scope.trainee.currentTrainee.id;
                    console.log(traineeTradeArea);
                    var url = '/beneficiary/AddTraineeTradeArea';

                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(traineeTradeArea),
                    }).then(function successCallback(response) {

                            if (response.data.status == 1) {
                                $scope.trainee.getTrainees();
                                $scope.trainee.examFormData = {};
                                $("#placement").modal('hide');
                            }
                            $scope.trainee.showResponseMessages(response.data);
                        },
                        function errorCallback(error) {
                            alert(error);
                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        }
        $scope.trainee.providersList = {};
        $scope.trainee.providersCountyList = {};
        $scope.trainee.providersCurrentCoursesList = {};
        $scope.trainee.currentSelectedCourses = function(providerId) {

            if ($scope.trainee.providersCountyList.length > 0) {

                var providers = $scope.trainee.providersCountyList;
                for (var i = 0; i < providers.length; i++) {
                    if (providers[i].provider.id == providerId) {
                        $scope.trainee.providersCurrentCoursesList = providers[i].courses;
                        break;
                    }
                }

            }

            console.log($scope.trainee.providersCurrentCoursesList);
        }
        $scope.trainee.getCountyProviders = function(providerFilters, type) {

            try {
                console.log(providerFilters)
                if (true) {
                    var searchString = '?';

                    if (angular.isDefined(providerFilters.countyId))
                        searchString += 'countyID=' + providerFilters.countyId;
                    if (angular.isDefined(providerFilters.district) && providerFilters.district !== "")
                        searchString += '&district=' + providerFilters.district;
                    if (angular.isDefined(providerFilters.type) && providerFilters.type !== "")
                        searchString += '&type=' + providerFilters.type;
                    if (angular.isDefined(providerFilters.searchString) && providerFilters.searchString !== "")
                        searchString += '&searchString=' + providerFilters.searchString;
                    var url = '/beneficiary/GetAllProviders' + searchString;
                    console.log(url);

                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url

                    }).then(function successCallback(response) {

                            console.log(response.data);
                            if (response.data.status == 1)
                                if (angular.isDefined(type))
                                    $scope.trainee.providersCountyList = response.data.data;
                                else
                                    $scope.trainee.providersList = response.data.data;
                            else {
                                $scope.trainee.providersList = {};

                                $.alert("No Benefeciaries available");
                            }

                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }

        }
        $scope.trainee.selectedTrainees = [];
        $scope.trainee.addCheckedTraine = function(traineeId, status) {


            var currentArray = $scope.trainee.selectedTrainees;
            if (currentArray.length > 0) {
                var exists = false;
                for (var i = 0; i < currentArray.length; i++) {
                    console.log(currentArray[i])
                    if (currentArray[i] == traineeId && !status) {
                        currentArray.splice(i, 1);
                        exists = true;
                        break;
                    }
                }
                if (!exists) {
                    currentArray.push(traineeId);
                }
                $scope.trainee.selectedTrainees = currentArray;
            } else {

                $scope.trainee.selectedTrainees.push(traineeId);
            }
            console.log($scope.trainee.selectedTrainees);
        }

        $scope.trainee.saveMultipleTraineesToTrainer = function() {
            try {
                if ($scope.trainee.selectedTrainees.length > 0) {
                    var trainerProviderTa = {
                        TrainerId: $scope.trainee.currentPickedProvider.provider.id,
                        TradeAreaId: $scope.trainee.traineeTradeArea.tradeAreaId,
                        TraineeIds: $scope.trainee.selectedTrainees.toString()
                    }
                    console.log(trainerProviderTa)
                    var url = '/beneficiary/saveMultipleTraine/';
                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(trainerProviderTa)
                    }).then(function successCallback(response) {
                            console.log(response)
                            $scope.trainee.showAddCategory = false;
                            if (response.data.status == 1) {
                                $scope.trainee.traineesList = {};
                                $("#placementMultiple").modal('hide');
                                $scope.trainee.selectedTrainees = [];

                            }
                            $scope.trainee.showResponseMessages(response.data);
                        },
                        function errorCallback(error) {
                            $scope.trainee.userAddressSuccess = true;
                        });
                } else {
                    $.alert("Please select atleast one trainee");
                }

            } catch (ex) {
                console.log(ex);
            }


        }

        $scope.trainee.pickProvider = function(provider) {
            $scope.trainee.currentPickedProvider = provider;
            var search = { countyId: provider.provider.countyId }
            $scope.trainee.getSearchTrainee(search);
        }

        $scope.trainee.providerTrainees = {};
        $scope.trainee.getProviderWithTrainees = function(provider) {
            $scope.trainee.currentPickedProvider = provider;

            console.log(provider);

            try {
                if (true) {

                    var url = '/beneficiary/getProviderWithTrainees?trainerId=' + provider.provider.id;
                    console.log(url);
                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.trainee.providerTrainees = response.data.data;

                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        };


        $scope.trainee.getProviders = function (name) {
            
            try {
                
                if (true) {
                    $scope.trainee.showTradeAreas = false;

                    var url = '/beneficiary/GetProviders/?countyId=' +
                        $scope.trainee.currentTrainee.countyId +
                        '&searchString=' +
                        name;
                        
                    console.log(url);
                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            if (response.data.status == 1) {
                                $scope.trainee.providersList = response.data.data;
                            } else {
                                $scope.trainee.providersList = {};
                            }


                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }


        /*---------------------------------------------END--------------------------------------------------*/

        /*=======================================================================================================*/
        /* **************************Get all courses/trade ares  Available *********************************/
        /*=======================================================================================================*/
        $scope.trainee.getSearchTrainee = function(searchTrainee) {
            try {

                if (true) {
                    var searchString = '?';

                    if (angular.isDefined(searchTrainee.countyId))
                        searchString += 'countyID=' + searchTrainee.countyId;
                    if (angular.isDefined(searchTrainee.district) && searchTrainee.district !== "")
                        searchString += 'district=' + searchTrainee.district;
                    if (angular.isDefined(searchTrainee.passportNo) && searchTrainee.district !== "")
                        searchString += 'passportNo=' + searchTrainee.passportNo;
                    var url = '/beneficiary/SearchGetTrainees' + searchString;
                    console.log(url);

                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url

                    }).then(function successCallback(response) {
                            if (response.data.status == 1)
                                $scope.trainee.traineesList = response.data.data;
                            else {
                                $scope.trainee.traineesList = {};

                                $.alert("No Benefeciaries available");
                            }

                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }

        /*---------------------------------------------END--------------------------------------------------*/


        /*=======================================================================================================*/
        /* **************************Get all approved trainees and on refined sites *********************************/
        /*=======================================================================================================*/
        $scope.trainee.getSearchTrainee = function(searchTrainee) {
            try {

                if (true) {
                    var searchString = '?';

                    if (angular.isDefined(searchTrainee.countyId))
                        searchString += 'countyID=' + searchTrainee.countyId;
                    if (angular.isDefined(searchTrainee.district) && searchTrainee.district !== "")
                        searchString += 'district=' + searchTrainee.district;
                    if (angular.isDefined(searchTrainee.passportNo) && searchTrainee.district !== "")
                        searchString += 'passportNo=' + searchTrainee.passportNo;
                    var url = '/beneficiary/SearchGetTrainees' + searchString;
                    console.log(url);

                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url

                    }).then(function successCallback(response) {
                            if (response.data.status == 1)
                                $scope.trainee.traineesList = response.data.data;
                            else {
                                $scope.trainee.traineesList = {};

                                $.alert("No Benefeciaries available");
                            }

                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }

        /*---------------------------------------------END--------------------------------------------------*/


        $scope.trainee.banks = {};
        $scope.trainee.getBanks = function() {
            try {
                if (true) {

                    var url = '/beneficiary/getBanks';
                    console.log(url);
                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.trainee.banks = response.data.data;

                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }


        /*=======================================================================================================*/
            /* **************************Get all course categories registerd on the DB *********************************/
            /*=======================================================================================================*/
            $scope.trainee.cycles = {};
            $scope.trainee.getCycles = function () {
               
                try {

                    if (true) {

                        var url = '/curriculum/getcycles';
                        console.log(url);
                        $scope.trainee.HTTPStatus = true;
                        $http({
                            method: 'get',
                            url: url
                            //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                            //                        data: { loginInfo: userCredentials }

                        }).then(function successCallback(response) {
                                console.log(response);
                                $scope.trainee.cycles = response.data.data;

                            },
                            function errorCallback(error) {

                            });
                    }
                } catch (ex) {
                    console.log(ex);
                }
            };
            /*---------------------------------------------END--------------------------------------------------*/




            $scope.trainee.bankBranches = {};
            $scope.trainee.getBankBranches = function (id) {
                try {
                    if (true) {

                        var url = '/beneficiary/getBankBranches?id='+id;
                        console.log(url);
                        $scope.trainee.HTTPStatus = true;
                        $http({
                            method: 'get',
                            url: url
                            //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                            //                        data: { loginInfo: userCredentials }

                        }).then(function successCallback(response) {
                                console.log(response);
                                $scope.trainee.bankBranches = response.data.data;

                            },
                            function errorCallback(error) {

                            });
                    }
                } catch (ex) {
                    console.log(ex);
                }


            };


        $scope.trainee.saveAttendance = function(attendanceForm, trainees) {
            if (trainees.length > 0) {
                var traineesAttendance = [];
                for (var i = 0; i < trainees.length; i++) {


                    if (angular.isUndefined(trainees[i].attendance) || trainees[i].attendance == '') {
                        $.alert('Please enter attendance for ' + trainees[i].fullName + ' row' + (i + 1));
                        break;
                    }
                    var traineeRecord = {
                        BeneficiaryId: trainees[i].beneficiary.id,
                        DaysAttended: trainees[i].attendance,
                        NumberOfType: attendanceForm.NumberOfType,
                        Type: attendanceForm.type

                    }
                    traineesAttendance.push(traineeRecord);

                }

                try {
                    console.log(traineesAttendance)
                    if (true) {

                        var url = '/beneficiary/addAttendance/';

                        $scope.trainee.HTTPStatus = true;
                        $http({
                            method: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: url,
                            dataType: "json",
                            data: JSON.stringify(traineesAttendance),
                        }).then(function successCallback(response) {
                                console.log(response);
                               
                                if (response.data.status == 1) {
                                    $scope.trainee.attendanceForm = {};
                                }
                                $scope.trainee.showResponseMessages(response.data);
                            },
                            function errorCallback(error) {
                                $.alert("An error occured while making the request");
                            });
                    }
                } catch (ex) {
                    console.log(ex);
                }


            }
        }
    

        $scope.trainee.currentPlacementType = 'Placement';

        $scope.trainee.showTransferForm = function (trainee) {
            $scope.trainee.currentTrainee = trainee.beneficiary;
            $scope.trainee.currentTrainee.county = trainee.county;
            $scope.trainee.currentTrainee.countyId = trainee.userAddress.countyId;
            $scope.trainee.currentPlacementType = 'Transfer';
            $("#placementMultiple").modal('hide');
            $("#placement").modal('show');
        
        }


    })