﻿//(function() {
//    'use strict';

var appModule = angular.module('CurriculumApp',
    [
        // Angular modules 
        'ngRoute'

        // Custom modules 

        // 3rd Party Modules
    ]);

//appModule.config(function ($routeProvider) {
//    $routeProvider
//        .when('/',
//            {
//                templateUrl: 'app/SharedFiles/login.html'
//            }
//    );
//    $routeProvider
//        .when('/courses',
//            {
//                templateUrl: 'app/SharedFiles/login.html'
//            }
//        );
//
//});
appModule.controller('CurriculumController',
    function($scope, $http) {


        /*=======================================================================================================*/
        /***************************Declare variables here***********************/
        /*=======================================================================================================*/
        $scope.curriculum = {};
        $scope.curriculum.testData = 'angular loader';
        $scope.curriculum.curriculumList = {};
        $scope.curriculum.currentPage = 'category';
        $scope.curriculum.CategoryFormData = {};

        /*-----------------------------------------END----------------------------------------------------------*/


        /*=======================================================================================================*/
        /***************************method to alert errors,will later on be moved to services***********************/
        /*=======================================================================================================*/
        $scope.curriculum.showResponseMessages = function(data) {
            if (data.status == 0)
                alert('error occured,could not save data');
            if (data.status == 1)
                alert(data.message)


        }


        /**
        _____________________________________________________________________________________________
        |* *******                                                                                   |
        | * Category methods and properties begin here                                               |
        |____________________________________________________________________________________________*/


        /*=======================================================================================================*/
        /***************************save category via add category on curriculum controller***********************/
        /*=======================================================================================================*/
        $scope.curriculum.saveCategory = function(categoryFormData) {


            try {
                console.log(categoryFormData)
                if (true) {

                    var url = '/curriculum/AddCategory';

                    $scope.curriculum.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(categoryFormData),
                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.curriculum.getCourseCategories();
                            $scope.curriculum.showAddCategory = false;
                            if (response.data.status == 1) {
                                $scope.curriculum.CategoryFormData = {};
                                $("#addCategory").modal('hide');
                            }
                            $scope.curriculum.showResponseMessages(response.data);
                        },
                        function errorCallback(error) {
                            alert(error);
                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        }
        /*-----------------------------------------END----------------------------------------------------------*/


        /*=======================================================================================================*/
        /***************************save trade area/course of the curriculum***********************/
        /*=======================================================================================================*/
        $scope.curriculum.saveCourse = function(courseFormData) {


            try {
                console.log(courseFormData)
                if (true) {

                    var url = '/curriculum/AddCourseTradeArea';

                    $scope.curriculum.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(courseFormData),
                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.curriculum.getCourses();
                            $scope.curriculum.showAddCategory = false;
                            if (response.data.status == 1) {
                                $scope.curriculum.CourseFormData = {};
                                $("#addCourse").modal('hide');
                            }
                            $scope.curriculum.showResponseMessages(response.data);
                        },
                        function errorCallback(error) {
                            alert(error);
                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        }


        /*=======================================================================================================*/
        /***************************save trade area/course of the curriculum***********************/
        /*=======================================================================================================*/

        $scope.curriculum.saveCourseExam = function(examFormData) {
            try {
                console.log(examFormData)
                if (true) {

                    var url = '/curriculum/AddCourseExam';
                    examFormData.TradeAreaId = $scope.curriculum.currentCurriculum.tradeAreaId;
                    examFormData.PhaseId = 2;//$scope.curriculum.currentCurriculum.Id;
                    console.log(examFormData);
                    $scope.curriculum.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(examFormData),
                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.curriculum.getCourses();
                            $scope.curriculum.showAddCategory = false;
                            if (response.data.status == 1) {
                                $scope.curriculum.examFormData = {};
                                $("#addCourseExam").modal('hide');
                            }
                            $scope.curriculum.showResponseMessages(response.data);
                        },
                        function errorCallback(error) {
                            alert(error);
                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        }
        /*-----------------------------------------END----------------------------------------------------------*/


        $scope.curriculum.fillCourseFormData = function(course) {

            $scope.curriculum.CourseFormData = {
                Name: course.name,
                CategoryID: course.categoryId
            }
            console.log($scope.curriculum.CourseFormData);


        }

        /*=======================================================================================================*/
        /* ********************check if category name exist before saving*****************************************/
        /*=======================================================================================================*/

        $scope.curriculum.checkCategoryName = function() {


        }
        /*-----------------------------------------------------------------------------------------------------*/


        /*=======================================================================================================*/
        /* ******************Delete category from database(change flag on database)******************************/
        /*=======================================================================================================*/

        $scope.curriculum.deleteCategory = function() {


        }

        /*---------------------------------------------END--------------------------------------------------*/


        /*=======================================================================================================*/
        /* **************************Get all course categories registerd on the DB *********************************/
        /*=======================================================================================================*/
        $scope.curriculum.getCourseCategories = function() {

            try {

                if (true) {

                    var url = '/curriculum/getcategories';
                    console.log(url);
                    $scope.curriculum.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
//                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
//                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.curriculum.curriculumList = response.data.data;

                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        };
        /*---------------------------------------------END--------------------------------------------------*/

        /*=======================================================================================================*/
        /* **************************Get all courses/trade ares  Available *********************************/
        /*=======================================================================================================*/
        $scope.curriculum.getCourses = function() {
            try {

                if (true) {

                    var url = '/curriculum/gettradeareas';
                    console.log(url);
                    $scope.curriculum.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            console.log(response);
                            $scope.curriculum.coursesList = response.data.data;

                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }
        }

        /*---------------------------------------------END--------------------------------------------------*/


    })