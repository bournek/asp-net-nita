﻿var academicModule = angular.module('TraineesApp');
academicModule.controller('AcademicsController',
    function($scope, $http) {
        $scope.academics = {};
        $scope.academics.currentPage = 'training';


        $scope.academics.selectedAcademics = [];
        $scope.academics.addCheckedTraine = function(academicsId, status) {


            var currentArray = $scope.academics.selectedAcademics;
            if (currentArray.length > 0) {
                var exists = false;
                for (var i = 0; i < currentArray.length; i++) {
                    console.log(currentArray[i])
                    if (currentArray[i] == academicsId && !status) {
                        currentArray.splice(i, 1);
                        exists = true;
                        break;
                    }
                }
                if (!exists) {
                    currentArray.push(academicsId);
                }
                $scope.academics.selectedAcademics = currentArray;
            } else {

                $scope.academics.selectedAcademics.push(academicsId);
            }
            console.log($scope.academics.selectedAcademics);
        }


        $scope.academics.searchTraineeAttendance = {};
        $scope.academics.getSearchTraineeAttendance = function(traineeFilters) {
            var searchString = '?';

            try {
                if (angular.isDefined(traineeFilters.countyId))
                    searchString += 'countyID=' + traineeFilters.countyId;
                if (angular.isDefined(traineeFilters.trainerId) && traineeFilters.trainerId !== "")
                    searchString += '&trainerId=' + traineeFilters.trainerId;
                if (angular.isDefined(traineeFilters.tradeAreaId) && traineeFilters.tradeAreaId !== "")
                    searchString += '&type=' + traineeFilters.tradeAreaId;
                if (angular.isDefined(traineeFilters.district) && traineeFilters.district !== "")
                    searchString += '&searchString=' + traineeFilters.searchString;
                var url = '/academics/SearchGetTraineesNarrow' + searchString;


                console.log(url);

                $scope.trainee.HTTPStatus = true;
                $http({
                    method: 'get',
                    url: url

                }).then(function successCallback(response) {
                        if (response.data.status == 1)
                            $scope.academics.traineesCompressedList = response.data.data;
                        else {
                            $scope.trainee.traineesCompressedList = {};

                            $.alert("No Benefeciaries available");
                        }

                    },
                    function errorCallback(error) {

                    });
            } catch (ex) {
                alert(ex)
            }
        }


        $scope.trainee.traineesInternshipList = {};
        $scope.academics.getTraineesForScoring = function (traineeFilters) {
            var searchString = '?';

            try {
                if (angular.isDefined(traineeFilters.countyId))
                    searchString += 'countyId=' + traineeFilters.countyId;
                if (angular.isDefined(traineeFilters.trainerId) && traineeFilters.trainerId !== "")
                    searchString += '&trainerId=' + traineeFilters.trainerId;
                if (angular.isDefined(traineeFilters.tradeAreaId) && traineeFilters.tradeAreaId !== "")
                    searchString += '&type=' + traineeFilters.tradeAreaId;
                if (angular.isDefined(traineeFilters.district) && traineeFilters.district !== "")
                    searchString += '&searchString=' + traineeFilters.searchString;
                var url = '/academics/GetTraineesForScoring' + searchString;


                console.log(url);

                $scope.trainee.HTTPStatus = true;
                $http({
                    method: 'get',
                    url: url

                }).then(function successCallback(response) {
                        if (response.data.status == 1)
                            $scope.academics.traineesInternshipList = response.data.data;
                        else {
                            $scope.trainee.traineesInternshipList = {};

                            $.alert("No Benefeciaries available");
                        }

                    },
                    function errorCallback(error) {

                    });
            } catch (ex) {
                alert(ex)
            }
        }


        $scope.academics.showResponseMessages = function(data) {
            if (data.status == 0)
                $.alert('error occured,could not save data');
            if (data.status == 1)
                $.alert(data.message)


        }
        $scope.academics.traineesReportingList = {};
        $scope.academics.getSearchTraineeReporting = function (traineeFilters) {
            var searchString = '?';

            try {
                if (angular.isDefined(traineeFilters.countyId))
                    searchString += 'countyID=' + traineeFilters.countyId;
                if (angular.isDefined(traineeFilters.trainerId) && traineeFilters.trainerId !== "")
                    searchString += '&trainerId=' + traineeFilters.trainerId;
                if (angular.isDefined(traineeFilters.tradeAreaId) && traineeFilters.tradeAreaId !== "")
                    searchString += '&type=' + traineeFilters.tradeAreaId;
                if (angular.isDefined(traineeFilters.district) && traineeFilters.district !== "")
                    searchString += '&searchString=' + traineeFilters.searchString;
                var url = '/academics/SearchGetTraineesNarrow' + searchString;


                console.log(url);

                $scope.trainee.HTTPStatus = true;
                $http({
                    method: 'get',
                    url: url

                }).then(function successCallback(response) {
                        if (response.data.status == 1)
                            $scope.academics.traineesReportingList = response.data.data;
                        else {
                            $scope.academics.traineesReportingList = {};

                            $.alert("No Benefeciaries available");
                        }

                    },
                    function errorCallback(error) {

                    });
            } catch (ex) {
                alert(ex)
            }
        }

        $scope.academics.saveReporting = function(dateReported) {
            console.log(dateReported);
            var trainees = $scope.academics.selectedAcademics;
            if (trainees.length > 0) {
                var traineesReporting = [];

                if (trainees.length > 0) {
                    for (var i = 0; i < trainees.length; i++) {


                        var traineeRecord = {
                            BeneficiaryId: trainees[i],
                            dateReported: dateReported
                        }

                        traineesReporting.push(traineeRecord);

                    }
                } else {
                    $.alert("Please select atleast one beneficiary");
                }
                try {
                    console.log(traineesReporting)
                    if (true) {

                        var url = '/academics/AddTraineeReporting/';

                        $scope.academics.HTTPStatus = true;
                        $http({
                            method: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: url,
                            dataType: "json",
                            data: JSON.stringify(traineesReporting),
                        }).then(function successCallback(response) {
                                console.log(response);

                                if (response.data.status == 1) {
                                    $scope.dateReported = {};
                                    $scope.academics.selectedAcademics = [];
                                    $scope.academics.getSearchTraineeReporting($scope.academics.searchTraineeAttendance);
                                }
                                $scope.academics.showResponseMessages(response.data);
                            },
                            function errorCallback(error) {
                                $.alert("An error occured while making the request");
                            });
                    }
                } catch (ex) {
                    console.log(ex);
                }


            }
        }
        $scope.academics.getProviderEmployers = function(beneficiary) {
            $scope.academics.currentTrainee = beneficiary.beneficiary;
            $scope.academics.currentTrainee.county = beneficiary.name;
            $scope.academics.currentTrainee.countyId = beneficiary.countyId;
            console.log(beneficiary);
            $scope.academics.getEmployers(beneficiary.beneficiary.trainerId);
        }

        $scope.academics.clearTraineesCompressedList = function () {
            
            $scope.academics.traineesReportingList = {};
            
        }

        $scope.academics.providerEmployers = {};
        $scope.academics.getEmployers = function(trainerId) {


            try {
                if (true) {

                    var url = '/academics/GetTrainerEmployers?id=' + trainerId;
                    console.log(url);
                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {

                            $scope.academics.trainerEmployers = response.data.data;

                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        };

        $scope.academics.traineeEmployer = {};
        $scope.academics.getEmployerDetails=function(beneficiary) {
            $scope.academics.currentTrainee = beneficiary;
            $scope.academics.traineeEmployer = {};
            try {
                if (true) {

                    var url = '/academics/GetTraineeEmployer?beneficiaryId=' + beneficiary.id;
                    console.log(url);
                    $scope.trainee.HTTPStatus = true;
                    $http({
                        method: 'get',
                        url: url
                        //                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },//{'Content-Type': 'application/json'},
                        //                        data: { loginInfo: userCredentials }

                    }).then(function successCallback(response) {
                            if (response.data.status == 1)
                                $scope.academics.traineeEmployer = response.data.data;
                            else {
                                $scope.academics.showResponseMessages(response.data);
                                $scope.academics.traineeEmployer = {};
                            }
                        },
                        function errorCallback(error) {

                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        }

        $scope.academics.assignTraineeToEmployer = function(employer,supervisor) {

            var traineeEmployer = {
                BeneficiaryId: $scope.academics.currentTrainee.id,
                EmployerId: employer.id,
                ContactPerson: supervisor.ContactPerson,
                ContactPersonMobile: supervisor.ContactPersonMobile,
                ContactPersonEmail: supervisor.ContactPersonEmail
            };
            console.log(traineeEmployer)
            try {
                
                if (true) {

                    var url = '/academics/AssignTraineeToEmployer/';

                    $scope.academics.HTTPStatus = true;
                    $http({
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        dataType: "json",
                        data: JSON.stringify(traineeEmployer),
                    }).then(function successCallback(response) {
                            console.log(response);

                            if (response.data.status == 1) {
                                $scope.dateReported = {};
                                $scope.academics.selectedAcademics = [];
                                $("#assignEmployee").modal('hide');
                                $scope.academics.getSearchTraineeAttendance($scope.academics.searchTraineeAttendance);
                            }
                            $scope.academics.showResponseMessages(response.data);
                        },
                        function errorCallback(error) {
                            $.alert("An error occured while making the request");
                        });
                }
            } catch (ex) {
                console.log(ex);
            }


        }


    })
    .filter('toArray', function () {
        return function (obj, addKey) {
            if (!angular.isObject(obj)) return obj;
            if (addKey === false) {
                return Object.keys(obj).map(function (key) {
                    return obj[key];
                });
            } else {
                return Object.keys(obj).map(function (key) {
                    var value = obj[key];
                    return angular.isObject(value) ?
                        Object.defineProperty(value, '$key', { enumerable: false, value: key }) :
                        { $key: key, $value: value };
                });
            }
        };
    });