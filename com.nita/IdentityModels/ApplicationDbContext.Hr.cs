using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;

namespace com.nita.IdentityModels
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {

        public DbSet<Country> Countries { get; set; }
        public DbSet<County> Counties { get; set; }
        public DbSet<Location> Locations { get; set; }
        //
        public DbSet<Department> HrDepartments { get; set; }
        public DbSet<Section> HrSections { get; set; }
        public DbSet<Division> HrDivisions { get; set; }
        //
        public DbSet<HrJobCatalogue> HrJobCatalogues { get; set; }
        public DbSet<RenumerationScheme> HrRenumerationSchemes { get; set; }
        public DbSet<EmployeeCategory> HrEmployeeCategories { get; set; }
        public DbSet<JobCategory> HrJobCategories { get; set; }
        //public DbSet<JobGroup> HrJobGroups { get; set; }
        public DbSet<LeaveGroup> HrLeaveGroups { get; set; }
        public DbSet<StaffProfile> HrStaff { get; set; }
        public DbSet<StaffDesignation> HrDesignations { get; set; }

        protected void OnStaffModelCreating(ModelBuilder builder)
        {      
            //
            builder.Entity<ApplicationUser>().HasOne(e => e.StaffProfile).WithOne(j => j.ApplicationUser).HasForeignKey<StaffProfile>(e => e.ApplicationUserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //Staff <=> Section
            builder.Entity<StaffProfile>().HasOne(k => k.Section).WithMany(p => p.Staff).HasForeignKey(k => k.SectionId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //Staff <=> Division
            builder.Entity<StaffProfile>().HasOne(k => k.Division).WithMany(p => p.Staff).HasForeignKey(k => k.DivisionId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //Staff <=> Designation
            builder.Entity<StaffProfile>().HasOne(l => l.Designation).WithMany(p => p.Staff).HasForeignKey(k => k.DesignationId).OnDelete(DeleteBehavior.ClientSetNull);
            //Required address
            builder.Entity<StaffProfile>().HasOne(e => e.Address).WithOne(p => p.Staff).HasForeignKey<StaffProfile>(e => e.AddressId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //OPtional Supervisor
            builder.Entity<StaffProfile>().HasMany(e => e.Team).WithOne(p => p.Supervisor).HasForeignKey(e => e.SupervisorId/*"SupervisorId"*/).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<StaffProfile>().HasOne(e => e.Termination).WithOne(p => p.Staff).HasForeignKey<StaffTermination>(e => e.StaffId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<StaffProfile>().HasOne(e => e.Location).WithMany(p => p.Staff).HasForeignKey(e => e.LocationId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //Staff <=> Dependants
            builder.Entity<StaffProfile>().HasMany(e => e.Dependants).WithOne(p => p.Staff).HasForeignKey(e => e.StaffId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //Staff <=> Academic PRoflie
            builder.Entity<StaffProfile>().HasOne(e => e.AcademicProfile).WithOne(p => p.Staff).HasForeignKey<AcademicProfile>(e => e.StaffId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //Staff <=> JobCatalogue
            builder.Entity<StaffProfile>().HasOne(e => e.JobCatalogue).WithMany(p => p.Employees).HasForeignKey(e => e.JobCatId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //AcademicProfile <=> Certifications
            builder.Entity<AcademicProfile>().HasMany(e => e.Certifications).WithOne(p => p.Profile).HasForeignKey(e => e.ProfileId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //Required Address
            builder.Entity<TraineeProfile>().HasOne(e => e.Address).WithOne(p => p.Trainee).HasForeignKey<TraineeProfile>(e => e.AddressId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //Required Address
            builder.Entity<TrainerProfile>().HasOne(e => e.Address).WithOne(p => p.Trainer).HasForeignKey<TrainerProfile>(e => e.AddressId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //

            //Dept - Section
            builder.Entity<Department>().HasMany(e => e.Sections).WithOne(p => p.Department).HasForeignKey(e => e.DeptId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<Department>().HasMany(e => e.DebitJournals).WithOne(p => p.DebitDept).HasForeignKey(e => e.DebitDeptId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<Department>().HasMany(e => e.CreditJournals).WithOne(p => p.CreditDept).HasForeignKey(e => e.CreditDeptId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            
            //Division - Section
            builder.Entity<Section>().HasMany(e => e.Divisions).WithOne(p => p.Section).HasForeignKey(e => e.SectionId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //

            builder.Entity<JobCategory>().HasMany(e => e.HrJobCatalogues).WithOne(p => p.Category).HasForeignKey(e => e.JobCatId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<EmployeeCategory>().HasMany(e => e.Employees).WithOne(p => p.EmpCat).HasForeignKey(e => e.EmpCatId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<HrJobCatalogue>().HasOne(e => e.Renumeration).WithMany(p => p.JobCatalogues).HasForeignKey(e => e.RenumerationId).IsRequired().OnDelete(DeleteBehavior.Cascade);


        }
    }
}
