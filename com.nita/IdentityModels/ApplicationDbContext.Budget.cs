using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;

namespace com.nita.IdentityModels
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        public DbSet<Cycle> Cycles { get; set; }
        public DbSet<BudgetPeriod> BudgetPeriod { get; set; }
        public DbSet<FinanceBudget> FBudgets { get; set; }

        protected void OnBudgetModelCreating(ModelBuilder builder)
        {      
            //
            builder.Entity<FinanceBudget>().HasOne(e => e.Cycle).WithMany(j => j.Budgets).HasForeignKey(e => e.CycleId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FinanceBudget>().HasOne(e => e.Period).WithMany(j => j.Budgets).HasForeignKey(e => e.PeriodId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FinanceBudget>().HasOne(e => e.GLAccount).WithMany(j => j.Budgets).HasForeignKey(e => e.AccountId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FinanceBudget>().HasOne(e => e.Department).WithMany(j => j.Budgets).HasForeignKey(e => e.DeptId).IsRequired().OnDelete(DeleteBehavior.Cascade);
        }
    }
}
