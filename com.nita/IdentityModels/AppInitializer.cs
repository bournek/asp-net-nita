﻿using com.nita.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public partial class AppInitializer
    {
        //
        //Admin
        const string adminName = "ABN_DEV";
        const string adminEmail = "example@nita.co.ke";
        const string adminPassword = "Admin@123";
        //
        private readonly IServiceProvider provider;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly ILogger logger;
        //
        public AppInitializer(IServiceProvider _provider, ILoggerFactory factory)
        {
            this.provider = _provider;
            this.logger = factory.CreateLogger<AppInitializer>();
            //
            this.hostingEnvironment = _provider.GetRequiredService<IHostingEnvironment>();
        }
        /// <summary>
        /// Initialise the application data storage and create some basic features
        /// </summary>
        /// <returns></returns>
        public async Task EnsureInit()
        {
            using (var serviceScope = provider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var db = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                try
                {                       
                    if (await db.Database.EnsureCreatedAsync())
                    {
                        logger.LogInformation(2, "InitializeDbAsync: Using Database '" + db.Database.GetDbConnection().ConnectionString + "'");

                        //var context = provider.GetService<ApplicationDbContext>();
                        logger.LogInformation(2, "InitializeDbAsync: Database Created.\n Doing migrations...");
                        //db.Database.Migrate();
                        //
                        await CreateBasicBlocks().ContinueWith(async x => {
                            //App Roles
                            await CreateDefaultRoles().ContinueWith(async y => {
                                //Admin User
                                await CreateAdminUser().ContinueWith(async z => {
                                    //Demo user acc.
                                    await CreateDemoUsers().ContinueWith(async a =>{
                                        //
                                        if (hostingEnvironment.IsDevelopment())
                                        {
                                            await CreateDevtPayments();
                                        }
                                    });
                                });
                            });
                        });
                        //
                        logger.LogInformation(2, "InitializeDbAsync: Migrations completed.");
                    }
                    else
                    {
                        logger.LogInformation(2, "InitializeDbAsync: All Looks Good 👌.");
                    }
                }catch(SqlException ex)
                {
                    logger.LogCritical(2, "InitializeDbAsync: Operation Failed!!! \n\r Error = "+ex.Message);
                    if (hostingEnvironment.IsDevelopment())//For developemnt
                    {
                        logger.LogInformation(2, "InitializeDbAsync: Application will suspend.");
                        throw ex;
                    }
                    else
                    {//Production,,
                        logger.LogInformation(2, "Application will Exit...");
                        Environment.Exit(2);
                    }
                }

            }
        }
        /// <summary>
        /// Create some basic frames required by the application..
        /// </summary>
        /// <returns></returns>
        private async Task CreateBasicBlocks()
        {
            var db = provider.GetRequiredService<ApplicationDbContext>();

            // If the admin role does not exist, create it.
            logger.LogInformation(2, "AccountTypes: Ensuring NitaAdmin Role admin exists");
            
            //Country
            var kenya = new Country()
            {
                Name = "Kenya",
                Code = "+254"
            };
            db.Countries.Add(kenya);
            await db.SaveChangesAsync();

            //Counties
            try
            {
                var path = Path.Combine(hostingEnvironment.WebRootPath, "lib/cached/counties-ke.json");
                var text = File.ReadAllText(path);
                var list = JsonConvert.DeserializeObject<List<County>>(text);
                foreach(var c in list)
                {
                    var county = new County()
                    {
                        Name = c.Name,
                        Code = c.Code,
                        CountryId = kenya.Id
                    };
                    kenya.Counties.Add(county);
                    await db.SaveChangesAsync();
                    //
                }
            }
            catch(Exception ex)
            {

                logger.LogCritical(0, "Create Counties failed.",ex);
            }
            //
            if (hostingEnvironment.IsDevelopment())
            {
                await CreateDevtData();
            }
        }
        /// <summary>
        /// Default system roles
        /// </summary>
        /// <returns></returns>
        private async Task CreateDefaultRoles()
        {
            //var options = serviceProvider.GetRequiredService<IOptions<IdentityDbContextOptions>>().Value;
            var userManager = provider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = provider.GetRequiredService<RoleManager<ApplicationRole>>();

            // If the admin role does not exist, create it.
            logger.LogInformation(2, "Roles: Ensuring NitaAdmin Role admin exists");
            //NitaAdmin
            if (!await roleManager.RoleExistsAsync(DefaultRoles.NitaSysAdmin))
            {
                logger.LogInformation(2, "Roles: NitaAdmin Role admin does not exist - creating");
                var roleCreationResult = await roleManager.CreateAsync(new ApplicationRole(DefaultRoles.NitaSysAdmin, RoleType.System));
                DumpIdentityResult("Roles: NitaAdmin Role Creation", roleCreationResult);
            }
            else
            {
                logger.LogInformation(2, "Roles: NitaAdmin Role exists");
            }
            //NitaAccessor
            if (!await roleManager.RoleExistsAsync(DefaultRoles.NitaAccessor))
            {
                logger.LogInformation(2, "Roles: NitaAccessor Role does not exist - creating");
                var roleCreationResult = await roleManager.CreateAsync(new ApplicationRole(DefaultRoles.NitaAccessor, RoleType.System));
                DumpIdentityResult("Roles: NitaAccessor Role Creation", roleCreationResult);
            }
            else
            {
                logger.LogInformation(2, "Roles: NitaAccessor Role exists");
            }
            //NitaCoordinator
            if (!await roleManager.RoleExistsAsync(DefaultRoles.NitaCoordinator))
            {
                logger.LogInformation(2, "Roles: NitaCoordinator Role does not exist - creating");
                var roleCreationResult = await roleManager.CreateAsync(new ApplicationRole(DefaultRoles.NitaCoordinator, RoleType.System));
                DumpIdentityResult("Roles: NitaCoordinator Role Creation", roleCreationResult);
            }
            else
            {
                logger.LogInformation(2, "Roles: NitaCoordinator Role exists");
            }
            //NitaFinance
            if (!await roleManager.RoleExistsAsync(DefaultRoles.NitaFinance))
            {
                logger.LogInformation(2, "Roles: NitaFinance Role does not exist - creating");
                var roleCreationResult = await roleManager.CreateAsync(new ApplicationRole(DefaultRoles.NitaFinance, RoleType.System));
                DumpIdentityResult("Roles: NitaFinance Role Creation", roleCreationResult);
            }
            else
            {
                logger.LogInformation(2, "Roles: NitaFinance Role exists");
            }
        }
        private async Task CreateAdminUser()
        {
            //var options = serviceProvider.GetRequiredService<IOptions<IdentityDbContextOptions>>().Value;
            var userManager = provider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = provider.GetRequiredService<RoleManager<ApplicationRole>>();

            
            // if the user does not exist, create it.
            logger.LogInformation(2, String.Format("CreateAdminUser: Ensuring User {0} exists", adminName));
            var user = await userManager.FindByNameAsync(adminName);
            if (user == null)
            {
                logger.LogInformation(2, "CreateAdminUser: User does not exist - creating");
                user = new ApplicationUser
                {
                    UserName = adminName,
                    Email = adminEmail,
                    AccountType = AccountType.NitaAdmin
                };
                var userCreationResult = await userManager.CreateAsync(user, adminPassword);
                DumpIdentityResult("CreateAdminUser: User Creation", userCreationResult);
                if (userCreationResult.Succeeded)
                {
                    logger.LogInformation(2, "CreateAdminUser: Adding new user to role admin");
                    var roleAdditionResult = await userManager.AddToRoleAsync(user, DefaultRoles.NitaSysAdmin);
                    if (roleAdditionResult.Succeeded)
                    {
                        roleAdditionResult = await userManager.AddToRoleAsync(user, DefaultRoles.NitaCoordinator);

                        await userManager.AddClaimAsync(user, new Claim(DefaultClaims.AdminClaim, "Allowed"));
                        await userManager.AddClaimAsync(user, new Claim(DefaultClaims.ManageStaffClaim, "Allowed"));
                        DumpIdentityResult("CreateAdminUser: Role Addition", roleAdditionResult);
                        //Admin Profile
                        var prof = new AdminProfile()
                        {
                            FullName = "ABN Portal Admin",
                            Closed = false,
                            ApplicationUserId = user.Id,
                            Type = AdminType.Abno
                        };

                        //
                        user.AdminProfile = prof;
                        //
                        var d = await userManager.UpdateAsync(user);
                        DumpIdentityResult("CreateAdminUser: Admin Profile Addition", d);
                        if (d.Succeeded)
                        {
                            //
                        }

                    }
                }
            }
            else
            {
                logger.LogInformation(2, "CreateAdminUser: User already exists");
            }
        }
       

        private void DumpIdentityResult(string prefix, IdentityResult result)
        {
            logger.LogInformation(2, String.Format("{0}: Result = {1}", prefix, result.Succeeded ? "Success" : "Failed"));
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    logger.LogInformation(2, String.Format("--> {0}: {1}", error.Code, error.Description));
                }
            }
        }
    }
}
