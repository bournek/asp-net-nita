﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using Nita.IdentityModels;

namespace com.nita.IdentityModels
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {

        public DbSet<Product> Products { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerInvoice> CustomerInvoices { get; set; }
        public DbSet<CustomerInvoiceProduct> CustomerInvoiceProducts { get; set; }

        public DbSet<ProductPrice> ProductPrices { get; set; }
        public DbSet<InvoicePayment> InvoicePayments { get; set; }
        protected void OnReceivablesModelCreating(ModelBuilder builder)
        {
            builder.Entity<Product>().HasMany(l => l.ProductPrices).WithOne(p => p.Product)
                .HasForeignKey(c => c.ProductId).OnDelete(deleteBehavior: DeleteBehavior.Cascade);
            //
            
        }
    }
}
