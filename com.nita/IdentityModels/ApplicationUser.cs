using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace com.nita.IdentityModels
{
    public enum RoleType
    {
        System = 554, User = 900
    }
    public enum AccountType
    {
        Trainee=98,Institution=99,NitaStaff=100,NitaAdmin=111,MasterCraftsMan = 120
    }
    public class ApplicationRole : IdentityRole<int>
    {
        public string Description { get; set; }

        public int Level { get; set; }
        public RoleType Type { get; set; }
        public ApplicationRole() : base() { }
        public ApplicationRole(string name) : this()
        {
            this.Name = name;
        }

        public ApplicationRole(string name, string description) : this(name)
        {
            this.Description = description;
        }
        public ApplicationRole(string name, int level) : this(name)
        {
            this.Level = level;
        }
        public ApplicationRole(string name, RoleType type) : this(name)
        {
            this.Type = type;
        }
    }
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser<int>
    {
        public AccountType AccountType { get; set; }
        public byte[] FingerTemplate1 { get; set; }
        public byte[] FingerTemplate2 { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual TraineeProfile TraineeProfile { get; set; }
        public virtual TrainerProfile TrainerProfile { get; set; }
        public virtual StaffProfile StaffProfile { get; set; }
        public virtual AdminProfile AdminProfile { get; set; }

        /// <summary>
        /// Navigation property for the roles this user belongs to.
        /// </summary>
        public virtual ICollection<ApplicationRole> Roles { get; } = new List<ApplicationRole>();

        /// <summary>
        /// Navigation property for the claims this user possesses.
        /// </summary>
        public virtual ICollection<IdentityUserClaim<int>> Claims { get; } = new List<IdentityUserClaim<int>>();

        /// <summary>
        /// Navigation property for this users login accounts.
        /// </summary>
        public virtual ICollection<IdentityUserLogin<int>> Logins { get; } = new List<IdentityUserLogin<int>>();

        public ApplicationUser()
        {
            DateCreated = DateTime.Now;

            Roles = new List<ApplicationRole>();
            Claims = new List<IdentityUserClaim<int>>();
            Logins = new List<IdentityUserLogin<int>>();
        }
    }
}
