﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum AttendanceState
    {
        Absent = 93, Present = 97 , Halfday = 111
    }
    public class TraineeAttendance
    {
        public int Id { get; set; }
        public int TraineeId { get; set; }
        public int PersonnelId { get; set; }
        public DateTime DateCaptured { get; } = DateTime.Now;
        public DateTime Date { get; set; }
        public AttendanceState Status { get; set; }
        public string Notes { get; set; }
        public virtual TraineeProfile Beneficiary { get; set; }
        public virtual StaffProfile Personnel { get; set; }
    }
}
