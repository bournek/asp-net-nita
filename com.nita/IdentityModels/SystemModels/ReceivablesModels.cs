﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using com.nita.IdentityModels;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Microsoft.EntityFrameworkCore.Metadata.Internal;


namespace Nita.IdentityModels
{
    public class ReceivablesModels
    {


    }

    public class Product
    {
        public int Id { get; set; }
        public double SellPrice { get; set; }
        public double? CostPrice { get; set; } = null;
        public double? MarkUp { get; set; }
        public string Code { get; set; }
        public string ProductName { get; set; }
        public int FProductCategoryId { get; set; }
        public virtual FProductCategory FProductCategory { get; set; }
        public int Status { get; set; } = 1;
        public int FTaxesId { get; set; }
        public int? FCurrencyId { get; set; }
        public virtual ICollection<ProductPrice> ProductPrices { get; set; }

        [ForeignKey(nameof(FTaxesId))]
        public virtual FinanceTax FTaxes { get; set; }

        public bool DiscountStatus { get; set; }
    }




    public class ProductPrice
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public double SellPrice { get; set; }
        public double? CostPrice { get; set; }
        public int Status { get; set; } = 1;
        public DateTime DateCreated { get; set; } = DateTime.Now;

        public int? FTaxesId { get; set; }
        public virtual Product Product { get; set; }
        
    }








    public class Customer
    {
        public int Id { set; get; }
        public int? FCurrencyId { set; get; }
        public string CustomerName { set; get; }
        public string CustomerCode { set; get; }
        public string CustomerMobile { set; get; }
        public string CustomerEmail { set; get; }
        public string CustomerAddress { set; get; }
        public int CustomerType { set; get; }
        public string RegNo { set; get; }
        public int Status { set; get; } = 1;
        public bool ActiveStatus { set; get; }
        public double? CustomerBalance { set; get; }
        public int FCustomerTypeId { get; set; }
        public virtual FCustomerType FCustomerType { get; set; }
        public virtual ICollection<CustomerInvoiceProduct> CustomerInvoiceProducts { get; set; }

        //public virtual FCur CustomerInvoices { get; set; }

    }

    public class CustomerInvoiceProduct
    {
        public int Id { get; set; }
        public int? CustomerInvoiceId { get; set; }
        public int? CustomerId { get; set; }
        public int? ProductId { get; set; }
        public double ProductSellPrice { get; set; }
        public int FTaxesId { get; set; }
        public DateTime DateCreated { get; set; }=DateTime.Now;
        public int ItemStatus { get; set; }
        public string  InvoiceTerms { get; set; }
        public int Status { get; set; } = 0;
        public int Quantity { get; set; }
        public double Discount { get; set; } = 0.00;

        public virtual FinanceTax FinanceTax { get; set; }
        public virtual Product Product { get; set; }
        public int FinanceTaxId { get; set; }
    }

    public class CustomerInvoice
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string InvoiceNo { get; set; }
        public double? PaidAmount { get; set; }
        public bool InvoiceStatus { get; set; } = false;
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public string Terms { get; set; }
        public int Recurrency { get; set; }
        public DateTime  DateDue { get; set; }
        public int FCurrencyId { get; set; }
       // public bool? PendingAmount { get; set; }

        public double? PendingAmount { get; set; }
        public virtual ICollection<CustomerInvoiceProduct> CustomerInvoiceProducts { get; set; }
        public virtual Customer Customer { get; set; }
    }

    public class InvoicePayment
    {
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public int CustomerId { get; set; }
        public int PaymodeId { get; set; }
        public int FinanceLedgerId { get; set; }
        public int FinanceAccountId { get; set; }
        public int AmountPaid { get; set; }
        public DateTime DatePaid { get; set; }
        public int Status { get; set; } = 1;
        public string TransactionNumber { get; set; }
        public string ReceivedBy { get; set; }
//=======
//        public int? FinanceAccountId { get; set; }
//        public int AmountPaid { get; set; }
//        public DateTime DatePaid { get; set; }=DateTime.Now;
//        public int Status { get; set; } = 1;
//        public string TransactionNumber { get; set; }
//        public string ReceivedBy { get; set; }
//        public string Description { get; set; }
//>>>>>>> bb29540261fcc55104fe6c2e3fa6114c425a9380
        public virtual CustomerInvoice CustomerInvoice { get; set; }
        public virtual FinanceAccount FinanceAccount { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual FinanceLedger FinanceLedger { get; set; }
        //public virtual PaymentMode PaymentMode { get; set; }
        public virtual PaymentAccount PaymentAccount { get; set; }
    }
}
