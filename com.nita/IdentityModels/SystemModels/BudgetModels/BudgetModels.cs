﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public class FinanceBudget
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int DeptId { get; set; }

        //public int YearId { get; set; }
        public int? CycleId { get; set; }
        public int? PeriodId { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public bool Enforced { get; set; }
        public virtual FinanceAccount GLAccount { get; set; }
        public virtual Department Department { get; set; }
        public virtual BudgetPeriod Period { get; set; }
        public virtual Cycle Cycle { get; set; }
    }
}
