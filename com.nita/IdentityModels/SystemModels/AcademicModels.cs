﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    /*=======================================================================================================*/
    /******************************This represents departs kind(department of scienc)*************************/
    /*=======================================================================================================*/
    public class Category
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public virtual ICollection<TradeArea> TradeAreas { get; set; }
    }


    /*-----------------------------------------END----------------------------------------------------------*/

    /*=======================================================================================================*/
    /******************************This represents the program kind(department of CS)*************************/
    /*=======================================================================================================*/
    public class TradeArea
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }

        public string Name { get; set; }

        public virtual ICollection<TraineeProfile> Trainees { get; set; } //
        [NotMapped] public virtual ICollection<TrainerProfile> Trainers { get; set; }
        public virtual AreaCurriculumn Curriculumn { get; set; }

        //public virtual ICollection<TraineeTradeArea> TraineeTradeAreas { get; set; }

        public TradeArea()
        {
            Trainers = new List<TrainerProfile>();
        }
    }
    /*-----------------------------------------END----------------------------------------------------------*/


    /*=======================================================================================================*/
    /********************This represents the units that make up a curriculum(department of CS)****************/
    /*=======================================================================================================*/
    public class AreaCurriculumn
    {
        public int Id { get; set; }
        public int AreaId { get; set; }

        public virtual TradeArea Area { get; set; }

        //Exam
    }
    /*-----------------------------------------END----------------------------------------------------------*/


    /*=======================================================================================================*/
    /********************This represents the phases****************/
    /*=======================================================================================================*/
    public class Phase
    {
        public int Id { get; set; }
        public string PhaseName { get; set; }

        public string PhaseCode { get; set; }

        public int TotalTrainees { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime Datecreated { get; set; } = DateTime.Now;

        public bool PhaseStatus { get; set; } = true;

        //Exam
        public virtual ICollection<NitaExam> Exams { get; set; }
    }

    /*-----------------------------------------END----------------------------------------------------------*/
    public class ProviderTradeAreas
    {
        public int Id { get; set; }
        public int TrainerId { get; set; }
        public int TradeAreaId { get; set; }

        public DateTime DateCreated { get; set; } = DateTime.Now;
        public virtual ICollection<TrainerProfile> Trainers { get; set; }
        public virtual ICollection<TradeArea> TradeArea { get; set; }
    }

    public class Beneficiary
    {
        public int Id { get; set; }
        public int? ApplicationUserId { get; set; }
        public int AddressId { get; set; }

        public string AdminNo { get; set; }
        public string FullName { get; set; }
        public string PassportNo { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        public MaritalStatus Marital { get; set; }
        public bool Closed { get; set; }
        public DateTime DateClosed { get; set; }
        public string CloseReason { get; set; }
        public string Disability { get; set; }

        //public int ? TraineeAccountId { get; set; }
        public string Domicile { get; set; }

        public int Status { get; set; } = 1;
        public bool ApprovedStatus { get; set; } = false;
        public bool PlacedStatus { get; set; } = false;
        public int CycleId { get; set; }

        public DateTime DateCreated { get; set; } = DateTime.Now;

//        public int? BeneficiaryStageId { get; set; }
        public virtual BeneficiaryStage BeneficiaryStage { get; set; }
        public virtual InternshipScore InternshipScore { get; set; }
        public virtual Cycle Cycle { get; set; }
        public virtual TraineeTradeArea TraineeTradeArea { get; set; }
        public virtual ICollection<Attendance> Attendances { get; set; }
        public virtual UserAddress Address { get; set; }
    }

    public class NitaExam
    {
        public int Id { get; set; }
        public int TradeAreaId { get; set; }
        public int CycleId { get; set; }
        public string Name { get; set; } //End
        public double TotalScore { get; set; } //30

        public virtual ICollection<ExamScore> Scores { get; set; }
    }


    public class Attendance
    {
        public int Id { get; set; }
        public int BeneficiaryId { get; set; }
        public int NumberOfType { get; set; }
        public int Year { get; set; } = DateTime.Now.Year;
        public int DaysAttended { get; set; }


        public string Type { get; set; }
    }


    public class ExamScore
    {
        public int Id { get; set; }
        public int ExamId { get; set; }
        public int TraineeId { get; set; } //Who sat the assesment
        public double Value { get; set; } //15
        public virtual TraineeProfile Trainee { get; set; }
        public virtual NitaExam Exam { get; set; }
    }

    public class TraineeTradeArea
    {
        public int Id { get; set; }
        public int TrainerId { get; set; }
        public int TraineeId { get; set; }
        public int BeneficiaryId { get; set; }
        public int TradeAreaId { get; set; }
        public int Status { get; set; } = 1;
        public int Type { get; set; } = 0;

        public DateTime DateCreated { get; set; } = DateTime.Now;

//        [ForeignKey(nameof(BeneficiaryId))]
//        public virtual Beneficiary Beneficiary { get; set; }
        [ForeignKey(nameof(TradeAreaId))] public virtual TradeArea TradeArea { get; set; }
    }

    public class BeneficiaryStage
    {
        public int Id { get; set; }
        [ForeignKey(nameof(BeneficiaryId))] public int BeneficiaryId { get; set; }
        public bool ReportStage { get; set; } = false;
        public DateTime ReportStageDate { get; set; }
        public bool TrainingStage { get; set; } = false;
        public DateTime TrainingStageDate { get; set; }
        public DateTime TrainingStageEndDate { get; set; }
        public bool InternshipStage { get; set; } = false;
        public DateTime InternshipStageDate { get; set; }
        public DateTime InternshipStageEndDate { get; set; }
        public bool GraduationStage { get; set; } = false;
        public DateTime GraduationStageDate { get; set; }
        public DateTime GraduationStageEndDate { get; set; }

        public bool Discontinued { get; set; } = false;
//        public virtual Beneficiary Beneficiary { get; set; }
    }

    public class TraineeProviderEmployer
    {
        public int Id { get; set; }
        public int BeneficiaryId { get; set; }
        public int EmployerId { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public int Status = 1;
        public string ContactPerson { get; set; }
        public string ContactPersonMobile { get; set; }
        public string ContactPersonEmail { get; set; }
    }

    public class InternshipScore
    {
        public int Id { get; set; }
        //[ForeignKey(nameof(BeneficiaryId))]
        public int BeneficiaryId { get; set; }
        public int TotalScore { get; set; }
        //[ForeignKey(nameof(CycleId))]
        public int CycleId { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public int Score { get; set; }
        public int Status { get; set; } = 1;
        public virtual Cycle Cycle { get; set; }

    }

    public class MarkEntryFlag
    {
        public int Id { get; set; }
        [ForeignKey(nameof(CycleId))] public int CycleId { get; set; }
        public bool Training { get; set; } = false;
        public bool Internship { get; set; } = false;
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public bool Assessment { get; set; }
        public bool Graduation { get; set; }
        public int Status { get; set; } = 1;
        public virtual Cycle Cycle { get; set; }
    }
}