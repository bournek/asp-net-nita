﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum PaymentType
    {
        ReceivePayments, MakePayments, AvailbleForBanking
    }
    public class PaymentAction
    {
        public int Id { get; set; }
        public string Caption { get; set; }
        public PaymentType Type { get; set; }
    }
    public class PaymentAccount
    {
        public int Id { get; set; }
        public int SOAPCredentialsId { get; set; }
        public int LedgerId { get; set; }
        public int AccountId { get; set; }
        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public DateTime DateCreated { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public PayMode Mode { get; set; } = PayMode.Cheque;
        public string BankTransferPayeeName { get; set; }
        public string Description { get; set; }
        public bool EFTEnabled { get; set; }
        public virtual SOAPServiceCredentials SOAPCredentials { get; set; }
        public virtual FinanceLedger Ledger { get; set; }
        public virtual FinanceAccount GLAccount { get; set; }
        public virtual ICollection<ExpenseClaimDisbursement> ECDisbursements { get; set; }
        public virtual ICollection<PaymentAction> ApprovedActions { get; set; }
        public virtual ICollection<FPaymentVoucher> PaymentVouchers { get; set; }
        public PaymentAccount()
        {
            ApprovedActions = new List<PaymentAction>();
            PaymentVouchers = new List<FPaymentVoucher>();
            ECDisbursements = new List<ExpenseClaimDisbursement>();
            DateCreated = DateTime.Now;
        }
    }
    public class SOAPServiceCredentials
    {
        public int Id { get; set; }
        public int ModeId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public virtual PaymentAccount PaymentAccount { get; set; }
    }
}
