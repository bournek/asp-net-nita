﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public class Currency
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public bool IsBase { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual ICollection<FPayment> Payments { get; set; }

        public Currency()
        {
            Payments = new List<FPayment>();
        }
    }
}
