﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public class PVoucherPrefix
    {
        public int Id { get; set; }
        public int LedgerId { get; set; }
        public string Code { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual FinanceLedger Ledger { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public PVoucherPrefix()
        {
            DateCreated = DateTime.Now;
        }
    }
}
