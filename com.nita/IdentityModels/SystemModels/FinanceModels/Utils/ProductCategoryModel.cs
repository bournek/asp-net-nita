﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 using   Nita.IdentityModels;
namespace com.nita.IdentityModels
{
    public class FProductCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public EntityStatus Status { get; set; }
        
    }
}
