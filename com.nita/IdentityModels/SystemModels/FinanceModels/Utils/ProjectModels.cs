﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public class FProject
    {
        public int Id { get; set; }
        public int LedgerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public double Amount { get; set; }
        public string ProjectNo { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double CompletedPercentage { get; set; }

        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual FinanceLedger Ledger { get; set; }
        public virtual ICollection<FinanceAccount> Accounts { get; set; }
        public virtual ICollection<StaffProfile> Coordinators { get; set; }
        public virtual ICollection<TraineeProfile> Beneficiaries { get; set; }
        public virtual ICollection<FInvoice> Invoices { get; set; }
        public virtual ICollection<ImprestWarrant> ImprestWarrants { get; set; }
        public virtual ICollection<PettyCashReq> PettyCashReqs { get; set; }
        public virtual ICollection<ExpenseClaim> Claims { get; set; }
        public virtual ICollection<PaymentJournal> CreditJournals { get; set; }
        public virtual ICollection<PaymentJournal> DebitJournals { get; set; }
        public FProject()
        {
            Invoices = new List<FInvoice>();
            Coordinators = new List<StaffProfile>();
            Beneficiaries = new List<TraineeProfile>();
            Claims = new List<ExpenseClaim>();
            CreditJournals = new List<PaymentJournal>();
            DebitJournals = new List<PaymentJournal>();
        }
    }
}
