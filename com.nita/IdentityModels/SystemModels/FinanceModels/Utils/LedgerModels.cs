﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public class FinanceLedger
    {
        public int Id { get; set; }
        public int CurrencyId { get; set; }
        public string Name { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual Currency Currency { get; set; }
        public virtual ICollection<PaymentAccount> PaymentAccounts { get; set; }
        public virtual ICollection<PVoucherPrefix> PVoucherPrefixes { get; set; }
        public virtual ICollection<FInvoice> Invoices { get; set; }
        public virtual ICollection<FPaymentVoucher> PaymentVouchers { get; set; }
        public virtual ICollection<FProject> Projects { get; set; }
        public virtual ICollection<ImprestWarrant> ImprestWarrants { get; set; }
        public virtual ICollection<PettyCashReq> PettyCashReqs { get; set; }
        public virtual ICollection<ExpenseClaim> Claims { get; set; }
        public virtual ICollection<PaymentJournal> Journals { get; set; }
        public FinanceLedger()
        {
            PaymentAccounts = new List<PaymentAccount>();
            PVoucherPrefixes = new List<PVoucherPrefix>();
            Invoices = new List<FInvoice>();
            PaymentVouchers = new List<FPaymentVoucher>();
            PettyCashReqs = new List<PettyCashReq>();
            Claims = new List<ExpenseClaim>();
            Journals = new List<PaymentJournal>();
        }
    }
}
