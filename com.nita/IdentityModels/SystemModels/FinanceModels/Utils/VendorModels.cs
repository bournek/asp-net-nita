﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    /// <summary>
    /// Vendor Type
    /// </summary>
    public class FVendorType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
    }
}
