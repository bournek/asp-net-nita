﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public class FiscalYear
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CloseDate { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual ICollection<FiscalYearQuarter> Quarters { get; set; }
        public virtual ICollection<BudgetPeriod> BudgetPeriods { get; set; }
        public FiscalYear()
        {
            Quarters = new List<FiscalYearQuarter>();
            BudgetPeriods = new List<BudgetPeriod>();
        }
    }

    public class FiscalYearQuarter
    {
        public int Id { get; set; }
        public int FiscalYearId { get; set; }
        public string Name { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public virtual FiscalYear FiscalYear { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
    }
}
