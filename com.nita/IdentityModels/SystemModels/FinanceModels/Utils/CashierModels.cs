﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    /// <summary>
    /// Cashier Office
    /// </summary>
    public class FCashier
    {
        public int Id { get; set; }
        public int DeptId { get; set; }
        public int LocationId { get; set; }
        //
        public string Name { get; set; }
        public int RefNo { get; set; }
        public virtual Location Location { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public DateTime DateCreated { get; set; }
        public virtual Department Department { get; set; }
        public virtual ICollection<FCAttendantProfile> Attendants { get; set; }
        public virtual ICollection<PaymentAccount> PaymentAccounts { get; set; }
        public virtual ICollection<FPaymentVoucher> PaymentVouchers { get; set; }
        public FCashier()
        {
            DateCreated = DateTime.Now;
            PaymentAccounts = new List<PaymentAccount>();
            PaymentVouchers = new List<FPaymentVoucher>();
            Attendants = new List<FCAttendantProfile>();
        }
    }
    public class FCAttendantProfile
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public int PersonnelId { get; set; }
        public int OfficeId { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual StaffProfile Staff { get; set; }
        public virtual FCashier Office { get; set; }
        public virtual StaffProfile Personnel{ get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public FCAttendantProfile()
        {
            DateCreated = DateTime.Now;
        }
    }
}
