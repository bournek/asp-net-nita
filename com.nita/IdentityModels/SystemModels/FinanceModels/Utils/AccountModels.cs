﻿using Nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum FAccountType
    {
        Asset =90,Liability = 91,Income = 92,Expense = 93,Equity = 94
    }
    public enum GLTransactionType
    {
        /// <summary>
        /// Increaments in value
        /// </summary>
        Debit = 776,
        /// <summary>
        /// Decreases in value
        /// </summary>
        Credit = 777
    }
    public enum GLTransactionKind
    {
        Regular,Tax, ExpenseClaimDisbursement, Journal,Unspecified
    }
    public class FAccountTypeRules
    {
        public int Id { get; set; }
        public FAccountType Type { get; set; }

    }
    /// <summary>
    /// 
    /// </summary>
    public class FinanceAccount
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public int ModeId { get; set; }
        //
        public string Number { get; set; }
        public string Name { get; set; }
        public bool HasBudget { get; set; }
        public FAccountType Type { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual FAccountClassification Classification { get; set; }
        public virtual FAccountGroup Group { get; set; }
        public virtual ICollection<FinanceBudget> Budgets { get; set; }
        public virtual FinanceAccount Parent { get; set; }//Optional
        public virtual PayAccount PayAccount { get; set; }//~ To a pay account
        /// <summary>
        /// Employer Contribution GL Account
        /// </summary>
        public virtual ICollection<PayAccount> ECPayAccounts { get; set; }//
        public virtual ICollection<FinanceTax> Taxes { get; set; }
        public virtual ICollection<PaymentAccount> PaymentAccounts { get; set; }
        public virtual ICollection<FinanceAccount> Children { get; set; }
        /// <summary>
        /// All Payments made againist this account. POSTED & Pending
        /// </summary>
        public virtual ICollection<FPayment> Payments { get; set; }
        /// <summary>
        /// All Payments made againist this account. POSTED & Pending
        /// </summary>
        public virtual ICollection<InvoicePayment> Receipts { get; set; }
        /// <summary>
        /// Scheduled Irregular payments under this GL account
        /// </summary>
        public virtual ICollection<IrregularEarning> IrregularPayments { get; set; }//
        //MISC.
        public virtual ICollection<GLTransaction> GLRecords { get; set; }
        public virtual ICollection<PaymentJournal> CreditJournals { get; set; }
        public virtual ICollection<PaymentJournal> DebitJournals { get; set; }
        public FinanceAccount()
        {
            Children = new List<FinanceAccount>();
            PaymentAccounts = new List<PaymentAccount>();
            IrregularPayments = new List<IrregularEarning>();
            GLRecords = new List<GLTransaction>();
            CreditJournals = new List<PaymentJournal>();
            DebitJournals = new List<PaymentJournal>();
        }
    }
    public class FAccountGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class FAccountSubGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class FAccountClassification
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class FPayment
    {
        public int Id { get; set; }
        public int CurrencyId { get; set; }

        //Optionals
        public int? VoucherId { get; set; }
        public int? AccountId { get; set; }
        public int? InvoiceId { get; set; }
        public int? WaiverId { get; set; }
        public int? EarningId { get; set; }
        public int? DeptId { get; set; }
        public int? WarrantId { get; set; }
        public int? ReqId { get; set; }
        public int? ClaimId { get; set; }
        public int? JournalId { get; set; }
        //
        public double GrossAmount { get; set; }
        public double Discount { get; set; }
        public string PayableTo { get; set; }
        public DateTime DateCreated { get; set; }
        public string PayeeName { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Deleted;
        public virtual ImprestWarrant ImprestWarrant { get; set; }
        public virtual ExpenseClaim ExpenseClaim { get; set; }
        public virtual ICollection<PaymentAccount> PaymentAccounts { get; set; }//May have more than one check outs(for regular, tax...)
        public virtual FinanceAccount GLAccount { get; set; }//Associated GL Account
        public virtual Department Department { get; set; }//Associated Department
        public virtual FPaymentVoucher Voucher { get; set; }
        public virtual FInvoice Invoice { get; set; } //Associated Invoice
        public virtual IrregularEarning IrregularEarning { get; set; }
        public virtual PettyCashReq PettyCashReq { get; set; }
        public virtual Currency CurrencyUsed { get; set; }
        public virtual SupplierInvoiceWaiver InvoiceWaiver { get; set; }
        public virtual PaymentJournal Journal { get; set; }
        /// <summary>
        /// GL Transaction posted to Legder
        /// </summary>
        public virtual ICollection<GLTransaction> GLTransactions { get; set; }//Associated Irregular Earnings
        public virtual ICollection<FinanceTax> Taxes { get; set; } //Applicable taxes to this payment
        public FPayment()
        {
            Taxes = new List<FinanceTax>();
            GLTransactions = new List<GLTransaction>();
            PaymentAccounts = new List<PaymentAccount>();
            DateCreated = DateTime.Now;
        }
    }
    public class GLTransaction
    {
        public int Id { get; set; }
        public int PaymentId { get; set; }
        /// <summary>
        /// Double Entry Account NP for this entry
        /// </summary>
        public int? AccountId { get; set; }// Nav Prop
        public GLTransactionType Type { get; set; }
        public EntityStatus Status { get; set; }
        public GLTransactionKind Kind { get; set; }
        public double Amount { get; set; }//
        public DateTime DateEntered { get; set; }
        public virtual FPayment Payment { get; set; }
        /// <summary>
        /// Double Entry Account for the payment
        /// </summary>
        public virtual FinanceAccount Account { get; set; }//Credited Account
        public GLTransaction()
        {
            DateEntered = DateTime.Now;
            Status = EntityStatus.Active;
        }
    }
}
