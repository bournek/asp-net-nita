﻿using Nita.IdentityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum TaxType
    {
        Retention,Full_V_A_T,IncomeTaxWithholding,V_A_T_Withholding
    }
    public enum RoundOffType
    {
        Round,Round_Up,Round_Down
    }
    public class TaxScheme
    {
        public int Id { get; set; }
        public TaxType Type { get; set; }
        public string Name { get; set; }
    }
    public class FinanceTax
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public TaxType Type { get; set; }
        public RoundOffType RoundType { get; set; }
        public string Name { get; set; }
        public double Rate { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual FinanceAccount GLAccount { get; set; }
        public virtual ICollection<TaxScheme> TaxSchemes { get; set; }
        public virtual ICollection<FPaymentVoucher> TaxVouchers { get; set; }
        public FinanceTax()
        {
            TaxSchemes = new List<TaxScheme>();
            TaxVouchers = new List<FPaymentVoucher>();
        }
    }
}
