﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum InvoiceTerm
    {
        Due_On_Receipt, Due_In_30_Days, Due_In_45_Days, Due_In_60_Days, Due_On_Day_Of_Next_Month, Due_At_The_End_Of_Month,Custom
    }
    public enum PVStatus
    {
        Pending=9890,Posted=3456,Cancelled = 45
    }
    public enum PVType
    {
        Regular = 113, PettyCash = 114, Imprest = 117, ExpenseClaim = 120, IrregularPayments = 124, Tax = 126
    }
    public enum RecoveryPolicy
    {
        SalaryDeduction,PropertyConfication
    }
    public enum JournalType
    {
        Payables = 990, Payroll = 993
    }
    public class FPaymentVoucher
    {
        public int Id { get; set; }
        public int LedgerId { get; set; }
        public int? ProjectId { get; set; }
        public int? ModeId { get; set; }
        public int? TaxId { get; set; }
        public int? SupplierId { get; set; }
        public int? SponsorId { get; set; }//
        public int? IWDisbursementId { get; set; }
        public int? ECDisbursementId { get; set; }
        public int? CashierId { get; set; }
        public int PersonnelId { get; set; }//Nita Staff
        /// <summary>
        /// Nita Staff who completed the payment
        /// </summary>
        public int? PostingPersonnelId { get; set; }//
        //
        public string VoucherNo{get;set;}
        public double MgmtAmount { get; set; }
        public string Description { get; set; }
        public string CheckNo { get; set; }//
        public DateTime VoucherDate { get; set; }
        public DateTime? PayDate { get; set; }
        public PVStatus Status { get; set; }
        public PVType Type { get; set; }
        /// <summary>
        /// Used againist payments with auto posting
        /// </summary>
        public string PaymentAccNo { get; set; }
        /// <summary>
        /// Tax that was applied on a 'Tax Voucher'
        /// </summary>
        public virtual FinanceTax TaxPaid { get; set; }
        /// <summary>
        /// Account used to withdraw moneis from GL Account
        /// </summary>
        public virtual PaymentAccount PaymentAccount { get; set; }
        public virtual FSponsor Sponsor { get; set; }
        /// <summary>
        /// Paid from cashier(Cash Payments)
        /// </summary>
        public virtual FCashier Cashier { get; set; }
        /// <summary>
        /// Paid to supplier (Cheques,EFT,..)
        /// </summary>
        public virtual FSupplier Supplier { get; set; }
        public virtual FinanceLedger Ledger { get; set; }
        public virtual StaffProfile Personnel { get; set; }
        public virtual StaffProfile PostingPersonnel { get; set; }
        public virtual ExpenseClaimDisbursement ECDisbursement { get; set; }
        public virtual ImprestDisbursement IWDisbursement { get; set; }
        /// <summary>
        /// Payments included in this voucher
        /// </summary>
        public virtual ICollection<FPayment> Payments { get; set; }

        public FPaymentVoucher()
        {
            Payments = new List<FPayment>();
            VoucherDate = DateTime.Now;
            Status = PVStatus.Pending;
        }
    }
    public class FInvoice
    {
        public int Id { get; set; }
        public int LedgerId { get; set; }
        public int? ProjectId { get; set; }
        public int SupplierId { get; set; }
        public int PersonnelId { get; set; }
        //
        public string OrderNo { get; set; }
        public int DocNo { get; set; }
        public double Amount { get; set; }
        public InvoiceTerm InvoiceTerm { get; set; }
        public DateTime InvoiceDate { get; set; }
        public InvoiceTerm Term { get; set; }
        public DateTime DueDate { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual FinanceLedger Ledger { get; set; }
        public virtual FProject Project { get; set; }
        public virtual FSupplier Supplier { get; set; }
        public virtual StaffProfile Personnel { get; set; }
        public virtual ICollection<FPayment> Payments { get; set; }
        public virtual ICollection<SupplierInvoiceWaiver> SupplierInvoiceWaivers { get; set; }
        public FInvoice()
        {
            Payments = new List<FPayment>();
            SupplierInvoiceWaivers = new List<SupplierInvoiceWaiver>();
        }
    }
    public class SupplierInvoiceWaiver
    {
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public int PaymentId { get; set; }
        public int PersonnelId { get; set; }
        public int RefNo { get; set; }
        public string Description { get; set; }
        public double GrossAmount { get; set; }
        public DateTime DateCreated { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual FInvoice Invoice { get; set; }
        public virtual FPayment Payment { get; set; }
        public virtual StaffProfile Personnel { get; set; }
        public SupplierInvoiceWaiver()
        {
            DateCreated = DateTime.Now;
        }
    }
    public class FSupplier
    {
        public int Id { get; set; }
        public int LocationId { get; set; }
        public int RefNo { get; set; }
        public string Name { get; set; }
        public int VendorTypeId { get; set; }
        public string Address { get; set; }
        public string TelNo { get; set; }
        public string Ext { get; set; }
        public string CellNo { get; set; }
        public string FaxNo { get; set; }
        public string Email { get; set; }

        //
        public string KRAPIN { get; set; }
        public string VATNo { get; set; }

        //
        public int BankCode { get; set; }
        public string BankAccount { get; set; }
        public bool IsActive { get; set; }
        //Options
        public double Discount { get; set; }
        public bool ExemptFromTax { get; set; }
        public bool HoldPayments { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual FVendorType VendorType { get; set; }
        public virtual Location Location { get; set; }
        public virtual ICollection<FInvoice> Invoices { get; set; }
        public virtual ICollection<FPaymentVoucher> PaymentVouchers { get; set; }
        public FSupplier()
        {
            Invoices = new List<FInvoice>();
            PaymentVouchers = new List<FPaymentVoucher>();
        }
    }
        
    public class FSponsor{
        public int Id { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual ICollection<FPaymentVoucher> Vouchers { get; set; }
        public string Name { get; set; }
    }

    public class ImprestWarrant
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public int LedgerId { get; set; }
        public int? ProjectId { get; set; }
        public int PersonnelId { get; set; }

        //
        public int Ref { get; set; }
        public string OrderNo { get; set; }
        public string NatureOfDuty { get; set; }
        public string ProposedItinerary { get; set; }
        public string Description { get; set; }
        public int EstimatedDaysAway { get; set; }
        public DateTime SurrenderDate { get; set; }
        public DateTime CreateDate { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual StaffProfile Staff { get; set; }
        public virtual FinanceLedger Ledger { get; set; }
        public virtual FProject Project { get; set; }
        public virtual StaffProfile Personnel { get; set; }
        public virtual ImprestDisbursement Disbursement { get; set; }
        public virtual ICollection<FPayment> Payments { get; set; }
        public virtual ICollection<ExpenseClaim> ExpenseClaims { get; set; }
        public ImprestWarrant()
        {
            Payments = new List<FPayment>();
            ExpenseClaims = new List<ExpenseClaim>();
        }
    }
    public class ImprestDisbursement
    {
        public int Id { get; set; }
        public int? SurrenderId { get; set; }
        //
        public int Ref { get; set; }
        public int? WarrantId { get; set; }
        public int PersonnelId { get; set; }
        public string Description { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public RecoveryPolicy RecoveryPolicy { get; set; }
        public DateTime DateCreated { get; set; }
        //
        public virtual ImprestSurrender Surrender { get; set; }
        public virtual ImprestWarrant Warrant { get; set; }
        public virtual StaffProfile Personnel { get; set; }
        public virtual ICollection<FPaymentVoucher> Vouchers { get; set; }
        public ImprestDisbursement()
        {
            DateCreated = DateTime.Now;
            RecoveryPolicy = RecoveryPolicy.SalaryDeduction;
            Vouchers = new List<FPaymentVoucher>();
        }
    }
    public class ImprestSurrender
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public int DisbursementId { get; set; }
        public int? ModeId { get; set; }
        public int PersonnelId { get; set; }
        //
        public DateTime DateSurrendered { get; set; } = DateTime.Now;
        public double AmountSpent { get; set; }
        public string ModeAccNumber { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        //
        public virtual PaymentAccount PaymentAccount { get; set; }//Optional
        public virtual StaffProfile Staff { get; set; }
        public virtual StaffProfile Personnel { get; set; }
        public virtual ImprestDisbursement Disbursement { get; set; }//Surrendering
    }
    public class ExpenseClaim
    {
        public int Id { get; set; }
        public int MileageId { get; set; }
        public int SubstainanceId { get; set; }
        public int StaffId { get; set; }
        public int LedgerId { get; set; }
        public int? ProjectId { get; set; }
        public int? WarrantId { get; set; }
        public int PersonnelId { get; set; }
        public int? DisbursementId { get; set; }
        //
        public int Ref { get; set; }
        public string OrderNo { get; set; }
        public string DutyCalled { get; set; }
        public double Amount { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual FinanceLedger Ledger { get; set; }
        public virtual ImprestWarrant Imprest { get; set; }
        public virtual FProject Project { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual Substainance Substainance { get; set; }
        public virtual MileageExpense Mileage { get; set; }
        public virtual StaffProfile Staff { get; set; }
        public virtual StaffProfile Personnel { get; set; }
        public virtual ExpenseClaimDisbursement Disbursement { get; set; }
        public virtual ICollection<FPayment> Payments { get; set; }
        public virtual ICollection<ClaimEntry> ClaimEntries { get; set; }
        public ExpenseClaim()
        {
            DateCreated = DateTime.Now;
            ClaimEntries = new List<ClaimEntry>();
            Payments = new List<FPayment>();
        }
    }
    public class MileageExpense
    {
        public int Id { get; set; }
        public int ClaimId { get; set; }
        public string RegNo { get; set; }
        public string DestFrom { get; set; }
        public string DesTo { get; set; }
        public double Distance { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual ExpenseClaim Claim { get; set; }
    }
    public class Substainance
    {
        public int Id { get; set; }
        public int ClaimId { get; set; }
        public int Days { get; set; }
        public double Rate { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual ExpenseClaim Claim { get; set; }
    }
    public class ClaimEntry
    {
        public int Id { get; set; }
        public int ExpenseId { get; set; }
        public int CurrencyId { get; set; }
        public double Quantity { get; set; }
        public string UnitName { get; set; }
        public double UnitCost { get; set; }
        public DateTime? DateIncured { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual ExpenseClaim Claim { get; set; }
    }
    public class ExpenseClaimDisbursement
    {
        public int Id { get; set; }
        public int ClaimId { get; set; }
        public int ModeId { get; set; }
        public int PersonnelId { get; set; }
        public DateTime DateDisbursed { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual StaffProfile Personnel { get; set; }
        public virtual ExpenseClaim Claim { get; set; }
        public virtual PaymentAccount PaymentAccount { get; set; }
        public virtual ICollection<FPaymentVoucher> Vouchers { get; set; }
        public ExpenseClaimDisbursement()
        {
            DateDisbursed = DateTime.Now;
            Vouchers = new List<FPaymentVoucher>();
        }
    }
    public class PettyCashReq
    {
        public int Id { get; set; }
        //
        public int? StaffId { get; set; }
        public int PersonnelId { get; set; }
        public int? TraineeId { get; set; }
        public int? TrainerId { get; set; }
        public int? ProjectId { get; set; }
        public int LedgerId { get; set; }
        public int DisbursementId { get; set; }
        //
        public int Ref { get; set; }
        public string Description { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public RecoveryPolicy RecoveryPolicy { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual FProject Project { get; set; }
        public virtual FinanceLedger Ledger { get; set; }
        public virtual TrainerProfile Trainer { get; set; }
        public virtual TraineeProfile Trainee { get; set; }
        public virtual StaffProfile Staff { get; set; }
        public virtual StaffProfile Personnel { get; set; }
        public virtual PettycashDisbursement Disbursement { get; set; }
        public virtual ICollection<FPayment> Payments { get; set; }
        public PettyCashReq()
        {
            Payments = new List<FPayment>();
            DateCreated = DateTime.Now;
        }
    }
    public class PettycashDisbursement
    {
        public int Id { get; set; }
        public int RequestId { get; set; }
        public int PersonnelId { get; set; }
        public string Desc { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateDisbursed { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual StaffProfile Personnel { get; set; }
        public virtual PettyCashReq Request { get; set; }
        public PettycashDisbursement()
        {
            DateCreated = DateTime.Now;
        }
    }
   
    public class PaymentJournal
    {
        public int Id { get; set; }
        public int LedgerId { get; set; }

        //Debit
        public int? DebitProjectId { get; set; }
        public int DebitDeptId { get; set; }
        public int DebitAccountId { get; set; }

        //Credit
        public int? CreditProjectId { get; set; }
        public int CreditDeptId { get; set; }
        public int CreditAccountId { get; set; }

        //
        public JournalType Type { get; set; }
        public int Ref { get; set; }
        public double Amount{ get; set; }
        public string Reference { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public bool Revaluation { get; set; }

        //
        public virtual FProject DebitProject { get; set; }
        public virtual FProject CreditProject { get; set; }

        //
        public virtual FinanceAccount DebitAccount { get; set; }
        public virtual FinanceAccount CreditAccount { get; set; }

        //
        public virtual Department DebitDept { get; set; }
        public virtual Department CreditDept { get; set; }

        //
        public virtual FinanceLedger Ledger { get; set; }
        //
        public virtual ICollection<FPayment> Payments { get; set; }
        public PaymentJournal()
        {
            Payments = new List<FPayment>();
        }
    }
}
