﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum PayeeType
    {
        Trainee,Others
    }
    public class ReceivePayment
    {
        public int Id { get; set; }
        public int LedgerId { get; set; }
        public int ModeId { get; set; }//Payment Mode Id
        public double Amount { get; set; }
        public string ReferenceNo { get; set; }
        public PayeeType ReceivedFrom { get; set; }
        public DateTime ReceiptDate { get; set; }
        public virtual FinanceLedger Ledger { get; set; }
        public virtual PaymentAccount PaymentAccount { get; set; }
        public virtual OtherPaymentDetail MoreDetails { get; set; }
    }

    public class OtherPaymentDetail
    {
        public int Id { get; set; }
        public int PaymentId { get; set; }
        public virtual Department Department { get; set; }
        public virtual FinanceAccount Account { get; set; }
        public virtual ReceivePayment Payment { get; set; }
    }
}
