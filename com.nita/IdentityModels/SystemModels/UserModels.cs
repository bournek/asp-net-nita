﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum Gender {
        Male = 1, Female = 2, Others = 3
    }
    public enum MaritalStatus
    {
        Single = 72, Married = 56, Diforced = 65, Enganged = 90, Windowed = 78, Unspecified
    }
    public enum AdminType
    {
        Nita = 90, Abno = 99
    }    

    public enum TrainerType
    {
        Institution = 445, MasterCraftman = 75
    }
    public enum Disability
    {
        NONE, Communication, Earing,Intellectual,Learning,MentalHealth,Physical,Medical,Vision,Others
    }
    public enum Title
    {
        Mr,Miss,Mrs,Rev,Hon,Sr,Other
    }
    public enum Religion
    {
        Christian,Muslim,Hindu,Other
    }
    public enum TrainerStatus
    {
        Active=12,Inactive=13,Terminated=14
    }
    public class UserType
    {
        public int Id { get; set; }
        public int Name { get; set; }
        //[Index(IsUnique = true)]
        public int Index { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }

        public UserType()
        {
            Users = new List<ApplicationUser>();
        }
    }
    public class TraineeProfile
    {
        public int Id { get; set; }
        public int? ApplicationUserId { get; set; }
        public int AddressId { get; set; }

        public int? TrainerId { get; set; }
        public int? ProgrammeId { get; set; }

        public int BankAccId { get; set; }
        public int IrPaymentProfileId { get; set; }
      
        public string AdminNo { get; set; }
        public string FullName { get; set; }
        public string PassportNo { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        public MaritalStatus Marital { get; set; }
        public bool Closed { get; set; }
        public DateTime DateClosed { get; set; }
        public string CloseReason { get; set; }
        public string Disability { get; set; }

        //public int ? TraineeAccountId { get; set; }
        public string Domicile { get; set; }
        public virtual BankAccount BankAccount { get; set; }
        public virtual UserAddress Address { get; set; }
        //public virtual  TradeArea Programme { get; set; }
        //public virtual TrainerProfile Trainer { get; set; }

        public virtual TraineeAccount Trainee  { get; set; }
       public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual TradeArea Programme { get; set; }
        public virtual TrainerProfile Trainer { get; set; }
        public virtual IrregularPaymentProfile IrPaymentProfile { get; set; }
        public virtual ICollection<TraineeAttendance> Attendance { get; set; }
        public virtual ICollection<IrregularEarning> IrrEarnings { get; set; }
        public virtual ICollection<PettyCashReq> PettyCashReqs { get; set; }
        public TraineeProfile()
        {     
            DateOfBirth = DateTime.Now.AddYears(-18);
            IrrEarnings = new List<IrregularEarning>();
            PettyCashReqs = new List<PettyCashReq>();
            Attendance = new List<TraineeAttendance>();
        }
    }

    public class TraineeAccount
    {
        public int Id { get; set; }
        public int? TraineeId { get; set; }
        public string AccountName { get; set; }
        public int BankId { get; set; }
        public string AccountNumber  { get; set; }
        public int BankBranchId { get; set; }
        public bool Status { get; set; } = true;
         public virtual TraineeProfile TraineeProfile { get; set; }

    }

    public class TrainerEmployer {
        public int Id { get; set; }
        public int TrainerId { get; set; }
        public string Name { get; set; }
        public int CountyId { get; set; }
        public string Industry { get; set; }
        public string Notes { get; set; }
    }

    public class LeadTrainerProfile {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string IdNumber { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
    public class TrainerProfile
    {
        public int Id { get; set; }

        //References
        //public int DivisionId { get; set; }
        public int ApplicationUserId { get; set; }
        public int AddressId { get; set; }
        public int? PersonnelId { get; set; }
        public int BankAccId { get; set; }
        public int IrPaymentProfileId { get; set; }
        public string IdNo { get; set; }
        public string Position { get; set; }//Designation Held
        public string ContactPersonName { get; set; }
        public string FullName { get; set; }
        public string EnterpriseName { get; set; }
        public string PostalAddress { get; set; }
        public string ContactEmail { get; set; }
        public DateTime DateCreated { get; set; }
        public TrainerType Type { get; set; }
        public TrainerStatus Status { get; set; } = TrainerStatus.Inactive;
        public virtual IrregularPaymentProfile IrrPaymentProfile { get; set; }
        //public bool ContactAssignment { get; set; }
        public virtual ICollection<TrainerContract> TrainerContracts { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<TradeArea> Areas { get; set; }
        public virtual ApplicationUser Personnel { get; set; }//Profile creator
        public virtual UserAddress Address { get; set; }
        public virtual BankAccount BankAccount { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual ICollection<IrregularEarning> IrEarnings { get; set; }
        public virtual ICollection<TraineeProfile> Trainees { get; set; }
        public virtual ICollection<EmployerProfile> Employers { get; set; }
        public virtual ICollection<PettyCashReq> PettyCashReqs { get; set; }
        public string Telephone { get; set; }

        public TrainerProfile()
        {
            Trainees = new List<TraineeProfile>();
            Employers = new List<EmployerProfile>();
            PettyCashReqs = new List<PettyCashReq>();
        }
    }

    public class Cycle {
        public int Id { get; set; }
        public string CycleName { get; set; }
        public int CycleCapacity { get; set; }
        public DateTime CycleFrom { get; set; }
        public DateTime CycleTo { get; set; }
        public int FYId { get; set; }
        public virtual ICollection<InternshipScore> Scores { get; set; }
        public virtual ICollection<FinanceBudget> Budgets { get; set; }

        public Cycle()
        {
            Scores = new List<InternshipScore>();
            Budgets = new List<FinanceBudget>();
        }
    }

    public class BudgetPeriod
    {
        public int Id { get; set; }
        public string PeriodName { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public int FYId { get; set; }
        public virtual ICollection<FinanceBudget> Budgets { get; set; }
    }


    public class StaffProfile
    {
        public int Id { get; set; }

        //References
        public int? DivisionId { get; set; }
        public int? SectionId { get; set; }
        public int ApplicationUserId { get; set; }
        public int AddressId { get; set; }
        public int? PersonnelId { get; set; }
        public int? SupervisorId { get; set; }
        public int? CashierId { get; set; }
        public int? LocationId { get; set; }
        public int JobCatId { get; set; }
        public int EmpCatId { get; set; }
        public int LeaveGroupId { get; set; }
        public int TerminationId { get; set; }
        public int DesignationId { get; set; }
        public int? AcademicProfileId { get; set; }
        public int BankAccId { get; set; }
        //
        public string FullName { get; set; }
        public string EmplNo { get; set; }
        public string PassportId { get; set; }
        public DateTime Hiredate { get; set; }
        public DateTime Dob { get; set; }
        public MaritalStatus Marital { get; set; }
        public string KraPin { get; set; }
        public string Nhif { get; set; }
        public string Nssf { get; set; }
        public string Race { get; set; }
        public Religion Religion { get; set; }
        public string SpouseName { get; set; }
        public bool IsSmoker { get; set; }
        public string Disability { get; set; }
        public string Visa { get; set; }
        public DateTime VisaExp { get; set; }
        public string Ppn { get; set; }
        public DateTime Ppnexp { get; set; }
        public string LicNo { get; set; }
        public string LicClass { get; set; }
        public DateTime LicExp { get; set; }
        public DateTime InsExp { get; set; }
        public bool Terminated { get; set; }
        public string Notes { get; set; }
        public string Exp { get; set; }
        public string Educ { get; set; }
        public string Skills { get; set; }
        public string Lang { get; set; }
        public string Medical { get; set; }
        public string EmgName { get; set; }
        public string EmgRel { get; set; }
        public string EmgTel { get; set; }
        public string EmgNotes { get; set; }
        public DateTime Rdate { get; set; }
        public Title Title { get; set; }
        public DateTime Adate { get; set; }
        public virtual BankAccount BankAccount { get; set; }
        public virtual AcademicProfile AcademicProfile { get; set; }
        public virtual HrJobCatalogue JobCatalogue { get; set; }
        public virtual EmployeeCategory EmpCat { get; set; }
        public virtual StaffDesignation Designation { get; set; }
        public virtual LeaveGroup LeaveGroup { get; set; }
        public virtual Section Section { get; set; }
        public virtual Division Division { get; set; }
        public virtual ApplicationUser Personnel { get; set; }
        public virtual StaffProfile Supervisor { get; set; }
        public virtual UserAddress Address { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual StaffTermination Termination { get; set; }
        /// <summary>
        /// Posted to our office at..
        /// </summary>
        public virtual Location Location { get; set; }
        public virtual ICollection<FCAttendantProfile> CashierProfiles { get; set; }//For cashier attendants
        public virtual ICollection<StaffProfile> Team { get; set; }
        public virtual ICollection<StaffDependant> Dependants { get; set; }
        public virtual ICollection<ImprestWarrant> ImprestWarrants { get; set; }
        public virtual ICollection<PettyCashReq> PettyCashReqs { get; set; }
        public virtual ICollection<ExpenseClaim> ExpenseClaims { get; set; }

        //PESONNEL ACTIONS
        /// <summary>
        /// Paid Vouchers
        /// </summary>
        public virtual ICollection<FPaymentVoucher> PaidVouchers { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<ImprestDisbursement> DisbursedImprests { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<ExpenseClaimDisbursement> DisbursedEClaims { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<ImprestSurrender> SurrenderedImprests { get; set; }
        public virtual ICollection<SupplierInvoiceWaiver> Waivers { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public StaffProfile()
        {
            Dob = DateTime.Now.AddYears(-18);
            Team = new List<StaffProfile>();
            Dependants = new List<StaffDependant>();

            //
            PettyCashReqs = new List<PettyCashReq>();
            ExpenseClaims = new List<ExpenseClaim>();
            DisbursedImprests = new List<ImprestDisbursement>();
            DisbursedEClaims = new List<ExpenseClaimDisbursement>();
            CashierProfiles = new List<FCAttendantProfile>();
            Waivers = new List<SupplierInvoiceWaiver>();
        }
    }
    public class AdminProfile
    {
        public int Id { get; set; }
        public int ApplicationUserId { get; set; }
        public string FullName { get; set; }
        public AdminType Type { get; set; }
        public bool Closed { get; set; }
        public DateTime DateClosed { get; set; }
        public string ClosureReason { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public AdminProfile()
        {
            Closed = false;
            Type = AdminType.Nita;
        }
    }
    public class EmployerProfile
    {
        public int Id { get; set; }
        public int TrainerId { get; set; }
        public string Name { get; set; }
        public virtual TrainerProfile TrainerProfile { get; set; }
        public virtual ICollection<TradeArea> Areas { get; set; }
        public EmployerProfile()
        {
            Areas = new List<TradeArea>();
        }
    }
    public class UserAddress
    {
        public int Id { get; set; }
        //
        [Required]
        public int CountyId { get; set; }
        public int StaffId { get; set; }
        public int TraineeId { get; set; }
        public int TrainerId { get; set; }
        //
        public string Nationality { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
        public string BuildingName { get; set; }
        public string NearbyLandmark { get; set; }
        public string City { get; set; }
        public string Street1 { get; set; }
        public string PostalCode { get; set; }
        public string PAddress { get; set; }
        public string House { get; set; }
        public string Pager { get; set; }
        public string HomeTel { get; set; }
        public string HomeFax { get; set; }
        public string Web { get; set; }
        public string WTel { get; set; }
        public string ContactNote { get; set; }
        public string WFax { get; set; }
        public string WEmail { get; set; }
        public virtual County County { get; set; }
        public virtual StaffProfile Staff { get; set; }
        public virtual TraineeProfile Trainee { get; set; }
        public virtual TrainerProfile Trainer { get; set; }
    }
}
