﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    /// <summary>
    /// Contact specifications
    /// </summary>
    public class TrainerContract
    {
        public int Id { get; set; }
        //
        public int TrainerId { get; set; }
        public int TerminationId { get; set; }
        //
        public bool Terminated { get; set; }
        public DateTime DateCreated { get; set; }

        public virtual TrainerProfile Trainer { get; set; }

        public virtual ContractTermination Termination { get; set; }
    }
    /// <summary>
    /// Nita staff Job category
    /// </summary>
    public class JobCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<StaffProfile> Employees { get; set; }
        public JobCategory()
        {
            Employees = new List<StaffProfile>();
        }
    }
    /// <summary>
    /// Nita employee category
    /// </summary>
    public class EmployeeCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<StaffProfile> Employees { get; set; }
        public EmployeeCategory()
        {
            Employees = new List<StaffProfile>();
        }
    }
    public class StaffDesignation
    {
        public int Id { get; set; }
        public string Name { get; set; } = "None";//Default
        public bool IsSupervisor { get; set; }
        public virtual ICollection<StaffProfile> Staff { get; set; }
        public StaffDesignation()
        {
            Staff = new List<StaffProfile>();
        }
    }
    /// <summary>
    /// Nita employee leave group
    /// </summary>
    public class LeaveGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    /// <summary>
    /// Nita employee termination type
    /// </summary>
    public class TerminationType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    /// <summary>
    /// Nita staff termination
    /// </summary>
    public class StaffTermination
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public int StaffId { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }
        public virtual TerminationType Type { get; set; }
        public virtual StaffProfile Staff { get; set; }
    }
    /// <summary>
    /// Trainer Contact termination
    /// </summary>
    public class ContractTermination
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public string Reason { get; set; }
        public virtual TrainerContract Contact { get; set; }
    }
}
