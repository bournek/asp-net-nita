﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum DeductionType
    {
        PAYE=13,NHIF=14,AllowableDeduction=15
    }
    public class PayrollDeduction
    {
        public int Id { get; set; }
        public int TaxId { get; set; }
        public string Name { get; set; }
        public DeductionType Type { get; set; }
        public EntityStatus Status { get; set; }
        public virtual ICollection<FinanceTax> Taxes { get; set; }//
        public PayrollDeduction()
        {
            Taxes = new List<FinanceTax>();
        }
    }


}
