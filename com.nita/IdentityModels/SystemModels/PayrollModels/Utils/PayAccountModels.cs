﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum PayType
    {
        Earning = 123, Deduction = 124,Loan = 125,Union = 126,Pension = 127
    }
    public enum PayAccountType
    {
        Regular=9,Irregular=11
    }
    public class PayAccount
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int? ECAccountId { get; set; }
        //
        public string Code { get; set; }
        public string Description { get; set; }
        public double PAYEThreshold { get; set; }
        public PayType PayType { get; set; }
        public PayAccountType AccountType { get; set; }
        public string ITaxField { get; set; }
        public EntityStatus Status { get; set; }
        public virtual FinanceAccount Account { get; set; }
        public virtual FinanceAccount EmployerContributionAccount { get; set; }
        public virtual ICollection<PayrollDeduction> Deductions { get; set; }
        public virtual ICollection<IrregularEarning> IrregularEarnings { get; set; }
        public PayAccount()
        {
            Deductions = new List<PayrollDeduction>();
            IrregularEarnings = new List<IrregularEarning>();
        }
    }
}
