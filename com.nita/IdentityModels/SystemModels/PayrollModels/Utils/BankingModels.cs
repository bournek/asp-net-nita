﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public class FBank
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        public string SwiftCode { get; set; }
        public string Address { get; set; }
        public string TelNo { get; set; }
        public string ContactEmail { get; set; }
        public string Website { get; set; }
        public string FaxNo { get; set; }
        public DateTime DateAdded { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public ICollection<BankBranch> Branches { get; set; }
        public FBank()
        {
            DateAdded = DateTime.Now;
            Branches = new List<BankBranch>();
        }
    }
    public class BankBranch
    {
        public int Id { get; set; }
        public int BankId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
        public string TelNo { get; set; }
        public string ContactEmail { get; set; }
        public string Website { get; set; }
        public string FaxNo { get; set; }
        public DateTime DateAdded { get; set; }
        public virtual FBank Bank { get; set; }
        public virtual ICollection<BankAccount> BAccounts { get; set; }
        public BankBranch()
        {
            DateAdded = DateTime.Now;
            BAccounts = new List<BankAccount>();
        }
    }
    public class BankAccount
    {
        public int Id { get; set; }
        public int BranchId { get; set; }
        public int TrainerId { get; set; }
        public int TraineeId { get; set; }
        public int StaffId { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public DateTime DateAdded { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual BankBranch BankBranch { get; set; }
        public virtual TrainerProfile Trainer { get; set; }
        public virtual TraineeProfile Trainee { get; set; }
        public virtual StaffProfile Staff { get; set; }
        public BankAccount()
        {
            DateAdded = DateTime.Now;
        }
    }
}
