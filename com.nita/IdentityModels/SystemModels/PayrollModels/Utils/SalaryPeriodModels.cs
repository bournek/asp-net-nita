﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum PeriodFrequency
    {
        Monthly = 77,Weekly =78,Irregular =79
    }
    public class SalaryPeriod
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Year { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public PeriodFrequency PayFreq { get; set; }
        public DateTime CloseDate { get; set; }
    }
}
