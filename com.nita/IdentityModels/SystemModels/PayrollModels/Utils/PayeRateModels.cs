﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public class PayeRate
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public double PersonalRelief { get; set; }
        //
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        //
        public virtual ICollection<TaxRate> Rates { get; set; }
        public PayeRate()
        {
            Rates = new List<TaxRate>();
            StartDate = DateTime.Now;
            EndDate = DateTime.Now.AddMonths(1);
        }
    }
    public class TaxRate
    {
        public int Id { get; set; }
        public int PayeId { get; set; }
        public double Min { get; set; }
        public double Max { get; set; }
        public double DeductionAmount { get; set; }
        public virtual PayeRate PayeRate { get; set; }
    }
}
