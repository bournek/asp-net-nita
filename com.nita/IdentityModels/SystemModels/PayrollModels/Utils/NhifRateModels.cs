﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public class NhifRate
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public virtual ICollection<NhifRange> Ranges{ get; set; }
        public NhifRate()
        {
            Ranges = new List<NhifRange>();
        }
    }
    public class NhifRange
    {
        public int Id { get; set; }
        public int RateId { get; set; }
        public double Min { get; set; }
        public double Max { get; set; }
        public double DeductionAmount { get; set; }
        public virtual NhifRate Rate { get; set; }
    }
}
