﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{

    public class IrregularEarning
    {
        public int Id { get; set; }
        public int PaymentId { get; set; }
        public int? TraineeId { get; set; }
        public int? TrainerId { get; set; }
        public int ProjectId { get; set; }
        public int PayAccountId { get; set; }
        //
        public bool IsConsultancy { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual TraineeProfile Trainee { get; set; }
        public virtual TrainerProfile Trainer { get; set; }
        public virtual FPayment Payment { get; set; }
        public virtual PayAccount PayAccount { get; set; }
        public virtual IrregularPaymentProject Project { get; set; }
        public IrregularEarning()
        {
            DateCreated = DateTime.Now;
        }
    }
}
