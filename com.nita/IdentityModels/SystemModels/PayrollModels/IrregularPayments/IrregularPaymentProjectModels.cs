﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum DurationUnit
    {
        Day,Week, Month,Year
    }
    public enum PayMode
    {
        MobileBanking = 554,EFT =555,Cash=556,Cheque=547,FOSA=578,Other
    }
    public class IrregularPaymentProject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public double Rate { get; set; }
        public double Duration { get; set; }
        public DurationUnit Unit { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CloseDate { get; set; }
        public bool Taxable { get; set; }
        public virtual ICollection<IrregularEarning> Earnings { get; set; }
        //public virtual ICollection<FinanceTax> Taxes { get; set; }
        public IrregularPaymentProject()
        {
            Earnings = new List<IrregularEarning>();
            //Taxes = new List<FinanceTax>();
        }
    }

}
