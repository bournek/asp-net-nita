﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    
    public class IrregularPaymentProfile
    {
        public int Id { get; set; }
        public int? TrainerId { get; set; }
        public int? TraineeId { get; set; }
        public double OtherIncome { get; set; }
        public PayMode PayMode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool ExemptTax { get; set; }
        public bool ApplyPayeRelief { get; set; }
        public virtual TrainerProfile Trainer { get; set; }
        public virtual TraineeProfile Trainee { get; set; }
    }
}
