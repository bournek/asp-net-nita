﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public class CurriculumModels
    {

    }

    public class Course
    {
        public int Id { get; set; }
        public string CourseName { get; set; }

        public string CourseDescription { get; set; }
        public string CourseCode { get; set; }
        public int Period { get; set; }

        public DateTime DateCreated { get; set; } = DateTime.Now;
        public int Status { get; set; } = 1;
        public int CreatedBy { get; set; }
        public int Approved { get; set; }


    }

    public class CourseCategories
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public string CategoryCode { get; set; }        
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public int Status { get; set; } = 1;
        public int CreatedBy { get; set; }
        public int Approved { get; set; } = 1;
    }




}