﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum EntityStatus
    {
        Deleted=67, Pending = 69, Inactive =199, Active = 200, Suspended =401, Closed = 550, Cancelled
    }
    public enum DeptType
    {
        Tution = 17, Administration = 77
    }
    /// <summary>
    /// A country Entity
    /// </summary>
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get;set; }
        public virtual ICollection<County> Counties { get; set; }
        public Country()
        {
            Counties = new List<County>();
        }
    }
    /// <summary>
    /// A county entity
    /// </summary>
    public class County
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public virtual Country Country { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        /// <summary>
        /// Collection of office locations
        /// </summary>
        public virtual ICollection<Location> OfficeLocations { get; set; }
        public County()
        {
            OfficeLocations = new List<Location>();
        }
    }
    /// <summary>
    /// A Nita Department
    /// </summary>
    public class Department
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DeptType Type { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public DateTime DateCreated { get; set; }
        public virtual ICollection<Section> Sections { get; set; }
        public virtual ICollection<FinanceBudget> Budgets { get; set; }
        public virtual ICollection<FPayment> Payments { get; set; }
        public virtual ICollection<PaymentJournal> CreditJournals { get; set; }
        public virtual ICollection<PaymentJournal> DebitJournals { get; set; }
        public Department()
        {
            Sections = new List<Section>();
            Budgets = new List<FinanceBudget>();
            Payments = new List<FPayment>();
            CreditJournals = new List<PaymentJournal>();
            DebitJournals = new List<PaymentJournal>();
            DateCreated = DateTime.Now;

        }
    }
    /// <summary>
    /// A Nita Department's Section
    /// </summary>
    public class Section
    {
        public int Id { get; set; }
        public int DeptId { get; set; }
        public string Name { get; set; }
        public virtual Department Department { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual ICollection<Division> Divisions { get; set; }
        public virtual ICollection<StaffProfile> Staff { get; set; }
        public DateTime DateCreated { get; set; }
        public Section()
        {
            Divisions = new List<Division>();
            Staff = new List<StaffProfile>();
            DateCreated = DateTime.Now;
        }
    }
    /// <summary>
    /// A Nita Section's Division
    /// </summary>
    public class Division
    {
        public int Id { get; set; }
        public int SectionId { get; set; }
        public string Name { get; set; }
        public virtual Section Section { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual ICollection<StaffProfile> Staff { get; set; }
        public DateTime DateCreated { get; set; }
        public Division()
        {
            Staff = new List<StaffProfile>();
            DateCreated = DateTime.Now;
        }
    }
    public class Location
    {
        public int Id { get; set; }
        public int CountyId { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual County County { get; set; }
        public virtual LatLng Coodinates { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual ICollection<StaffProfile> Staff { get; set; }
        public virtual ICollection<FCashier> CashierOffices { get; set; }
        public virtual ICollection<FSupplier> Suppliers { get; set; }
        public Location()
        {
            DateCreated = DateTime.Now;
            Staff = new List<StaffProfile>();
            CashierOffices = new List<FCashier>();
            Suppliers = new List<FSupplier>();
        }
    }
    public class LatLng
    {
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
