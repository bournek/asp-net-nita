﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    /// <summary>
    /// Nita employee termination type
    /// </summary>
    public class TerminationType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    /// <summary>
    /// Nita staff termination
    /// </summary>
    public class StaffTermination
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public int StaffId { get; set; }
        public DateTime Date { get; set; }
        public string Notes { get; set; }
        public virtual TerminationType Type { get; set; }
        public virtual StaffProfile Staff { get; set; }
    }
}
