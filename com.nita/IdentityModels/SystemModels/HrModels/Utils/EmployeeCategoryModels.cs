﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    /// <summary>
    /// Nita employee category
    /// </summary>
    public class EmployeeCategory
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public DateTime DateCreated = DateTime.Now;
        public virtual ICollection<StaffProfile> Employees { get; set; }
        public EmployeeCategory()
        {
            Employees = new List<StaffProfile>();
        }
    }
}
