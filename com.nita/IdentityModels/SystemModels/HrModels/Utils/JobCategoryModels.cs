﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    /// <summary>
    /// Nita staff Job category
    /// </summary>
    public class JobCategory
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public DateTime DateCreated = DateTime.Now;
        public virtual ICollection<HrJobCatalogue> HrJobCatalogues { get; set; }
        public JobCategory()
        {
            HrJobCatalogues = new List<HrJobCatalogue>();
        }
    }
}
