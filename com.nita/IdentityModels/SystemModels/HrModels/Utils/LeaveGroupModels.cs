﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{

    /// <summary>
    /// Nita employee leave group
    /// </summary>
    public class LeaveGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
