﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public enum RenumerationType
    {
        Wages, Salary
    }
    public class HrJobCatalogue
    {
        public int Id { get; set; }
        public int JobCatId { get; set; }
        public int RenumerationId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int Capacity { get; set; }//Available slots in organisation
        public DateTime DateCreated { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public virtual JobCategory Category { get; set; }
        public virtual RenumerationScheme Renumeration { get; set; }
        public virtual ICollection<StaffProfile> Employees { get; set; }
        public HrJobCatalogue()
        {
            Employees = new List<StaffProfile>();
            DateCreated = DateTime.Now;
        }
    }
    public class RenumerationScheme
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        //
        public double GrossEarning { get; set; }
        //
        public double MiscDailyRate { get; set; }
        public double MiscHourRate { get; set; }
        public double MiscOvertimeRate { get; set; }
        public double MaxMedExp { get; set; }
        public RenumerationType Type { get; set; } = RenumerationType.Salary;
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public virtual JobGroup JobGroup { get; set; }
        public virtual ICollection<PayrollDeduction> Decuctions { get; set; }
        public virtual ICollection<HrJobCatalogue> JobCatalogues { get; set; }//Used to pay many job profiles
        //OTHER DETAILS ON PAYMENTS
        public RenumerationScheme()
        {
            JobCatalogues = new List<HrJobCatalogue>();
            Decuctions = new List<PayrollDeduction>();
        }
    }
}
