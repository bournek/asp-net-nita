﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    /// <summary>
    /// 
    /// </summary>
    public class StaffDesignation
    {
        public int Id { get; set; }
        public string Name { get; set; } = "None";//Default
        public bool IsSupervisor { get; set; }
        public virtual ICollection<StaffProfile> Staff { get; set; }
        public StaffDesignation()
        {
            Staff = new List<StaffProfile>();
        }
    }
}
