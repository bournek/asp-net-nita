﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public class JobGroup
    {
        public int Id { get; set; }
        public int? CurrencyId { get; set; }
        //
        public int Rank { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public double MinPay { get; set; }
        public double MaxPay { get; set; }
        public double Increment { get; set; }
        //
        public EntityStatus Status { get; set; } = EntityStatus.Active;
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual ICollection<RenumerationScheme> RenumerationSchemes { get; set; }
        //
        public JobGroup()
        {
            StartDate = DateTime.Now;
            EndDate = DateTime.Now.AddMonths(1);
            RenumerationSchemes = new List<RenumerationScheme>();
        }
    }
}
