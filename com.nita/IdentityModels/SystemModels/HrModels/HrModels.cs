﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{

    /// <summary>
    /// Contact specifications
    /// </summary>
    public class TrainerContract
    {
        public int Id { get; set; }
        public string ContractName { get; set; }
        public int CycleId { get; set; }
        public int TrainerId { get; set; }
        //public int TerminationId { get; set; }
        public string ContractNumber { get; set; }
        public string ContractStatus { get; set; }
        public bool Terminated { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual TrainerProfile Trainer { get; set; }
        public virtual ContractTermination Termination { get; set; }
    }

    /// <summary>
    /// Trainer Contact termination
    /// </summary>
    public class ContractTermination
    {
        public int Id { get; set; }
        public int? ContractId { get; set; }
        public string Reason { get; set; }
        public virtual TrainerContract Contact { get; set; }
    }

    public class TradeAreaContract
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public int TradeAreaId { get; set; }
        public int Capacity { get; set; }
        public int LeadTrainerId { get; set; }
    }
    public class AcademicProfile
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public string AcademicRank { get; set; }
        public virtual StaffProfile Staff { get; set; }
        public virtual ICollection<AcademicCertification> Certifications { get; set; }
        public AcademicProfile()
        {
            Certifications = new List<AcademicCertification>();
        }
    }
    public class AcademicCertification
    {
        public int Id { get; set; }
        public int ProfileId { get; set; }
        //
        public string FileName { set; get; }
        public virtual AcademicProfile Profile { get; set; }
    }

    public class StaffDependant
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public string Name { get; set; }
        public virtual StaffProfile Staff { get; set; }
    }
}
