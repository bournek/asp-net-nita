﻿using com.nita.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace com.nita.IdentityModels
{
    public partial class AppInitializer
    {
        //Test MC
        const string testMasterCraftMan = "Test_MC";
        const string mcEmail = "mc@nita.co.ke";
        const string mcPassword = "testMC@123";
        //Test Training Inst.
        const string testTrainingInst = "Test_TI";
        const string tiEmail = "ti@nita.co.ke";
        const string tiPassword = "testTI@123";
        //Trainee 1
        const string testTrainee = "Test_Trainee1";
        const string ttEmail = "tt@nita.co.ke";
        const string ttPassword = "testTT@123";
        //Trainee 2
        const string testTrainee2 = "Test_Trainee2";
        const string ttEmail2 = "tt2@nita.co.ke";
        const string ttPassword2 = "testTT2@123";

        //GL Accounts
        const string CreditorsAccount = "Creditors";
        const string WithholdingTaxPayableAccount = "Withholding Tax Payable";
        const string OperationalExpensesAccount = "Operational Expenses";

        /// <summary>
        /// @Bourne K
        /// </summary>
        /// <returns></returns>
        private async Task CreateDemoUsers()
        {
            logger.LogInformation(5, "Creating Demo Staff..");
            await CreateDemoStaff().ContinueWith(async w =>
            {
                await CreateDemoTrainers().ContinueWith(async x => {
                    await CreateDemoTrainee();
                    logger.LogInformation(5, "Demo staff Init Complete...");
                });
            });
        }
        /// <summary>
        /// @Bourne K
        /// App achitecture required for sample tests
        /// </summary>
        /// <returns></returns>
        private async Task CreateDevtData()
        {
            logger.LogInformation(5, "Creating Devt Data..");
            var db = provider.GetRequiredService<ApplicationDbContext>();

            try
            {
                //Dept
                var dept = new Department()
                {
                    Name = "Adminstration"
                };
                db.HrDepartments.Add(dept);
                //Dept 1
                var dept1 = new Department()
                {
                    Name = "Accounting & Finance"
                };
                db.HrDepartments.Add(dept1);
                //Dept 2
                var dept2 = new Department()
                {
                    Name = "Procurement"
                };
                db.Add(dept2);
                await db.SaveChangesAsync();
                //Section
                var sect = new Section()
                {
                    Name = "Finance",
                    DeptId = dept1.Id
                };
                //
                dept.Sections.Add(sect);
                await db.SaveChangesAsync();

                //Default Leave Group
                var lvGrp = new LeaveGroup()
                {
                    Name = "Annual"
                };
                db.HrLeaveGroups.Add(lvGrp);
                db.SaveChanges();

                //Employeee Category

                var empcat = new EmployeeCategory()
                {
                    Name = "Parmanent"

                };
                db.HrEmployeeCategories.Add(empcat);
                db.SaveChanges();

                //Default Leave Group
                var jobGrp = new JobGroup()
                {
                    Name = "Group K",
                    MaxPay = 94254,
                    MinPay = 62254,
                    Increment = 2178,
                    Code = "GRP-K",
                    Rank = 13,
                    StartDate = DateTime.ParseExact("2017-07-01", "yyyy-MM-dd", CultureInfo.InvariantCulture),
                    EndDate = DateTime.ParseExact("2019-06-30", "yyyy-MM-dd", CultureInfo.InvariantCulture)
                };
                db.PJobGroups.Add(jobGrp);
                db.SaveChanges();

                //Job Category
                var jobtyp = new JobCategory()
                {
                    Name = "Nita Staff"
                };
                db.HrJobCategories.Add(jobtyp);
                db.SaveChanges();

                //Renumetration Scheme
                var renum = new RenumerationScheme()
                {
                    Type = RenumerationType.Salary,
                    GroupId = jobGrp.Id
                };
                db.HrRenumerationSchemes.Add(renum);
                db.SaveChanges();

                //JobCatagolue
                var jc = new HrJobCatalogue()
                {
                    Name = "Finance Manager",
                    Capacity = 1,
                    JobCatId = jobtyp.Id,
                    RenumerationId = renum.Id
                };
                db.HrJobCatalogues.Add(jc);
                db.SaveChanges();

                //Designation
                var design = new StaffDesignation()
                {
                    Name = "Nita Staff"
                };
                db.HrDesignations.Add(design);
                db.SaveChanges();

                //Currency
                var cur = new Currency()
                {
                    Name = "Kenya Shilling",
                    Code = "KSH",
                    Alias = "Kshs.",
                    IsBase = true
                };
                db.FCurrencies.Add(cur);
                db.SaveChanges();
                //
                var usd = new Currency()
                {
                    Name = "US Dollar",
                    Code = "USD",
                    Alias = "$",
                };
                db.FCurrencies.Add(usd);
                db.SaveChanges();


                var start = DateTime.ParseExact("2017-07-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                var end = DateTime.ParseExact("2018-06-30", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                //
                var yr = new FiscalYear()
                {
                    Name = "2017/2018",
                    BeginDate = start,
                    EndDate = end,
                    CloseDate = end,
                    Status = EntityStatus.Active
                };
                //
                db.FiscalYears.Add(yr);
                db.SaveChanges();
                //
                var period = new BudgetPeriod()
                {
                    FYId = yr.Id,
                    PeriodFrom = start,
                    PeriodTo = end,
                    PeriodName = "2017 - 2018 Budget Period"
                };
                yr.BudgetPeriods.Add(period);
                db.SaveChanges();



                //KCB
                var bank = new FBank()
                {
                    Name = "Kenya Commercial Bank",
                    Code = "012"
                };
                db.PBanks.Add(bank);
                db.SaveChanges();
                //
                var branch1 = new BankBranch()
                {
                    Name = "Kasarani",
                    Code = "78"
                };
                branch1.BankId = bank.Id;
                bank.Branches.Add(branch1);
                db.SaveChanges();
                //Equity
                var bank1 = new FBank()
                {
                    Name = "Equity Bank",
                    Code = "010"
                };
                db.PBanks.Add(bank1);
                db.SaveChanges();
                //
                var branch2 = new BankBranch()
                {
                    Name = "Kisii",
                    Code = "104"
                };
                //
                branch2.BankId = bank1.Id;
                //
                bank.Branches.Add(branch2);
                db.SaveChanges();
                //Coop Bank
                var bank2 = new FBank()
                {
                    Name = "Cooperative Bank",
                    Code = "020"
                };
                db.PBanks.Add(bank2);
                db.SaveChanges();
                //
                var branch3 = new BankBranch()
                {
                    Name = "Ndiani",
                    Code = "57"
                };
                //
                branch3.BankId = bank2.Id;
                //
                bank.Branches.Add(branch3);
                db.SaveChanges();

                //
                //
                var acc = new FinanceAccount()
                {
                    Name = "Accounts And Other Payables",
                    Number = "AC 4500",
                    Type = FAccountType.Liability

                };
                db.FinanceAccounts.Add(acc);
                db.SaveChanges();
                //
                var acc1 = new FinanceAccount()
                {
                    Name = CreditorsAccount,
                    Number = "6500",
                    Type = FAccountType.Liability,
                    ParentId = acc.Id
                };
                db.FinanceAccounts.Add(acc1);
                db.SaveChanges();
                //
                var acc2 = new FinanceAccount()
                {
                    Name = WithholdingTaxPayableAccount,
                    Number = "1200",
                    Type = FAccountType.Liability,
                    ParentId = acc.Id
                };
                db.FinanceAccounts.Add(acc2);
                db.SaveChanges();
                //
                //
                var acc3 = new FinanceAccount()
                {
                    Name = OperationalExpensesAccount,
                    Number = "15-00-00",
                    Type = FAccountType.Expense
                };
                db.FinanceAccounts.Add(acc3);
                db.SaveChanges();
                //
                var acc4 = new FinanceAccount()
                {
                    Name = "Beneficiary Expenses",
                    Number = "15-00-01",
                    Type = FAccountType.Expense,
                    ParentId = acc3.Id,
                    HasBudget = true
                };
                db.FinanceAccounts.Add(acc4);
                db.SaveChanges();
                //
                var lgr = new FinanceLedger()
                {
                    Name = "Main Legder",
                    CurrencyId = cur.Id
                };
                db.FLedgers.Add(lgr);
                db.SaveChanges();
                //
                var budget = new FinanceBudget()
                {
                    PeriodId = period.Id,
                    Amount = 450000,
                    DeptId = dept.Id,
                    AccountId = acc4.Id,
                    Enforced = true,
                    Name = "Stipends Budget 2017/2018"
                };
                //
                dept.Budgets.Add(budget);
                db.SaveChanges();
                //
                var vprefix = new PVoucherPrefix()
                {
                    Code = "NITA/MAIN/",
                    LedgerId = lgr.Id
                };
                lgr.PVoucherPrefixes.Add(vprefix);
                db.SaveChanges();
                //
                var pmode = new PaymentAccount()
                {
                    Name = "KCB Stipend Account",
                    AccountNumber = "45223221455784556",
                    BankTransferPayeeName = "NITA Kenya",
                    Status = EntityStatus.Active,
                    LedgerId = lgr.Id,
                    Mode = PayMode.Cheque,
                    EFTEnabled = true
                };
                //
                var act1 = new PaymentAction()
                {
                    Type = PaymentType.AvailbleForBanking,
                    Caption = "Available for Banking"
                };
                var act2 = new PaymentAction()
                {
                    Type = PaymentType.MakePayments,
                    Caption = "Make Payments"
                };
                var act3 = new PaymentAction()
                {
                    Type = PaymentType.ReceivePayments,
                    Caption = "Receive Payments"
                };
                //
                pmode.ApprovedActions.Add(act1);
                pmode.ApprovedActions.Add(act2);
                pmode.ApprovedActions.Add(act3);
                //
                pmode.SOAPCredentials = new SOAPServiceCredentials() { Name = "name", Password = "password" };
                //
                pmode.AccountId = acc2.Id;
                //
                lgr.PaymentAccounts.Add(pmode);
                db.SaveChanges();
                //
                var pmode1 = new PaymentAccount()
                {
                    Name = "NITA MPESA PAYBILL ACC.",
                    AccountNumber = "452233",
                    BankTransferPayeeName = "NITA Kenya",
                    Status = EntityStatus.Active,
                    LedgerId = lgr.Id,
                    Mode = PayMode.MobileBanking
                };
                //
                pmode1.ApprovedActions.Add(act2);
                pmode1.ApprovedActions.Add(act3);
                ////
                pmode1.AccountId = acc4.Id;
                //
                lgr.PaymentAccounts.Add(pmode1);
                db.SaveChanges();
                //
                var pmode2 = new PaymentAccount()
                {
                    Name = "NITA HQ CASH ACC.",
                    AccountNumber = "CASHIER ACCOUNT",
                    BankTransferPayeeName = "NITA Kenya",
                    Status = EntityStatus.Active,
                    LedgerId = lgr.Id,
                    Mode = PayMode.Cash
                };
                //
                pmode2.ApprovedActions.Add(act1);
                pmode2.ApprovedActions.Add(act2);
                ////
                pmode2.AccountId = acc4.Id;
                //
                lgr.PaymentAccounts.Add(pmode2);
                db.SaveChanges();
                //
                var tax = new FinanceTax()
                {
                    Name = "V.A.T @ 16",
                    AccountId = acc2.Id,
                    Rate = 16
                };
                tax.TaxSchemes.Add(new TaxScheme()
                {
                    Name = "V.A.T Withholding",
                    Type = TaxType.V_A_T_Withholding
                });
                db.FTaxes.Add(tax);
                db.SaveChanges();

                //
                var vtyp = new FVendorType()
                {
                    Name = "Individual"
                };
                db.FVendorTypes.Add(vtyp);
                db.SaveChanges();

                //Location
                var loc = new Location()
                {
                    Name = db.Counties.First().Name + " Office",
                    CountyId = db.Counties.First().Id,
                };

                db.Locations.Add(loc);
                db.SaveChanges();

                //supplier
                var sup = new FSupplier()
                {
                    Name = "Likoni Hardware Ent.",
                    BankCode = bank.Id,
                    Discount = 0,
                    IsActive = true,
                    VendorTypeId = vtyp.Id,
                    RefNo = 1,
                    LocationId = loc.Id
                };
                db.FSuppliers.Add(sup);
                db.SaveChanges();



                //Acc type

                var pacc = new PayAccount()
                {
                    AccountId = acc4.Id,
                    AccountType = PayAccountType.Irregular,
                    Code = "ACC-BEN-STIPENDS",
                    PayType = PayType.Earning,
                    Description = "Pay account for benefiaciary stipends payment"
                };

                db.PPayAccounts.Add(pacc);
                db.SaveChanges();
                //
                var proj = new IrregularPaymentProject()
                {
                    EffectiveDate = DateTime.Now,
                    EndDate = DateTime.Now,
                    CloseDate = DateTime.Now.AddMonths(1),
                    Description = "Stipend Payments to beneficiaries for the first two weeks",
                    Name = "First two weeks stipend payments",
                    Duration = 2,
                    Rate = 1500,
                    Unit = DurationUnit.Week,
                    Taxable = false
                };
                //
                db.PIrrPaymentProjects.Add(proj);
                db.SaveChanges();
            }
            catch
            {

            }


            logger.LogInformation(5, "Create Devt Data Completed..");
        }
        /// <summary>
        /// Sample Payments
        /// </summary>
        /// <returns></returns>
        private async Task CreateDevtPayments()
        {
            logger.LogInformation(5, "Create Devt Payments..");
            var db = provider.GetRequiredService<ApplicationDbContext>();
            try {
                //The Ledger
                var lgr = db.FLedgers.First();
                //
                var sup = db.FSuppliers.First();
                //
                var cur = db.FCurrencies.Single(p => p.IsBase);
                //
                var tax = db.FTaxes.First();

                //GL Accounts
                var acc1 = db.FinanceAccounts.Single(p => p.Name == CreditorsAccount);
                var acc2 = db.FinanceAccounts.Single(p => p.Name == WithholdingTaxPayableAccount);
                //
                var personnel = db.HrStaff.First();

                //Invoice
                var inv = new FInvoice()
                {
                    DocNo = 1,
                    InvoiceDate = DateTime.ParseExact("02/12/2018", "MM/dd/yyyy", CultureInfo.InvariantCulture),
                    InvoiceTerm = InvoiceTerm.Custom,
                    DueDate = DateTime.ParseExact("04/12/2018", "MM/dd/yyyy", CultureInfo.InvariantCulture),
                    OrderNo = "890",
                    Amount = 14000,
                    PersonnelId = personnel.Id

                };
                inv.LedgerId = lgr.Id;
                inv.ProjectId = null;
                inv.SupplierId = sup.Id;
                //
                db.FInvoices.Add(inv);
                db.SaveChanges();

                var p1 = new FPayment()
                {
                    CurrencyId = cur.Id,
                    AccountId = acc2.Id,
                    DeptId = db.HrDepartments.First().Id,
                    Discount = 20,
                    GrossAmount = 14000
                };
                p1.Taxes.Add(tax);
                //
                inv.Payments.Add(p1);
                db.SaveChanges();
                //
                p1.InvoiceId = inv.Id;
                db.SaveChanges();
                //
                var t1 = new GLTransaction()
                {
                    Amount = await Utils.ComputeAmountPayable(p1),
                    Kind = GLTransactionKind.Regular,
                    Type = GLTransactionType.Credit,
                    AccountId = acc1.Id
                };
                //
                p1.GLTransactions.Add(t1);
                db.SaveChanges();
                //Invoice 2
                var inv2 = new FInvoice()
                {
                    DocNo = 2,
                    InvoiceDate = DateTime.ParseExact("02/12/2018", "MM/dd/yyyy", CultureInfo.InvariantCulture),
                    InvoiceTerm = InvoiceTerm.Due_In_45_Days,
                    OrderNo = "120",
                    Amount = 63000,
                    PersonnelId = personnel.Id

                };
                inv2.DueDate = inv2.InvoiceDate.AddDays(45);
                inv2.LedgerId = lgr.Id;
                inv2.ProjectId = null;
                inv2.SupplierId = sup.Id;
                //
                db.FInvoices.Add(inv2);
                db.SaveChanges();
                var p2 = new FPayment()
                {
                    CurrencyId = cur.Id,
                    DeptId = db.HrDepartments.First().Id,
                    AccountId = acc2.Id,
                    Discount = 200,
                    GrossAmount = 63000,
                };

                p2.Taxes.Add(tax);
                inv2.Payments.Add(p2);
                db.SaveChanges();
                //tax
                var trans = new GLTransaction()
                {
                    AccountId = tax.AccountId,
                    Type = GLTransactionType.Debit,
                    Kind = GLTransactionKind.Tax,
                    Amount = await Utils.ComputeTaxPayable(p2)
                };
                p2.GLTransactions.Add(trans);
                //Payable
                var trans2 = new GLTransaction()
                {
                    AccountId = acc1.Id,//
                    Type = GLTransactionType.Debit,
                    Kind = GLTransactionKind.Regular,
                    Amount = await Utils.ComputeAmountPayable(p2)
                };
                //
                p2.GLTransactions.Add(trans2);
                //
                p2.InvoiceId = inv2.Id;
                db.SaveChanges();

                //
                //P Voucher
                var voucher = new FPaymentVoucher()
                {
                    LedgerId = 1,
                    PersonnelId = personnel.Id,
                    CheckNo = "454411222",
                    Description = "TP Payment Voucher",
                    SupplierId = 1,
                    VoucherDate = DateTime.Now,
                    VoucherNo = "00001",
                    //SponsorId = null,
                    ProjectId = null,
                    Status = PVStatus.Pending,
                    Type = PVType.Regular
                };
                //
                var p3 = new FPayment()
                {

                    Discount = 10.0,
                    GrossAmount = 45000,
                    InvoiceId = 2,
                };
                //
                p3.CurrencyId = db.FCurrencies.First().Id;
                p3.DeptId = db.HrDepartments.First().Id;
                p3.AccountId = 4;

                //
                p3.GLTransactions.Add(new GLTransaction()
                {
                    AccountId = 2,
                    Amount = 45000,
                    Status = EntityStatus.Active,
                    Type = GLTransactionType.Debit,
                    Kind = GLTransactionKind.Regular,
                });
                //
                voucher.Payments.Add(p3);
                //
                db.FPaymentVouchers.Add(voucher);
                db.SaveChanges();
                //
                p3.VoucherId = voucher.Id;
                //
                db.SaveChanges();
            }
            catch
            {
            }

            logger.LogInformation(5, "Create Devt Payments Completed..");
        }
        /// <summary>
        /// @Bourne K
        /// A demo staff account and Admin profile
        /// </summary>
        /// <returns></returns>
        private async Task CreateDemoStaff()
        {
            //
            var userManager = provider.GetRequiredService<UserManager<ApplicationUser>>();
            var db = provider.GetRequiredService<ApplicationDbContext>();
            
            //Get Admin User
            var user = userManager.Users.Single(l => l.UserName == adminName);

            //
            var loc = db.Locations.First();
            //Staff Admin Profile

            //add address
            var address = new UserAddress()
            {
                PostalCode = "41210"
            };
            address.CountyId = db.Counties.First().Id;
            //
            db.Addresses.Add(address);
            db.SaveChanges();

            //Bank Account
            var bacc = new BankAccount()
            {
                Name = "Abno Staff Admin",
                Number = "00121223122548855",
                BranchId = db.PBanks.Include(k => k.Branches).First().Branches.First().Id
            };
            //
            db.PBankAccounts.Add(bacc);
            db.SaveChanges();
            try
            {
                //
                var staffProfile = new StaffProfile()
                {
                    FullName = "Abno Staff Admin",
                    EmplNo = "Admin"
                };
                //Section
                staffProfile.SectionId = db.HrSections.First().Id;
                //Employee Type
                staffProfile.EmpCatId = db.HrEmployeeCategories.First().Id;
                //Job catalogue
                staffProfile.JobCatId = db.HrJobCatalogues.First().Id;
                //Job Type
                var lvGrp = db.HrLeaveGroups.First();
                staffProfile.LeaveGroupId = lvGrp.Id;
                //Designation
                staffProfile.DesignationId = db.HrDesignations.First().Id;
                //Address
                staffProfile.AddressId = address.Id;
                //Bank Account
                staffProfile.BankAccId = bacc.Id;
                //Location
                staffProfile.LocationId = loc.Id;
                //
                user.StaffProfile = staffProfile;
                //
                //Update
                var d = await userManager.UpdateAsync(user);
                DumpIdentityResult("CreateAdminUser: Staff Profile Addition", d);
                if (d.Succeeded)
                {
                    //
                    
                }
            }
            catch
            {
                logger.LogWarning(0, "Create Demo staff failed.");
            }
        }
        /// <summary>
        /// @Bourne K
        /// </summary>
        /// <returns></returns>
        private async Task CreateDemoTrainers()
        {
            //
            var userManager = provider.GetRequiredService<UserManager<ApplicationUser>>();
            var db = provider.GetRequiredService<ApplicationDbContext>();
            //
            var admin = userManager.Users.Include(p => p.StaffProfile).Single(p => p.UserName == adminName);

            var usr1 = await userManager.FindByNameAsync(testMasterCraftMan);
            if (usr1 == null)
            {
                //Master Craft
                usr1 = new ApplicationUser()
                {
                    AccountType = AccountType.MasterCraftsMan,
                    UserName = testMasterCraftMan,
                    Email = mcEmail,
                };

                var result = await userManager.CreateAsync(usr1, mcPassword);

                if (result.Succeeded)
                {
                    //
                    var addr = new UserAddress()
                    {
                        CountyId = db.Counties.First().Id,
                        City = "Ukunda",
                        NearbyLandmark = "Ukubda A.I.C Church"
                    };
                    db.Addresses.Add(addr);
                    db.SaveChanges();
                    //
                    var mc = new TrainerProfile()
                    {
                        Type = TrainerType.MasterCraftman,
                        FullName = "Test Master Craftman",
                        EnterpriseName = "TCM Enterprises",
                        ContactPersonName = "Bourne Koloh"
                    };
                    //Bank Acc
                    var bankAcc = new BankAccount()
                    {
                        Name = mc.FullName,
                        Number = "47845645623541",
                        BranchId = db.PBanks.Include(l => l.Branches).First().Branches.First().Id,

                    };
                    //
                    db.PBankAccounts.Add(bankAcc);
                    db.SaveChanges();
                    //
                    mc.AddressId = addr.Id;
                    //
                    mc.BankAccId = bankAcc.Id;
                    //
                    mc.PersonnelId = admin.StaffProfile.Id;
                    //
                    usr1.TrainerProfile = mc;

                    result = await userManager.UpdateAsync(usr1);

                    DumpIdentityResult("Create MC Profile", result);

                    if (result.Succeeded)
                    {
                        //
                    }
                }
                else
                {
                    DumpIdentityResult("Create MC:", result);
                }
            }
            else
            {
                logger.LogInformation(5, "Test Master Craftman already exists.");
            }

            //Training Inst.
            var usr2 = await userManager.FindByNameAsync(testTrainingInst);
            if (usr2 == null)
            {
                usr2 = new ApplicationUser()
                {
                    AccountType = AccountType.Institution,
                    UserName = testTrainingInst,
                    Email = tiEmail
                };

                var result = await userManager.CreateAsync(usr2, tiPassword);

                if (result.Succeeded)
                {
                    //
                    var addr = new UserAddress()
                    {
                        CountyId = db.Counties.First().Id,
                        City = "Shimba Hills",
                        NearbyLandmark = "Shimba hills game researve"
                    };
                    db.Addresses.Add(addr);
                    db.SaveChanges();
                    //
                    var ti = new TrainerProfile()
                    {
                        Type = TrainerType.Institution,
                        FullName = "Shimba Hills Vocational Training Inistitute",
                        EnterpriseName = "Shimba Hills Vocational Training Inistitute",
                        ContactPersonName = "Philip Khalifa"
                    };
                    //Bank Acc
                    var bankAcc = new BankAccount()
                    {
                        Name = ti.FullName,
                        Number = "77454112111212",
                        BranchId = db.PBanks.Include(l => l.Branches).First().Branches.First().Id,
                    };
                    //
                    db.PBankAccounts.Add(bankAcc);
                    db.SaveChanges();
                    //
                    ti.AddressId = addr.Id;
                    //
                    ti.BankAccId = bankAcc.Id;
                    //
                    ti.PersonnelId = admin.StaffProfile.Id;
                    //
                    usr2.TrainerProfile = ti;

                    result = await userManager.UpdateAsync(usr2);

                    DumpIdentityResult("Create Training Inst. Profile", result);

                    if (result.Succeeded)
                    {
                        //
                    }
                }
                else
                {
                    DumpIdentityResult("Create Training Prov. User", result);
                }
            }
            else
            {
                logger.LogInformation(5, "Test Training provider already exists.");
            }
        }
        /// <summary>
        /// @Bourne K
        /// </summary>
        /// <returns></returns>
        private async Task CreateDemoTrainee()
        {
            //
            var userManager = provider.GetRequiredService<UserManager<ApplicationUser>>();
            var db = provider.GetRequiredService<ApplicationDbContext>();

            //
            var admin = userManager.Users.Include(p => p.StaffProfile).Single(p => p.UserName == adminName);
                try
                {
                    //Trainee profile
                    var usr1 = await userManager.FindByNameAsync(testTrainee);
                    if (usr1 == null)
                    {
                        //
                        usr1 = new ApplicationUser()
                        {
                            AccountType = AccountType.Trainee,
                            UserName = testTrainee,
                            Email = ttEmail,
                        };

                        var result = await userManager.CreateAsync(usr1, ttPassword);

                        if (result.Succeeded)
                        {
                            //
                            var addr = new UserAddress()
                            {
                                CountyId = db.Counties.Last().Id,
                                City = "Mathare",
                                NearbyLandmark = "Airfoce Base"
                            };
                            db.Addresses.Add(addr);
                            db.SaveChanges();
                            //
                            var trainee = new TraineeProfile()
                            {
                                FullName = "Yusuf Ibrahim Khalifa",
                                DateOfBirth = DateTime.Now.AddYears(-20),
                                Disability = ((int)Disability.NONE).ToString(),
                                Gender = Gender.Male,
                                Marital = MaritalStatus.Single,
                                PassportNo = "45565221"
                            };
                            //Bank Acc
                            var bankAcc = new BankAccount()
                            {
                                Name = trainee.FullName,
                                Number = "000124457785665",
                                BranchId = db.PBanks.Include(l => l.Branches).First().Branches.Last().Id,

                            };
                            //
                            db.PBankAccounts.Add(bankAcc);
                            db.SaveChanges();
                            //
                            trainee.BankAccId = bankAcc.Id;
                            //
                            trainee.AddressId = addr.Id;
                            //
                            //trainee.PersonnelId = admin.StaffProfile.Id;
                            //
                            usr1.TraineeProfile = trainee;

                            result = await userManager.UpdateAsync(usr1);

                            DumpIdentityResult("Create Trainee Profile", result);

                            if (result.Succeeded)
                            {
                                //

                            }
                        }
                        else
                        {
                            DumpIdentityResult("Create Trainee:", result);
                        }
                    }
                    else
                    {
                        logger.LogInformation(5, "Test Trainee 1 already exists.");
                    }
                    //
                    var usr2 = await userManager.FindByNameAsync(testTrainee2);
                    if (usr2 == null)
                    {
                        //
                        usr1 = new ApplicationUser()
                        {
                            AccountType = AccountType.Trainee,
                            UserName = testTrainee2,
                            Email = ttEmail2,
                        };

                        var result = await userManager.CreateAsync(usr2, ttPassword2);

                        if (result.Succeeded)
                        {
                            //
                            var addr1 = new UserAddress()
                            {
                                CountyId = db.Counties.Last().Id,
                                City = "Ukunda",
                                NearbyLandmark = "Cooperative Bank"
                            };
                            db.Addresses.Add(addr1);
                            db.SaveChanges();
                            //
                            var trainee2 = new TraineeProfile()
                            {
                                FullName = "Betrice Walongoa",
                                DateOfBirth = DateTime.Now.AddYears(-22),
                                Disability = ((int)Disability.NONE).ToString(),
                                Gender = Gender.Female,
                                Marital = MaritalStatus.Single,
                                PassportNo = "4556522145"
                            };
                            //Bank Acc
                            var bankAcc1 = new BankAccount()
                            {
                                Name = trainee2.FullName,
                                Number = "7130124457785665",
                                BranchId = db.PBanks.Include(l => l.Branches).First().Branches.Last().Id,

                            };
                            //
                            db.PBankAccounts.Add(bankAcc1);
                            db.SaveChanges();
                            //
                            trainee2.BankAccId = bankAcc1.Id;
                            //
                            trainee2.AddressId = addr1.Id;
                            //
                            //trainee.PersonnelId = admin.StaffProfile.Id;
                            //
                            usr2.TraineeProfile = trainee2;

                            result = await userManager.UpdateAsync(usr2);

                            DumpIdentityResult("Create Trainee Profile", result);

                            if (result.Succeeded)
                            {
                                //
                            }
                        }
                    }
                }
                catch
                {

                }
        }
    }
}
