using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;

namespace com.nita.IdentityModels
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {        
        //Payroll
        public DbSet<PayAccount> PPayAccounts { get; set; }
        public DbSet<JobGroup> PJobGroups { get; set; }
        public DbSet<NhifRate> PNhifRates { get; set; }
        public DbSet<SalaryPeriod> PSalaryPeriods { get; set; }
        public DbSet<PayeRate> PPayeRates { get; set; }
        public DbSet<FBank> PBanks { get; set; }
        public DbSet<BankAccount> PBankAccounts { get; set; }
        public DbSet<PayrollDeduction> PDeductions { get; set; }
        //
        public DbSet<IrregularPaymentProject> PIrrPaymentProjects { get; set; }
        public DbSet<IrregularEarning> PIrrEarnings { get; set; }
        public DbSet<IrregularPaymentProfile> PIrrPaymentProfiles { get; set; }
        //
        protected void OnPayrollModelCreating(ModelBuilder builder)
        {
            //
            builder.Entity<BankAccount>().HasOne(l => l.Trainee).WithOne(k => k.BankAccount).HasForeignKey<TraineeProfile>(p => p.BankAccId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<BankAccount>().HasOne(l => l.Trainer).WithOne(k => k.BankAccount).HasForeignKey<TrainerProfile>(p => p.BankAccId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<BankAccount>().HasOne(l => l.Staff).WithOne(k => k.BankAccount).HasForeignKey<StaffProfile>(p => p.BankAccId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FinanceAccount>().HasOne(l => l.PayAccount).WithOne(l => l.Account).HasForeignKey<PayAccount>(k => k.AccountId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FinanceAccount>().HasMany(l => l.ECPayAccounts).WithOne(l => l.EmployerContributionAccount).HasForeignKey(k => k.ECAccountId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<TrainerProfile>().HasOne(l => l.IrrPaymentProfile).WithOne(l => l.Trainer).HasForeignKey<IrregularPaymentProfile>(k => k.TrainerId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<TraineeProfile>().HasOne(l => l.IrPaymentProfile).WithOne(l => l.Trainee).HasForeignKey<IrregularPaymentProfile>(k => k.TraineeId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<TrainerProfile>().HasMany(l => l.IrEarnings).WithOne(l => l.Trainer).HasForeignKey(k => k.TrainerId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<TraineeProfile>().HasMany(l => l.IrrEarnings).WithOne(l => l.Trainee).HasForeignKey(k => k.TraineeId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<IrregularEarning>().HasOne(l => l.PayAccount).WithMany(l => l.IrregularEarnings).HasForeignKey(k => k.PayAccountId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<IrregularEarning>().HasOne(l => l.Payment).WithOne(l => l.IrregularEarning).HasForeignKey<IrregularEarning>(k => k.PaymentId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<JobGroup>().HasMany(l => l.RenumerationSchemes).WithOne(l => l.JobGroup).HasForeignKey(k => k.GroupId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
