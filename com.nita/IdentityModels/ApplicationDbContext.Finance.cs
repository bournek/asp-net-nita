using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using Nita.IdentityModels;

namespace com.nita.IdentityModels
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {        
        //Finance
        public DbSet<FinanceAccount> FinanceAccounts { get; set; }
        //Payments
        public DbSet<FPayment> FWallet { get; set; }
        //Receipts
        public DbSet<InvoicePayment> FReceiptBook { get; set; }
        public DbSet<Currency> FCurrencies { get; set; }
        public DbSet<FinanceLedger> FLedgers { get; set; }
        public DbSet<PaymentAccount> FPaymentAccounts { get; set; }
        public DbSet<PaymentAction> FPaymentActions { get; set; }
        public DbSet<FiscalYear> FiscalYears { get; set; }
        public DbSet<FinanceTax> FTaxes { get; set; }
        public DbSet<FVendorType> FVendorTypes { get; set; }
        public DbSet<PVoucherPrefix> PVoucherPrefixes { get; set; }
        public DbSet<FInvoice> FInvoices { get; set; }
        public DbSet<FSupplier> FSuppliers { get; set; }
        public DbSet<FCashier> FCashierOffices { get; set; }
        public DbSet<FCAttendantProfile> FCashiers { get; set; }
        public DbSet<FPaymentVoucher> FPaymentVouchers { get; set; }
        public DbSet<FSponsor> FSponsors { get; set; }
        public DbSet<FProject> FProjects { get; set; }
        public DbSet<FProductCategory> FProductCategories { get; set; }
        public DbSet<FCustomerType> FCustomerTypes { get; set; }
        public DbSet<ImprestSurrender> FImprestSurrenders { get; set; }
        public DbSet<ExpenseClaim> FExpenseClaims { get; set; }
        public DbSet<ExpenseClaimDisbursement> FEDisbursements { get; set; }
        public DbSet<PettyCashReq> FPettyCashReqs { get; set; }
        public DbSet<PettycashDisbursement> FPCDisbursement { get; set; }
        public DbSet<ImprestDisbursement> FImprestDisbursements { get; set; }
        public DbSet<ImprestWarrant> FImprestWarrants { get; set; }
        public DbSet<SupplierInvoiceWaiver> FSIWaivers { get; set; }
        public DbSet<PaymentJournal> FJournals { get; set; }
        protected void OnFinanceModelCreating(ModelBuilder builder)
        {
            //
            builder.Entity<FinanceAccount>().HasMany(l => l.PaymentAccounts).WithOne(l => l.GLAccount).HasForeignKey(k => k.AccountId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FinanceAccount>().HasMany(p => p.Children).WithOne(l => l.Parent).HasForeignKey(k => k.ParentId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FinanceAccount>().HasMany(p => p.Payments).WithOne(l => l.GLAccount).HasForeignKey(k => k.AccountId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FinanceAccount>().HasMany(p => p.Budgets).WithOne(l => l.GLAccount).HasForeignKey(k => k.AccountId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FinanceAccount>().HasMany(p => p.CreditJournals).WithOne(l => l.CreditAccount).HasForeignKey(k => k.CreditAccountId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FinanceAccount>().HasMany(p => p.DebitJournals).WithOne(l => l.DebitAccount).HasForeignKey(k => k.DebitAccountId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FiscalYear>().HasMany(k => k.Quarters).WithOne(k => k.FiscalYear).HasForeignKey(p => p.FiscalYearId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FinanceLedger>().HasMany(k => k.PaymentAccounts).WithOne(l => l.Ledger).HasForeignKey(p => p.LedgerId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FinanceLedger>().HasMany(k => k.Journals).WithOne(l => l.Ledger).HasForeignKey(p => p.LedgerId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FinanceAccount>().HasMany(l => l.Taxes).WithOne(l => l.GLAccount).HasForeignKey(l => l.AccountId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<PaymentAccount>().HasOne(l => l.SOAPCredentials).WithOne(l => l.PaymentAccount).HasForeignKey<SOAPServiceCredentials>(l => l.ModeId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FSupplier>().HasOne(l => l.Location).WithMany(l => l.Suppliers).HasForeignKey(l => l.LocationId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FInvoice>().HasOne(l => l.Project).WithMany(l => l.Invoices).HasForeignKey(l => l.ProjectId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FInvoice>().HasMany(l => l.Payments).WithOne(l => l.Invoice).HasForeignKey(l => l.InvoiceId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<SupplierInvoiceWaiver>().HasOne(l => l.Payment).WithOne(l => l.InvoiceWaiver).HasForeignKey<SupplierInvoiceWaiver>(l => l.PaymentId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<SupplierInvoiceWaiver>().HasOne(l => l.Personnel).WithMany(l => l.Waivers).HasForeignKey(l => l.PersonnelId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FInvoice>().HasOne(l => l.Ledger).WithMany(l => l.Invoices).HasForeignKey(l => l.LedgerId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FInvoice>().HasMany(l => l.SupplierInvoiceWaivers).WithOne(l => l.Invoice).HasForeignKey(l => l.InvoiceId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FPaymentVoucher>().HasOne(l => l.Cashier).WithMany(l => l.PaymentVouchers).HasForeignKey(l => l.CashierId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FPaymentVoucher>().HasOne(l => l.PaymentAccount).WithMany(l => l.PaymentVouchers).HasForeignKey(l => l.ModeId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FPaymentVoucher>().HasOne(l => l.Ledger).WithMany(l => l.PaymentVouchers).HasForeignKey(l => l.LedgerId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FPayment>().HasOne(l => l.CurrencyUsed).WithMany(l => l.Payments).HasForeignKey(l => l.CurrencyId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FPayment>().HasMany(l => l.GLTransactions).WithOne(l => l.Payment).HasForeignKey(l => l.PaymentId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FPaymentVoucher>().HasOne(l => l.Sponsor).WithMany(l => l.Vouchers).HasForeignKey(l => l.SponsorId).IsRequired(false).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FPaymentVoucher>().HasOne(l => l.Supplier).WithMany(l => l.PaymentVouchers).HasForeignKey(l => l.SupplierId).IsRequired(false).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FinanceAccount>().HasMany(l => l.GLRecords).WithOne(l => l.Account).HasForeignKey(l => l.AccountId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FPaymentVoucher>().HasOne(l => l.Personnel).WithMany(l => l.PaidVouchers).HasForeignKey(l => l.PersonnelId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FPaymentVoucher>().HasOne(l => l.TaxPaid).WithMany(l => l.TaxVouchers).HasForeignKey(l => l.TaxId).IsRequired(false).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FProject>().HasOne(l => l.Ledger).WithMany(l => l.Projects).HasForeignKey(l => l.LedgerId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FProject>().HasMany(l => l.DebitJournals).WithOne(l => l.DebitProject).HasForeignKey(l => l.DebitProjectId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FProject>().HasMany(l => l.CreditJournals).WithOne(l => l.CreditProject).HasForeignKey(l => l.CreditProjectId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FBank>().HasMany(l => l.Branches).WithOne(l => l.Bank).HasForeignKey(l => l.BankId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<BankBranch>().HasMany(l => l.BAccounts).WithOne(l => l.BankBranch).HasForeignKey(l => l.BranchId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ImprestWarrant>().HasOne(l => l.Ledger).WithMany(l => l.ImprestWarrants).HasForeignKey(l => l.LedgerId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ImprestWarrant>().HasOne(l => l.Project).WithMany(l => l.ImprestWarrants).HasForeignKey(l => l.ProjectId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ImprestWarrant>().HasOne(l => l.Staff).WithMany(l => l.ImprestWarrants).HasForeignKey(l => l.StaffId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ImprestWarrant>().HasMany(l => l.Payments).WithOne(l => l.ImprestWarrant).HasForeignKey(l=>l.WarrantId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ImprestWarrant>().HasOne(l => l.Disbursement).WithOne(l => l.Warrant).HasForeignKey<ImprestDisbursement>(l=>l.WarrantId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);

            //
            builder.Entity<ImprestDisbursement>().HasOne(l => l.Surrender).WithOne(l => l.Disbursement).HasForeignKey<ImprestSurrender>(l => l.DisbursementId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ImprestDisbursement>().HasMany(l => l.Vouchers).WithOne(l => l.IWDisbursement).HasForeignKey(l => l.IWDisbursementId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ExpenseClaim>().HasOne(l => l.Mileage).WithOne(l => l.Claim).HasForeignKey<MileageExpense>(l => l.ClaimId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<ExpenseClaim>().HasOne(l => l.Substainance).WithOne(l => l.Claim).HasForeignKey<Substainance>(l => l.ClaimId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<FPayment>().HasOne(l => l.Department).WithMany(l => l.Payments).HasForeignKey(l => l.DeptId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);

            //
            builder.Entity<ExpenseClaim>().HasOne(l => l.Staff).WithMany(l => l.ExpenseClaims).HasForeignKey(l => l.StaffId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ExpenseClaim>().HasOne(l => l.Disbursement).WithOne(l => l.Claim).HasForeignKey<ExpenseClaimDisbursement>(l => l.ClaimId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<ExpenseClaim>().HasOne(l => l.Project).WithMany(l => l.Claims).HasForeignKey(l => l.ProjectId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ExpenseClaim>().HasOne(l => l.Ledger).WithMany(l => l.Claims).HasForeignKey(l => l.LedgerId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<PettyCashReq>().HasOne(l => l.Staff).WithMany(l => l.PettyCashReqs).HasForeignKey(l => l.StaffId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<PettyCashReq>().HasOne(l => l.Trainee).WithMany(l => l.PettyCashReqs).HasForeignKey(l => l.TraineeId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<PettyCashReq>().HasOne(l => l.Trainer).WithMany(l => l.PettyCashReqs).HasForeignKey(l => l.TrainerId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<PettyCashReq>().HasMany(l => l.Payments).WithOne(l => l.PettyCashReq).HasForeignKey(l => l.ReqId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<PettyCashReq>().HasOne(l => l.Ledger).WithMany(l => l.PettyCashReqs).HasForeignKey(l => l.LedgerId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<PettyCashReq>().HasOne(l => l.Disbursement).WithOne(l => l.Request).HasForeignKey<PettycashDisbursement>(l => l.RequestId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ExpenseClaim>().HasOne(l => l.Imprest).WithMany(l => l.ExpenseClaims).HasForeignKey(l => l.WarrantId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ExpenseClaim>().HasMany(l => l.ClaimEntries).WithOne(l => l.Claim).HasForeignKey(l => l.ExpenseId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<ExpenseClaim>().HasMany(l => l.Payments).WithOne(l => l.ExpenseClaim).HasForeignKey(l => l.ClaimId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<PettyCashReq>().HasOne(l => l.Project).WithMany(l => l.PettyCashReqs).HasForeignKey(l => l.ProjectId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FCashier>().HasMany(l => l.Attendants).WithOne(l => l.Office).HasForeignKey(l => l.OfficeId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FCashier>().HasOne(l => l.Location).WithMany(l => l.CashierOffices).HasForeignKey(l => l.LocationId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<FCAttendantProfile>().HasOne(l => l.Staff).WithMany(l => l.CashierProfiles).HasForeignKey(l => l.StaffId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ImprestDisbursement>().HasOne(l => l.Personnel).WithMany(l => l.DisbursedImprests).HasForeignKey(l => l.PersonnelId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ImprestSurrender>().HasOne(l => l.Staff).WithMany(l => l.SurrenderedImprests).HasForeignKey(l => l.StaffId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ExpenseClaimDisbursement>().HasOne(l => l.Personnel).WithMany(l => l.DisbursedEClaims).HasForeignKey(l => l.PersonnelId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ExpenseClaimDisbursement>().HasMany(l => l.Vouchers).WithOne(l => l.ECDisbursement).HasForeignKey(l => l.ECDisbursementId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<ExpenseClaimDisbursement>().HasOne(l => l.PaymentAccount).WithMany(l => l.ECDisbursements).HasForeignKey(l => l.ModeId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
            //
            builder.Entity<PaymentJournal>().HasMany(l => l.Payments).WithOne(l => l.Journal).HasForeignKey(l => l.JournalId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
