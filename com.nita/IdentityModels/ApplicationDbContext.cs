using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;

namespace com.nita.IdentityModels
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        private readonly IConfiguration configuration;
        
        //
        public DbSet<TrainerProfile> Trainers { get; set; }
        public DbSet<TraineeProfile> Trainees { get; set; }
        public DbSet<UserAddress> Addresses { get; set; }
        public DbSet<TradeAreaContract> TradeAreaContract { get; set; }
        public DbSet<TrainerContract> TrainerContract { get; set; }
        public DbSet<BankBranch> BankBranch { get; set; }
        public DbSet<TrainerEmployer> TrainerEmployer { get; set; }
        public DbSet<LeadTrainerProfile> LeadTrainers { get; set; }
        public ApplicationDbContext(DbContextOptions options, IConfiguration configuration) : base(options) {
            this.configuration = configuration;
        }
        public ApplicationDbContext() : base(new DbContextOptions<ApplicationDbContext>())
        {
            //Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            //
            builder.Entity<ApplicationUser>().HasMany(e => e.Claims).WithOne().HasForeignKey(e => e.UserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<ApplicationUser>().HasMany(e => e.Logins).WithOne().HasForeignKey(e => e.UserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            //builder.Entity<ApplicationUser>().HasOne(e => e.AccountType).WithMany(l=>l.Users).HasForeignKey(e => e.AccountTypeId).IsRequired().OnDelete(DeleteBehavior.SetNull);
            //
            builder.Entity<ApplicationUser>().HasOne(e => e.AdminProfile).WithOne(j => j.ApplicationUser).HasForeignKey<AdminProfile>(e => e.ApplicationUserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<ApplicationUser>().HasOne(e => e.TrainerProfile).WithOne(j => j.ApplicationUser).HasForeignKey<TrainerProfile>(e => e.ApplicationUserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //

            // builder.Entity<TraineeProfile>().HasOne(l => l.Trainer).WithMany(l => l.Trainees).HasForeignKey(l => l.TrainerId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);

            builder.Entity<TraineeProfile>().HasOne(l => l.Trainer).WithMany(l => l.Trainees).HasForeignKey(l => l.TrainerId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);

            builder.Entity<TraineeProfile>().HasOne(prop => prop.Trainee).WithOne(p => p.TraineeProfile).HasForeignKey<TraineeAccount>(l=>l.TraineeId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);

            builder.Entity<TrainerContract>().HasOne(prop => prop.Termination).WithOne(p => p.Contact).HasForeignKey<ContractTermination>(l=>l.ContractId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);

            //Create Staff Rxn
            OnStaffModelCreating(builder);
            //payroll
            OnPayrollModelCreating(builder);
            //Finance
            OnFinanceModelCreating(builder);
            //Curriculumn
            OnCurriculumnModelCreating(builder);

            //receivables
            OnReceivablesModelCreating(builder);
            //Budget
            OnBudgetModelCreating(builder);
            //builder.Entity<ApplicationUser>().HasOne(e => e.Trainees).WithOne(j => j.ApplicationUser).HasForeignKey<TraineeProfile>(e => e.ApplicationUserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            
        }
    }
}
