using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;

namespace com.nita.IdentityModels
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {

        //Curriculumn Entities
        public DbSet<TradeArea> TradeAreas { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Phase> Phases { get; set; }
        public DbSet<UserAddress> Address { get; set; }
        public DbSet<NitaExam> NitaExams { get; set; }
        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<TraineeTradeArea> TraineeTradeAreas { get; set; }
        public DbSet<ProviderTradeAreas> ProviderTradeAreas { get; set; }
        public DbSet<Beneficiary> Beneficiary { get; set; }
        public DbSet<MarkEntryFlag> MarkEntryFlags { get; set; }
        public DbSet<InternshipScore> InternshipScores { get; set; }
        public DbSet<TraineeAccount> TraineeAccount { get; set; }
        public DbSet<TraineeProviderEmployer> TraineeProviderEmployers { get; set; }
        public DbSet<BeneficiaryStage> BeneficiaryStages { get; set; }
        public DbSet<TraineeAttendance> AttendanceRegister { get; set; }
        protected void OnCurriculumnModelCreating(ModelBuilder builder)
        {
           
            //
          //  builder.Entity<TradeArea>().HasMany(e => e.Trainees).WithOne(p => p.Programme).HasForeignKey(e => e.ProgrammeId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);
           

            //Curriculmn
            //
          //  builder.Entity<TradeArea>().HasOne(l => l.Curriculumn).WithOne(p => p.Area).HasForeignKey<AreaCurriculumn>(l => l.AreaId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
           builder.Entity<InternshipScore>().HasOne(p => p.Cycle).WithMany(l => l.Scores).HasForeignKey(k => k.CycleId).IsRequired().OnDelete(DeleteBehavior.ClientSetNull);


            //

            builder.Entity<TradeArea>().HasMany(e => e.Trainees).WithOne(p => p.Programme).HasForeignKey(e => e.ProgrammeId).IsRequired(false).OnDelete(DeleteBehavior.ClientSetNull);



        }
    }
}
