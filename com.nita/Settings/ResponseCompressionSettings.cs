﻿namespace com.nita.Settings
{
    public class ResponseCompressionSettings
    {
        public string[] MimeTypes { get; set; }
    }
}
