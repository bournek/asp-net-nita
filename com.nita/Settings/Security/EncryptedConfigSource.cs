﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace com.nita.Settings.Security
{
    public class EncryptedConfigurationSource : IConfigurationSource
    {
        public string JsonFilePath { get; }
        public string EncryptedFilePath { get; }

        private string _settingsBasePath;

        public EncryptedConfigurationSource(string jsonFilePath, string settingsBasePath)
        {
            _settingsBasePath = settingsBasePath;
            JsonFilePath = jsonFilePath;
            EncryptedFilePath = Regex.Replace(JsonFilePath, Regex.Escape(".json") + "$", ".enc");
        }

        public IConfigurationProvider Build(IConfigurationBuilder builder)
        {
            return new EncryptedConfigurationProvider(this);
        }

        public void UpdateStoredSettings()
        {
            if (!File.Exists(JsonFilePath))
                return;
            var jsonRoot = JObject.Parse(File.ReadAllText(JsonFilePath));

            string keyPath = GetEncryptionKeyPath(jsonRoot);
            if (String.IsNullOrEmpty(keyPath))
                return; // no encryption is to be done on this file
            Aes aes = GetEncryptionAlgorithm(keyPath);

            // Get/Create encrypted file
            var fiEncrypted = new FileInfo(EncryptedFilePath);
            JObject settingsJson = new JObject();
            if (fiEncrypted.Exists)
                settingsJson = GetEncryptedContents(File.ReadAllBytes(fiEncrypted.FullName), aes);

            // Add new properties to file
            List<JProperty> sensitiveProps = GetSensitiveProperties(jsonRoot);
            foreach (var prop in sensitiveProps)
            {
                var key = prop.Path.Replace("SENSITIVE_", "").Replace(".", ":");
                settingsJson[key] = prop.Value; //overwrite existing
            }

            // Encrypt changes
            using (MemoryStream msEncrypt = new MemoryStream())
            using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, aes.CreateEncryptor(), CryptoStreamMode.Write))
            {
                using (StreamWriter swEncrypt = new StreamWriter(csEncrypt, System.Text.Encoding.UTF8))
                    swEncrypt.Write(settingsJson.ToString());
                File.WriteAllBytes(EncryptedFilePath, msEncrypt.ToArray());
            }

            // Remove sensitive properties from plaintext settings file.
            foreach (var prop in sensitiveProps)
                prop.Remove();
            File.WriteAllText(JsonFilePath, jsonRoot.ToString());
        }

        internal string GetEncryptionKeyPath(JObject jsonRoot)
        {
            var path = jsonRoot["EncryptionKeyPath"];
            if (path == null) return null;
            var fiEncKey = new FileInfo(Path.Combine(_settingsBasePath, path.ToString()));
            if (!fiEncKey.Exists)
                throw new Exception("EncryptionKeyPath was specified but cannot be found");
            return fiEncKey.FullName;
        }

        internal JObject GetEncryptedContents(byte[] encrypted, Aes aes)
        {
            using (MemoryStream msDecrypt = new MemoryStream(encrypted))
            using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, aes.CreateDecryptor(), CryptoStreamMode.Read))
            using (StreamReader srDecrypt = new StreamReader(csDecrypt, System.Text.Encoding.UTF8))
            {
                string plaintext = srDecrypt.ReadToEnd();
                return JObject.Parse(plaintext);
            }
        }

        internal Aes GetEncryptionAlgorithm(string keyPath)
        {
            if (!File.Exists(keyPath))
                throw new InvalidDataException("EncryptionKeyPath key cannot be found");

            byte[] data = File.ReadAllBytes(keyPath);
            if (data.Length != 48)
                throw new InvalidDataException("EncryptionKeyPath key does not contain valid key and IV. Must be 48 bytes length.");

            var aes = Aes.Create();

            byte[] key = new byte[32];
            Array.Copy(data, key, 32);
            aes.Key = key;

            byte[] iv = new byte[16];
            Array.Copy(data, 32, iv, 0, 16);
            aes.IV = iv;

            return aes;
        }

        private List<JProperty> GetSensitiveProperties(JObject jsonRoot)
        {
            var sensitiveProps = new List<JProperty>();
            foreach (JToken item in new JsonInOrderIterator(jsonRoot))
            {
                if (item.Parent.Type != JTokenType.Property)
                    continue; // we're only looking for "SENSITIVE_x":"y" so parent must be a property.

                var prop = item.Parent as JProperty;
                if (prop.Name.StartsWith("SENSITIVE_"))
                    sensitiveProps.Add(prop);
            }
            return sensitiveProps;
        }
    }
}
