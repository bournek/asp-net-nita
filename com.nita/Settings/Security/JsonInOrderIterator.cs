﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.Settings.Security
{
    public class JsonInOrderIterator : IEnumerable
    {
        private readonly JObject _root;
        public JsonInOrderIterator(JObject root)
        {
            _root = root;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public System.Collections.IEnumerator GetEnumerator()
        {
            foreach (var item in DoObject(_root))
                yield return item;
        }

        private System.Collections.IEnumerable DoObject(JObject obj)
        {
            foreach (JProperty prop in obj.Properties())
                foreach (var item in DoProperty(prop))
                    yield return item;
        }

        private System.Collections.IEnumerable DoArray(JArray ary)
        {
            foreach (JToken value in ary.Values())
            {
                if (value.Type == JTokenType.Property)
                    foreach (var item in DoProperty(value as JProperty))
                        yield return item;
                else
                    yield return value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="prop"></param>
        /// <returns></returns>
        private System.Collections.IEnumerable DoProperty(JProperty prop)
        {
            var value = prop.Value;
            if (value.Type == JTokenType.Object)
                foreach (var res in DoObject(value as JObject))
                    yield return res;
            else if (value.Type == JTokenType.Array)
                foreach (var res in DoArray(value as JArray))
                    yield return res;
            else
                yield return value;
        }
    }
}
