﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.Settings
{
    public class DefaultRoles
    {
        public const string NitaSysAdmin = "NitaAdmin";//For Nita admin
        public const string NitaAccessor = "NitaAccessor";
        public const string NitaCoordinator = "NitaCoordinator";
        public const string NitaFinance = "NitaFinance";
    }
    public class DefaultClaims
    {
        public const string ManageTrainersClaim = "ManageTrainers";
        public const string ManageTraineesClaim = "ManageTrainees";
        public const string AdminClaim = "SysAdmin";
        public const string ManageStaffClaim = "ManageStaff";
        public const string TrianeeAccessorClaim = "TraineeAccessor";
        public const string CoordinatorClaim = "TrainingCoordinator";
    }

    public class DefaultAppAccount
    {
        public const string AdminAccount = "Admin";
        public const string StaffAccount = "Staff";
        public const string TrainerAccount = "Trainer";
        public const string TraineeAccount = "Trainee";
    }
}
