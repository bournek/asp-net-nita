﻿using com.nita.IdentityModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.nita.Settings
{
    public enum PaymentState
    {
        NotPaid, HalfPaid,FullyPaid
    }
    public static class Utils
    {
        public static async Task<PaymentState> GetPaymentState(List<FPayment> payments,ApplicationDbContext db)
        {
            var total = 0.0; var paid = 0.0;
            //
            foreach (var p in payments)
            {
                var payment = db.FWallet.Include(l => l.CurrencyUsed).Include(l => l.Department)
                    .Include(l => l.GLAccount).Include(l=>l.PaymentAccounts).Single(l => l.Id == p.Id);
                //
                var x = await ComputeAmountPayable(payment);
                //
                total += x;
                //Has been associated to a payment acc..
                if (payment.PaymentAccounts.Count > 0)
                {
                    paid += x;
                }
            }
            //
            if (paid == total)
                return PaymentState.FullyPaid;
            else if (paid > 0)
                return PaymentState.HalfPaid;
            //
            return PaymentState.NotPaid;
        }

        public static async Task<int> GenerateInvoiceWaiverRef(ApplicationDbContext db)
        {
            var result = 1;
            while (await db.FSIWaivers.OrderByDescending(l=>l.RefNo).AnyAsync(l => l.RefNo == result))
            {
                result++;
            }
            //
            return result;
        }
        public static async Task<int> GenerateExpenseClaimRef(ApplicationDbContext db)
        {
            var result = 1;
            while (await db.FExpenseClaims.OrderByDescending(l=>l.Ref).AnyAsync(l => l.Ref == result))
            {
                result++;
            }
            //
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static async Task<int> GeneratePettyCashRefNo(ApplicationDbContext  db)
        {
            var result = 1;
            while(await db.FPettyCashReqs.OrderByDescending(l => l.Ref).AnyAsync(l=>l.Ref == result))
            {
                result++;
            }
            //
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static async Task<int> GenerateJournalRefNo(ApplicationDbContext db)
        {
            var result = 1;
            while (await db.FJournals.OrderByDescending(l => l.Ref).AnyAsync(l => l.Ref == result))
            {
                result++;
            }
            //
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static async Task<int> GenerateCashierOfficeRefNo(ApplicationDbContext db)
        {
            var result = 1;
            while (await db.FCashierOffices.OrderByDescending(l => l.RefNo).AnyAsync(l => l.RefNo == result))
            {
                result++;
            }
            //
            return result;
        }
        /// <summary>
        /// Ensure that taxes are included in select
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        public static async Task<double> ComputeTaxPayable(FPayment payment)
        {
            var totalPayable = 0.0;

            return await Task.Run(() => {
                //
                if (payment.Taxes!= null && payment.Taxes.Count > 0)
                {
                    foreach (var t in payment.Taxes)
                    {
                        var n = payment.GrossAmount - payment.Discount; //less discount first
                        //calc. tax
                        totalPayable += ApplyTax(n, t);
                    }
                    //
                }

                //
                return totalPayable;

            });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        public static async Task<double> ComputeAmountPayable(FPayment payment)
        {
            var totalPayable = 0.0;

            return await Task.Run(() => {
                //
                if (payment.Taxes != null && payment.Taxes.Count > 0)
                {
                    foreach (var t in payment.Taxes)
                    {
                        var n = payment.GrossAmount - payment.Discount; //less discount first
                        //calc. tax
                        var tax = ApplyTax(n, t);
                        //
                        totalPayable += (n - tax);//then Less tax
                    }
                    //
                }
                else
                {
                    totalPayable += (payment.GrossAmount - payment.Discount);
                }

                //
                return totalPayable;

            });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="budget"></param>
        /// <returns></returns>
        public static async Task<double> GetSpentBudgetAmount(FinanceBudget budget)
        {
            //
            if (budget.Amount <= 0)
                return 0;
            //
            var dept = budget.Department;
            //
            var acc = budget.GLAccount;
            //
            return await Task.Run(() =>
            {
                var totalSpent = 0.0;
                //ON CASH SPENDING
                //var payments = dept.Payments.Where(l=>l.PaymentAccount != null && (l.DatePaid.CompareTo(cycle.CycleFrom)>=0 && l.DatePaid.CompareTo(DateTime.Now)<=0)); 
                //ON ACRUAL
                if (budget.Cycle != null)
                {
                    //var payments = dept.Payments.Where(l => l.DateCreated.CompareTo(cycle.CycleFrom) >= 0 && l.DateCreated.CompareTo(DateTime.Now) <= 0).ToList();
                    var payments = dept.Payments.Where(l => l.AccountId == acc.Id && l.DateCreated.CompareTo(budget.Cycle.CycleFrom) >= 0 && l.DateCreated.CompareTo(budget.Cycle.CycleTo) <= 0).ToList();
                    //
                    if (payments.Count > 0)
                    {
                        foreach (var p in payments)
                        {
                            totalSpent += (p.GrossAmount - p.Discount);//Total quoted - Discount given
                        }
                    }
                }
                else if (budget.Period != null)
                {
                    var payments = dept.Payments.Where(l => l.AccountId == acc.Id && l.DateCreated.CompareTo(budget.Period.PeriodFrom) >= 0 && l.DateCreated.CompareTo(budget.Period.PeriodTo) <= 0).ToList();
                    //
                    if (payments.Count > 0)
                    {
                        foreach (var p in payments)
                        {
                            totalSpent += (p.GrossAmount - p.Discount);//Total quoted - Discount given
                        }
                    }
                }
                else
                {
                    //
                }

                //
                return totalSpent;
            });
        }
        public static async Task<FinanceBudget> GetCurrentDeptBudget(Department dept, FinanceAccount acc)
        {
            FinanceBudget budget = null;
            
            return await Task.Run(()=> 
            {
                //
                if (dept.Budgets.Any(l => l.Cycle != null && (l.Cycle.CycleFrom.CompareTo(DateTime.Now) <= 0 && l.Cycle.CycleTo.CompareTo(DateTime.Now) >= 0) && l.AccountId == acc.Id && acc.HasBudget))
                {
                    //Use Cycle
                    //
                    budget = dept.Budgets.SingleOrDefault(l => (l.Cycle.CycleFrom.CompareTo(DateTime.Now) <= 0 && l.Cycle.CycleTo.CompareTo(DateTime.Now) >= 0) && l.AccountId == acc.Id && acc.HasBudget);
                    //

                }
                else if (dept.Budgets.Any(l => l.Period != null && l.AccountId ==acc.Id && acc.HasBudget && (l.Period.PeriodFrom.CompareTo(DateTime.Now) <= 0 && l.Period.PeriodTo.CompareTo(DateTime.Now) >= 0)))
                {
                    //Use Period
                    //
                    budget = dept.Budgets.SingleOrDefault(l => (l.Period.PeriodFrom.CompareTo(DateTime.Now) <= 0 && l.Period.PeriodTo.CompareTo(DateTime.Now) >= 0) && l.AccountId == acc.Id && acc.HasBudget);

                }
                //
                return budget;
            });
        }
        public static async Task<int> GenerateSupplierRef(ApplicationDbContext db)
        {
            return await  Task.Run(() => {
                var Ref = 1000;
                //
                while (db.FSuppliers.Any(k => k.RefNo == Ref))
                {
                    Ref++;
                }
                //
                return Ref;
            });
        }
        public static async Task<Cycle> GetCurrentDept(ApplicationDbContext db, int did)
        {
            return await Task.FromResult<Cycle>(null);
        }
        public static double ApplyAllowableDeduction(ApplicationDbContext db, double amount)
        {
            return 0.0;
        }
        public static double ApplyNHIF(ApplicationDbContext db, double amount)
        {
            return 0.0;
        }
        public static double ApplyPAYE(ApplicationDbContext db, double amount)
        {
            return 0.0;
        }
        public static async Task<double> CalculatePayableAmount(List<FPayment> payments)
        {
            var total = 0.0;
            foreach (var p in payments)
            {
                //if (p.Taxes.Count > 0)
                //{
                //    foreach (var t in p.Taxes)
                //    {
                //        var d = ApplyTax(p.GrossAmount, t);
                //        total += (p.GrossAmount - d);//Less tax
                //    }
                //}
                //else
                //{
                //    total += p.GrossAmount;
                //}
                total += await ComputeAmountPayable(p);
            }
            //
            return total;
        }
        public static async Task<double> CalculatePayableTax(List<FPayment> payments)
        {
            var tax = 0.0;
            foreach (var p in payments)
            {
                //if (p.Taxes.Count > 0)
                //{
                //    foreach (var t in p.Taxes)
                //    {
                //        var d = ApplyTax(p.GrossAmount, t);
                //        tax += (p.GrossAmount - d);
                //    }
                //}
                tax += await ComputeTaxPayable(p);
            }
            //
            return tax;
        }
        public static int GetInvoiceDocNo(ApplicationDbContext db)
        {
            var docNo = 1;

            while (db.FInvoices.OrderByDescending(l => l.DocNo).Any(l => (l.DocNo == docNo)))
            {
                docNo++;
            }
            //
            return docNo;
        }
        public static async Task<int> GenerateWarrantRef(ApplicationDbContext db)
        {
            return await Task.Run(async () => {

                var result = 1;
                while (await db.FImprestWarrants.OrderByDescending(l => l.Ref).AnyAsync(l => (l.Ref == result)))
                {
                    result++;
                }
                //
                return result;
            });
        }
        public static async Task<int> GenerateDisbursementRef(ApplicationDbContext db)
        {
            return await Task.Run(async () => {

                var result = 1;
                while (await db.FImprestDisbursements.OrderByDescending(l => l.Ref).AnyAsync(l => (l.Ref == result)))
                {
                    result++;
                }
                //
                return result;
            });
        }
        public static int GenerateVoucherNo(ApplicationDbContext db)
        {
            var result = 1000;

            while (db.FPaymentVouchers.OrderByDescending(l => l.VoucherNo).Any(l => (int.Parse(l.VoucherNo) == result)))
            {
                result++;
            }

            //
            return result;
        }
        public static double ApplyTax(double amt, List<FinanceTax> taxes)
        {
            var result = 0.0;
            foreach(var t in taxes)
            {
                result += ApplyTax(amt,t);
            }
            //
            return result;
        }
        /// <summary>
        /// Get Applicable tax amount
        /// </summary>
        /// <param name="amt"></param>
        /// <param name="tax"></param>
        /// <returns></returns>
        public static double ApplyTax(double amt, FinanceTax tax)
        {
            var result = 0.0;
            var tt = (amt * tax.Rate / 100);
            switch (tax.RoundType)
            {
                //
                case RoundOffType.Round_Down:
                    //
                    result += Math.Floor(tt);
                    break;
                case RoundOffType.Round_Up:
                    result += Math.Ceiling(tt);
                    break;
                default:
                    //
                    result += Math.Round(tt, 2, MidpointRounding.ToEven);
                    break;
            }

            //
            return result;

        }
        public static string DecimalToWords(decimal number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + DecimalToWords(Math.Abs(number));

            string words = "";

            int intPortion = (int)number;
            decimal fraction = (number - intPortion) * 100;
            int decPortion = (int)fraction;

            words = NumberToWords(intPortion);
            if (decPortion > 0)
            {
                words += " and ";
                words += NumberToWords(decPortion);
            }
            return words;
        }
        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "cents " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }

        public static string SanitizeForJSON(string input)
        {
            var result = "";
            result = result.Replace('\'', '`');
            //
            return result;
        }
    }
}
