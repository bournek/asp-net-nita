﻿using System.Collections.Generic;

namespace com.nita.Settings
{
    /// <summary>
    /// The settings for the current application.
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// Gets or sets the short name of the application, used for display purposes where the full name will be too long.
        /// </summary>
        public string SiteShortTitle { get; set; }

        /// <summary>
        /// Gets or sets the full name of the application.
        /// </summary>
        public string SiteTitle { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DefaultPassword { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Domain { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public AccountsConfig Accounts { get; set; } = new AccountsConfig();
        /// <summary>
        /// 
        /// </summary>
        /// 
        public MessagingSettings Messaging { get; set; } = new MessagingSettings();

        public AppTheme Theme { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MessagingSettings
    {
        /// <summary>
        /// 
        /// </summary>
        public SmtpSettings Smtp { get; set; } = new SmtpSettings();
        /// <summary>
        /// 
        /// </summary>
        public SmsSettings Sms { get; set; } = new SmsSettings();
    }
    /// <summary>
    /// 
    /// </summary>
    public class AccountsConfig
    {
        /// <summary>
        /// 
        /// </summary>
        public string PrePaymentCA { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PrePaymentRA { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string BadDebtAcc { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AccumFund { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DeprAcc { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CGAcc { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OBAcc { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<JAccount> ARDebtorAccs { get; set; } = new List<JAccount>();
        /// <summary>
        /// 
        /// </summary>
        public List<JAccount> APCreditorAccs { get; set; } = new List<JAccount>();
        /// <summary>
        /// 
        /// </summary>
        public bool ReceiptPrinting { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool PostCompletedPayments { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class JAccount
    {
        /// <summary>
        /// 
        /// </summary>
        public int AccId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsDefault { get; set; }
    }
    public class SmtpSettings
    {
        /// <summary>
        /// 
        /// </summary>
        public string Server { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Port { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool EnableSsl { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class SmsSettings
    {
        /// <summary>
        /// 
        /// </summary>
        public string Server { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Port { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Secret { get; set; }
    }

    public class AppTheme
    {
        public Color PrimaryColor { get; set; }
        public Color SecondaryColor { get; set; }
        public Color AccentColor { get; set; }
        public string LargeLogo { get; set; }
        public string SmallLogo { get; set; }
    }
    public class Color
    {
        public int Red { get; set; }
        public int Green { get; set; }
        public int Blue { get; set; }
    }
}